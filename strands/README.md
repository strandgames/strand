# Strand Games

## Layout

This is the typical directory layout of a Strand game

```
.
|-- doc
|-- media
|   `-- .orig
`-- src
    |-- audio
    |-- bin
    |   |-- audio
    |   |-- fonts
    |   `-- images
    |-- images
    `-- web
        |-- audio
        |-- fonts
        `-- images
```

The game itself is run from the `src` directory, which will typically contain a bunch of `.str` files as well as the `audio` and `images` directories.

Usually there will be `story.str` which internally references the other `.str` file as required.

Various helper scripts can be kept here too. They do not go into the deployment `bin` directory.

eg:

```
src/
|-- audio/
|-- bin/
|-- core.str
|-- go.bat
|-- gobin.bat
|-- gobin.sh
|-- goweb.bat
|-- images/
|-- init.kl
|-- kit.str
|-- map.str
|-- picton.str
|-- push.bat
|-- story.str
```

## Deployment

In order to run a game, a deployment `bin` directory is created where binaries and media are processed and copied.

The `bin` directory is created and updated by the helper script `gobin.sh` (or `gobin.bat`) which also runs the game.

Here is a typical `gobin.sh` script. You can perform these steps manually or write your own script as you wish.

Below is an explanation of these steps.

```
TOP=~/strand
STR=$TOP/strands
BIN=$STR/imgui
SDK=$STR/sdk
mkdir -p bin
mkdir -p bin/images
mkdir -p bin/audio
mkdir -p bin/fonts
cp -v init.kl bin
cp -v $SDK/bin/web/fontlist.txt bin
cp -v $SDK/bin/web/fonts/*.ttf bin/fonts
cp -v $BIN/game bin/picton
cp -v images/* bin/images
cp -v audio/*.ogg bin/audio
cp -v $BIN/audio/click.ogg bin/audio
$STR/strands -bin 
mv story.stz bin
$STR/strands -genmeta
mv imagemeta.json bin
bin/picton -d 3 bin
```

The script identifies a number of directories from which to obtain files. You only need to adjust the definition of `TOP` to point to wherever your `strand` top level directory was installed.

The `mkdir` section establishes the `bin` directory and its subdirectories.

Then various support files are copied in, including; `fontlist.txt`, the font `.ttf` files (ttf is used regardless of your system). The `game` binary, which is renamed to that of the game. The images and audio from the `src` directory.

NOTE: if you wish to change the fonts available, you must procure your additional .`ttf` files _and_ also put their names into `fontlist.txt`.

The next steps are preprocesing. For deployment, we do not run from individual source `.str` files. Instead the command-line `strand` binary is used as a preproccesing tool (which is why it must be compiled even for GUI).

```
$STR/strands -bin
```

the `-bin` option instructs the tool to load the game `.str` files starting from `story.str`, remove comments, combine and compress them into `story.stz`, which compactly contains the whole game.

Next, the images are preprocessed.

```
$STR/strands -genmeta
```

This instructs the tool to generate `imagemeta.json`. A file of all image metadata. This contains a list of images available, their dimensions _and_ clickable polygon regions within them (if any).

This file is then copied into the deployment `bin` directory.

NB: The image meta data is optional, but recommended. If it is not present at runtime, there will be no image click regions and also the game will not know in advance the image dimensions.

For games without click regions, the image dimensions are helpful but not essential. In this case, the game will "guess" the dimensions and be forced to reflow the text _again_ once the picture is loaded - unless it guessed right!

Finally the last line;

```
bin/picton -d 3 bin
```

Runs the game. You may wish to remove this line from the script and type it manually. Since in the absence of changes, you will obviously not need to copy the sources each time.

## Running

eg:

```
bin/picton -d 3 bin
```

This runs the `picton` game binary and supplies `bin` as its main parameter. This `bin` is the location of the `story.stz` file and the relevant support and media files as set up by `gobin.sh` (or manually).

the parameter `-d 3` is entirely optional, and set the debug level. 3 is a good choice when developing.

NB: if the binary was built _without_ `-DLOGGING`, there will can be no debug output regardless.
















































