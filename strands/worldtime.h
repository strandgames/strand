//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <chrono>

struct WorldTime
{
    typedef std::chrono::steady_clock Clock;
    typedef std::chrono::time_point<Clock> WTime;

    WorldTime() { _init(); }

    static WTime now()
    {
        return Clock::now();
    }

    static double difference(WTime a, WTime b)
    {
        std::chrono::duration<double> dt = a - b;
        return dt.count();
    }

    double startDuration(WTime a)
    {
        return difference(a, _start);
    }

    double duration() const
    {
        return difference(now(), _start);
    }

    static int64_t epoch()
    {
        return std::chrono::system_clock::now().time_since_epoch().count();
    }

    static int64_t any()
    {
        // could be used as a random number seed
        // however low 2 digits tend to be zero
        return std::chrono::high_resolution_clock::now().time_since_epoch().count();
    }

private:

    WTime _start;

    void _init()
    {
        _start = now();
    }


};
