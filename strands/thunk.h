//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include "env.h"

struct Thunk: public Head
{
    struct Info
    {
        Term        _expr;
        EnvArgs*    _args;
    };

    Thunk(Term& expr, Env& env): Head(type_thunk)
    {
        Info* i = new Info;
        i->_expr = expr;
        i->_args = env._args;
        ASSIGN(Info**, i);
        //*(Info**)&_n = i;
    }

    ~Thunk() { delete info(); }

    Info* info() const { return (Info*)_n; }

    // convert to sbuf, for string output
    void toSBuf(SBuf& sb) const
    {
        Info* i = info();
        assert(i->_expr);

#ifdef DEBUGOUT
        // indicate thunk object
        sb.add('!');
#endif        
        i->_expr->toSBuf(sb);

    }

private:

    // dummy
    Thunk(const Thunk&) {}
    Thunk& operator=(const Thunk&) { return *this; }
};


