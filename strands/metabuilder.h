//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

// NB: this header only included for TOOLS not runtime

#include "utils.h"
#include "immeta.h"
#include "svgp.h"
#include "gpc.h"

struct ImageMetaBuilder
{
    typedef std::string string;
    typedef HNode::Nodes::iterator nodeIterator;

    string      _filename;
    SVGParser   _parser;
    Point2f     _scale;
    ImageMeta*  _meta;
    int         _z = 0;

    ImageMetaBuilder(ImageMeta* m) : _meta(m) {}

    bool processRect(NodeTag* node)
    {
        assert(_meta);
        string id = node->getAttr("id").toString();        
        double x = node->getAttr("x").toDouble();
        double y = node->getAttr("y").toDouble();
        double w = node->getAttr("width").toDouble();
        double h = node->getAttr("height").toDouble();

        bool r = !id.empty() && w > 0 && h > 0;
        if (r)
        {
            auto e = new ImageMeta::EltClick(id);
            
            // the z-order is defined by the order in a SVG
            e->_z = ++_z;

            x *= _scale.x;
            y += _scale.y;
            w *= _scale.x;
            h *= _scale.y;

            Poly p;
            p.add(Point2f(x,y));
            p.add(Point2f(x + w,y));
            p.add(Point2f(x + w,y + h));
            p.add(Point2f(x,y + h));
            e->_polys.add(p);

            _meta->add(e);
        }
        return r;
    }
    
    bool processPath(NodeTag* node)
    {
        //LOG1("path ", node->toString());
        assert(_meta);

        string d = node->getAttr("d").toString();
        string id = node->getAttr("id").toString();

        // eg fill:none;stroke:#000;stroke-width:1px;
        string style = node->getAttr("style").toString();
        
        VarSet vs;
        SVGParser::parseAttributes(vs, style);
            
        bool r = !d.empty() && !id.empty();
    
        if (r)
        {
            VarSet idprops;
            bool show = false;
            
            // we can store extra properties in the ID
            const char* ids = id.c_str();
            const char* ide = strchr(ids, ':');
            if (ide)
            {
                // id has additional properties
                const char* ee = ide + 1;
                SVGParser::parseAttributes(idprops, &ee);

                // remove ":..."
                id = id.substr(0, ide - ids);
                show = idprops.find(IFI_SHOW).isTrue();
            }
            
            LOG3("path ", id << " of:" << d);

            SVGPath p(id, d);

            // apply the scale which converts paths into [0,1]
            p._scale = _scale;

            var v = vs.find("stroke-width");
            if (v)
            {
                float w = v.toDouble();
                if (w > 0) p._strokeW = w;
            }
            v = vs.find("stroke");
            if (v.isString())
            {
                // #RRGGBB
                string s = v.toString();
                p._col.parse(s);
            }
            
            auto e = new ImageMeta::EltClick(id);
            e->_show = show;
            
            // the z-order is defined by the order in a SVG
            e->_z = ++_z;

            r = p.makePolys(e->_polys, _filename.c_str());

            if (!r)
            {
                LOG1("path error in ", _filename);
            }

            // add clickable to meta
            if (r) _meta->add(e);
            else delete e;
        }
        return r;
    }

    Point2 imageSize() const
    {
        return Point2{_parser._viewBox._w, _parser._viewBox._h};
    }

    bool processSubPaths(NodeTag* node)
    {
        bool r = true;
        for (nodeIterator it = node->_content.begin();
                 it != node->_content.end(); ++it)
        {
            HNode* n = it;
            if (n->type() == HNode::nodeTypeTag)
            {
                NodeTag* nt = (NodeTag*)n;
                if (equalsIgnoreCase(nt->_tag, "g"))
                {
                    // go into the group
                    r = processSubPaths(nt);
                }
                else if (equalsIgnoreCase(nt->_tag, "path"))
                {
                    r = processPath(nt);
                    if (!r) break;
                }
                else if (equalsIgnoreCase(nt->_tag, "rect"))
                {
                    // sometimes the so-called path is actually a single
                    // rectangle
                    r = processRect(nt);
                    if (!r) break;
                }
            }
        }
        return r;
    }
    
    bool processSVG(const char* data)
    {
        bool r;

        _parser._filename = _filename;
        r = _parser.parse(data);

        if (r)
        {
            _scale.x = 1.0f/_parser._viewBox._w;
            _scale.y = 1.0f/_parser._viewBox._h;

            NodeTag* svg = _parser._svgroot;
            r = processSubPaths(svg);

        
            //parser.showAttr(parser._svgroot);
            //parser.showTags();
        }
    
        return r;
    }

#if 0
    static void simplifyDP(Poly& poly, float epsilon)
    {
        // Simplify the polygon using the Douglas-Peucker algorithm
        
        int n = poly.size();
        if (n >= 3)
        {
            Poly::Points& points = poly._points;
            
            // Ensure the polygon is closed
            if (points.front() != points.back())
            {
                points.push_back(points.front());
            }

            Poly::Points simplifiedPoints;
            Poly::simplifyDP(epsilon, points, simplifiedPoints, 0, points.size() - 1);
        
            // Add the last point to close the polygon
            simplifiedPoints.push_back(simplifiedPoints.front());

            points = simplifiedPoints;
            poly.recalcBounds();

            LOG1("simpify poly DP, original", n << " result:" << poly.size() << " %:" << 100.0f*poly.size()/n);
        }
    }
#endif    

    static double areaTriangle(const Point2f& a, Point2f& b, Point2f c)
    {
        double v;
        v = a.x * b.y - b.x * a.y;
        v += b.x * c.y - c.x * b.y;
        v += c.x * a.y - a.x * c.y;
        if (v < 0) v = -v;
        return v/2;
    }
    
    static void simplify(Poly& poly)
    {
        // Visvalingam–Whyatt algorithm
        int n = poly.size();
        if (n >= 3)
        {
            double pa = poly.area();

            //LOG1("simpify poly ", n << " area:" << pa);
            
            Point2f* pts = new Point2f[n];
            memcpy(pts, &poly._points[0], n*sizeof(Point2f));
            
            double epsa = 1.0e-4f * pa;

            while (n >= 3)
            {
                double mina = epsa;
                int minPoint = -1; // none
                
                // find min point
                for (int i = 2; i < n; ++i)
                {
                    double ta = areaTriangle(pts[i-2], pts[i-1], pts[i]);
                    
                    if (ta < mina)
                    {
                        minPoint = i-1; // indicate b
                        mina = ta;
                    }
                }

                if (mina >= epsa) break; // no triangle small enough, done.

                //LOG1("simplify triangle ", mina << " " << epsa);

                assert(minPoint >= 0);
                
                // remove minpoint
                memmove(pts + minPoint,
                        pts + minPoint + 1,
                        (n - minPoint - 1)*sizeof(Point2f));

                --n;
            }

            if (n < 3) // degenerated
            {
                LOG1("simplify polygon degenerated ", n);
                poly.clear();
            }
            else if (n != poly.size())
            {
                LOG3("simplify poly from ", poly.size() << " to " << n);

                // replace points
                poly._points.resize(n);
                memcpy(&poly._points[0], pts, n*sizeof(Point2f));
                poly.recalcBounds();
            }

            delete [] pts;
        }
    }

    static void makeGPCPolyFromBox(gpc_polygon* gp, const RRect& ri)
    {
        // fill in a GPC polygon from a box
        gp->num_contours = 1;
        gp->hole = GMALLOC(int);
        gp->hole[0] = 0;  // false
        gpc_vertex_list* gv = GMALLOC(gpc_vertex_list);
        gp->contour = gv;
        gv->num_vertices = 4;
        gpc_vertex* vp = GMALLOCA(gpc_vertex, 4);
        gv->vertex = vp;

        vp[0].x = ri.x1; vp[0].y = ri.y1;
        vp[1].x = ri.x1; vp[1].y = ri.y2;
        vp[2].x = ri.x2; vp[2].y = ri.y2;
        vp[3].x = ri.x2; vp[3].y = ri.y1;
    }

    bool makePolysFromRegion(const ST::Region& rg, Polys& polys)
    {
        float w = _meta->_w;
        float h = _meta->_h;

        assert(w > 0 && h > 0);

        LOG1("Building polys for ", _meta->_name << " from rects: " << rg.numRects);

        gpc_polygon* pi = GMALLOC(gpc_polygon);
        gpc_polygon* polyunion = 0;

        // banded by Y, add left side
        for (int i = 0; i < rg.numRects; ++i)
        {
            const RRect& ri = rg.rects[i];

            makeGPCPolyFromBox(pi, ri);
            if (!polyunion)
            {
                polyunion = pi;
                pi = GMALLOC(gpc_polygon);
            }
            else
            {
                gpc_polygon_clip(GPC_UNION,
                                 polyunion, pi, polyunion);
                gpc_free_polygon(pi);
            }
        }

        GFREE(pi);

        // now make poly from GPC poly

        //LOG1("GPC poly contours:", polyunion->num_contours);

        int nc = polyunion->num_contours;
        int cc = 0;
        for (int i = 0; i < nc; ++i)
        {
            Poly pi;
            bool hole = polyunion->hole[i];
            if (!hole)
            {
                gpc_vertex_list& gv = polyunion->contour[i];

                //LOG1("GPC poly vertices:", gv.num_vertices);
                for (int j = 0; j < gv.num_vertices; ++j)
                {
                    gpc_vertex& vi = gv.vertex[j];
                    pi.addNext(Point2f(vi.x/w, vi.y/h));
                }
                simplify(pi);
                if (pi.size())
                {
                    polys.add(pi);
                    ++cc;
                }
            }
        }

        //LOG1("build poly for ", _meta->_name << " contours:" << cc);
        
        if (cc)
        {
            // can we throw away some polys?
            double ta = 0;
            int pn = polys.size();
            assert(pn);

            // make a list of poly areas and the total
            double* aa = (double*)new double[pn];
            int i = 0;
            //double amax = 0;
            //int imax = 0;
            for (auto& pi : polys._polys)
            {
                double a = pi.area();
                aa[i] = a;
                ta += a;
                //if (a > amax) { amax = a; imax = i;}
                ++i;
                
                //LOG1("poly ", i-1 << " " << pi.size() << " area: " << a << " total " << ta << " " << a/ta);
            }
            
            //LOG("max poly is ", imax << " at " << amax);

#if 0
            // max mode. keep just the biggest
            Polys::Polyst::iterator it = polys._polys.begin();
            i = 0;
            while (it != polys._polys.end())
            {
                if (i != imax)
                {
                    it = polys._polys.erase(it);
                    --cc;
                }
                else
                {
                    LOG1("keeping max poly ", (*it).area());
                    ++it;
                }
                ++i;
            }
            
#else            
            Polys::Polyst::iterator it = polys._polys.begin();
            i = 0;
            while (it != polys._polys.end())
            {
                double af = aa[i]/ta;
                if (af < 0.01)   // 1%
                {
                    //LOG1("poly size ", (*it).size() << " area small " << 100*af << "%");
                    it = polys._polys.erase(it);
                    --cc;
                }
                else ++it;
                ++i;
            }

            delete [] aa;
#endif            

#ifdef LOGGING
            // print final list of polys area %
            /*
            ta = 0;
            for (auto& pi : polys._polys) ta += pi.area();
            for (auto& pi : polys._polys)
            {
                double af = pi.area()/ta;
                LOG1("keeping poly size ", pi.size() << " area " << 100*af << "%");

            }
            */
#endif            
        }

        LOG1("Polys for ", _meta->_name << " count:" << polys.size());

        gpc_free_polygon(polyunion);
        GFREE(polyunion);

        return cc > 0;
    }

    void makeMaskClickable(const string& click, const ST::Region& mask)
    {
        assert(_meta);

        auto e = new ImageMeta::EltClick(click);
        e->_z = 1;

        bool r = makePolysFromRegion(mask, e->_polys);

        if (r)
        {
            LOG1("Click for ", _meta->_name << " \"" << click << "\" polygon sizes: " << e->_polys.vertexSizeString());

            _meta->add(e);
        }
        else
        {
            LOG1("makeMaskClickable poly empty in ", _filename);
            delete e;
        }
    }
};
