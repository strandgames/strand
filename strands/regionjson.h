//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include "region.h"
#include "growbuf.h"
#include "jsonwalker.h"

namespace ST
{

struct RegionJson
{
    typedef std::string string;
    
    static void addRect(GrowString& gs, const RRect& r)
    {
        JSONWalker::addValue(gs, r.x1);
        JSONWalker::addValue(gs, r.y1);
        JSONWalker::addValue(gs, r.x2);
        JSONWalker::addValue(gs, r.y2);
    }
    
    static void buildJson(GrowString& gs, const Region& r)
    {
        // region to json
        
        gs.add('{');

        JSONWalker::addKeyValue(gs, IFI_SIZE, r.numRects);
        if (r.numRects)
        {
            JSONWalker::toAdd(gs);            
            JSONWalker::addKey(gs, IFI_RECTS);
            gs.add('[');
            for (int i = 0; i < r.numRects; ++i) addRect(gs, r.rects[i]);
            gs.add(']');
        }
        
        gs.add('}');
        gs.add('\n'); // formatting
    }

    static bool parseVal(int& v, const char*& s)
    {
        bool more = false;
        
        v = u_atoi(&s);  // skips space
        while (u_isspace(*s)) ++s;
        if (*s == ',') { ++s; more = true; }
        return more;
    }
    
    static bool parseRectsArray(Region& rg, const string& js)
    {
        // [x1,y1,x2,y2...]  ints

        int index = 0;

        const char* s = js.c_str();
        while (u_isspace(*s)) ++s;
        bool r = *s == '[';
        if (r)
        {
            ++s;
            while (*s)
            {
                RRect rt;

                r = parseVal(rt.x1, s) && parseVal(rt.y1, s) && parseVal(rt.x2, s);
                if (!r)
                {
                    //LOG1("rect array incomplete ", index);
                    break; // incomplete
                }
                
                bool more = parseVal(rt.y2, s);

                if (index < rg._size)
                {
                    rg.rects[index++] = rt;
                    ++rg.numRects;
                }

                if (!more) break;
            }
        }
        
        return r;
    }

    static bool parseJson(Region& rg, const string& js)
    {
        // json to region
        // { "size":n, "rects":[x1,y1,x2,y2,...] }

        rg.reset();

        bool r = true;
        int numRects = 0;

        // need to find the size first which is numRects
        for (JSONWalker jw(js); r && jw.nextKey(); jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st) { r = false;  break; }
            
            if (!isObject)
            {
                var v = jw.collectValue(st);
                if (jw._key == IFI_SIZE)
                {
                    numRects = v.toInt();
                    break; // this pass only finds the size
                }
            }
        }

        r = numRects > 0;
        
        if (r)
        {
            rg._size = numRects;
            rg.rects = new RRect[numRects];
            
            for (JSONWalker jw(js); r && jw.nextKey(); jw.next())
            {
                bool isObject;
                const char* st = jw.checkValue(isObject);
                if (!st) { r = false;  break; }

                if (isObject)
                {
                    if (jw._key == IFI_RECTS)
                    {
                        string subjs;
                        r = jw.atList(subjs) && parseRectsArray(rg, subjs);

                        // did we get all the rects back?
                        r = r && rg.numRects == rg._size;
                    }
                }
            }

            if (r) rg.recalcExtent();
            else rg.reset();
        }
        return r;
    }
};

}
