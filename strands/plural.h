//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include <string.h>

static inline bool isVowel(int c)
{
    return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
}

static inline std::string pluralOf(const std::string& s)
{
    std::string ss;
    int n = s.size();
    if (n > 1)
    {
        int l = s[n-1];
        int l1 = s[n-2];
        int l2 = n > 2 ? s[n-3]: 0;
        if (l == 'f')
        {
            // wolf
            ss = s.substr(0,n-1) + "ves";
        }
        else if (l2 == 'm' && l1 == 'a' && l == 'n')
        { 
            // woman, man
            ss = s.substr(0,n-3) + "men";
        }
        else if (l == 'e' && l1 == 'f')
        {
            // wife
            ss = s.substr(0,n-2) + "ves";            
        }
        else if (l == 'x' && (l1 == 'e' || l1 == 'i'))
        {
            // index, matrix
            ss = s.substr(0, n-2) + "ices";
        }
        else if (l == 's' || l == 'x' || (l == 'h' && (l1 == 's' || l1 == 'c')))
        {
            ss = s + "es";
        }
        else if (l == 'z')
        {
            // quiz
            ss = s + "zes";            
        }
        else if (l == 'y' && !isVowel(l1))
        {
            ss = s.substr(0, n-1) + "ies";
        }
        else if (l == 'o' && !isVowel(l1))
        {
            // potato
            ss = s + "es";            
        }
        else
        {
            ss = s + 's';
        }
    }
    else ss = s;
    return ss;
}




