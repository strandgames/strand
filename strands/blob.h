//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include "head.h"

// forward
struct KLLib;

struct Blob: public Head
{
    struct Info
    {
        KLLib*      _lib;

        // print the name of lib before blob `toString`
        bool        _annotateString;
        
        void*       _object;
    };
    
    Blob(void* object, KLLib* lib): Head(type_blob)
    {
        ASSIGN(Info**, make(object, lib));
        //*(Info**)&_n = make(object, lib);
    }

    Blob(Info* i) : Head(type_blob)
    {
        ASSIGN(Info**, i);
        //*(Info**)&_n = i;        
    }

    ~Blob() { _purge(); }

    static Info* make(void* object, KLLib* lib)
    {
        assert(lib);
        assert(object);

        Info* i = new Info;
        i->_lib = lib;
        i->_object = object;
        i->_annotateString = true;
        return i;
    }

    Info* info() const { return (Info*)_n; }

    bool annotateString() const
    {
        Info* i = info();
        return !i || i->_annotateString;
    }

    void annotateString(bool v)
    {
        Info* i = info();
        if (i) i->_annotateString = v;
    }

    // convert to sbuf, for string output
    void toSBuf(SBuf& sb) const;
    int compare(const Blob& v) const;

private:

    void _purge();

    // dummy
    Blob(const Blob&) {}
    Blob& operator=(const Blob&) { return *this; }

    void info(Info* i)
    {
        assert(!_n);
        ASSIGN(Info**, i);
        //*(Info**)&_n = i;
    }

};


