//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <atomic>
#include "ti.h"
#include "logged.h"

struct ImageDecodeState
{
    // input
    const unsigned char* _data;
    int _dsize;

    bool _started = false;
    bool _failed = false;

#ifdef COOP    
    ImageDecodeState(const unsigned char* data, int sz)
        : _data(data), _dsize(sz) {}
#else
    ImageDecodeState(const unsigned char* data, int sz)
        : _data(data), _dsize(sz), _done(false) {}
#endif    

    virtual ~ImageDecodeState() {}

    bool decodeStep()
    {
        // return true when done or failed
        
        if (_failed || !_data || !_dsize)
        {
            _failed = true;
            return true;
        }

        bool r = _decodeStep();

        if (_failed)
        {
            delete _pix;
            _pix = 0;
        }

        return r;
            
    }

    // can be overriden (eg webp)
    virtual bool decodeAll()  
    {
        //TIMER("decodeAll step");
        while (!decodeStep()) ;
        return true;
    }

#ifndef COOP    
    void decodeAllThread()
    {
        decodeAll();
        _done = true;
    }

    bool isDone()
    {
        return _done;
    }
#endif    
    
    virtual bool _decodeStep() = 0;

    // output
    int _w = 0;
    int _h = 0;
    int _chans = 0;
    unsigned char* _pix = 0;

#ifndef COOP
    bool _threadBegun = false;
    std::atomic<bool> _done;
#endif    

    
};

extern ImageDecodeState* createSTBIDecodeState(const unsigned char* data, int sz);

