//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

// fix for macos/ios build - troubles with objective-c building
extern "C" {
    int stem(char * p, int i, int j);
};

#include "fda.h"
#include "fdz.h"
#include "strands.h"
#include "logged.h"
#include "kl.h"
#include "imwebp.h"
#include "imdec.h"
#include "metabuilder.h"
#include "scramble.h"
#include "spineinfo.h"
#include "region.h"
#include "imwebpenc.h"
#include "worldtime.h"
#include "hunkfile.h"

#define DEFAULT_STORY        "story"
#define DEFAULT_VOICE_FILE   "voice.text"

// threshold for image masks
#define ALPHA_THRESH    0xd0

namespace ST
{
Term::Terms Term::_allTerms(&Term::compareLess);
};

#ifdef STANDALONE

typedef int GetcharFn(void* ctx);
typedef bool SaveGameFn(void* ctx, const std::string&);

static GetcharFn* getcharFn;
static void* getcharCtx;

static SaveGameFn* saveGameFn;
static void* saveGameCtx;

// these are fake versions to be used by the standalone interpreter.
bool isMobile;
bool isWeb;

// here we hijack getchar in the same way that IFI does
#define getchar() (*getcharFn)(getcharCtx)
#define SaveGameStandalone(_f) (*saveGameFn)(saveGameCtx, _f)

#include "strandi.h"
#include "pstrands.h"

#define SAVE_HUNK_STRAND "strand"

#undef getchar

static bool writeBinary(ST::ParseStrands& ps,
                        const char* fname,
                        bool compress,
                        bool encrypt)
{
    const GrowString& buf = ps._sourceCollector;

    // must compress to also encrypt
    if (!compress) encrypt = false; 

    bool encode = !ps._scrambleKey.empty() && encrypt;
    
    if (encode) ScrambleSetKey(ps._scrambleKey.c_str());
    
    FD out;
    bool r = out.open(fname, FD::fd_new);
    if (r)
    {
        unsigned char* d = (unsigned char*)buf.start();
        unsigned int sz = buf.size();

        if (compress)
        {
            // first see if it compresses.
            // if so the compress header will be part of the output
            // buffer and the size adjusted
            Compress cp;
            compress = writezPrepare(cp, d, sz);

            if (compress)
            {
                if (encode)
                {
                    // scramble and replace compress buffer with scramble buf
                    unsigned int sz2 = cp._outsz;
                    unsigned char* d2 = Scramble(cp._outbuf, sz2, std::string());
                    delete cp._outbuf;
                    cp._outbuf = d2;
                    cp._outsz = sz2;
                }
                
                r = out.write(cp._outbuf, cp._outsz);
            }
            else
            {
                LOG1("WARNING writebinary did not compress ", fname);
            }
        }

        if (!compress)
        {
            r = out.write(d, sz);
        }
    }
    return r;
}

static bool writeVoices(ST::ParseStrands::VoiceSet& vs, const char* fname)
{
    FD out;
    bool r = out.open(fname, FD::fd_new);
    if (r)
    {
        FDBuf fout(out);
        for (auto& v : vs)
        {
            fout.printf("%s: rec:%s\n", v._actor.c_str(), v._filename.c_str());
            fout.write((const unsigned char*)v._speech.c_str(), v._speech.size());
            fout.putc('\n');
            fout.putc('\n');
        }
    }
    return r;
}

struct KLEnv
{
    typedef std::string string;

    struct CapStdStream: public StdStream
    {
        string _buf;
        bool _emit(StdStreamBuf* buf) override
        {
            _buf += (const char*)*buf;
            return true;
        }
    };

    void addKLSymbols(ST::Term::Terms& tl)
    {
        for (auto t : tl)
        {
            ParseContext ctx(&_kl);
            Symbol(t->_name.c_str()).intern(&ctx);            
        }
    }
    
    KL                  _kl;
    ST::Strandi*        _strandi;
    FILE*               _inputFile = 0;
    char                _inputFileBuf[256];
    int                 _inputFilePos = 0;

    KLEnv(ST::Strandi& s) : _strandi(&s)
    {
        extern void initKLStrandi(KL* host, ST::Strandi* s);
        initKLStrandi(&_kl, _strandi);
        _inputFileBuf[0] = 0;
    }

    bool loadFile(const string& f, int errlev = 1)
    { return _kl.loadFile(f, _kl._env, errlev); }


    static int _klGetchar(void* ctx)
    {
        KLEnv* kle = (KLEnv*)ctx;
        return kle->klgetChar();
    }

    static bool _klSaveGame(void* ctx, const std::string& fname)
    {
        KLEnv* kle = (KLEnv*)ctx;
        assert(kle);
        return kle->klSaveGame(fname);
    }

    bool klSaveGame(const std::string& fname)
    {
        LOG1("console save '", fname << "'");

        int t = _strandi->getTimelineTime();

        bool ok = t > 0 && !fname.empty();

        string sdata;
        int size = 0;
        
        if (ok)
        {
            sdata = _strandi->timelineSaveState();
            size = sdata.size();
            //LOG1("save data size:", size);
            ok = size > 0;
        }

        if (ok)
        {
            // ensure we use a save suffix
            string f = changeSuffix(fname, ".sav");

            string path = f; // save right here!
            LOG1("saving '", path << "' size:" << size);

            HunkFile hf(path);
            HunkHead hh((uchar*)sdata.c_str(), size); // not owned

            string title = _strandi->runSingleTermCap("GAME_TITLE");
            if (title.empty())
            {
                LOG1("Game has no title! ", fname);
                ok = false;
            }

            if (ok)
            {
                // add type=strand so we know its for the strand backend
                // and use the title for the name so that save games must match
                hf.add(SAVE_HUNK_STRAND, title, hh);

                ok = hf.write();
                if (ok)
                {
                    LOG1("saved '", title << "' in " << path);
                }
                else
                {
                    LOG1("save, cannot write to file '", path << "'\n");
                }
            }
        }

        return ok;
    }

    void _klFillInputBuf()
    {
        assert(_inputFile);

        for (;;)
        {
            // will contain the newline as well
            _inputFileBuf[0] = 0;
            fgets(_inputFileBuf, sizeof(_inputFileBuf)-1, _inputFile);

            char* p = _inputFileBuf;

            if (!*p) break;  // we had no more from file, so done.

            // remove comments (might also remove final newline)
            while (*p)
            {
                if (*p == '/' && p[1] == '/')
                {
                    *p = 0;
                    break;
                }
                ++p;
            }

            // delete any leading space
            p = _inputFileBuf;
            while (u_isspace(*p)) ++p;
            if (p != _inputFileBuf)
            {
                char* q = _inputFileBuf;
                while ((*q++ = *p++) != 0) ;
            }

            // to rationalise newlines between DOS and unix,
            // remove all end whitespace including newlines
            p = _inputFileBuf;
            p += strlen(p);
            while (p != _inputFileBuf)
            {
                --p;
                if (!u_isspace(*p)) break;
                *p = 0;
            }

            // if we still have something, accept it otherwise get another
            p = _inputFileBuf;            
            if (*p)
            {
                p += strlen(p);
                *p++ = '\n'; // add standard newline
                *p = 0;

                //printf("fill line '%s'\n", _inputFileBuf);
                break;
            }
        }
        
        _inputFilePos = 0;
    }

    int klgetChar()
    {
        int c = 0;

        if (_inputFile)
        {
            c = _inputFileBuf[_inputFilePos];
            if (!c)
            {
                _klFillInputBuf();
                c = _inputFileBuf[_inputFilePos];
            }

            if (c) ++_inputFilePos;
            else
            {
                // input file finished
                _inputFile = 0;
            }
        }

        if (!c)
        {
            // actually get a console char 
            c = ::getchar();
        }
        return c;
    }

    ST::Capture* evalKL(const string& buf, ST::Capture* args)
    {
        // NB: args can be null
        
        //LOG1(TAG "eval KL ", buf << " capture: " << (args ? _strandi->textify(*args) : "none"));
            
        int cc = 0;
            
        ST::Capture* r = new ST::Capture;
        List arglist;
        List argit;
        
        if (args)
        {
            List::iterator it(arglist);
            LValue lv(it);
            for (auto& e : args->_elts)
            {
                if (e._v)
                {
                    lv = *KL::varToTerm(e._v);
                    ++cc;
                }
                else if (e._term)
                {
                    ParseContext ctx(&_kl);
                    Term t = Symbol(e._term->_name.c_str()).find(&ctx);
                    if (t)
                    {
                        lv = *t;
                        ++cc;
                    }
                }
                else
                {
                    // append string if valid
                    if (e._s.size())
                    {
                        lv = Stringt(e._s);
                        ++cc;
                    }
                }
            }

            if (cc)
            {
                ParseContext ctx(&_kl);
                Term s = Symbol("LAST").intern(&ctx); // ctx just helps find term
                // note: undefined IT from KL means no capture.

                //Tree e;
                //List et(e, _kl->_env._env);
                //Env env(et, _kl->_env._args);
                _kl._bindvar(s, &arglist, _kl._env, true, false);
            }
        }

        // for some reason the KL cannot access "IT" ???
        assert(_strandi->_ctx);
        ST::Term* git = _strandi->_ctx->getIt();
        if (git)
        {
            //LOG1("binding kl IT to ", _strandi.textify(git));
            
            // find KL symbol bounds to Strandi "it"
            ParseContext pctx(&_kl);
            Term t = Symbol(git->_name.c_str()).find(&pctx);
            if (t)
            {
                Term termit = Symbol("IT").intern(&pctx);
                
                List::iterator it(argit);
                LValue lv(it);
                lv = *termit;
                //LOG1("binding KL symbol IT to ", _strandi->textify(git));
                _kl._bindvar(termit, &argit, _kl._env, true, false);
            }
            else
            {
                //LOG1("Warning bind IT, no term for ", _strandi->textify(git));
            }
        }

        CapStdStream tout;
        StdStream* old = _kl.setOutput(&tout);
        _kl.loadString(buf.c_str(), _kl._env); // eval!
        _kl.setOutput(old);
        r->add(tout._buf); // ignore if empty

        //LOG1(TAG "eval KL result ", tout._buf);

        if (r->empty())
        {
            delete r;
            r = 0;
        }
        
        return r;
    }
};

namespace ST
{

// keep track of images processed in case there are repeats
static std::vector<string> imagesDone;

static Region* makeImageMask(const string& name,
                             const ImageDecodeState& decoder,
                             int opts)
{
    // opts & 1 == left
    // opts & 2 == right
    // opts & 4 == quick, assume no mask if none in first few lines

    // NB: XX quick currently not implemented

    Region* mask = 0;

    bool ok = decoder._chans == 4 && !decoder._failed && decoder._pix;
    //bool quick = opts & 4;
    
    if (ok)
    {
        mask = new Region;
        int stride = decoder._chans * decoder._w;

        //LOG1("creating image mask for ", name << " " << decoder._w << "x" << decoder._h);

        const unsigned char* p0 = decoder._pix;
        for (int i = 0; i < decoder._h; ++i)
        {
            const unsigned char* p = p0; // each line
            const unsigned char* pe = p0 + decoder._w*4; // end of line
            
            while (p != pe)
            {
                do
                {
                    // XX assumed to be alpha channel
                    if (p[3] >= ALPHA_THRESH) break;
                    p += 4;
                } while (p != pe);

                if (p != pe)
                {
                    // add horizontal spans above alpha thresh
                    // p is start of span. find end of span
                    const unsigned char* q = p;

                    for (;;)
                    {
                        q += 4;
                        if (q == pe) break;
                        if (q[3] < ALPHA_THRESH) break;
                    }

                    *mask += RRect((p - p0)/4, i, (q - p)/4, 1);

                    p = q;
                }
            }

            p0 += stride;
        }

        bool hasMask = true;
        
        if (mask->numRects == 1)
        {
            RRect& ri = mask->rects[0];
            if (ri.width() == decoder._w && ri.height() == decoder._h)
            {
                // mask is whole image
                hasMask = false;
            }
        }

        if (!hasMask || mask->size() == 0)
        {
            delete mask; mask = 0;
        }
    }

    //    if (mask) LOG1("mask for ", name << " " << *mask << " within image " << decoder._w << "x" << decoder._h);
    return mask;
}
    
static bool copyImage(ParseStrands& ps,
                      const string& fpath,
                      unsigned char* d,
                      unsigned int sz,
                      bool scramble)
{
    // return true if copy already present or copy performed.
    // false if copy failed.
    
    assert(!ps._imageOutputDir.empty());

    // copy image
    string f = filenameOf(fpath);
    string ofile = makePath(ps._imageOutputDir, f);

    if (FD::existsFile(ofile))
    {
        // assume that an existing destination file
        // is already correct
        // this wont be the case if the password has changed
        // or the source has been edited.
        //LOG1("image resource already present ", ofile);

        time_t t1 = FD::mtime(fpath);
        time_t t2 = FD::mtime(ofile);

        if (t1 <= t2) return true; // original is older than existing out

        LOG1("copying newer image file ", fpath);
    }
    
    LOG1("copying ", fpath << " to " << ofile);

    if (scramble)
    {
        // encode
        //LOG1("scrambling ", ofile);
        unsigned char* ds = Scramble(d, sz, f);
            
#if 0
        // check
        unsigned int s2 = sz;
        unsigned char* d2 = new unsigned char[s2];
        memcpy(d2, ds, s2);
        DescrambleAll(d2, s2, f);
        if (memcmp(d2, d, s2) && s2 == fd._size)
        {
            LOG1("ERROR scamble fail ", ofile);
            assert(0);
        l}
        delete d2;
#endif                            
            
        d = ds;
    }

    FD of;
    bool ok = of.open(ofile.c_str(), FD::fd_new) && of.write(d, sz);
    if (!ok)
    {
        LOG1("unable to write to file ", ofile);
    }
        
    if (scramble) delete d;
    return ok;
}

static bool processResolutions(GrowString& gs,
                               ParseStrands& ps,
                               const string& fpath,
                               unsigned char* d,
                               unsigned int sz,
                               ImageDecodeState* decoder,
                               ImageMeta& im,
                               bool encode)
{

    // make the low res versions of this original
    // fpath is the full media path of the original we're using, but not
    // its game name.
    
    if (ps._imageOutputDir.empty()) return true; // nothing to do

    bool ok = true;
    bool copyOriginal = true; // default
    bool filtered = false;

    if (decoder && !ps._resolutions.empty())
    {
        int side = u_max(decoder->_w, decoder->_h);

        // if we match a pattern, then we are filtered, so that
        // we do not match the other resolution patters, otherwise
        // we match those and not the pattern
        for (auto& ri : ps._resolutions)
        {
            if (!ri._pattern.empty() &&
                findFirstIgnoreCase(fpath, ri._pattern))
            {
                filtered = true;
                break;
            }
        }
        
        // when we have resolutions specified, must also specify the original
        // if we want it otherwise it's not copied.
        copyOriginal = false;

        //int minside = 0;
        for (auto& ri : ps._resolutions)
        {
            bool hasPattern = !ri._pattern.empty();
            bool match = !hasPattern;
            
            if (filtered)
                match = hasPattern && findFirstIgnoreCase(fpath, ri._pattern);
            
            if (match)
            {
                //if (!minside || ri._value < minside) minside = ri._value;
            
                // zero indicates to take the original regardless of size
                // or if the original exactly matches the original
                if (ri._value == 0 || ri._value == side)
                {
                    copyOriginal = true;
                    break;
                }

                // if there is a resolution bigger than what we have
                // then we also take the original
                if (ri._value > side)
                {
                    copyOriginal = true;
                    break;
                }
            }
        }

#if 0        
        if (side < minside)
        {
            // if all requested sizes are BIGGER than the original
            // we must take the original otherwise we wont have anything!
            copyOriginal = true;
        }
#endif        
        
    }

    // do we want the original?
    if (copyOriginal)
        ok = copyImage(ps, fpath, d, sz, encode);
    else
    {
        // mark the original as not actually present. it has only been
        // used for low res versions. But the metadata will still be present.
        im._present = false;
    }

    // only have a decoder if we are an image and not anim file
    if (decoder)
    {
        for (auto& ri : ps._resolutions)
        {
            if (!ri._value) continue; // skip specification of original

            bool hasPattern = !ri._pattern.empty();
            bool match = !hasPattern;
            
            if (filtered)
                match = hasPattern && findFirstIgnoreCase(fpath, ri._pattern);

            if (match)
            {                
                int h2, w2;
                float sc = 1;

                // determine name for version
                string namestem = changeSuffix(filenameOf(fpath), 0);

                // all low-res versions are encoded to webp
                string vername = changeSuffix(namestem + ri.tailName(),
                                              ".webp");

                // where we will write the version
                string ofile = makePath(ps._imageOutputDir, vername);

                // make the game ID name for the version.
                // this will never be requested by the game, but is
                // the ID for the metadata
                string vname = "images/" + vername;

                // the resolution value refers to the longest side.
                if (decoder->_w >= decoder->_h)
                {
                    if (ri._value < decoder->_w)
                    {
                        w2 = ri._value;
                        sc = (float)w2/decoder->_w;
                        h2 = sc*decoder->_h;
                    }
                }
                else if (ri._value < decoder->_h)
                {
                    h2 = ri._value;
                    sc = (float)h2/decoder->_h;
                    w2 = sc*decoder->_w;
                }

                // only ever reduce size
                if (sc < 1)
                {
                    size_t fz = FD::existsFileSize(ofile);

                    ImageMeta imv(vname);
                    
                    // add version to metadata
                    imv._original = im._name;
                    imv._w = w2;
                    imv._h = h2;
                    
                    // add version to original meta
                    im._versions.push_back(vname);
                    
                    if (fz == 0)
                    {
                        ImageData idst;
                        ImageData isrc;

                        isrc.width = decoder->_w;
                        isrc.height = decoder->_h;
                        isrc.pixelStride = decoder->_chans;
                        isrc.lineStride = isrc.width*isrc.pixelStride;
                        isrc.data = decoder->_pix;
            
                        idst.width = w2;
                        idst.height = h2;
                        idst.pixelStride = decoder->_chans;
                        idst.lineStride = idst.width*idst.pixelStride;

                        int quality = ri._quality;

                        // fallback quality default
                        if (!quality) quality = 85;

                        LOG1("creating low res ", vername << " " << idst.width << 'x' << idst.height << " from " << isrc.width << 'x' << isrc.height << " quality:" << quality);

                        idst.data = new uint8_t[idst.lineStride * idst.height];

                        //LOG1("resampling ", vername);
                        ok = ResampleMix(&isrc, &idst);
                    
                        if (ok)
                        {
                            unsigned int cz;
                            unsigned char* cdat;
                            ok = WebPEncode(idst, quality, &cz, &cdat);

                            if (ok)
                            {
                                ok = copyImage(ps, vername, cdat, cz, encode);
                                WebPEncodeFree(cdat);

                                // size of data for meta
                                fz = cz;
                            }
                            else
                            {
                                LOG1("webp failed to encode ", vername);
                            }
                        }
                        else
                        {
                            LOG1("failed to resize image ", im._name);
                        }

                        // clean up resampled image data
                        delete idst.data;
                        idst.data = 0;
                    }

                    if (ps._genImageMeta)
                    {
                        // assign the file size
                        // this was either from an existing file or
                        // as calculated after compression
                        imv._fileSize = fz; 
                        imv.buildJson(gs);
                        gs.add('\n'); // tidy
                    }
                }
            }

            if (!ok) break;
        }
    }
    return ok;
}

static void processImage(ParseStrands& ps,
                         GrowString& gs,
                         const string& fpath,
                         const string& filename,
                         const char* click = 0)

{
    // fpath is the full resource path
    // filename is the path of the original (the game path)
    // which might not be the same as the end of the resource path.
    // but is used as the ID and is the string given by the game.
    
    if (fpath.empty()) return; // ignore
    if (::contains(imagesDone, fpath)) return;
    imagesDone.push_back(fpath);
    
    LOG2("image meta for ", fpath);

    // filename and fpath can be animation .json

    // use the original name as that will be the one in the metadata
    ImageMeta im(filename);
    string suf = suffixOf(fpath);

    // image media can have SVG which we ignore here.
    bool ok = !equalsIgnoreCase(suf, ".svg");
    bool isAnimJson = equalsIgnoreCase(suf, ".json");
    bool isAnimAtlas = equalsIgnoreCase(suf, ".atlas");

    // json and .atlas files 
    bool isAnim = isAnimJson || isAnimAtlas;

    // do not encode text animation files
    bool encode = !ps._scrambleKey.empty() && !isAnim;

    if (ok)
    {
        ImageDecodeState* decoder = 0;

        FDLoad fd(fpath);
        if (fd)
        {
            bool ok = true;

            // ignore meta for atlas file
            if (ps._genImageMeta && !isAnimAtlas)
            {
                if (isAnimJson)
                {
                    SpineInfo si;
                    string json((char*)fd._data);
                    if (si.readFrom(json))
                    {
                        Point2f d = si.effectiveDim();
                        im._w = d.x;
                        im._h = d.y;
                    }
                    else
                    {
                        LOG1("WARNING, cannot read data from spine json ", fpath);
                    }
                }
                else if (equalsIgnoreCase(suf, ".webp"))
                {
                    // now need to decode full image.
                    // used to be just
                    // ok = WebpGetInfo(fd, fd._size, &w, &h, &chans);
                    decoder = createWEBPDecodeState(fd._data, fd._size);
                }
                else
                {
                    decoder = createSTBIDecodeState(fd._data, fd._size);
                }

                if (decoder)
                {
                    ok = decoder->decodeAll() && !decoder->_failed;
                    if (ok)
                    {
                        im._w = decoder->_w;
                        im._h = decoder->_h;
                        im._fileSize = decoder->_dsize;
                        LOG3("decoded ", fpath << " size " << im._w << "x" << im._h);
                    }
                    else
                    {
                        LOG1("unable to decode image ", fpath);
                    }
                }
            }
        }
        else
        {
            LOG("can't open '", fpath << "'");
            ok = false;
        }

        if (ok && ps._genImageMeta && !isAnim)
        {
            // calculate transparency mask
            if (decoder && !decoder->_failed)
            {
                int opts = 1 | 2 | 4; // left, right, no left also quick

                // returns a pixel detail mask
                // usually need to quantize
                Region* r = makeImageMask(filename, *decoder, opts);
                
                // convert mask to poly and add to clickables?
                if (r && click)
                {
                    ImageMetaBuilder mb(&im);
                    mb._filename = filename;

                    // less quantization for click region than mask
                    r->quantize(4);

                    // use low quantise mask to derive click polygons
                    mb.makeMaskClickable(click, *r);
                }

                if (r)
                {
                    // now quantize to store in the image mask for text
                    r->quantize(16);
                    
                    //LOG1("mask size for ", filename << " = " << r->size());
                    im.setMask(r);  // consume
                }
            }
            
            // is there an SVG companion?
            string companion =
                ps.locateImageResource(changeSuffix(filename, ".svg"));

            if (!companion.empty())
            {
                FDLoad fd(companion);
                if (fd)
                {
                    LOG1("found companion file ", companion);

                    ImageMetaBuilder mb(&im);
                    mb._filename = companion;
                    if (mb.processSVG((const char*)fd._data))
                    {
                        //LOG1("meta json ", meta);

                        if (!im._w || !im._h)
                        {
                            // if we didn't get a size, try to
                            // recover it from the svg
                            Point2 sz = mb.imageSize();
                            im._w = sz.x;
                            im._h = sz.y;
                        }
                    }
                    else
                    {
                        LOG1("Failed to load companion SVG ", companion);
                    }
                }
            }
        }

        if (ok)
        {
            // note we might not have a decoder (anim file)
            ok = processResolutions(gs, ps, fpath, fd._data, fd._size,
                                    decoder, im, encode);
        }

        delete decoder;
    }

    if (ok && ps._genImageMeta && !isAnimAtlas)
    {
        //LOG1("meta ", im);
        im.buildJson(gs);
        gs.add('\n'); // tidy
    }
}

static void processVideo(ParseStrands& ps,
                         GrowString& gs,
                         const string& fpath,
                         const string& filename)

{
    // fpath is the full resource path
    // filename is the path of the original (the game path)
    // which might not be the same as the end of the resource path.
    // but is used as the ID and is the string given by the game.
    
    if (fpath.empty()) return; // ignore
    if (::contains(imagesDone, fpath)) return;
    imagesDone.push_back(fpath);
    
    LOG2("image meta for ", fpath);

    bool ok = false;
    unsigned int sz = 0;
    
    FDLoad fd(fpath);
    if (fd)
    {
        unsigned char* d = fd._data;
        sz = fd._size;
        ok = copyImage(ps, fpath, d, sz, false);
    }

    // use the original name as that will be the one in the metadata
    ImageMeta im(filename);
    im._fileSize = sz;

    // XX need to also set w and h

    // do not encode text animation files
    //bool encode = !ps._scrambleKey.empty() && !isAnim;

    if (ok && ps._genImageMeta)
    {
        //LOG1("meta ", im);
        im.buildJson(gs);
        gs.add('\n'); // tidy
    }
}

    
static void processImages(ParseStrands& ps,
                          const FlowVisitor::MediaSet& allmedia)
{

    // also extract images used in animations
    GrowString gs;
    gs.add('{');
    gs.add('\n');

    bool encode = !ps._scrambleKey.empty();
    if (encode) ScrambleSetKey(ps._scrambleKey.c_str());
    
    for (Flow::EltMedia* mi : allmedia)
    {
        string fpath;  // image path to use
        bool isAnimation = false;
        string atlas, atlaspath;
        string back, backpath;
        
        if (mi->isImage() || mi->isVideo())
        {
            fpath = ps.locateImageResource(mi->_filename);
            if (fpath.empty())
            {
                LOG1("Cannot locate resource ", mi->_filename);
                continue;
            }
        }
        else if (mi->isAnim())  // json file
        {
            isAnimation = true;
            
            // animation corresponding to atlas version
            fpath = ps.locateImageResource(mi->_filename);
             
            if (mi->_attr)
            {
                for (auto& e : *mi->_attr)
                {
                    if (e.first == IFI_IMAGE)  // atlas
                    {
                        const char* s = e.second.rawString();
                        if (s)
                        {
                            atlas = s;
                            atlaspath = ps.locateImageResource(atlas);
                        }
                    }
                    else if (e.first == IFI_BACKIMAGE)  // background optional
                    {
                        const char* s = e.second.rawString();
                        if (s)
                        {
                            back = s;
                            backpath = ps.locateImageResource(back);
                        }
                    }
                }
            }

            if (atlas.empty())
            {
                // try guessing the corresponding atlas image for the animation
                // will be a webp or a png
                atlas = changeSuffix(mi->_filename, ".webp");
                atlaspath = ps.locateImageResource(atlas);
                
                if (atlaspath.empty())
                {
                    atlas = changeSuffix(mi->_filename, ".png");
                    atlaspath = ps.locateImageResource(atlas);
                }
            }
        }
        else continue; // ignore other media types

        if (!fpath.empty())
        {
            // handle the main file

            if (mi->isVideo())
            {
                processVideo(ps, gs, fpath, mi->_filename);
            }
            else
            {
                // do we have a "click" attribute assigned from assets?
                var cl = mi->findAttr(MEDIA_ATTR_CLICK);
                const char* click = cl.rawString();  // may be null

                processImage(ps, gs, fpath, mi->_filename, click);
            
                if (isAnimation)
                {
                    string afile = changeSuffix(mi->_filename, ".atlas");
                    string afilepath = ps.locateImageResource(afile);

                    // copy the atlas file
                    processImage(ps, gs, afilepath, afile);

                    // copy the atlas image
                    processImage(ps, gs, atlaspath, atlas);

                    // copy the background image, if defined
                    processImage(ps, gs, backpath, back);
                }
            }
        }
        else
        {
            LOG1("WARNING: Cannot locate resource ", mi->_filename);
        }
    }

    gs.add('}');
    gs.add('\n'); // tidy
    int sz = gs.size();
    gs.add(0);  // so we can print it, but don't write it
    
    LOG3("meta ", gs);

    const char* outfile = "imagemeta.json";
    FD fdout;
    if (fdout.open(outfile, FD::fd_write | FD::fd_create | FD::fd_trunc))
    {
        if (!fdout.write((const unsigned char*)(const char*)gs, sz))
        {
            LOG1("problem writing ", outfile);
        }
    }
}

static void processAudio(ParseStrands& ps,
                         const FlowVisitor::MediaSet& allmedia)
{
    std::vector<string> audioDone;
    if (ps._audioOutputDir.empty()) return; // nothing to do

    bool encode = !ps._scrambleKey.empty();

    if (encode)
        ScrambleSetKey(ps._scrambleKey.c_str());

    for (Flow::EltMedia* mi : allmedia)
    {
        if (mi->isAudio())
        {
            string fpath = ps.locateAudioResource(mi->_filename);
            if (fpath.empty())
            {
                LOG1("Cannot locate resource ", mi->_filename);
                continue;
            }

            // prevent repeats
            //if (::contains(audioDone, fpath)) continue;
            assert(!::contains(audioDone, fpath));
            audioDone.push_back(fpath);

            FDLoad fd(fpath);
            if (fd)
            {
                string f = filenameOf(fpath);
                string ofile = makePath(ps._audioOutputDir, f);

                if (FD::existsFile(ofile))
                {
                    //LOG1("audio resource already present ", ofile);
                    
                    time_t t1 = FD::mtime(fpath);
                    time_t t2 = FD::mtime(ofile);

                    // original is older than existing out
                    if (t1 <= t2) continue;
                    
                    LOG1("copying newer audio file ", fpath);
                }

                LOG1("copying ", fpath << " to " << ofile);

                unsigned char* d = fd._data;
                unsigned int sz = fd._size;

                if (encode)
                {
                    // encode
                    //LOG1("scrambling ", ofile);
                    // use just filename for label (not path)
                    unsigned char* ds = Scramble(d, sz, f);

#if 0
                    // check
                    unsigned int s2 = sz;
                    unsigned char* d2 = new unsigned char[s2];
                    memcpy(d2, ds, s2);
                    DescrambleAll(d2, s2, f);
                    if (memcmp(d2, d, s2) && s2 == fd._size)
                    {
                        LOG1("ERROR scamble fail ", ofile);
                        assert(0);
                    }
                    delete d2;
#endif                            
                    d = ds;
                }
                
                FD of;
                if (of.open(ofile.c_str(), FD::fd_new) && of.write(d, sz))
                {
                    ;
                }
                else
                {
                    LOG1("unable to write to file ", ofile);
                }
                
                if (encode) delete d;
            }
            else
            {
                LOG("WARNING: can't open '", fpath << "'");
            }
        }
    }
}

} // namespace ST


int main(int argc, char** argv)
{
    int debug = 0;
    Logged initLog;
    ST::ParseStrands ps;

    // write all files out as compressed story
    bool loadGameFiles = true;
    bool compress = true;
    bool genFlow = false;
    bool autoChoose = false;
    bool enableLog = false;
    bool disableChoice = false;

    
    const char* initFilename = "init";
    const char* inputFilename = 0;

    std::vector<std::string> files;
    bool ok = true;
    
    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (!u_stricmp(argv[i], "-log")) enableLog = true;
            else if (!u_stricmp(argv[i], "-d"))
            {
                debug = 1;
                if (i < argc-1 && u_isdigit(*argv[i+1]))
                    debug = atoi(argv[++i]);
            }
            else if (!u_stricmp(argv[i], "-bin"))
            {
                // collect up all the source code
                // as we read it in.
                ps._collectSource = true;
            }
            else if (!u_stricmp(argv[i], "-only"))
            {
                // do not load other game files by processing
                // TERM_GAME_FILES
                // this option is useful when all files are merged into one
                loadGameFiles = false;
            }
            else if (!u_stricmp(argv[i], "-nocompress"))
            {
                // if emitting bin, do not compress
                compress = false;
            }
            else if (!u_stricmp(argv[i], "-genvoice")) ps._collectVoices = true;
            else if (!u_stricmp(argv[i], "-genmeta")) ps._genImageMeta = true;
            else if (!u_stricmp(argv[i], "-o") && i < argc-1)
            {
                // copy images to output dir and optionally encrypt
                // if -key given
                ps._imageOutputDir = argv[++i];
            }
            else if (!u_stricmp(argv[i], "-oa") && i < argc-1)
            {
                ps._audioOutputDir = argv[++i];
            }
            else if (!u_stricmp(argv[i], "-key") && i < argc-1)
            {
                ps._scrambleKey = argv[++i];
            }
            else if (!u_stricmp(argv[i], "-mobile"))
            {
                // set fake version for testing in game script
                isMobile = true;
            }
            else if (!u_stricmp(argv[i], "-web"))
            {
                // set fake version for testing in game script
                isWeb = true;
            }
            else if (!u_stricmp(argv[i], "-imagepath") && i < argc-1)
            {
                // path is semicolon separated list of directories
                const char* ip = argv[++i];
                std::vector<std::string> ipath;
                ::split(ipath, ip, ';');
                if (ipath.empty())
                {
                    LOG1("imagepath expects a path ", ip);
                    ok = false;
                }
                else
                {
                    // append to existing path
                    ::merge(ps._imagePaths, ipath);
                }
            }
            else if (!u_stricmp(argv[i], "-audiopath") && i < argc-1)
            {
                // path is semicolon separated list of directories
                const char* ip = argv[++i];
                std::vector<std::string> ipath;
                ::split(ipath, ip, ';');
                if (ipath.empty())
                {
                    LOG1("audiopath expects a path ", ip);
                    ok = false;
                }
                else
                {
                    // append to existing path
                    ::merge(ps._audioPaths, ipath);
                }
            }
            else if (!u_stricmp(argv[i], "-dump")) ps._dump = true;
            else if (!u_stricmp(argv[i], "-play") && i < argc-1)
            {
                inputFilename = argv[++i];
            }
            else if (!u_stricmp(argv[i], "-res") && i < argc-1)
            {
                // expect a list of resolutions
                // 1000,2k,0,foo=1000,2000,0
                const char* f = argv[++i];

                std::string currentPattern;
                
                while (*f)
                {
                    ST::ParseStrands::Resolution res;
                    if (res.parse(f))
                    {
                        if (!res._pattern.empty())
                        {
                            currentPattern = res._pattern;
                        }
                        else
                        {
                            // otherwise propagate pattern
                            res._pattern = currentPattern;
                        }
                        
                        //LOG1("got resolution ", res);
                        ps._resolutions.push_back(res);
                    }
                    else
                    {
                        LOG1("resolution bad size '", f << "'");
                        break;
                    }

                    if (*f == ',') ++f; // skip separator
                    else if (*f == ';')
                    {
                        currentPattern.clear(); // and clear pattern
                        ++f; // skip separator
                    }
                }
            }
            else if (!u_stricmp(argv[i], "-flow"))
            {
                genFlow = true;
            }
            else if (!u_stricmp(argv[i], "-autochoose"))
            {
                // mark to auto choose singletons.
                // not particularly useful except for testing
                autoChoose = true;
            }
            else if (!u_stricmp(argv[i], "-nochoice"))
            {
                disableChoice = true;
            }
            else
            {
                printf("unrecognised option '%s'\n", argv[i]);
            }
        }
        else
        {
            files.push_back(argv[i]);
        }
    }

    if (enableLog)
    {
        initLog.openFile("strand.log");
    }

    if (ok && !files.size())
    {
        // look for default story file.
        files.push_back(DEFAULT_STORY);
        printf("Loading default story '%s'\n", files[0].c_str());
    }

    if (!ps._scrambleKey.empty() && !ps._collectSource)
    {
        // if key given, must have image dir
        // unless we're just writing bin
        if (ps._imageOutputDir.empty() && ps._audioOutputDir.empty())
            ok = false;
    }
    
    if (!ok)
    {
        printf("Usage: %s [-bin [-nocompress]] [-genvoice] [-dump] [-only] [-d [n]] [-mobile] [-web] [-play <scriptfile>] [-genmeta] [-o <outdir> [-audiopath <foo;bar>] [-imagepath <foo;bar>] [-key <key>]] file...\n", argv[0]);
        return -1;
    }

    if (debug)
    {
        printf("debug level %d\n", debug);
        ps._debug = debug;
        Logged::_logLevel = debug;
    }

    bool v = ps.loadFiles(files, 0, loadGameFiles);

    if (v)
    {
        ST::FlowVisitor::MediaSet allMedia;

        // are we generating media output
        bool genMediaOutput = ps._genImageMeta
            || !ps._imageOutputDir.empty()
            || !ps._audioOutputDir.empty();

        bool v = ps.validate(&allMedia);

        if (v && ps._startTerm)
        {
            if (ps._collectSource)
            {
                // emit compressed binary
                const char* sfile = DEFAULT_STORY STORY_COMPRESSED_SUFFIX;
                v = writeBinary(ps, sfile, compress, true);
                if (!v)
                {
                    LOG1("ERROR writing binary file '", sfile << "'");
                }
            }
            else if (ps._collectVoices)
            {
                writeVoices(ps._voiceSet, DEFAULT_VOICE_FILE);
            }
            else if (!inputFilename && genMediaOutput)
            {
                // emit image meta data json from media
                // don't do this if we have "-play <inputfilename>"
                // wherein we are to run the script first
                // and emit later (see below)
                ST::processImages(ps, allMedia);
                ST::processAudio(ps, allMedia);
            }
            else
            {
                ST::Strandi si(&ST::Term::_allTerms);
                si.setdebug(debug);
                si._autoChooseSingletons = autoChoose;

                KLEnv kle(si);
                kle.loadFile(initFilename, 2);  // soft error

                // set up global getchar hook to redirect to our handler
                getcharFn = &KLEnv::_klGetchar;
                getcharCtx = &kle;

                saveGameFn = &KLEnv::_klSaveGame;
                saveGameCtx = &kle;

                using namespace std::placeholders;
                si.setEvaluator(std::bind(&KLEnv::evalKL, &kle, _1, _2));

                kle.addKLSymbols(ST::Term::_allTerms);

                bool ok = true;
                FILE* fin = 0;
                uint64 randomSeed = 12345;  // default seed for scripts

                if (inputFilename)
                {
                    fin = fopen(inputFilename, "r");
                    if (fin)
                    {
                        kle._inputFile = fin;

                        // when scripting we skip single choices
                        si._autoChooseSingletons = true;
                    }
                    else
                    {
                        printf("ERROR: cannot open input file '%s'\n", inputFilename);
                        ok = false;
                    }
                }
                else
                {
                    // When we're not running an input file take
                    // a real-word seed
                    int64_t r = WorldTime::epoch();
                    randomSeed = r;
                }

                // feed in a random seed from the outside to start
                kle._kl.setSeed(randomSeed);
                si.randomise(randomSeed);

                if (ok)
                {
                    if (genFlow)
                    {
                        // make a flow report
                        si.prepare();
                        if (si._prepared)
                        {
                            ST::Strandi::FlowGraph fg;
                            si.generateFlow(fg);
                            LOG1(si.flowReport(fg), "\n");
                        }
                    }
                    else
                    {
                        if (disableChoice)
                            si._elevateChoices = false;

                        if (!si.start(ps._startTerm))
                        {
                            v = false;
                            ERR0("cannot start");
                        }
                    }
                }

                if (fin) fclose(fin);

                if (inputFilename && genMediaOutput)
                {
                    // generate media output from the media items collected
                    // by the playthrough.
                    std::string t = GAME_METATAG_PREFIX;
                    t += toUpper(IFI_ICONNAME);
                    std::string iconname = si.runSingleTermCap(t);
                    if (!iconname.empty())
                    {
                        //LOG1("Manually collecting seen icon ", iconname);
                        auto em = ST::Term::makeFakeMedia(iconname);
                        ST::FlowVisitor::addEltMedia(si._mediaSeen, em);
                    }
                    
                    ST::processImages(ps, si._mediaSeen);
                    ST::processAudio(ps, si._mediaSeen);
                }
            }
        }
    }

    return v ? 0 : 1;
}


#endif // STANDALONE
