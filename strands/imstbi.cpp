//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include <assert.h>
#include "imdec.h"
#include "logged.h"
#include "ti.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_STDIO
#define STBI_NO_BMP
#define STBI_NO_TGA
#define STBI_NO_GIF
#define STB_IMAGE_STATIC
#include "stb_image.h"
#undef STB_IMAGE_IMPLEMENTATION

struct STBIDecodeState: public ImageDecodeState
{
    STBIDecodeState(const unsigned char* data, int sz)
        : ImageDecodeState(data, sz)
    {
    }

    ~STBIDecodeState() override
    {
        if (_pix)
        {
            stbi_image_free(_pix);
            _pix = 0;
        }
    }

    bool _decodeStep() override
    {
        // return true when done or failed
        assert(!_failed);

        _pix = stbi_load_from_memory(_data, _dsize,
                                     &_w, &_h,
                                     &_chans,
                                     4);  // require 4 channels out

        // regardless of input, we will have 4 chan output
        _chans = 4;

        if (!_pix) _failed = true;
        return true;
    }
};
    
ImageDecodeState* createSTBIDecodeState(const unsigned char* data, int sz)
{
    return new STBIDecodeState(data, sz);
}
