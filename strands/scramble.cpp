//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include <string.h>
#include <assert.h>
#include "types.h"
#include "bytes.h"
#include "scramble.h"
#include "logged.h"
#include "worldtime.h"
#include "rand.h"
#include "cutils.h"

#define SCRAMBLE_ID 0x45525453         // STRE
#define SCRAMBLE_VERSION        1

struct RC4
{
    unsigned char SBox[256];
    unsigned int  key[4]; // 128 bits
    unsigned int  keySizeWords = 0;

    unsigned char si, sj;

    void init()
    {
        unsigned char K[256];
        unsigned int i, j;

        for (i = 0; i < 256; ++i) SBox[i] = i;

        //for (i = 0; i < 32; ++i) memcpy(K + i * 8, key, 8);
        j = 64/keySizeWords;
        for (i = 0; i < j; ++i)
            memcpy(K + i*keySizeWords*4, key, keySizeWords*4);
        
        // initial permutation of S
        j = 0;
        for (i = 0; i < 256; ++i) 
        {
            j = (j + SBox[i] + K[i]) & 0xff;
            unsigned int t = SBox[i];
            SBox[i] = SBox[j];
            SBox[j] = t;
        }

        si = 0;
        sj = 0;
    }

    unsigned int next()
    {
        /* generate the next key value */
        unsigned char ti, tj;
        
        ti = SBox[++si];
        sj += ti;
        tj = SBox[sj];

        SBox[si] = tj;
        SBox[sj] = ti;
        
        ti += tj;
        return SBox[ti];
    }
    
    void setKey64(unsigned int a, unsigned int b)
    {
        key[0] = a;
        key[1] = b;
        keySizeWords = 2;
    }

    void setKey128(uint32_t* k)
    {
        keySizeWords = 4;
        for (unsigned int i = 0; i < keySizeWords; ++i) key[i] = k[i];
    }
};

// NB: do not make this static otherwise android fails.
unsigned char TheKey[16+1] = "MISUNDERSTANDING";

static void setup(RC4& rc4, uint64_t r)
{
    uint32_t key1[4];
    unsigned char* kp = TheKey;
    for (int i = 0; i < 4; ++i)
    {
        key1[i] = rbyte32(kp); kp += 4;
    }

    // combine nonce into key
    uint32_t rlo = (uint32_t)r;
    uint32_t rhi = (uint32_t)(r >> 32);
    key1[0] ^= rhi;
    key1[1] ^= rlo;

    rc4.setKey128(key1);
    rc4.init();
}

struct ScrambleState
{
    int         _failCode = SCRAMBLE_FAIL_NONE;
    uint64_t    _nonce;
    RC4         _rc4;
    int         _version;
    std::string _label;
};

#define REM  (size - (dp - data))
#define DGET  ((*dp++) ^= ss->_rc4.next())

bool DescrambleOK(const ScrambleState* ss)
{
    assert(ss);
    return ss->_failCode == SCRAMBLE_FAIL_NONE;
}

ScrambleState* DescrambleStart(unsigned char* data,
                               unsigned int& size,
                               const char* label)
{
    ScrambleState* ss = new ScrambleState;

    // scramble header magic number + version# + 64 bit nonse
    unsigned int hsize = 4 + 1 + 8;

    bool ok = (size > hsize);  // have header + more

    if (ok)
    {
        unsigned char* dp = data;
        unsigned int id = rbyte32(dp); dp += 4;
        unsigned int version = *dp++;

        if (!label) label = ""; // omit label check/unknown file
        
        // otherwise ignore as this is not scrambled
        ok = id == SCRAMBLE_ID;

        //LOG1("Descrable, file ", label << " scramble:" << ok);

        if (ok)
        {
            //LOG1("Descramble ", label);
            ok = version <= SCRAMBLE_VERSION;

            if (!ok)
            {
                ss->_failCode = SCRAMBLE_FAIL_VERSION;
                //LOG1("Descramble wrong version ", version << " expected " << SCRAMBLE_VERSION << " in " << label);
            }
        }
        else
        {
            ss->_failCode = SCRAMBLE_FAIL_ID; // not scrambled
        }
        
        if (ok)
        {
            ss->_version = version;
            ss->_nonce = rbyte64(dp); dp += 8;// read nonce

            //LOG1("Descramble, reading nonce, ", ss->_nonce);
            setup(ss->_rc4, ss->_nonce);

            if (version > 0)
            {
                ok = REM > 0;
                if (ok)
                {
                    // expect pad bytes
                    // expect 1-98 chars
                    unsigned int padz = DGET;

                    ok = REM > padz;
                    if (ok)
                    {
                        //LOG1("Descramble padding ", padz);
                        
                        // pass over the padding data decoding it
                        while (padz--) DGET;
                    }
                }
            }

            if (ok)
            {
                ok = REM > 0;
                
                if (ok)
                {
                    // read label;
                    // read length which is first 0-255 chars of label
                    unsigned int lz = DGET;
                
                    ok = REM > lz;
                    if (ok)
                    {
                        unsigned char* lp = dp;
                        unsigned int n = lz;
                        while (n--) DGET;
                    
                        // label in file must agree with that provided up to 255 chars
                        // if we didn't provide a label, the accept whatever
                        unsigned int a = u_strlenz(label);
                        if (a)
                        {
                            ss->_label = label; // accept our label

                            // if given label shorter, fail
                            // otherwise must match up to length in file
                            ok = a >= lz && !memcmp(label, lp, lz);
                        }
                        else
                        {
                            // if we don't have a label, recover from file.
                            // NB: may be truncated
                            ss->_label = std::string((char*)lp, lz);
                        }
                        
                        /*
                          if (ok)
                          {
                          if (!a)
                          {
                          LOG1("### accepting ommitted label for ", ss->_label);
                          }
                          else
                          {
                          LOG1("### matching labels ", label << " with " << std::string((char*)lp, lz));
                          }
                          }
                        */

                        if (!ok)
                        {
                            ss->_failCode = SCRAMBLE_FAIL_LABEL;
                            //LOG1("Descramble bad label '", ss->_label << "' of " << lz);
                        }
                    }
                }
            }

            // remaining data
            unsigned int n = REM;

            if (ok)
            {
                // now decode data and move it to start
                size = n;
                while (n--) *data++ = DGET;
            }
            else
            {
                // even though we failed, continue to descramble the
                // remaining data, but leave it in place
                while (n--) DGET;
            }
        }
    }

    if (!ok && !ss->_failCode) ss->_failCode = SCRAMBLE_FAIL_DATA;
    return ss;
}

void DescrambleNext(ScrambleState* ss, unsigned char* dp, unsigned int size)
{
    unsigned int n = size;
    while (n--) DGET;
}

void DescrambleEnd(ScrambleState* ss)
{
    delete ss;
}

int DescrambleAll(unsigned char* data, unsigned int& size, const char* label)
{
    // return scramble fail code, 0 if ok
    ScrambleState* ss = DescrambleStart(data, size, label);
    int r = ss->_failCode;
    DescrambleEnd(ss);
    return r;
}

struct NonceGen
{
    Ranq1     _ran;

    NonceGen() : _ran(WorldTime::any()) {}

    uint64_t gen()
    {
        uint64_t v = _ran.gen64();

        // re-seed mixed with current time
        // this changes the internal state so that the next gen
        // is not related
        _ran.seed(v + WorldTime::any());

        return v;
    }

    unsigned char genChar()
    {
        // use all the 64 bits
        uint64_t v = gen();
        unsigned char c = 0;
        for (int i = 0; i < 8; ++i)
        {
            c += v & 0xff;
            v >>= 8;
        }
        return c;
    }

    unsigned char genPadChar()
    {
        return _ran.gen32() & 0xff;
    }
};

static NonceGen nonceGen;

unsigned char* Scramble(const unsigned char* data,
                        unsigned int& size,
                        const std::string& label)
{
    // NB: caller must delete return buffer
    uint64_t nonce = nonceGen.gen();

    // scramble header magic number + version# + 64 bit nonse
    unsigned int ssize = 4 + 1 + 8;

    // then we have the label size (1 byte) then the label data
    // NB: label can be empty
    unsigned int lz = label.size();
    if (lz > 255) lz = 255; // clamp
    ssize += 1 + lz;  // first 255 bytes of label + 1 for label size

    // then we have the data
    ssize += size;

#if SCRAMBLE_VERSION > 0
    // allow 1-98 bytes of pad
    unsigned int padz = (nonceGen.genChar() % 97) + 1;
    ssize += padz + 1; // padz bytes plus 1 for length
#endif    

    // create new memory image
    unsigned char* sdata = new unsigned char[ssize];

    // write header
    unsigned char* sp = sdata;
    wbyte32(SCRAMBLE_ID, sp); sp +=4; // magic header
    *sp++ = SCRAMBLE_VERSION;
    wbyte64(nonce, sp); sp += 8;

    // from now everything is encrypted, including label
    RC4 rc4;
    setup(rc4, nonce);

#if SCRAMBLE_VERSION > 0
    *sp++ = padz ^ rc4.next();  // write length
    while (padz)  // write pad chars
    {
        --padz;
        *sp++ = nonceGen.genPadChar() ^ rc4.next();
    }
#endif    
    
    // write label
    *sp++ = lz ^ rc4.next();  // write label length (can be zero)
    unsigned char* dp = (unsigned char*)label.c_str();
    while (lz)
    {
        --lz;
        *sp++ = *dp++ ^ rc4.next();
    }
    
    // write data
    unsigned int n = size;
    while (n)
    {
        --n;
        *sp++ = *data++ ^ rc4.next();
    }

    size = ssize;
    return sdata;
}

#ifdef STANDALONE

// the runtime does not need to set the key as it is already patched

#include "md5.h"

void ScrambleSetKey(const char* pw)
{
    MD5 md5;
    unsigned char* d = md5.hash(pw);
    memcpy(TheKey, d, 16);
}

#endif // STANDALONE
