#include <assert.h>
#include "imwebpenc.h"
#include "src/webp/encode.h"
#include "logged.h"
#include "ti.h"

bool WebPEncode(ImageData& img, int quality,
                 unsigned int* sz, unsigned char** out)
{
    assert(img.pixelStride == 4); // RGBA
    assert(sz && out);



    *out = 0;
    *sz = WebPEncodeRGBA(img.data,
                         img.width,
                         img.height,
                         img.lineStride,
                         (float)quality,
                         out);

    //LOG1("WEPEncoded ", img.width << "x" << img.height << " size: " << img.lineStride*img.height << " into size:" << *sz << " at " << (void*)*out);
    
    return (*sz && *out);
}


void WebPEncodeFree(unsigned char* d)
{
    WebPFree(d);
}

