REM Edit the next line to locate your strand build directory
set TOP=b:
set STR=%TOP%\strands

set SDK=..\..
set TOOLS=%TOP%\tools
rm -rf web
mkdir web
xcopy /E /I /Y %SDK%\bin\web web
mkdir web\images
mkdir web\audio
..\..\bin\strands -bin 
%STR%\strands -bin 
move /Y story.stz web
%TOOLS%\keypatch\keypatch web/index.wasm web/index.wasm -key password
%STR%\strands -res 800q80,2000q85,0 -genmeta -o web/images -oa web/audio -key password
move /Y imagemeta.json web
start http://localhost/index.html
%SDK%\bin\civetweb -listening_ports 80 -document_root web
