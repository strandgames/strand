//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
//
//  info@strandgames.com
//

START
INITCORE
GAMECOVER
STORY
GAME_DETAILS
TOP_BEGIN
\nGame Over.

GAME_FILES
core.str picton.str

GAME_TITLE
The Picton Files: Murder at the Manor

GAME_AUTHOR
Jkj Yuio

GAME_VERSION
1.17

GAME_ORGANISATION
Strand Games

GAME_BACKIMAGE

GAME_COVERTEXT
:color:blue,font:Kanit Thin,weight:200

GAME_FONT
merriweathersans-light

MAP_BACKIMAGE

GAME_META
:ui_ucanvas:false,ui_sidebar:false,autolink:false

// hook here to CREDITS_PLAIN (converted from HTML) or other plain
GAME_CREDITS
CREDITS_PLAIN_WEB

CREDITS_PLAIN_WEB
The game is developed in _Strand_ and presented with graphics rendered from 3D models, including backgrounds and characters. The game will include ambient sounds and other effects where possible. Gameplay will consist of hybrid choice-parser input with clickables.

Thanks for playing!

GAME_CREDITS_HTML
<h1>GAME_TITLE<br/><em>by GAME_ORGANISATION</em></h1>
<h3>Implemented in Strands</h3>
<h4>Strand Games Team</h4>
<ul>
<li>Hugh Steers</li>
<li>Stefan Meier</li>
<li>Thomas Dettbarn</li>
<li>Stefan Bylund</li>
</ul>
<h4>Writing</h4>
<ul>
<li>GAME_AUTHOR</li>
</ul>
<h4>Beta Testing</h4>
<ul>
<li>Christopher Merriner</li>
</ul>

// hook quit here
GAME_QUIT
GAME_QUIT1

GAME_QUIT1?
Are you sure?
*+ yes
GAMEOVER
*+ no

GAME_CHEAT
There's no cheating!
/// HELP

HELPME
This is interactive fiction. You can move around and interact with the world and with people. You can type commands like "get lamp" and "kill troll with axe" and also select choices.

Type \LOOK to refresh the room description. Move about with commands like "go north", "go east" and so on. These can be abbreviated to "n" and "e" for example.
\nAsk people about things, for example, "ask butler about dog" etc.
