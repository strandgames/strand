REM Edit the next line to locate your strand build directory
set TOP=b:
set STR=%TOP%\strands
set SDK=..\..
set TOOLS=%TOP%\tools
rm -rf web
mkdir web
xcopy /E /I /Y %SDK%\bin\web web
mkdir web\images
mkdir web\audio
..\..\bin\strands -bin 
%STR%\strands -bin 
move /Y story.stz web
%SDK%\bin\strands -genmeta
%STR%\strands -genmeta -o web/images -oa web/audio
move /Y imagemeta.json web
start http://localhost/index.html
%SDK%\bin\civetweb -listening_ports 80 -document_root web
