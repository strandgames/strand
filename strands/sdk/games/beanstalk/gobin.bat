REM Edit the next line to locate your strand build directory
set STR=b:\strands
set BIN=%STR%\imgui
set SDK=%STR%\sdk
set TOOLS=%TOP%\tools
mkdir bin
mkdir bin\images
mkdir bin\audio
copy /Y init.kl bin
copy /Y %SDK%\bin\web\fontlist.txt bin
xcopy /E /I /Y %SDK%\bin\web\fonts bin\fonts
copy /Y %BIN%\audio\click.ogg bin\audio
copy /Y beanstalk.str bin\story.stz
copy /Y %BIN%\game.exe bin
%STR%\strands -story beanstalk.str -genmeta -o bin/images -imagepath bin/images -oa bin/audio
move /Y imagemeta.json bin
bin\game -d 3

