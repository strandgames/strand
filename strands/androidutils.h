//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#ifdef __ANDROID__

#pragma once

#include <android/native_activity.h>
#include <android/configuration.h>
#include <assert.h>
#include "logged.h"

extern "C"
{
const void* sapp_android_get_native_activity();
};

#if 0
static inline void unpackAssets()
{
    auto act = (ANativeActivity*)sapp_android_get_native_activity();
    if (act)
    {
        AAssetManager* am = act->assetManager;
        assert(am);

        AAssetDir* assetDir = AAssetManager_openDir(am, "");

        static const int bufSize = 1024*1024;
        char buf[bufSize];

        const char* filename;
        while ((filename = AAssetDir_getNextFileName(assetDir)) != NULL)
        {
            AAsset* asset = AAssetManager_open(am,
                                               filename,
                                               AASSET_MODE_STREAMING);

            if (!asset) break;

            FILE* fout = fopen(filename, "wb");
            if (fout)
            {
                int nb_read = 0;
                while ((nb_read = AAsset_read(asset, buf, bufSize)) > 0)
                    fwrite(buf, nb_read, 1, fout);
                
                fclose(fout);
            }
            
            AAsset_close(asset);
        }
    }
}
#endif

inline unsigned char*
android_loadfile(const char* name, unsigned int& sz)
{
    // caller MUST free data
    unsigned char* data = 0;
    
    auto act = (ANativeActivity*)sapp_android_get_native_activity();
    if (act)
    {
        AAssetManager* am = act->assetManager;
        assert(am);

        // Open your file
        AAsset* file = AAssetManager_open(am, name, AASSET_MODE_BUFFER);

        if (file)
        {
            // Get the file length
            size_t fileLength = AAsset_getLength(file);

            if (fileLength)
            {
                // Allocate memory to read your file
                data = new unsigned char[fileLength];

                // Read your file
                AAsset_read(file, data, fileLength);

                sz = fileLength;
            }

            AAsset_close(file);
        }
    }

    return data;
}

inline int android_dpi()
{
    /* return the screen density (DPI).
     * In theory, it should return one from the following table of
     * values, but in practice it seems to return its own number!
     */
    
    int dpi = 0;

    /*
      ACONFIGURATION_DENSITY_DEFAULT = 0,
      ACONFIGURATION_DENSITY_LOW = 120,
      ACONFIGURATION_DENSITY_MEDIUM = 160,
      ACONFIGURATION_DENSITY_TV = 213,
      ACONFIGURATION_DENSITY_HIGH = 240,
      ACONFIGURATION_DENSITY_XHIGH = 320,
      ACONFIGURATION_DENSITY_XXHIGH = 480,
      ACONFIGURATION_DENSITY_XXXHIGH = 640,
      ACONFIGURATION_DENSITY_ANY = 0xfffe,
      ACONFIGURATION_DENSITY_NONE = 0xffff,
    */
    
    auto act = (ANativeActivity*)sapp_android_get_native_activity();
    if (act)
    {
        AAssetManager* am = act->assetManager;
        assert(am);

        AConfiguration* ac = AConfiguration_new();
        AConfiguration_fromAssetManager(ac, am);

        // can return values not in above list;
        // eg;
        // 440 = phone
        // 360 = pad
        dpi = AConfiguration_getDensity(ac);

        if (dpi >= ACONFIGURATION_DENSITY_ANY)
        {
            // mark as unknown
            dpi = 0;
        }

        AConfiguration_delete(ac);
    }
    return dpi;
}

inline const char* android_internal_datapath()
{
    const char* path = 0;
    
    auto act = (ANativeActivity*)sapp_android_get_native_activity();
    if (act)
    {
        path = act->internalDataPath;
    }
    return path;
}

inline void android_finish()
{
    auto act = (ANativeActivity*)sapp_android_get_native_activity();
    if (!act) return;
    ANativeActivity_finish(act);
}

#define JAVA_CALL_ID_URL  1

inline bool android_java_call(int id, const char* s)
{
    auto act = (ANativeActivity*)sapp_android_get_native_activity();
    if (!act) return false;

    JavaVM* jvm = act->vm;
    JNIEnv* jniEnv = act->env;

    jint lResult = jvm->AttachCurrentThread(&jniEnv, 0);
    if (lResult)
    {
        LOG1("android_call_java, failed to attach ", lResult);
        return false;
    }

    jclass ClassNativeActivity = jniEnv->GetObjectClass(act->clazz);
    jmethodID _method = jniEnv->GetMethodID(ClassNativeActivity,
                                             "HandleNative", "(ILjava/lang/String;)V");

    if (_method)
    {
        jstring jstr = jniEnv->NewStringUTF(s);
        jniEnv->CallVoidMethod(act->clazz, _method, id, jstr);
    }
    else
    {
        LOG1("android_call_java method not found ", _method);
    }

    jvm->DetachCurrentThread();

    return true;
}


#endif  // __ANDROID__


