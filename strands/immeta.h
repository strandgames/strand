//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <iostream>
#include <string>
#include <list>
#include <vector>
#include "jsonwalker.h"
#include "ifischema.h"
#include "svgpath.h"
#include "regionjson.h"
#include "utils.h"
#include "mediatype.h"

#define META_TEXT_AREA_NAME "text_area"

struct ImageMeta: public MediaTraits
{
    // meta data for a single image

    // game path of image, eg images/foo.webp
    string      _name; 
    int         _w = 0;
    int         _h = 0;

    // optional mask for image
    // this is pixel coordinate mask probably quantized but in
    // the coordinate space of the original image
    // NB: not stored for non-original
    // the mask is used for images inline to text.
    ST::Region* _mask = 0;

    // if we are a version, this is the name of the original image
    // which should also be listed in the meta data.
    string      _original;

    // if we are an original, this is a list of lower res versions
    // these names should also be in the meta data.
    std::vector<string> _versions;

    // are we present?
    // used by originals when they are no present, but are still in the
    // metadata
    bool        _present = true;

    // if known this helps loading
    unsigned int _fileSize = 0;

    MediaType   _mType = m_void;

    bool isOriginal() const { return _original.empty(); }

    void setName(const string& n)
    {
        _name = n;
        _mType = mediaSuffix(suffixOf(_name));
    }

    void setMask(ST::Region* r)
    {
        // consume r
        assert(!_mask);
        _mask = r;
    }

    enum EltType
    {
        m_elt_void = 0,
        m_elt_click,
        m_elt_text,
    };

    struct Elt
    {
        Elt(EltType t) : _type(t) {}
        virtual ~Elt() {}

        EltType  _type;

#ifdef TOOLS_BUILD        
        friend std::ostream& operator<<(std::ostream& os, const Elt& e)
        {
            GrowString gs;
            e.buildJson(gs);
            gs.add(0);
            return os << gs;
        }

        virtual void buildJson(GrowString& gs) const = 0;
#endif // TOOLS_BUILD
        
    };

    struct EltPolyRegion: public Elt
    {
        string          _label;
        Polys           _polys;
        int             _z = 0;
        
        EltPolyRegion(EltType t) : Elt(t) {}

#ifdef TOOLS_BUILD        
        void _buildJsonPart(GrowString& gs) const
        {
            JSONWalker::addKeyValue(gs, IFI_NAME, _label);
            JSONWalker::addKeyValue(gs, IFI_ZORDER, _z);
            _polys.buildJson(gs);
        }
#endif
        
    };
        
    struct EltClick: public EltPolyRegion
    {
        // clickable region
        
        // draw even when not hovered or hit (IFI_SHOW)
        // eg signposts & overlays
        bool            _show = false;

        // whether this is actually clickable (not persistent)
        bool            _active = true;

        EltClick() : EltPolyRegion(m_elt_click) {}
        EltClick(const string& s) : EltPolyRegion(m_elt_click)
        {
            _label = s;
        }

#ifdef TOOLS_BUILD        
        void buildJson(GrowString& gs) const override
        {
            // { name:path, polys }
            JSONWalker::toAdd(gs);
            gs.add('{');
            EltPolyRegion::_buildJsonPart(gs);
            if (_show)
                JSONWalker::addBoolValue(gs, IFI_SHOW, true);

            gs.add('}');
            gs.add('\n'); // formatting
        }
#endif        
    };
    
    typedef std::list<Elt*>     Elts;
    Elts                        _elts;

    ImageMeta() {}
    ImageMeta(const string& name)
    {
        setName(name);
    }
    
    ~ImageMeta() { clear(); }
    
    void add(Elt* e) { _elts.push_back(e); }

    void clear()
    {
        ::purge(_elts);
        delete _mask; _mask = 0;
    }

    bool hasClickables() const
    {
        for (auto ei : _elts)
            if (ei->_type == m_elt_click) return true;

        return false;
    }

    bool hasVersions() const { return !_versions.empty(); }

#ifdef TOOLS_BUILD    
    void buildJson(GrowString& gs) const
    {
        // imagemeta:{ name:name, width:w, height:w, clickables:[{..}]}
        // mask:{..}
        // original:"images/foo.webp",present:false,versions:["images/foo_1.webp"]
        
        JSONWalker::toAdd(gs);
        JSONWalker::addKey(gs, IFI_IMAGEMETA);
        gs.add('{');
        
        JSONWalker::addKeyValue(gs, IFI_NAME, _name);

        if (_w && _h)
        {
            JSONWalker::addKeyValue(gs, IFI_WIDTH, _w);
            JSONWalker::addKeyValue(gs, IFI_HEIGHT, _h);
        }

        if (_fileSize)
        {
            // file wont be > 2G
            JSONWalker::addKeyValue(gs, IFI_FILESIZE, (int)_fileSize);
        }

        if (!_present)
        {
            // only ever store when not present
            JSONWalker::addBoolValue(gs, IFI_PRESENT, false);
        }

        if (!_original.empty())
            JSONWalker::addStringValue(gs, IFI_ORIGINAL, _original);

        if (!_versions.empty())
        {
            // add versions array
            JSONWalker::toAdd(gs);
            JSONWalker::addKey(gs, IFI_VERSIONS);
            gs.add('[');
            for (const auto& vi : _versions)
            {
                JSONWalker::toAdd(gs);
                JSONWalker::addValue(gs, vi);
            }
            gs.add(']');
        }

        if (!_elts.empty())
        {
            JSONWalker::toAdd(gs);
            JSONWalker::addKey(gs, IFI_CLICKABLES);
            gs.add('[');
            for (auto e : _elts) e->buildJson(gs);
            gs.add(']');
        }

        if (_mask)
        {
            JSONWalker::toAdd(gs);
            JSONWalker::addKey(gs, IFI_MASK);
            ST::RegionJson::buildJson(gs, *_mask);
        }
        
        gs.add('}');
    }
#endif    

    bool parseJsonClicksArray(const string js)
    {
        // [{"name":"C_BOOKS","show":true,"z:"2,"polys":[{"verts":[]}..] ]

        bool r = true;
        
        JSONWalker jw(js);
        for (jw.beginArray(); r && !jw._error && !jw._end;jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st)  { r = false; break; }

            // expect a list of clickable objects
            if (!isObject)
            {
                LOG1("Bad json clickable ", st);
                continue;
            }

            // expect all objects
            string clickobj;
            if (!jw.atObj(clickobj)) continue;
            
            EltClick* ec = new EltClick;
            
            // the clickable obj
            for (JSONWalker jw(clickobj); r && jw.nextKey(); jw.next())
            {
                const char* st = jw.checkValue(isObject);
                if (!st) { r = false; break; }

                if (!isObject)
                {
                    var v = jw.collectValue(st);
                    if (jw._key == IFI_NAME) ec->_label = v.toString();
                    else if (jw._key == IFI_SHOW) ec->_show = v.isTrue();
                    else if (jw._key == IFI_ZORDER) ec->_z = v.toInt();
                }
                else
                {
                    if (jw._key == IFI_POLYS)
                    {
                        string subjs;
                        r = jw.atList(subjs) && ec->_polys.parseJson(subjs);
                    }
                }
            }

            if (r) _elts.push_back(ec);
            else delete ec;
        }
        return r;
    }
    
    bool parseJson(const string& js)
    {
        // {"name":"fred","width":2000,"height":2526,"clickables:[...],
        // "mask:{region}
        // }
        
        bool r = true;
        for (JSONWalker jw(js); r && jw.nextKey(); jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st) { r = false; break; }

            if (!isObject)
            {
                var v = jw.collectValue(st);
                if (jw._key == IFI_NAME) setName(v.toString());
                else if (jw._key == IFI_WIDTH) _w = v.toInt();
                else if (jw._key == IFI_HEIGHT) _h = v.toInt();
                else if (jw._key == IFI_PRESENT) _present = v.isTrue();
                else if (jw._key == IFI_ORIGINAL) _original = v.toString();
                else if (jw._key == IFI_FILESIZE) _fileSize = v.toInt();
            }
            else
            {
                if (jw._key == IFI_CLICKABLES)
                {
                    string subjs;
                    r = jw.atList(subjs) && parseJsonClicksArray(subjs);
                }
                else if (jw._key == IFI_MASK)
                {
                    string subjs;
                    ST::Region* rg = new ST::Region;
                    r = jw.atObj(subjs) &&
                        ST::RegionJson::parseJson(*rg, subjs);
                    if (r)
                    {
                        //LOG1("immeta mask region ", *rg);
                        setMask(rg);
                    }
                    else
                    {
                        LOG1("failed to parse json region ", subjs);
                        delete rg;
                    }
                }
                else if (jw._key == IFI_VERSIONS)
                {
                    string subjs;
                    r = jw.atList(subjs)
                        && JSONWalker::decodeStringArray(_versions, subjs);

                    //for (const auto& vi : _versions) LOG1("got a version '", vi << "'");
                }
            }
        }
        return r;
    }

#ifdef TOOLS_BUILD    
    friend std::ostream& operator<<(std::ostream& os, const ImageMeta& m)
    {
        GrowString gs;
        m.buildJson(gs);
        gs.add(0);
        return os << gs;
    }
#endif
    
};

struct ImagesMeta
{
    typedef std::string string;
    typedef std::list<ImageMeta*> Metas;

    // meta data for each image
    Metas       _metas;

    ~ImagesMeta() { clear(); }

    ImageMeta* find(const string& name) const
    {
        for (auto m : _metas) if (m->_name == name) return m;
        return 0;
    }

    ImageMeta* findOriginal(ImageMeta* im, float* scale = 0) const
    {
        // optionally return the scale factor to the original
        float sc = 1;
        if (im && !im->isOriginal())
        {
            ImageMeta* i2 = find(im->_original);
            if (i2)
            {
                sc = (float)im->_w/i2->_w;
                im = i2;
            }
        }

        if (scale) *scale = sc;
        return im;
    }

    ImageMeta* findOriginal(const string& name, float* scale = 0) const
    {
        // optionally return the scale factor to the original
        return findOriginal(find(name), scale);
    }

    Point2 findDimensions(const string& name) const
    {
        Point2 sz;
        ImageMeta* m = find(name);
        if (m)
        {
            sz.x = m->_w;
            sz.y = m->_h;
        }
        return sz;
    }

    const ST::Region* findMask(const string& name, float* scale) const
    {
        ImageMeta* m = findOriginal(name, scale);
        if (m)
        {
            return m->_mask; // might be null
        }
        return 0;
    }

    bool hasClickables(const string& name) const
    {
        ImageMeta* m = findOriginal(name);
        return m && m->hasClickables();
    }

    const ImageMeta* findBiggestImage(bool* hasVersions) const
    {
        // find the biggest image by width
        // set `hasVersions` if any image has versions
        static ImageMeta* maxi = 0;
        static bool versions = false;
        if (!maxi)
        {
            for (auto m : _metas)
            {
                if (m->_present)
                {
                    if (!maxi || m->_w > maxi->_w) maxi = m;
                }
                if (m->hasVersions()) versions = true;
            }
            
            if (maxi)
            {
                //LOG3("Max image width ", maxi->_w << " on " << maxi->_name << " versions " << versions);
            }
        }

        *hasVersions = versions;
        return maxi;
    }
    
    const ImageMeta* findBestVersion(const string& name, int target) const
    {
        // target = 0 => best
        
        ImageMeta* best = 0;
        ImageMeta* fallback = 0;

        if (!target)
        {
            // XX set a large value to find the biggest present
            target = 1000000;
        }

        ImageMeta* m = find(name);
        if (m)
        {
            assert(m->isOriginal());

            if (m->_present)
            {
                // if original is within target, it can compete with
                // best varation.
                int side = u_max(m->_w, m->_h);
                if (side > target) fallback = m;
                else best = m;
            }
            
            for (auto& vi : m->_versions)
            {
                ImageMeta* mi = find(vi);
                if (!mi || !mi->_present) continue; // sanity

                int side = u_max(mi->_w, mi->_h);

                if (side > target)
                {
                    // smallest fallback bigger than target
                    if (!fallback || mi->_w < fallback->_w) fallback = mi;
                }
                else
                {
                    // biggest less than or equal target
                    if (!best || mi->_w > best->_w) best = mi;
                }
            }
        }
        else
        {
            LOG1("WARNING: cannot find meta for ", name);
        }

        if (!best) best = fallback;

        // can be zero
        return best; 
    }

    bool parseJson(const string& js)
    {
        return parseJson(js.c_str());
    }

    bool parseJson(const char* js)
    {
        // { "imagemeta":{...}, "imagemeta":{} ... } 
        bool r = true;
        for (JSONWalker jw(js); r && jw.nextKey(); jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st)  { r = false;  break;}
            
            // expect JSON to be a set of "imagemeta" objects
            if (jw._key == IFI_IMAGEMETA && isObject)
            {
                string subjs;
                auto im = new ImageMeta;
                r = jw.atObj(subjs) && im->parseJson(subjs);
                if (r) _metas.push_back(im);
                else delete im;
            }
        }
        return r;
    }

    bool restoreFromJson(const char* js)
    {
        bool res = parseJson(js);

        // some poly regions are not actually clickables.
        // for example text areas. Identify these and ensure they
        // are not seens as clickable.
        if (res)
        {
            for (auto mi : _metas)
            {
                // it's possible that some meta wont have dimensions
                // eg animation
                if (!mi->_w || !mi->_h) continue;

                for (auto ei : mi->_elts)
                {
                    if (ei->_type == ImageMeta::m_elt_click)
                    {
                        auto ec = (ImageMeta::EltClick*)ei;

                        // XX this can be removed later
                        if (equalsIgnoreCase(ec->_label, META_TEXT_AREA_NAME))
                        {
                            // we are actually a text area
                            ec->_active = false; // not clickable
                        }
                        
#ifdef VN1 
                        if (ec->_textarea)
                        {

                            extern void makeRegionFromPolys(ST::Region& r,
                                                            const Polys& polys,
                                                            int w, int h);


                            makeRegionFromPolys(ec->_textRegion,
                                                ec->_polys, mi->_w, mi->_h);

                            // calculate inside region
                            // XX hardcoded margins
                            ec->_textRegion.contractBy(ec->_textRegionInside,
                                                       8, 8);

                            if (!ec->_textRegionInside)
                            {
                                // did not make a region
                                LOG1("WARNING text area did not make a region ", ec->_label);

                                ec->_textarea = false; // downgrade
                                ec->_textRegion.reset();
                            }
                        }

                        if (ec->_textarea)
                        {
                            _hasTextAreas = true;
                            //LOG2("making text area ", ec->_textRegionInside);
                        }
#endif // VN1                        
                    }
                }
            }
        }
        
        return res;
    }

#ifdef TOOLS_BUILD    
    friend std::ostream& operator<<(std::ostream& os, const ImagesMeta& ms)
    {
        GrowString gs;
        gs.add('{');
        for (auto m : ms._metas) m->buildJson(gs);
        gs.add('}');
        gs.add(0);
        return os << gs;
    }
#endif    

    void clear() { ::purge(_metas); }
};
