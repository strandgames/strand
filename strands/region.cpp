//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include <stdio.h>
#include "region.h"

using namespace ST;

#define REGION_DEBUGxx

static RRect zeroRect;

#define _bcopy(_s, _d, _n)  memmove(_d, _s, _n)
#define _min(_a, _b) ((_a) <= (_b) ? (_a) : (_b))
#define _max(_a, _b) ((_a) >= (_b) ? (_a) : (_b))

#define EMPTY_REGION(reg) ((reg)->numRects = 0)
#define REGION_NOT_EMPTY(reg) ((reg)->numRects)

#define ADD_RECT(rgn, nx1, ny1, nx2, ny2)	\
{						\
    RRect *rp;					\
    if (rgn->numRects == rgn->_size) {		\
        int newSize = 2*rgn->_size;		\
	REALLOC_RECTS(rgn, newSize);		\
    }						\
    rp = &rgn->rects[rgn->numRects++];		\
    rp->x1 = nx1;				\
    rp->y1 = ny1;				\
    rp->x2 = nx2;				\
    rp->y2 = ny2;				\
}

static void ALLOC_RECTS(Region* rgn, int newSize)
{
    if (newSize < 1) newSize = 1;
    rgn->_size= newSize;
    rgn->rects = (RRect *)malloc(rgn->_size * sizeof(RRect));
}

static void REALLOC_RECTS(Region* rgn, int newSize)
{
    if (newSize < 1) newSize = 1;
    rgn->_size= newSize;
    rgn->rects = (RRect *)realloc(rgn->rects, rgn->_size * sizeof(RRect));
}

static inline RRect* findBandEnd(RRect* start, RRect* end)
{
    RRect *result;
    for (result = start; result < end && start->y1 == result->y1; result++) ;
    return result;
}

#define FIND_BAND_END(result, start, end) \
    result = findBandEnd(start, end)


void Region::_init(const RRect* r, int size)
{
    if (size > 1) 
    {
	ALLOC_RECTS(this, size);
    }
    else 
    {
	_size = 0;
	rects = &extents;
    }

    if (r && (r->x1 >= r->x2 || r->y1 >= r->y2)) r = NULL;

    if (r) 
    {
	numRects = 1;
	rects[0] = extents = *r;
    }
    else 
    {
	numRects = 0;
	extents = zeroRect;
    }
}


void Region::_copy(Region* dst, const Region* src)
{
    if (dst == src) return;

    dst->_destroy();
    if (src->numRects > 0)
    {
        dst->_init(0, src->numRects);
        dst->numRects = src->numRects;
        memcpy(dst->rects, src->rects, src->numRects * sizeof(RRect));
        dst->extents = src->extents;
    }
}

void Region::reset(RRect* rp)
{
    if (_size > 0) free_rects(rects);
    _size = 0;
    rects = &extents;

    if (rp && (rp->x1 >= rp->x2 || rp->y1 >= rp->y2)) rp = NULL;
    if (rp) {
	numRects = 1;
	extents = *rp;
    } else {
	numRects = 0;
	extents = zeroRect;
    }
}

void Region::translate(int dx, int dy)
{
    RRect *rp, *rpEnd;

    for (rp = rects, rpEnd = &rects[numRects]; rp < rpEnd; rp++) 
    {
	rp->x1 += dx;
	rp->x2 += dx;
	rp->y1 += dy;
	rp->y2 += dy;
    }

    if (_size > 0)
    {
	extents.x1 += dx;
	extents.x2 += dx;
	extents.y1 += dy;
	extents.y2 += dy;
    }
}

#ifdef REGION_DEBUG

int regionsDiffer(Region* reg1, Region* reg2)
{
    return (reg1->numRects != reg2->numRects ||
	    memcmp(&reg1->extents, &reg2->extents, sizeof(RRect)) ||
	    memcmp(reg1->rects, reg2->rects, reg1->numRects * sizeof(RRect)));
}

void Region::print()
{
    RRect *r = &extents;
    int i;

    if (!_validate())
	printf("REGION IS NOT VALID!!\n");
    printf("extents: %d,%d,%d,%d\n", r->x1, r->y1, r->x2, r->y2);
    for (i = 0, r = rects; i < numRects; i++, r++)
        printf("%d,%d,%d,%d\n", r->x1, r->y1, r->x2, r->y2);
}

bool Region::_validate()
{
    int i;
    RRect *rpPrev, *rp;
    
    if (numRects == 0) return true;
    if (numRects == 1 && _size == 0) return true;
    if (numRects > _size) return false;
    for (i = 1, rpPrev = rects, rp = rpPrev+1; i < numRects;
         i++, rpPrev++, rp++)
	if ((rp->y1 >= rp->y2 || rp->x1 >= rp->x2) ||
	    rp->y1 < rpPrev->y1 ||
	    (rp->y1 < rpPrev->y2 &&
	     (rp->y1 != rpPrev->y1 || rp->y2 != rpPrev->y2)) ||
	    (rp->y1 == rpPrev->y1 &&
	     (rp->x1 < rpPrev->x1 ||
	      rp->x1 <= rpPrev->x2)))
	    return false;
    return true;
}

#endif /* DEBUG */

/* Add the band with y extent from bandY1 to bandY2 and x extents defined
   by the x1, x2 fields of the rectangles from band upto (but not including)
   bandEnd. If the new band abuts the last band in the region (passed as
   lastBandInRegion) and the x coordinates correspond, then the new band
   is simply merged with the last band. A pointer to the new last band in
   the region is returned. */

// Region *rgn;			/* region being added to */
//RRect *lastBandInRegion;		/* last band in that region */
//RRect *band, *bandEnd;		/* defines the (x1,x2)s for the new band */
//int bandY1;			/* the top of the new band */
//int bandY2;			/* the bottom of the new band */

static RRect *
addBand(Region* rgn, RRect* lastBandInRegion, RRect* band,
        RRect* bandEnd, int bandY1, int bandY2)
{
    /* The (char *) stuff is because I am not interested in the
       number of rectangles as much as the relative sizes of the
       two bands and so this might avoid a few shifts in the code. */
    int bandSize = (char *)bandEnd - (char *)band;
    RRect *rgnEnd = &rgn->rects[rgn->numRects];
    RRect *rp1, *rp2;
    int lastBandSize = (char *)rgnEnd - (char *)lastBandInRegion;

    /* Check to see if we can merge the bands */
    if (bandSize == lastBandSize &&
	lastBandInRegion->y2 == bandY1) {
	RRect *rp1, *rp2;
	for (rp1 = lastBandInRegion, rp2 = band;
	     rp1 < rgnEnd; rp1++, rp2++)
	    if (rp1->x1 != rp2->x1 ||
		rp1->x2 != rp2->x2)
		goto cannot_merge;
	/* extend the y2 fields to cover both bands */
	for (rp1 = lastBandInRegion; rp1 < rgnEnd; rp1++)
	    rp1->y2 = bandY2;
	return lastBandInRegion;
    }

cannot_merge: ;
    bandSize /= sizeof(RRect);	/* number of RRects in the band */
    /* ensure there is sufficient space */
    if (rgn->numRects + bandSize > rgn->_size) {
	int newSize = rgn->_size;
	do {
	    newSize <<= 1;
	} while (rgn->numRects + bandSize > newSize);
	REALLOC_RECTS(rgn, newSize);
	rgnEnd = &rgn->rects[rgn->numRects];
    }
    /* and add the band to the region */
    for (rp1 = rgnEnd, rp2 = band; rp2 < bandEnd; rp1++, rp2++) {
	rp1->x1 = rp2->x1;
	rp1->x2 = rp2->x2;
	rp1->y1 = bandY1;
	rp1->y2 = bandY2;
    }
    rgn->numRects += bandSize;
    return rgnEnd;
}

/* If the band from prevStart to currentStart abuts the band
   from currentStart to the end of the region and the x coordinates
   in the band coincide, then merge the bands. Return a pointer to the new
   last band */

//Region *rgn;			/* the region to manipulate */
//RRect *prevStart;		/* start of previous band in region */
//RRect *currentStart;	/* current band start */

static RRect *
maybeMergeBands(Region* rgn, RRect* prevStart, RRect* currentStart)
{
    RRect *currentEnd = &rgn->rects[rgn->numRects];
    int prevSize, currentSize;
    RRect *rp1, *rp2;
    int y;

    prevSize = (char *)currentStart - (char *)prevStart;
    currentSize = (char *)currentEnd - (char *)currentStart;
    if (prevSize != currentSize ||
	prevStart->y2 < currentStart->y1) {
	return currentStart;
    }
    for (rp1 = prevStart, rp2 = currentStart;
	 rp1 < currentStart;
	 rp1++, rp2++) {
	if (rp1->x1 != rp2->x1 || rp1->x2 != rp2->x2)
	    return currentStart;
    }
    /* The bands have the same pattern of rectangles and so they
       can be merged */
    y = currentStart->y2;	/* loop invariant */
    for (rp1 = prevStart; rp1 < currentStart; rp1++)
	rp1->y2 = y;
    rgn->numRects -= (currentSize / sizeof(RRect));
    return prevStart;
}

void Region::UnionRegion(Region* newr, const Region* src1, const Region* src2)
{
    RRect *oldNewRects;		/* holds the old value of new->rects */
    RRect *src1Rects;	/* start of current src1 band */
    RRect *src2Rects;	/* ditto src2 */

    /* Trivial cases */
    if (src1 == src2) 
    {
	_copy(newr, src1);
	return;
    }
    if (src1->numRects == 0) 
    {
	_copy(newr, src2);
	return;
    }
    if (src2->numRects == 0) 
    {
	_copy(newr, src1);
	return;
    }
    
    /* We have to keep the old rects array in new around in case
       new is the same region as either src1 or src2. The old array
       is freed after the union is completed. */
    if (newr->_size > 0) {
	oldNewRects = newr->rects;
    } else
	oldNewRects = NULL;
    src1Rects = src1->rects;
    src2Rects = src2->rects;

    /* Make new into an empty region with a reasonable guess
       about the size of the result (but leave the extents alone
       since they are used later). */
    ALLOC_RECTS(newr, src1->numRects + src2->numRects);
    /* Deal with the simple cases when the two source regions do not
       overlap vertically. Note that we could use <= instead of < for
       the test but then we would need to check the join to see if we
       needed to merge the last band of the top region with the first
       band of the bottom region. */
    if (src1->extents.y2 < src2->extents.y1) {
	_bcopy(src1Rects, newr->rects, src1->numRects * sizeof(RRect));
	_bcopy(src2Rects, newr->rects + src1->numRects,
	      src2->numRects * sizeof(RRect));
	newr->numRects = src1->numRects + src2->numRects;
    } else if (src2->extents.y2 < src1->extents.y1) {
	_bcopy(src2Rects, newr->rects, src2->numRects * sizeof(RRect));
	_bcopy(src1Rects, newr->rects + src2->numRects,
	      src1->numRects * sizeof(RRect));
	newr->numRects = src1->numRects + src2->numRects;
    } else {
	/* The regions overlap vertically so we need to merge them */
	RRect *src1End		/* end of the rects array in src1 */
	    = src1Rects + src1->numRects;
	RRect *src2End		/* ditto src2 */
	    = src2Rects + src2->numRects;
	RRect *src1BandEnd;	/* end of current band in src1 */
	RRect *src2BandEnd;	/* ditto src2 */
	RRect *lastBandInNew;	/* points to the start of the last band */
	int topY;	/* top Y coord of the current new band */
	int botY;	/* bottom of current new band */

	lastBandInNew = newr->rects;
	newr->numRects = 0;

	/* find the ends of the 2 current bands */
	FIND_BAND_END(src1BandEnd, src1Rects, src1End);
	FIND_BAND_END(src2BandEnd, src2Rects, src2End);

	/* Set topY to the start of the first non-overlapping
	   band in the source */
	if (src1Rects->y1 < src2Rects->y1)
	    topY = src1Rects->y1;
	else
	    topY = src2Rects->y1;
 	while (src1Rects < src1End && src2Rects < src2End) {
	    /* Handle the next non-overlapping section of the sources */
	    if (src1Rects->y1 < src2Rects->y1) {
		topY = _max(topY, src1Rects->y1);
		botY = _min(src1Rects->y2, src2Rects->y1);
		if (topY < botY)
		    lastBandInNew =
			addBand(newr, lastBandInNew, src1Rects, src1BandEnd,
				topY, botY);
		topY = src2Rects->y1;
	    } else if (src2Rects->y1 < src1Rects->y1) {
		topY = _max(topY, src2Rects->y1);
		botY = _min(src2Rects->y2, src1Rects->y1);
		if (topY < botY)
		    lastBandInNew =
			addBand(newr, lastBandInNew, src2Rects, src2BandEnd,
				topY, botY);
		topY = src1Rects->y1;
	    } else
		topY = src1Rects->y1;
	    /* Advance to (possibly) the next overlapping band */
	    botY = _min(src1Rects->y2, src2Rects->y2);
	    if (topY < botY) {
		RRect *rp1, *rp2;
		int last, current;
		int x1;

		rp1 = src1Rects; rp2 = src2Rects;
		last = lastBandInNew - newr->rects;
		current = newr->numRects;
		x1 = _min(rp1->x1, rp2->x1);
		while (rp1 < src1BandEnd && rp2 < src2BandEnd) {
		    if (rp1->x2 < rp2->x1) {
			ADD_RECT(newr, x1, topY, rp1->x2, botY);
			rp1++;
			if (rp1 < src1BandEnd)
			    x1 = _min(rp1->x1, rp2->x1);
			else
			    x1 = rp2->x1;
		    } else if (rp2->x2 < rp1->x1) {
			ADD_RECT(newr, x1, topY, rp2->x2, botY);
			rp2++;
			if (rp2 < src2BandEnd)
			    x1 = _min(rp1->x1, rp2->x1);
			else
			    x1 = rp1->x1;
		    } else {
			if (rp1->x2 < rp2->x2)
			    rp1++;
			else if (rp2->x2 < rp1->x2)
			    rp2++;
			else {
			    ADD_RECT(newr, x1, topY, rp1->x2, botY);
			    rp1++, rp2++;
			    if (rp1 < src1BandEnd)
				if (rp2 < src2BandEnd)
				    x1 = _min(rp1->x1, rp2->x1);
				else
				    x1 = rp1->x1;
			    else
				if (rp2 < src2BandEnd)
				    x1 = rp2->x1;
			}
		    }
		}
		if (rp1 < src1BandEnd) {
		    ADD_RECT(newr, x1, topY, rp1->x2, botY);
		    rp1++;
		    while (rp1 < src1BandEnd) {
			ADD_RECT(newr, rp1->x1, topY, rp1->x2, botY);
			rp1++;
		    }
		}
		if (rp2 < src2BandEnd) {
		    ADD_RECT(newr, x1, topY, rp2->x2, botY);
		    rp2++;
		    while (rp2 < src2BandEnd) {
			ADD_RECT(newr, rp2->x1, topY, rp2->x2, botY);
			rp2++;
		    }
		}
		lastBandInNew =
		    maybeMergeBands(newr, newr->rects + last,
				    newr->rects + current);
	    }
	    /* Advance to the next band */
	    topY = botY;
	    if (botY == src1Rects->y2) {
		src1Rects = src1BandEnd;
		FIND_BAND_END(src1BandEnd, src1Rects, src1End);
	    }
	    if (botY == src2Rects->y2) {
		src2Rects = src2BandEnd;
		FIND_BAND_END(src2BandEnd, src2Rects, src2End);
	    }
	}
	/* Add in any unused parts of the source regions */
	if (src1Rects < src1End) {
	    /* maybe merge the next band of src1 with new */
	    addBand(newr, lastBandInNew, src1Rects, src1BandEnd,
		    _max(topY, src1Rects->y1), src1Rects->y2);
	    /* and just copy the rest since it should be
	       impossible to merge them */
	    if (src1End - src1BandEnd + newr->numRects > newr->_size)
		REALLOC_RECTS(newr, (int)(src1End - src1BandEnd
					 + newr->numRects));
	    _bcopy(src1BandEnd, &newr->rects[newr->numRects],
		  (char *)src1End - (char *)src1BandEnd);
	    newr->numRects += src1End - src1BandEnd;
	}
	if (src2Rects < src2End) {
	    /* maybe merge the next band of src2 with newr */
	    addBand(newr, lastBandInNew, src2Rects, src2BandEnd,
		    _max(topY, src2Rects->y1), src2Rects->y2);
	    /* and just copy the rest since it should be
	       impossible to merge them */
	    if (src2End - src2BandEnd + newr->numRects > newr->_size)
		REALLOC_RECTS(newr, (int)(src2End - src2BandEnd
					 + newr->numRects));
	    _bcopy(src2BandEnd, &newr->rects[newr->numRects],
		  (char *)src2End - (char *)src2BandEnd);
	    newr->numRects += src2End - src2BandEnd;
	}
    }
    if (oldNewRects)
	free_rects(oldNewRects);
    newr->extents.x1 = _min(src1->extents.x1, src2->extents.x1);
    newr->extents.y1 = _min(src1->extents.y1, src2->extents.y1);
    newr->extents.x2 = _max(src1->extents.x2, src2->extents.x2);
    newr->extents.y2 = _max(src1->extents.y2, src2->extents.y2);
    if (newr->numRects <= 1) {
	free_rects(newr->rects);
	newr->_size = 0;
	newr->rects = &newr->extents;
    } else
	REALLOC_RECTS(newr, newr->numRects);
}

void Region::expand(int left, int right, int top, int bot)
{
    // expand the extent limits by the given amounts
    // This just works on the extent limits and takes no account
    // of concave parts.
    
    if (numRects == 0) return;

    RRect* rp = rects;    
    RRect* rpEnd = rp + numRects;

    int ytop = rects[0].y1;
    int ybot = rpEnd[-1].y2;

    extents.y1 = ytop - top;
    extents.y2 = ybot + bot;
    
    for (; rp < rpEnd; ++rp)
    {
        if (rp->y1 == ytop) rp->y1 = extents.y1;
        if (rp->y2 == ybot) rp->y2 = extents.y2;

        // widen each band

        // at band start now
        rp->x1 -= left;
        if (rp->x1 < extents.x1) extents.x1 = rp->x1;

        // skip to band end
        int thisBandY1;
	for (thisBandY1 = rp->y1, rp++;
	     rp < rpEnd && rp->y1 == thisBandY1;
	     rp++)
            ;

        int r = rp[-1].x2 + right;
        rp[-1].x2 = r;
        if (r > extents.x2) extents.x2 = r;
    }
}

void Region::expandMinMax(int minx, int maxx)
{
    RRect* rp = rects;    
    RRect* rpEnd = rp + numRects;
    
    for (; rp < rpEnd; ++rp)
    {
        // widen each band

        // at band start now
        if (rp->x1 > minx)
        {
            rp->x1 = minx;
            if (minx < extents.x1) extents.x1 = minx;
        }

        // skip to band end
        int thisBandY1;
	for (thisBandY1 = rp->y1, rp++;
	     rp < rpEnd && rp->y1 == thisBandY1;
	     rp++)
            ;

        --rp;  // end of last band

        if (rp->x2 < maxx)
        {
            rp->x2 = maxx;
            if (maxx > extents.x2) extents.x2 = maxx;
        }
    }
}

void Region::fillHoles()
{
    RRect* rp = rects;    
    RRect* rpEnd = rp + numRects;
    
    for (; rp < rpEnd; ++rp)
    {
        // at band start now
        
        // skip to band end
        int thisBandY1;
        RRect* re;
	for (thisBandY1 = rp->y1, re = rp + 1;
	     re < rpEnd && re->y1 == thisBandY1;
	     re++)
            ;

        --re;  // end of last band

        int d = re - rp;  // number to lose (ie span - 1)
        
        if (d > 0)
        {
            rp->x2 = re->x2;

            memmove(rp + 1, re + 1,
                    (numRects - (re - rects) - 1)*sizeof(RRect));
            numRects -= d;
            rpEnd -= d;
        }

        // rp points to single box span
    }
}


static void setExtents(Region* rgn)
{
    RRect *rp, *rpEnd;
    int thisBandY1;
    int minx, maxx;

    if (rgn->numRects == 0) {
	rgn->extents = zeroRect;
	return;
    }
    rp = rgn->rects;
    rgn->extents.y1 = rp->y1;
    rgn->extents.y2 = rp[rgn->numRects-1].y2;
    for (minx = rp->x1, maxx = rp->x2, rpEnd = rp + rgn->numRects;
	 rp < rpEnd; ) {
	if (rp->x1 < minx) minx = rp->x1;
	/* Skip to the end of this band */
	for (thisBandY1 = rp->y1, rp++;
	     rp < rpEnd && rp->y1 == thisBandY1;
	     rp++) ;
	if (rp[-1].x2 > maxx) maxx = rp[-1].x2;
    }
    rgn->extents.x1 = minx;
    rgn->extents.x2 = maxx;
    if (rgn->numRects <= 1) {
	free_rects(rgn->rects);
	rgn->_size = 0;
	rgn->rects = &rgn->extents;
    } else
	REALLOC_RECTS(rgn, rgn->numRects);
}

void Region::IntersectRegion(Region* newr,
                             const Region* src1, const Region* src2)
{
    RRect *oldNewRects;		/* holds the old value of new->rects */
    RRect *src1Rects;	/* start of current src1 band */
    RRect *src2Rects;	/* ditto src2 */

    /* Trivial cases */
    if (src1 == src2) {
	_copy(newr, src1);
	return;
    }
    if (src1->numRects == 0 || src2->numRects == 0 ||
	!RECTS_INTERSECT(&src1->extents, &src2->extents)) {
	newr->extents = zeroRect;
	newr->numRects = 0;
	return;
    }
    if (src1->numRects == 1 && src2->numRects == 1) {
	newr->extents.x1 = _max(src1->extents.x1, src2->extents.x1);
	newr->extents.y1 = _max(src1->extents.y1, src2->extents.y1);
	newr->extents.x2 = _min(src1->extents.x2, src2->extents.x2);
	newr->extents.y2 = _min(src1->extents.y2, src2->extents.y2);
	newr->numRects = 1;
	newr->rects[0] = newr->extents;
	return;
    }
    /* We have to keep the old rects array in newr around in case
       newr is the same region as either src1 or src2. The old array
       is freed after the intersect is completed. */
    if (newr->_size > 0) {
	oldNewRects = newr->rects;
    } else
	oldNewRects = NULL;
    src1Rects = src1->rects;
    src2Rects = src2->rects;
    {
	/* The regions may intersect, so we have more work to do */
	RRect *src1End		/* end of the rects array in src1 */
	    = src1Rects + src1->numRects;
	RRect *src2End		/* ditto src2 */
	    = src2Rects + src2->numRects;
	RRect *src1BandEnd;	/* end of current band in src1 */
	RRect *src2BandEnd;	/* ditto src2 */
	RRect *lastBandInNew;	/* points to the start of the last band */
	int topY;	/* top Y coord of the current new band */
	int botY;	/* bottom of current new band */

	/* Make newr into an empty region with a reasonable guess
	   about the size of the result (but leave the extents alone
	   since they are used later). */
	ALLOC_RECTS(newr, src1->numRects + src2->numRects);
	newr->numRects = 0;
	lastBandInNew = newr->rects;

	/* find the ends of the 2 current bands */
	FIND_BAND_END(src1BandEnd, src1Rects, src1End);
	FIND_BAND_END(src2BandEnd, src2Rects, src2End);

 	while (src1Rects < src1End && src2Rects < src2End) {
	    /* Find the lower limit of the next non-overlapping band */
	    if (src1Rects->y1 < src2Rects->y1)
		topY = src2Rects->y1;
	    else
		topY = src1Rects->y1;
	    /* Find the lower limit of the next intersecting band */
	    botY = _min(src1Rects->y2, src2Rects->y2);
	    if (topY < botY) {
		RRect *rp1, *rp2;
		int last, current;
		rp1 = src1Rects; rp2 = src2Rects;
		last = lastBandInNew - newr->rects;
		current = newr->numRects;
		while (rp1 < src1BandEnd && rp2 < src2BandEnd) {
		    if (rp1->x2 <= rp2->x1)
			rp1++;
		    else if (rp2->x2 <= rp1->x1)
			rp2++;
		    else {
			ADD_RECT(newr, _max(rp1->x1, rp2->x1), topY,
				 _min(rp1->x2, rp2->x2), botY);
			if (rp1->x2 < rp2->x2)
			    rp1++;
			else if (rp2->x2 < rp1->x2)
			    rp2++;
			else
			    rp1++, rp2++;
		    }
		}
		lastBandInNew =
		    maybeMergeBands(newr, newr->rects + last,
				    newr->rects + current);
	    }
	    /* Advance to the next band */
	    topY = botY;
	    if (botY == src1Rects->y2) {
		src1Rects = src1BandEnd;
		FIND_BAND_END(src1BandEnd, src1Rects, src1End);
	    }
	    if (botY == src2Rects->y2) {
		src2Rects = src2BandEnd;
		FIND_BAND_END(src2BandEnd, src2Rects, src2End);
	    }
	}
    }
    if (oldNewRects)
	free_rects(oldNewRects);
    setExtents(newr);
}

bool Region::SubtractRegion(Region* newr,
                            const Region* src1, const Region* src2)
{
    RRect *oldNewRects;		/* holds the old value of newr->rects */
    RRect *src1Rects;	/* start of current src1 band */
    RRect *src2Rects;	/* ditto src2 */

    // we calculate `newr' = `src1' - `src2'
    // return whether we intersected.

    /* Trivial case */
    if (src1 == src2) 
    {
	newr->extents = zeroRect;
	newr->numRects = 0;
        return true;
    }
    
    /* If the extents of the two source regions do not intersect
       then we can trivially set the result to the first region */
    if (src1->numRects == 0 || src2->numRects == 0 ||
	!RECTS_INTERSECT(&src1->extents, &src2->extents)) 
    {
	_copy(newr, src1);
	return false; // didnt intersect
    }

    /* We have to keep the old rects array in newr around in case
       newr is the same region as either src1 or src2. The old array
       is freed after the subtract is completed. */
    if (newr->_size > 0) 
	oldNewRects = newr->rects;
    else
	oldNewRects = NULL;

    src1Rects = src1->rects;
    src2Rects = src2->rects;

    {
	/* The regions may intersect, so we have more work to do */
	RRect *src1End		/* end of the rects array in src1 */
	    = src1Rects + src1->numRects;
	RRect *src2End		/* ditto src2 */
	    = src2Rects + src2->numRects;
	RRect *src1BandEnd;	/* end of current band in src1 */
	RRect *src2BandEnd;	/* ditto src2 */
	RRect *lastBandInNew;	/* points to the start of the last band */
	int topY;	/* top Y coord of the current new band */
	int botY;	/* bottom of current new band */

	/* Make newr into an empty region with a reasonable guess
	   about the size of the result (but leave the extents alone
	   since they are used later). */
	ALLOC_RECTS(newr, src1->numRects + src2->numRects);
	newr->numRects = 0;
	lastBandInNew = newr->rects;

	/* find the ends of the 2 current bands */
	FIND_BAND_END(src1BandEnd, src1Rects, src1End);
	FIND_BAND_END(src2BandEnd, src2Rects, src2End);

	/* Set topY to the start of the first non-overlapping
	   band in the source */
	if (src1Rects->y1 < src2Rects->y1)
	    topY = src1Rects->y1;
	else
	    topY = src2Rects->y1;
 	while (src1Rects < src1End && src2Rects < src2End) {
	    /* Find the limits of the next non-overlapping band and
	       if it is in src1 then copy it into the newr region */
	    if (src1Rects->y1 < src2Rects->y1) {
		topY = _max(topY, src1Rects->y1);
		botY = _min(src1Rects->y2, src2Rects->y1);
		if (topY < botY)
		    lastBandInNew =
			addBand(newr, lastBandInNew, src1Rects, src1BandEnd,
				topY, botY);
		topY = src2Rects->y1;
	    } else
		topY = src1Rects->y1;
	    /* Find the lower limit of the next intersecting band */
	    botY = _min(src1Rects->y2, src2Rects->y2);
	    if (topY < botY) {
		RRect *rp1, *rp2;
		int last, current;
		int x1;

		rp1 = src1Rects; rp2 = src2Rects;
		last = lastBandInNew - newr->rects;
		current = newr->numRects;
		/* x1 is the x coordinate of the next part of the src1 band
		   to check */
		x1 = rp1->x1;
		while (rp1 < src1BandEnd && rp2 < src2BandEnd) {
		    if (rp2->x2 <= x1)
			/* discard any src2 rectangles which do not overlap */
			rp2++;
		    else if (rp2->x1 >= rp1->x2) {
			/* if the left edge of rp2 is after the right edge
			   of rp1 then they do not intersect, so add rp1 */
			ADD_RECT(newr, x1, topY, rp1->x2, botY);
			rp1++;
			if (rp1 < src1BandEnd) x1 = rp1->x1;
		    } else {
			/* the two rectangles intersect */
			if (rp2->x1 <= x1)
			    /* rp2 clips off part or all of left of rp1 */
			    if (rp2->x2 >= rp1->x2) {
				/* it was all of rp1 */
				rp1++;
				if (rp1 < src1BandEnd) x1 = rp1->x1;
			    } else {
				/* only part was clipped, so move x1 up */
				x1 = rp2->x2;
				rp2++;
			    }
			else {
			    /* we get to add part of the left of rp1 */
			    ADD_RECT(newr, x1, topY, rp2->x1, botY);
			    /* check to see if there is anything left
			       of rp1 */
			    if (rp2->x2 >= rp1->x2) {
				/* all gone */
				rp1++;
				if (rp1 < src1BandEnd) x1 = rp1->x1;
			    } else {
				x1 = rp2->x2;
				rp2++;
			    }
			}
		    }
		}
		/* add any rectangles that are left from src1BandEnd */
		if (rp1 < src1BandEnd) {
		    ADD_RECT(newr, x1, topY, rp1->x2, botY);
		    rp1++;
		    while (rp1 < src1BandEnd) {
			ADD_RECT(newr, rp1->x1, topY, rp1->x2, botY);
			rp1++;
		    }
		}
		lastBandInNew =
		    maybeMergeBands(newr, newr->rects + last,
				    newr->rects + current);
	    }
	    /* Advance to the next band */
	    topY = botY;
	    if (botY == src1Rects->y2) {
		src1Rects = src1BandEnd;
		FIND_BAND_END(src1BandEnd, src1Rects, src1End);
	    }
	    if (botY == src2Rects->y2) {
		src2Rects = src2BandEnd;
		FIND_BAND_END(src2BandEnd, src2Rects, src2End);
	    }
	}
	/* lastly add in the rest of src1 (if any) */
	if (src1Rects < src1End) {
	    /* maybe merge the next band of src1 with newr */
	    addBand(newr, lastBandInNew, src1Rects, src1BandEnd,
		    _max(src1Rects->y1, topY), src1Rects->y2);
	    /* and just copy the rest since it should be
	       impossible to merge them */
	    if (src1End - src1BandEnd + newr->numRects > newr->_size)
		REALLOC_RECTS(newr, (int)(src1End - src1BandEnd
					 + newr->numRects));
	    _bcopy(src1BandEnd, &newr->rects[newr->numRects],
		  (char *)src1End - (char *)src1BandEnd);
	    newr->numRects += src1End - src1BandEnd;
	}
    }
    if (oldNewRects)
	free_rects(oldNewRects);
    setExtents(newr);
    return true;
}


bool Region::pointInside(int x, int y, RRect** bound) const
{
    // return true if (x,y) is inside the region
    // if true then bound points to the rect that contains the point.

    // if false, then bound gives is the next possible place in the region:
    // if bound.y1 > y then the region is further in y.
    // otherwise if bound.y2 < y, then y is in the region, but x isnt.
    // in which case if bound.x1 > x, then this is the next x in region.

    RRect *rp, *end;
    int y1;

    *bound = 0;
    if (numRects == 0) return false;
    if (x < extents.x1 || x >= extents.x2 ||
	y < extents.y1 || y >= extents.y2)
	return false;

    /* Skip the bands which are before the point */
    rp = rects;
    end = &rp[numRects];
    while (rp < end && y >= rp->y2) 
    {
	/* skip this band */
	y1 = rp->y1; rp++;
	while (rp < end && rp->y1 == y1) rp++;
    }
    if (rp == end) return false; 

    /* If we get this far then either the next band contains
       the point or the point is not in the region */

    *bound = rp;
    if (y < rp->y1) return false; // bound y1 contains the next y in region
    
    y1 = rp->y1;
    while (rp < end && y1 == rp->y1) 
    {
	if (x >= rp->x2) 
        {
	    rp++;
	    continue;
	}
        *bound = rp;
	if (x < rp->x1) return false;
	/* Got it */
	//*bound = rp;
	return true;
    }
    return false;
}

RectInResult Region::rectInside(const RRect* rect) const
{
    int y, endY, y1;
    RRect *rp, *end;
    RectInResult resultY = RECT_OUT;
    bool resultXValid = false;
    RectInResult resultX = RECT_OUT;
    int leftX, rightX;

    if (numRects == 0 || !RECTS_INTERSECT(&extents, rect))
	return RECT_OUT;

    rp = rects; end = &rp[numRects];
    y = rect->y1; endY = rect->y2;
    leftX = rect->x1; rightX = rect->x2;
    while (rp < end && y < endY) 
    {
	if ((resultY == RECT_PART && resultX != RECT_OUT) ||
	    (resultX == RECT_PART && resultY != RECT_OUT))
	    return RECT_PART;
	y1 = rp->y1;
	/* If this band is past the end of the rectangle then we
	   can just stop now. */
	if (y1 >= endY) break;
	/* If the lower edge of this band is greater than y then
	   the band must overlap the rectangle. */
	if (rp->y2 > y) 
        {
	    RectInResult thisResultX;
	    int x;
	    /* If the upper edge of the band is greater than y then
	       the stretch y upto rp->y1 is not covered */
	    if (y1 > y) 
		resultY = RECT_PART;
            else 
            {
		resultY = RECT_IN;
		/* Subtract the extent of this band from the top of
		   the rectangle */
		y = _min(endY, rp->y2);
	    }
	    /* Now check the individual rectangles in the band to see
	       if they cover the rectangle horizontally. */
	    thisResultX = RECT_OUT;
	    x = leftX;
	    while (x < rightX && rp < end && rp->y1 == y1) 
            {
		if (thisResultX == RECT_PART) break;
		if (rp->x1 >= rightX)
		    break;
		if (rp->x2 > x) 
                {
		    if (rp->x1 > x)
			thisResultX = RECT_PART;
		    else 
                    {
			thisResultX = RECT_IN;
			x = _min(rightX, rp->x2);
		    }
		}
		rp++;
	    }
	    if (x < rightX && thisResultX == RECT_IN)
		thisResultX = RECT_PART;
	    if (resultXValid) 
            {
		if (resultX != thisResultX)
		    resultX = RECT_PART;
	    }
            else 
            {
		resultX = thisResultX;
		resultXValid = true;
	    }
	}
	/* Skip the rest of the band */
	while (rp < end && rp->y1 == y1) rp++;
    }
    if (y < endY && resultY == RECT_IN)
	resultY = RECT_PART;
    if (resultX == RECT_IN && resultY == RECT_IN)
	return RECT_IN;
    if (resultX == RECT_OUT || resultY == RECT_OUT)
	return RECT_OUT;
    return RECT_PART;
}

void Region::InvertRegion(Region* newr, const Region* src, const RRect* bounds)
{
    Region temp(*bounds);
    SubtractRegion(newr, &temp, src);
}

#if 1
void Region::recalcExtent()
{
    setExtents(this);
}

#else
void Region::recalcExtent()
{
    if (numRects > 0)
    {
        extents = rects[0];
        for (int i = 1; i < numRects; ++i)
        {
            RRect& ri = rects[i];
            if (ri.x1 < extents.x1) extents.x1 = ri.x1;
            if (ri.x2 > extents.x2) extents.x2 = ri.x2;

            if (ri.y1 < extents.y1) extents.y1 = ri.y1;
            if (ri.y2 > extents.y2) extents.y2 = ri.y2;
        }
    }
    else extents = zeroRect;
}
#endif

