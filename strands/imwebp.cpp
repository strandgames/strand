//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include <assert.h>
#include "imwebp.h"
#include "src/webp/decode.h"
#include "logged.h"
#include "ti.h"

struct WEBPDecodeState: public ImageDecodeState
{
    WEBPDecodeState(const unsigned char* data, int sz)
        : ImageDecodeState(data, sz)
    {
        _remaining = _dsize;
        _chunk = 1024*64;
    }

    WebPDecoderConfig _config;
    WebPIDecoder* _idec = 0;
    
    int _remaining;
    int _chunk;
    int _pos = 0;

    ~WEBPDecodeState() override
    {
        if (_started)
        {
            // whether failed or not
            if (_idec) WebPIDelete(_idec);
            WebPFreeDecBuffer(&_config.output);
            delete _pix; _pix = 0;
        }
    }

    bool _decodeStep() override
    {
        // return true when done or failed
        assert(!_failed);

        //LOG1("webp decode step ", _pos);

        if (!_started)
        {
            _started = true;
            WebPInitDecoderConfig(&_config);
            if (WebPGetFeatures(_data, _dsize, &_config.input) == VP8_STATUS_OK)
            {
                _config.options.no_fancy_upsampling = 1;
                _config.options.bypass_filtering = 1;
                //_config.options.use_threads = 1;

                _w = _config.input.width;
                _h = _config.input.height;

                // regardless of input, we will have 4 chan output
                _chans = 4;

                int stride = _w*4; // XXX assume alpha
                int sz = _h*stride;

                // allocate our own memory buffer for the image
                _pix = new uint8_t[sz];
                _config.output.colorspace = MODE_RGBA;
                _config.output.u.RGBA.rgba = _pix;
                _config.output.u.RGBA.stride = stride;
                _config.output.u.RGBA.size = sz;
                _config.output.is_external_memory = 1;

                _idec = WebPIDecode(0, 0, &_config);
                if (!_idec) _failed = true;

            }
            else
            {
                _failed = true;
            }
        }

        if (!_failed)
        {
            assert(_idec);
            
            int r = _remaining;
            if (r > _chunk) r = _chunk;
            
            assert(r > 0);

            VP8StatusCode status = WebPIAppend(_idec, _data + _pos, r);
            if (status == VP8_STATUS_OK || status == VP8_STATUS_SUSPENDED)
            {
                _remaining -= r;
                _pos += r;

                /*
                int w,h,y;
                if (WebPIDecGetRGB(_idec, &y, &w, &h, 0))
                {
                    LOG1("webp partial at ", y << " (" << w << ',' << h << ')');
                }
                */
            }
            else
            {
                _failed = true;
            }
        }

        // either failed or we have no remaining, ie done
        return _failed || !_remaining;
    }

#if 0
    // we can override this, but
    // this is actually slower than decoding by parts
    bool decodeAll() override
    {
        //TIMER("decodeAll webp");
        // decode all in one
        _pix = WebPDecodeRGBA(_data, _dsize, &_w, &_h);
        _chans = 4;
        return _pix != 0;
    }
#endif
    
};

#if 0
unsigned char* decodeWebp(const unsigned char* data,
                          int dsize,
                          int* w,
                          int* h,
                          int* chans,
                          int desired_channels,
                          ProgressCB* cbf,
                          void* cbc)
{
    uint8_t* pix = 0;

    if (!data || !dsize) return 0;
    
    WebPDecoderConfig config;
    WebPInitDecoderConfig(&config);

    if (WebPGetFeatures(data, dsize, &config.input) == VP8_STATUS_OK)
    {
        config.options.no_fancy_upsampling = 1;

        config.output.colorspace = MODE_RGBA;

        int w1 = config.input.width;
        int h1 = config.input.height;

        int stride = w1*4; // XXX assume alpha
        int sz = h1*stride;

        // allocate our own memory buffer for the image
        pix = new uint8_t[sz];
        config.output.u.RGBA.rgba = pix;
        config.output.u.RGBA.stride = stride;
        config.output.u.RGBA.size = sz;
        config.output.is_external_memory = 1;

        *w = w1;
        *h = h1;

        bool ok = false;

        // decode all in one
        //ok = WebPDecode(data, dsize, &config) == VP8_STATUS_OK;

        // decode incrementally so we can call the callback as we go
        WebPIDecoder* const idec = WebPIDecode(0, 0, &config);
        if (idec)
        {
            int remaining = dsize;

            // 16K chunks
            // brings time per chunk ~10ms
            int chunk = 1024*16; 
            const unsigned char* dp = data;
            for (;;)
            {
                //TIMER("webp chunk");

                int r = remaining;
                if (r > chunk) r = chunk;

                //LOG1("webp decode chunk ", r);
                
                VP8StatusCode status = WebPIAppend(idec, dp, r);
                if (status == VP8_STATUS_OK || status == VP8_STATUS_SUSPENDED)
                {
                    remaining -= r;
                    dp += r;
                }
                else break;
                
                if (!remaining)
                {
                    // done
                    ok = true;
                    break;
                }
                
                if (cbf) (*cbf)(cbc);
            }
            WebPIDelete(idec);
        }

        if (!ok)
        {
            delete pix; pix = 0;
        }
    }
    
    WebPFreeDecBuffer(&config.output);
    *chans = 4;

    return pix;
}

#endif // 0

#if 0
unsigned char* DecodeWebp(const unsigned char* data,
                          int dsize,
                          int* w,
                          int* h,
                          int* chans,
                          int desired_channels)
{
    uint8_t* pix;

    // decode all in one

    pix = WebPDecodeRGBA((const uint8_t*)data, (size_t)dsize, w, h);
    if (pix)
    {
        *chans = 4;
    }
    return pix;
}

void DecodeWebpFree(unsigned char* data)
{
    if (data) WebPFree(data);
}
#endif

bool WebpGetInfo(const unsigned char* data,
              int dsize,
              int* w,
              int* h,
              int* chans)
{
    bool ok = false;
    
    if (!data || !dsize) return ok;
    
    WebPDecoderConfig config;
    WebPInitDecoderConfig(&config);

    ok = WebPGetFeatures(data, dsize, &config.input) == VP8_STATUS_OK;

    if (ok)
    {
        *w = config.input.width;
        *h = config.input.height;
        *chans = config.input.has_alpha ? 4 : 3;
    }

    WebPFreeDecBuffer(&config.output);
   
    return ok;
}
       
ImageDecodeState* createWEBPDecodeState(const unsigned char* data, int sz)
{
    return new WEBPDecodeState(data, sz);
}
