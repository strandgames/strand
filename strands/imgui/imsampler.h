//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

struct SamplerHolder
{
    bool               _valid = false;
    sg_sampler         _samp;

    sg_sampler getSampler()
    {
        if (!_valid)
        {
            _valid = true;
            sg_sampler_desc sampd = {};
            sampd.min_filter = SG_FILTER_LINEAR;
            sampd.mag_filter = SG_FILTER_LINEAR;
            sampd.wrap_u = SG_WRAP_CLAMP_TO_EDGE;
            sampd.wrap_v = SG_WRAP_CLAMP_TO_EDGE;
            //sampd.mipmap_filter = SG_FILTER_LINEAR;
            sampd.mipmap_filter = SG_FILTER_NEAREST;
            _samp = sg_make_sampler(&sampd);

        }
        return _samp;
    }

    void _purge()
    {
        if (_valid)
        {
            sg_destroy_sampler(_samp);
            _valid = false;
        }
    }

    ~SamplerHolder()
    {
        _purge();
    }
};
