//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <list>
#include "utils.h"
#include "imanim.h"
#include "textflow.h"
#include "imrender.h"
#include "sokstate.h"

#define TAG_IMTEXT  "ImText, "

#define GET_REF(_s)  ((Par*)((_s)->_ref))

// forward
struct ImText;
extern ImTexLoader* texLoader;
extern SOKState state;

struct ImTextFont: TFont
{
    float getHeight() const override
    {
        const ImFont& f = font();
        float h = f.FontSize;
        return h;
    }
    
    float getAscent() const override
    {
        const ImFont& f = font();
        float h = f.Ascent;
        return h;
    }
    
    float getDescent() const override
    {
        const ImFont& f = font();
        float h = f.Descent;
        return h;
    }
    
    int compare(const TFont* f) const override { return this == f; }

    void setHeight(float f) override {}


    float getStringWidth(const std::string& s) const override 
    {
        const ImFont& f = font();
        float w = 0;
        int sz = s.size();
        if (sz)
        {
            const char* t = s.c_str();
            const char* te = t + sz;
            ImVec2 sz = f.CalcTextSizeA(f.FontSize, FLT_MAX, 0, t, te, 0);
            w = sz.x;
        }
        //LOG1("measuring '", s << "' as (" << w);
        return w;
    }

    void render(const std::string& s,
                const StrandCommon::Rect& box, const TextProperties& tp) const override;

    bool clipped(const StrandCommon::Rect& box) const override;
    void renderBackground(const ST::Region&) const override;

    const ImFont& font() const
    {
        // default to current
        return _font ? *_font : *ImGui::GetFont();
    }

    ImText*             _host = 0;
    ImFont*             _font = 0;
};



struct ImText: public ImRender
{
    struct Par
    {
        enum partype
        {
            par_void = 0,
            par_text,
            par_img,
            par_anim,
        };

        Par(ImText* host, partype t) : _host(host), _type(t) {}
        virtual ~Par() {}

        ImText*         _host;
        partype         _type;

        // is fresh until the next input
        bool            _fresh = true;
        bool            _scroll = false;

        virtual int size() const { return 0; }

        bool isObj() const
        { return (_type == Par::par_img || _type == Par::par_anim); }
        bool isImg() const { return _type == par_img; }
        bool isAnim() const { return _type == par_anim; }
        bool isText() const { return _type == par_text; }
    };

    struct ParText: public Par
    {
        string          _text;
        bool            _hilite = false;

        ParText(ImText* host, const char* s, bool hilite)
            : Par(host, par_text), _text(s), _hilite(hilite) {}
        
        int size() const override { return _text.size(); }
    };

    struct ParObj: public Par
    {
        // ParObj is a base class for all objects like images and animation
        string   _name;
        
        string   _tag;
        bool     _small = false; // make the picture small
        bool     _large = false; // fullscreen
        BoxAlign _alignH, _alignV;
        bool     _textWrap = false;  // wrap text inside mask
        int      _sizeOverride = 0; // game suggested percentage
        bool     _intext = false;
        
        // when in dialog with this image
        bool     _talking = false;

        bool    tagged() const { return !_tag.empty(); }

        ParObj(ImText* host, partype t, const string& name, const string& tag)
            : Par(host, t), _name(name), _tag(tag) {}
    };

    struct ParImg: public ParObj
    {
        // if we have a duplicate further down the list
        ParImg*   _dup = 0;
        
        ParImg(ImText* host, const string& name, const string& tag)
            : ParObj(host, par_img, name, tag) {}

        void preload()
        {
            // will initiate loading if not in pool
            texLoader->getTex(_name, _host->_renderCount);
        }
    };

    struct ParAnim: public ParObj
    {
        AnimState _a;
        StrandCommon::Rect      _renderBox; // only valid while render
        
        ParAnim(ImText* host, const string& name)
            : ParObj(host, par_anim, name, string()) { _a.clear(); }
    };

    typedef std::list<Par*> Pars;

    Pars        _pars;
    bool        _textLayoutNeeded = false;
    int         _maxText = 1024*8;
    int         _size = 0;
    int         _maxImg = 3;
    uint64      _renderCount;
    //uint64      _imgUseCount = 0; // inc for every image render
    int         _margins = 0;  // each side
    bool        _fontChanged = false;

    // for renderImageView
    ParObj*     _viewImage = 0;
    bool        _viewImageReady = false;

    bool        _vnmode = false;
    Roster*     _roster = 0;

    // set this prior to any layout
    // this is the text area without window padding
    Boxf        _viewBox;

    TextFlowBuilder  _tbuild;
    ImTextFont _font;
    ImTextFont _fontBold;
    ImTextFont _fontItalic;

    ImageMeta::EltClick* _currentImageClick;

    const ImagesMeta* _imMeta = 0;

    void setImagesMeta(const ImagesMeta* m)
    {
        _imMeta = m;
    }
    
    void setRoster(Roster* r) { _roster = r; }

    void fontSizeChanged()
    {
        _tbuild.fontSizeChanged();
    }

    void setFont(ImFont* f)
    {
        if (_font._font != f)
        {
            _font._font = f;
            _tbuild.setFont(&_font);
            _fontChanged = true;
        }
    }

    void setBoldFont(ImFont* f)
    {
        if (_fontBold._font != f)
        {
            _fontBold._font = f;
            _tbuild.setFontBold(&_fontBold);
            _fontChanged = true;
        }
    }

    void setItalicFont(ImFont* f)
    {
        if (_fontItalic._font != f)
        {
            _fontItalic._font = f;
            _tbuild.setFontItalic(&_fontItalic);
            _fontChanged = true;
        }
    }

    ImText()
    {
        _font._host = this;
        _fontBold._host = this;
        _fontItalic._host = this;

        _tbuild.setFont(&_font);

        using namespace std::placeholders;
        _tbuild._renderStartSignal = std::bind(&ImText::renderStart, this);
        _tbuild._renderEndSignal = std::bind(&ImText::renderEnd, this);
        _tbuild._renderObj = std::bind(&ImText::renderObj, this, _1);
    }
    
    ~ImText()
    {
        clear();
    }

    void clear()
    {
        purge(_pars);
        _size = 0;
    }

    void replace(const char* s)
    {
        clear();
        add(s);
        textChanged();
    }

    void add(const char* s, bool hilite = false)
    {
        if (s && *s)
        {
            //LOG1(TAG_IMTEXT "adding '", AddCodes(s) << "'");
            // whole string will be a single par regardless

            bool done = false;

            Par* pl = 0;

            // find last text par for possible text combine
            for (Pars::reverse_iterator it = _pars.rbegin();
                 it != _pars.rend(); ++it)
            {
                Par* pi = *it;
                if (pi->isText())
                {
                    pl = pi;
                    break;
                }
#if 0                
                else if (pi->isObj())
                {
                    // skip over objects that allow text wrap and
                    // are inline to text.
                    auto po = (ParObj*)pi;
                    if (!po->_intext && !po->_textWrap)
                    {
                        //LOG1(TAG_IMTEXT "cannot combine across ", po->_name);
                        break;
                    }
                }
#endif                
                else break;
            }
            
            if (pl)
            {
                assert (pl->isText());
                ParText* plt = (ParText*)pl;
                if (plt->_fresh && plt->_hilite == hilite)
                {
                    // consider combining with last text
                    //LOG1(TAG_IMTEXT "combining ", strlen(s));

                    int s1 = plt->_text.size();
                    concatStrings(plt->_text, s);
                    
                    // correct overall text size
                    _size += (plt->_text.size() - s1);
                    
                    textChanged();
                    done = true;
                }
                else
                {
                    //LOG1(TAG_IMTEXT "combining rejected ", strlen(s));
                }
            }
            
            if (!done)
            {
                //LOG1(TAG_IMTEXT "new par of ", strlen(s) << " because " << _pars.size());
                Par* p = new ParText(this, s, hilite);
                _add(p);
            }
        }
    }

    void setLarge(ParObj* p)
    {
        for (Par* pi : _pars)
        {
            if (pi->isObj())
            {
                ParObj* po = (ParObj*)pi;
                po->_large = (po == p);
                if (po == p) setViewImage(p);
            }
        }
    }

    ParObj* getLarge()
    {
        for (Par* pi : _pars)
        {
            if (pi->isObj())
            {
                ParObj* po = (ParObj*)pi;
                if (po->_large) return po;
            }
        }
        return 0;
    }

    void addImage(const PictureInfo& pnfo)
    {
        //LOG1("adding image ", pnfo);

        if (pnfo._preload)
        {
            LOG2("preloading image ", pnfo._name);
            ParImg pi(this, pnfo._name, pnfo._tag);

            // initiate loading
            pi.preload();
        }
        else
        {
            ParImg* p = new ParImg(this, pnfo._name, pnfo._tag);

            p->_textWrap = pnfo._textWrap; 

            // initiate loading
            p->preload();

            // we can give images a size suggestion
            p->_sizeOverride = pnfo._sizeOverride;
            p->_intext = pnfo._intext;

            // mark earlier duplicates of this image
            for (Par* pi : _pars)
            {
                if (pi->isImg())
                {
                    // find the last duplicate if any
                    // or last image with same tag
                    ParImg* p2 = (ParImg*)pi;
                    if (!p2->_dup &&
                        ((p->tagged() && p2->_tag == pnfo._tag)
                         || p2->_name == pnfo._name))
                    {
                        p2->_dup = p;
                        break;
                    }
                }
            }
            _add(p);

            // indicate this object is large, and clear others
            if (pnfo._large) setLarge(p);
        }
    }

    bool onLastPage() const
    {
        return _tbuild._textFlow.onLastPage();
    }

    void disableToCurrentPage()
    {
        // mark all pages seen up to current
        _tbuild._textFlow.disableToCurrentPage();
    }

    void seenAllText(bool fromClick)
    {
        // all text no longer fresh

        bool scroll = fromClick;
        if (onLastPage()) scroll = false;
        
        for (auto pi: _pars)
        {
            if (!scroll)
            {
                pi->_fresh = false;
                pi->_scroll = false;
            }
            else
            {
                if (pi->_fresh) pi->_scroll = true;
            }
        }
        
        // when we've seen all text, we're sending a new command
        // the text will be reformatted and it will not be fresh
        _tbuild._textFlow.resetCurrentPage();  // set to 1
    }

    bool renderEnd()
    {
        if (_dragging) return true;
        
        bool m = ImGui::IsMouseReleased(0) && ImGui::IsItemHovered();
        
        // handle clickable
        if (m && _currentImageClick)
        {
            // handle the image clickable
            if (linkClickedLabel(_currentImageClick->_label))
            {
                if (!_currentImageClick->_show) setViewImage(0);
                return true;
            }
        }

        if (_viewImage && (m || ImGui::IsKeyPressed(ImGuiKey_Escape)))
        {
            // click will cancel image view
            setViewImage(0);
            return true;
        }
                
        if (!_viewImage && ImGui::IsItemHovered())
        {
            ImVec2 p = ImGui::GetMousePos();
            int x = p.x - _viewBasePos.x;
            int y = p.y - _viewBasePos.y;
                
            //LOG1("hover pos ", x << "," << y);
            TextSection* s = _tbuild.updateHover(x, y);

            if (s && s->_props._canhover)
            {
                if (m)
                {
                    if (s->_boxPos)
                    {
                        // clicked on an object
                        Par* p = GET_REF(s);
                        assert(p);
                        if (p->isObj()) setViewImage((ParObj*)p);
                    }
                    else
                    {
                        //LOG1("clicked on ", s->_text << " with " << s->_altText);
                        //setClickCommand(s->_altText);
                        linkClickedLabel(s->_altText);
                    }
                }
            }
        }

        return true;
    }

    bool renderStartItem(int w, int h)
    {
        ImVec2 pos = ImGui::GetCursorScreenPos();
        ImVec2 size(w, h);
        ImVec2 bot(pos.x + size.x, pos.y + size.y);
        
        _viewBasePos.x = pos.x;
        _viewBasePos.y = pos.y;
            
        ImRect bb(pos, bot);
        ImGui::ItemSize(size); 
        bool v = ImGui::ItemAdd(bb, 0);

        /*
        {
            // XX debug hack to draw the viewport
            ImGuiWindow* window = ImGui::GetCurrentWindow();
            ImVec4 tint_col = ImVec4(1,1,1,1);
            ImU32 tint = ImGui::ColorConvertFloat4ToU32(tint_col);
            window->DrawList->AddRectFilled(pos, bot, tint);
        }
        */
        
        return v;
    }
    
    bool renderStart()
    {
        StrandCommon::Rect r = _tbuild._textFlow.extent();

        // use full width for item regardless
        // but keep the height of the text.
        float vw = _viewBox.width();
        if (vw > r._w) r._w = vw;
        
        // this could be from any picture 
        _currentImageClick = 0;

        bool ok = renderStartItem(r._w, r._h);
        return ok;
    }

#if 0    
    bool haveRenderPaths(const string& name)
    {
        // do we have any clickable paths?
        
        assert(_imMeta);
        ImageMeta* m = _imMeta->find(name);
        if (m)
        {
            for (auto ei : m->_elts)
            {
                if (ei->_type == ImageMeta::m_elt_click) return true;
            }
        }
        return false;
    }
#endif    

    bool aspectChanged(const string& name, int tw, int th, const StrandCommon::Rect& box)
    {
        // has the aspect ratio changed enough to relayout?
        assert(th);

        bool res = false;

        float ta = ((float)tw)/th;
        float ba = ((float)box._w)/box._h; // not empty

        float da = fabs(ta - ba);
        if (da > 0.1f)
        {
            LOG2("different aspect ratio for ", name << " by " << da);

            // request re-layout
            _textLayoutNeeded = true;
            res = true;
        }
        return res;
    }

#ifdef USESPINE
    void layoutParAnim(Par* p)
    {
        ParAnim* pa = (ParAnim*)p;

        // now guess the aspect ratio.

        // most pictures are landscape 4:3
        int w = 4000;
        int h = 3000;

        // NB: add way to get dimensions from image meta.

        if (pa->_a._dim)
        {
            //LOG1("layoutParAnim used dimensions ", pa->_a._dim);
            w = pa->_a._dim.x;
            h = pa->_a._dim.y;
        }
            
        layoutParObj(w, h, pa);
    }

    static void render_anim(const ImDrawList* dl, const ImDrawCmd* cmd)
    {
        assert(cmd);
        ParAnim* pa = (ParAnim*)cmd->UserCallbackData;
        assert(pa);

        /*
        // first set the viewport rectangle to render in, same as
        // the ImGui draw command's clip rect
        int cx = (int) cmd->ClipRect.x;
        int cy = (int) cmd->ClipRect.y;
        int cw = (int) (cmd->ClipRect.z - cmd->ClipRect.x);
        int ch = (int) (cmd->ClipRect.w - cmd->ClipRect.y);
        //LOG1("render anim clip ", cx << " " << cy << " " << cw << " " << ch);
        */

        int cx = pa->_renderBox._x;
        int cy = pa->_renderBox._y;
        int cw = pa->_renderBox._w;
        int ch = pa->_renderBox._h;

        float dpi = app_dpi_scale;

        cx *= dpi;
        cy *= dpi;
        cw *= dpi;
        ch *= dpi;

        SSpineCtx* ctx = SSpineCtx::get();
        int ni = ctx->render(pa->_a);

        if (ni > 0)
        {
            sg_apply_scissor_rect(cx, cy, cw, ch, true);
            sg_apply_viewport(cx, cy, cw, ch, true);

            //LOG1(TAG_IMTEXT "view x:", cx << " y:" << cy << " w:" << cw << " h:" << ch << " skw:" << pa->_a._skel_w << " skh:" << pa->_a._skel_h << " dpi: " << dpi);

            // attach texture
            assert(pa->_a._tex && pa->_a._tex->valid());

            simgui_image_t si =
                simgui_image_from_imtextureid(pa->_a._tex->_tid);
            simgui_image_desc_t sid = simgui_query_image_desc(si);
            
            state.anim.bind.fs.images[SLOT_spine_tex] = sid.image;
            state.anim.bind.fs.samplers[SLOT_spine_samp] = sid.sampler;
            sg_apply_pipeline(state.anim.pip);
            sg_apply_bindings(&state.anim.bind);

            float sx = 2.0f/pa->_a._skel_w;
            float sy = 2.0f/pa->_a._skel_h;

            spine_vs_params_t vs_params;
            vs_params.offset[0] = -pa->_a._skel_x * sx - 1.0f;
            vs_params.offset[1] = -pa->_a._skel_y * sy - 1.0f;
            
            vs_params.scale[0] = sx;
            vs_params.scale[1] = sy;

            sg_range sgr1 = SG_RANGE(vs_params);
            sg_apply_uniforms(SG_SHADERSTAGE_VS, SLOT_spine_vs_params, &sgr1);
            sg_draw(0, ni, 1);
        }
    }

    void renderAnim(ParAnim* pa)
    {
        ImGuiWindow* window = ImGui::GetCurrentWindow();
        ImTex* btex = pa->_a._texBackimage;

        // background texture to animation?
        if (btex && btex->valid())
        {
            // actual inside of border image region
            const StrandCommon::Rect& box = pa->_renderBox;
        
            ImVec2 img_pos(box._x, box._y);
            ImVec2 img_bot(box._x + box._w, box._y + box._h);
            ImU32 tint = IM_COL32_WHITE;

            ImVec2 uv0(0.0f, 0.0f); // Top-left
            ImVec2 uv1(1.0f, 1.0f); // Lower-right


            window->DrawList->AddImage(btex->_tid,
                                       img_pos,
                                       img_bot,
                                       uv0, uv1,
                                       tint);
        }
        
        window->DrawList->AddCallback(render_anim, pa);
    }

    void renderObjAnim(TextSection* ts, ParAnim* pa)
    {
        StrandCommon::Rect box = ts->extent();
        if (!box) return;

        //LOG1("rendering anim ", pa->_name << " at " << box);

        bool hilite = ts->_props._hilite;
        const TextFlowInfo& ti = _tbuild._textFlow._info;

        box._x += _viewBasePos.x;
        box._y += _viewBasePos.y;

        // clipped?
        if (!getClipRect().intersects(box))
        {
            //LOG1("Anim clipped ", pa->_name);
            return;
        }

        int border = ti._objBorderSize;
        Colour bcol = hilite ? ti._objBorderColHi : ti._objBorderCol;
        renderBorder(border, box, bcol);

        if (pa->_a.loaded(_renderCount)) // update LRU
        {
            if (pa->_a._active)
            {
                if (pa->_a._dim)
                {
                    int tw = pa->_a._dim.x;
                    int th = pa->_a._dim.y;

                    // layout box includes border within which, we place the image
                    box._x += border;
                    box._y += border;
                    box._w -= border*2;
                    box._h -= border*2;

                    // XX aspect not maintained in vnmode anyway.
                    if (_vnmode || !aspectChanged(pa->_name, tw, th, box))
                    {
                        SSpineCtx* ctx = SSpineCtx::get();
                        bool changed = ctx->update(pa->_a);

                        if (changed)
                        {
                            //int w = pa->_a._skel_w;
                            //int h = pa->_a._skel_h;

                            //LOG1("render anim ", pa->_name);

                            pa->_renderBox = box;
                            renderAnim(pa);
                        }
                    }
                }
            }
        }
    }

    ParAnim* bump(AnimInfo* ai)
    {
        // find animation and bump to active
        ParAnim* p = 0;

        // see if we have this animation already
        Pars::iterator it = _pars.begin();
        while (it != _pars.end())
        {
            Par* pi = *it;
            if (pi->_type == Par::par_anim)
            {
                ParAnim* pa = (ParAnim*)pi;
                if (!p && pa->_a.name() == ai->_name)
                {
                    p = pa;

                    // remove from list and continue to deactivate
                    // all others, then add to end later.
                    it = _pars.erase(it);
                    continue;
                }
                else
                {
                    // deactivate all others
                    pa->_a._active = false; 
                }
            }
            ++it;
        }

        if (p)
        {
            // put back on end and activate
            _pars.push_back(p);
            p->_a._active = true;
        }

        return p;
    }

    void animOp(ParAnim* p)
    {
        assert(p);
        assert(p->_a.loaded(_renderCount));

        SSpineCtx* ctx = SSpineCtx::get();
        if (!ctx->animOp(p->_a)) // marks as done
        {
            LOG1(TAG_IMTEXT "animation op failed ", p->_a.name());
        }
    }

    void setAnimation(AnimInfo* ai)
    {
        // consume ai
        // look for existing animation and bump to last
        
        ParAnim* p = bump(ai);
        
        if (ai->_op)
        {
            // ops operate immediately
            if (p)
            {
                // overwrite current info but keep same assets
                p->_a.mergeInfo(ai); 
                animOp(p);
            }
            else
            {
                LOG1(TAG_IMTEXT "animation not found for op, ", ai->_name);
            }
        }
        else
        {
            if (!p)
            {
                p = new ParAnim(this, ai->_name);

                // bump will have disabled all others
                p->_a._active = true;
                _add(p);
            }

            if (ai->_large) setLarge(p);

            // either load now or queue
            p->_a.queueInfo(ai);

            // kick off the loading (if not present)
            p->_a.preload(_renderCount);
        }
    }

    void renderViewAnimSecond(ParAnim* pa)
    {
        const TextFlowInfo& ti = _tbuild._textFlow._info;
            
        float wmax = _viewBox.width();
        float hmax = _viewBox.height();

        int hpad = ti._objPadBox._h + ti._objPadBox._y;
        int wpad = ti._objPadBox._w * 2;

        hmax -= hpad;
        wmax -= wpad;

        if (hmax <= 0 || wmax <= 0) return; // emergency

        if (pa->_a.loaded(_renderCount))
        {
            if (pa->_a._active)
            {
                if (pa->_a._dim)
                {
                    int tw = pa->_a._dim.x;
                    int th = pa->_a._dim.y;

                    SSpineCtx* ctx = SSpineCtx::get();
                    if (ctx->update(pa->_a))
                    {

                        float scw = ((float)wmax)/tw;
                        float sch = ((float)hmax)/th;
                        float sc = u_min(scw, sch);

                        int sw = sc*tw;
                        int sh = sc*th;

                        float padw = (_viewBox.width() - sw)/2;
                        if (padw < 0) padw = 0;  // should not happen

                        //LOG1("rendering anim into ", tw << "," << th);
                        //LOG1("rendering anim scaled into ", sw << "," << sh << " pad=" << padw << " within " << wmax << " of " << cr.x);
                    
                        StrandCommon::Rect box(padw, ti._objPadBox._y, sw,  sh);
                        box._y += _viewBasePos.y;

                        /*
                        // if we want a full-size border
                        int border = ti._objBorderSize;
                        Colour bcol = ti._objBorderCol;
                        StrandCommon::Rect bbox = box;
                        bbox.expandBy(border, border);
                        renderBorder(border, bbox, bcol);
                        */
                    
                        pa->_renderBox = box;
                        renderAnim(pa);
                    }
                }
            }
        }
    }
#endif  // SPINE
        
    void renderObjImg(TextSection* ts, ParImg* pi)
    {
        StrandCommon::Rect box = ts->extent(); // only one in layout
        if (!box)
        {
            //LOG1("renderObj ", pi->_name << " didn't fit " << box);
            return;
        }

        //LOG1("rendering obj ", po->_name << " at " << box);

        ImTex* tex = 0;
        ImTexLoader::Rec* r = texLoader->getLoadedRec(pi->_name, _renderCount);
        if (r) tex = &r->_tex;
        if (tex)
        {
            //r->_lruUseCount = ++_imgUseCount;
            
            if (r->_justLoaded)
            {
                // first time loaded
                r->_justLoaded = false; // one off

                // XX in vmmode, we dont preserve the aspect ratio
                if (!_vnmode)
                {
                    // are we the correct aspect ratio?
                    int tw = tex->_w;
                    int th = tex->_h;

                    // prevent draw if aspect changed
                    if (aspectChanged(pi->_name, tw, th, box)) tex = 0;
                }
            }
        }

        if (tex)
        {
            ImVec2 uv0(0.0f, 0.0f); // Top-left
            ImVec2 uv1(1.0f, 1.0f); // Lower-right

            Colour tint = Colour(Colour::white);

            const TextFlowInfo& ti = _tbuild._textFlow._info;
            float overlap = _tbuild._textFlow._textOverlap;

            if (overlap > 0)
            {
                float vy = _tbuild._textFlow._viewBox[PY1];
                int by = (box.top() + box.bottom())/2;
                float dy = by - vy;
                if (dy < 0) dy = 0;
                
                float ov1 = overlap - dy;
                if (ov1 > 0)
                {
                    float af = ov1/overlap;
                    af = 1 - af*af;
                    //LOG1("fade obj img ", af << " overlap " << ov1);
                    tint = tint.withAlpha(af);
                }
            }
            
            box._x += _viewBasePos.x;
            box._y += _viewBasePos.y;

            bool hilite = ts->_props._hilite;

            // no borders on masked images
            int border = ti._objBorderSize;
            if (ts->_boxMask) border = 0;

            assert(_imMeta);
            ImageMeta* m = _imMeta->findOriginal(pi->_name);
            if (_dragging) m = 0; // stop click during drag

            auto hit = renderImage(m, tex, ti, hilite, box,
                                   uv0, uv1,
                                   border,
                                   tint);
            
            if (hit) _currentImageClick = hit;
        }
    }
        
    void renderObj(TextSection* ts)
    {
        Par* p = GET_REF(ts);
        assert(p);
        assert(p->isObj());

        if (p->isImg())
        {
            renderObjImg(ts, (ParImg*)p);
        }
#ifdef USESPINE        
        else if (p->isAnim())
        {
            renderObjAnim(ts, (ParAnim*)p);
        }
#endif         
    }

    void layoutParObj(int w, int h, ParObj* pi)
    {
        assert(w > 0 && h > 0);
        
        const TextFlowInfo& ti = _tbuild._textFlow._info;

        // we use the line height to optimize the picture height
        int th = _tbuild.nominalLineHeight();
        assert(th > 0);

        const RRect& bounds = _tbuild._textFlow._region.extent();

        int wmax = bounds.width();
        int hmax = _viewBox.height(); 
        int borders = ti._objBorderSize*2;

        const ST::Region* maskp = 0;

        // conform text around any image mask?
        bool useMask = pi->_textWrap;
        float maskscale = 1;
        
        if (useMask)
        {
            // also get the scale, which is the relative size of the original
            maskp = _imMeta->findMask(pi->_name, &maskscale);

            // dont have borders on masked objects
            if (maskp) borders = 0;
        }

        int sizepc = ti._pictureSizePercent;

        // if we have a size override, use it
        if (pi->_sizeOverride)
        {
            sizepc = pi->_sizeOverride;

            if (sizepc > 100) sizepc = 100;
            else if (sizepc < 10) sizepc = 10;
        }
            
        wmax = (sizepc * wmax)/100;

        // tagged pictures are allowed to be full height
        if (!pi->tagged())
        {
            hmax = (sizepc * hmax)/100;
        }

        // pictures can be marked small
        if (pi->_small) wmax /= 2;
        
        // leave space for padding on right and borders
        int hpad = ti._objPadBox._w + borders;
        wmax -= hpad;
        if (wmax < 0) return;

        // and for space above and below
        int vpad = ti._objPadBox._y + ti._objPadBox._h + borders;
        hmax -= vpad;
        if (hmax < 0) return;

        // scaled width and height of image not including border & padding
        int sw = w;
        int sh = h;
        float sc = 1;

        //LOG1("layout obj ", w << " " << hmax << ", " << h << " " << wmax);

        if (sh > hmax)
        {
            sc = ((float)hmax)/sh;
            sw = ROUND(sc * w);
            sh = hmax;
        }

        if (sw > wmax)
        {
            sc *= ((float)wmax)/sw;
            sh = ROUND(sc * h); 
            sw = wmax;
        }

        if (sh < hmax && sw < wmax)
        {
            // we're too small!
            float sch = ((float)hmax)/sh;
            float scw = ((float)wmax)/sw;
            sc = u_min(sch, scw);

            //LOG1("increasing image scale by ", sc);
            sw = ROUND(sc * w);
            sh = ROUND(sc * h); 
        }

        // now wish to adjust scale to make the picture an exact number
        // of text lines high, including padding and border
        // round to closest number
        int lines = ROUND(((float)(sh + vpad))/th);
        float sc1 = ((float)(lines*th - vpad))/sh;

        //LOG1("adjusting ", pi->_name << " line height by " << sc1);

        // recalculate scaled size with new overall new scale from original
        // NB: must round down otherwise might be 1 pixel into next line.
        sh = ROUNDDOWN(h*sc*sc1);
        sw = ROUNDDOWN(w*sc*sc1);

        if (sh > hmax)
        {
            // oh dear. We rounded up to the next line and now we're too high
            // need to be one line less
            if (--lines)
            {
                sc1 = ((float)(lines*th - vpad))/sh;
                sh = ROUNDDOWN(h*sc*sc1);
                sw = ROUNDDOWN(w*sc*sc1);
            }
        }

        // if this happens, there is no space to fit, so give up
        if (sh > hmax) return;

        // final scale causes the scaled height to be short due to
        // rounding. tweak the width to accommodate.
        for (;;)
        {
            float dh = h - (((float)sh)/sw)*w;
            if (dh <= 0) break;

            //LOG1("tweak ", pi->_name << " short by " << dh);
            
            // scale means height is too short
            if (!--sw) break; // emergency!
        }


        // the height wont be exactly a number of lines because we've
        // had to round down the scaled height to an integer.
        // we'll get something line 7.9 lines
        //LOG1("box height in lines ", (double)sh/th);

        if (sw > 0 && sh > 0)
        {
            sh += borders;
            sw += borders;
            
            //LOG1("layout par obj ", pi->_name << ": " << sw << "," << sh << " ah:" << pi->_alignH << " av:" << pi->_alignV);
            _tbuild.addObject(StrandCommon::Rect(0,0,sw,sh),
                              pi, pi->_alignH, pi->_alignV,
                              sc*sc1*maskscale,
                              !pi->_fresh,
                              maskp);
        }
    }

    void layoutParImg(Par* p)
    {
        // layout an image in the space
        ParImg* pi = (ParImg*)p;

        // now guess the aspect ratio.

        // most pictures are landscape 4:3
        int w = 4000;
        int h = 3000;

        // if we are for dialog, guess square
        if (pi->tagged())
        {
            w = 2000;
            h = 2000;
        }

        // dont just check for images here, demand load it
        ImTex* tex = texLoader->getTex(pi->_name, _renderCount);

        // if we have the actual texture, use the actual values.
        if (tex)
        {
            w = tex->_w;
            h = tex->_h;
        }
        else
        {
            // do we have meta?
            assert(_imMeta);
            Point2 sz = _imMeta->findDimensions(pi->_name);

            if (sz)
            {
                w = sz.x;
                h = sz.y;
                //LOG1("meta recovered size of ", pi->_name << " as " << sz);
            }
        }

        //LOG1("layout par obj ", pi->_name << " as " << w << "x" << h);
        layoutParObj(w, h, pi);
    }

    void layoutParText(Par* pi)
    {
        ParText* pt = (ParText*)pi;
        TextProperties tp;
        tp._hilite = pt->_hilite;
        tp._disabled = !pt->_fresh;
        tp._scroll = pi->_scroll;
        
        //LOG1("layout par text '", pt->_text << '\'');
        _tbuild.addMarkdown(pt->_text, &tp, pi);
    }

    void layoutParsFlow()
    {
        // draw all the text in order
        // draw only the last image of any dup set
        // tagged images will be in a dup group
        // in vnmode only draw intext pictures

        for (auto pi: _pars)
        {
            switch (pi->_type)
            {
            case Par::par_text:
                layoutParText(pi);
                break;
            case Par::par_img:
                {
                    ParImg* pm = (ParImg*)pi;
                    if (!pm->_dup)
                    {
                        if (!_vnmode || pm->_intext)
                            layoutParImg(pi);
                    }
                }
                break;
            case Par::par_anim:
#ifdef USESPINE
                {
                    ParAnim* pa = (ParAnim*)pi;
                    if (!_vnmode || pa->_intext) layoutParAnim(pi);
                }
#endif                
                break;
            }
        }
    }

    void layoutParsDialog()
    {
        // start at the first talking channel picture, but draw the last in the dup set
        // draw all the text afterwards and all the non-channel pictures and non-talking
        
        bool started = false;
        
        for (auto pi: _pars)
        {
            switch (pi->_type)
            {
            case Par::par_text:
                if (started) layoutParText(pi);
                break;
            case Par::par_img:
                {
                    ParImg* pim = (ParImg*)pi;
                    bool tagged = pim->tagged();
                    
                    if (!started)
                    {
                        if (tagged && pim->_talking)
                        {
                            started = true;
                        
                            // move to the last picture of the tag set
                            // this is the first picture we draw.
                            while (pim->_dup) pim = pim->_dup;
                            layoutParImg(pim);
                        }
                    }
                    else
                    {
                        // already drawn the last tagged picture
                        // draw any non-tagged picture
                        // but only if it's the last of a dup set
                        if ((!tagged || !pim->_talking) && !pim->_dup)
                        {
                            // any other pictures in dialog are on right
                            pim->_alignH = boxalign_right;
                            pim->_small = true; // and smaller
                            layoutParImg(pim);
                        }
                        
                    }
                }
                break;
            }
        }
    }

    Person* findPerson(const string& name) const
    {
        if (_roster)
        {
            for (Person& pi : *_roster)
                if (pi._name == name) return &pi;
        }
        return 0;
    }

    void trimFinishedTalking()
    {
        for (Pars::iterator it = _pars.begin(); it != _pars.end();)
        {
            bool inc = true;
            
            if ((*it)->_type == Par::par_img)
            {
                ParImg* pim = (ParImg*)(*it);
                bool tagged = pim->tagged();

                if (tagged)
                {
                    // if we were talking and the person is no longer
                    // in the roster, we've finished talking.
                    // remove all their tags
                
                    Person* pr = findPerson(pim->_tag);
                    if (!pr && pim->_talking)
                    {
                        it = _pars.erase(it);
                        //LOG1("Finished talking to ", pim->_tag);
                        delete pim;
                        inc = false;
                    }
                }
            }

            if (inc) ++it;
        }
    }
    
    bool layoutObjAlignment()
    {
        bool anyTalking = false;
        for (auto pi: _pars)
        {
            if (pi->isObj())
            {
                ParObj* pim = (ParObj*)pi;
                bool tagged = pim->tagged();

                bool talking = false;

                if (tagged)
                {
                    Person* pr = findPerson(pim->_tag);

                    // no dialog in vnmode
                    if (pr && !_vnmode)
                    {
                        // this person has both an image and is in roster
                        // we are talking.
                        talking = true;

                        // remember we had this picture in a dialog
                        pim->_talking = true;

                        //LOG1("talking to ", pim->_tag);
                    }
                }

                // clear any small indicator
                pim->_small = false;
                
                if (talking)
                {
                    anyTalking = true;
                    
                    // put dialogue images on top left
                    // NB: will need to put on right with multiple characters
                    pim->_alignH = boxalign_left;
                    pim->_alignV = boxalign_top;
                }
                else
                {
                    // regular images aligned left 
                    pim->_alignH = boxalign_left;
                    pim->_alignV = boxalign_none;                    
                }
            }
        }
        return anyTalking;
    }

    void setViewBox(const Boxf& b)
    {
        _viewBox = b;
    }

    void paginate(const Boxf& page)
    {
        _tbuild._textFlow.paginate(_viewBox.height(), page.height());
    }

    bool paginateNeeded()
    {
        return _tbuild._textFlow.paginate(_viewBox.height(), 0);
    }

    void layoutParsPrepare()
    {
        _tbuild.clear();
        _tbuild._textFlow._vnmode = _vnmode;

        Colour tc(PROPI(P_TEXT_COL, 0));
        if (!tc)
            tc = StyleColour(ImGuiCol_Text);

        Colour tchi(PROPI(P_TEXT_COL_HI, 0));
        if (!tchi)
            tchi = StyleColour(ImGuiCol_HeaderActive);
        
        _tbuild.setColour(tc, tchi);

        Colour tdc = StyleColour(ImGuiCol_TextDisabled);
        Colour tdchi = StyleColour(ImGuiCol_Header);
        _tbuild.setDisabledColour(tdc, tdchi);

        Colour bc = StyleColour(ImGuiCol_Button).brighter(1.5f);
        Colour bch = StyleColour(ImGuiCol_ButtonHovered).brighter(1.5f);
        _tbuild.setLinkColor(bc, bch);

        TextFlowInfo& ti = _tbuild._textFlow._info;
        ti._objBorderSize = PROPI(P_BOXBORDER_SIZE, P_BOXBORDER_SIZE_DEF);
        ti._objBorderCol = StyleColour(ImGuiCol_TextDisabled);
        //ti._objBorderColHi = StyleColour(ImGuiCol_Text);

        ti._objBorderColHi = bch;

        _tbuild._propsLink._underline = true;
        _tbuild._propsLink._canhover = true;

        ti._squeezeWords = PROPI(P_COMPACTWORDS, P_COMPACTWORDS_DEF);
        ti._squeezeLines = PROPI(P_COMPACTLINES, P_COMPACTLINES_DEF);

        int padtop = PROPI(P_BOXPADTOP, P_BOXPADTOP_DEF);
        int padright = PROPI(P_BOXPADRIGHT, P_BOXPADRIGHT_DEF);
        int padbot = padtop; // for now

        // scale up for large fonts, but not down
        float rpadScale = fontSize/30.0f;
        padright *= rpadScale;

        ti._objPadBox = StrandCommon::Rect(0,padtop,padright,padbot);
        ti._enableFullBlanks = PROPB(P_ENABLE_FULLBLANKS, false);
        ti._pictureSizePercent = PROPI(P_PICTURE_SIZE, P_PICTURE_SIZE_DEF);
    }

    void layoutParsNormal()
    {
        // non-vn
        // ASSUME already called `layoutParsPrepare`

        int ch = _viewBox.height();
        int cw = _viewBox.width() - _margins*2;
        
        if (cw > 0 && ch > 0)
        {
            //LOG1("renderText, performing layout in ", cr.x << "," << cr.y);
            // only reset once we have space to layout
            _textLayoutNeeded = false;

            int h = 1000 * _tbuild.nominalLineHeight();

            // the region will be relative to the cursor position
            // there is no background region
            _tbuild.setRegion(ST::Region(RRect(_margins, 0, cw, h)));

            // remove images when we're finished talking
            trimFinishedTalking();

            // and figure out if we are in a dialogue
            bool inDialog = layoutObjAlignment();

            if (inDialog)
            {
                layoutParsDialog();
                _tbuild.performLayout();

                int underflowmin = 10000;

                int cc = 0;
                for (;;)
                {
                    StrandCommon::Rect ex = _tbuild.extent();

                    // does the end of the text overflow the bottom
                    // of the view?
                    int dy = ex.bottom() - ch;

                    //LOG1("dialog layout ", cc << " " << ex << " dy:" << dy);
                    if (dy <= 0) break; // no, fits ok

                    TextSection* ts = _tbuild.findFirstObject();
                    if (!ts)
                    {
                        //LOG1("No picture for dialog ", 0);
                        break;
                    }

                    StrandCommon::Rect picbox = ts->extent();
                    if (!picbox)
                    {
                        //LOG1("Dialog picture did not layout ", picbox);
                        break; // did not layout
                    }
                        
                    const TextFlowInfo& ti = _tbuild._textFlow._info;
                    //int th = _tbuild.nominalLineHeight();
                        
                    int pictop = picbox._y;

                    // adjust for top padding
                    pictop -= ti._objPadBox._y; 

                    // top of image to bottom of page
                    dy = ex.bottom() - pictop;

                    // how much does this exceed visible space?
                    dy -= ch;

                    // if the picture and the text below exceeds
                    // visible space, then the picture will be off the
                    // top.
                    // we add a displacement to the picture position
                    // and re-layout

                    if (dy <= 0)
                    {
                        //LOG1("pic box, underflow ", dy);

                        // underflow
                        if (!dy) break;  // perfect

                        int under = -dy;
                        bool stop = false;

                        // emergency break after 5 tries
                        if (++cc >= 5)
                        {
                            LOG2("layoutpars emergency break ", under);
                            stop = true;
                        }
                            
                        if (!stop && under < underflowmin)
                        {
                            // better!
                            underflowmin = under;
                        }
                        else
                        {
                            // accept this underflow as best
                            // add fake footer space to make the
                            // overall size place the picture at the exact
                            // place.
                            _tbuild._textFlow._footpadding = under;
                            //LOG1("pic box, underflow footer ", under);
                            break;
                        }
                    }

                    // display down by overflow and re-layout
                    ts->_boxPos._y += dy;

                    //LOG1("pic box ", cc << " " << picbox << " overflow:" << dy << " pictop:" << pictop << " picposy:" << ts->_boxPos._y);
                        
                    // restore original fit region
                    _tbuild.setRegion(ST::Region(RRect(_margins, 0, cw, h)));
                    _tbuild.reperformLayout();
                }
            }
            else
            {
                layoutParsFlow();
                _tbuild.performLayout();
                // NB: pagination done after layout
            }

            //LOG1("renderText, performed layout, ", _tbuild.extent());
        }
    }

    bool renderFirst()
    {
        // return true if layout performed
        bool v = false;
        
#ifdef USESPINE        
        SSpineCtx* ssc = SSpineCtx::get();
        ssc->_renderCount = _renderCount;
#endif

        if (_fontChanged)
        {
            _textLayoutNeeded = true;
            _fontChanged = false;
        }

        _viewImageReady = viewImageReady();

        // no image or not yet ready to render
        if (!_viewImageReady)
        {
            if (_textLayoutNeeded)
            {
                layoutParsPrepare();
                layoutParsNormal();

                // if layout worked, needed is reset
                if (!_textLayoutNeeded) v = true;
            }
        }
        return v;
    }

    int calculateScroll(int vh) const
    {
        // get the layout bounds up to (including) the current render page
        StrandCommon::Rect r = _tbuild.extent();

        // any scroll will be how much the layout overflows the view
        int dy = r.bottom() - vh;

        // if no overflow, then no scroll
        if (dy < 0) dy = 0;

        return dy;
    }

    void renderSecond(float textOverlap)
    {
        // if we are viewing a single image.
        if (_viewImageReady)
        {
            // need to call renderStartItem to reserve the screen object
            renderStartItem(_viewBox.width(), _viewBox.height());
            renderViewSecond();
            renderEnd();
        }
        else
        {
            // calls renderStart, render, renderObj and renderEnd

            // figure out where the text will appear relative to the
            // current text box, accounting for the scroll amount
            // The text is rendered relative to this box as the cursor is
            // set to the box top. So the text rendered will be at
            // scrolly - box y1
            float sy = ImGui::GetScrollY();
            Boxf vw = _viewBox;
            vw.move(0, sy - vw[PY1]);

            // viewbox is used for text fade
            _tbuild._textFlow._viewBox = vw;
            _tbuild._textFlow._textOverlap = textOverlap; // for fade
            
            _tbuild.renderPages();
        }

        static int dragy = 0;

        if (ImGui::IsItemHovered())
        {
            int dy = 0;
            ImVec2 drag = ImGui::GetMouseDragDelta(0);
            if (drag.y)
            {
                //LOG1("drag ", drag.x << "," << drag.y);
                dy = drag.y - dragy;
                dragy = drag.y;
                _dragging = true;
            }
            else
            {
                dragy = 0;
                _dragging = false;
            }

            if (dy)
            {
                float y = ImGui::GetScrollY();
                y -= dy;
                ImGui::SetScrollY(y);
            }
        }
    }
    
    void setViewImage(ParObj* pi)
    {
        if (pi != _viewImage)
        {
            resetZoom();
            _viewImageReady = false;
            
            if (pi) _viewImage = pi;
            else
            {
                _viewImage = 0; // cancel

                // actually it isn't needed, but the scroll will be
                // in the wrong place.
                _textLayoutNeeded = true;
            }
        }
    }

    ImTex* getLoadedTex(ParImg* pi)
    {
        // if pi is loaded, get the tex
        auto r = texLoader->getLoadedRec(pi->_name, _renderCount);
        return r ? &r->_tex : 0;
    }

    bool viewImageReady()
    {
        // return true only if we have an image to view and it is ready
        bool r = false;

        if (_viewImage)
        {
            if (_viewImage->isImg())
            {
                // if the image has a dup, load the last one loaded.
                ParImg* pi = (ParImg*)_viewImage;
                while (pi)
                {
                    if (!getLoadedTex(pi)) break;
                    r = true;
                    _viewImage = pi;  // change to dup view
                    pi = pi->_dup;
                }
            }
#ifdef USESPINE
            else if (_viewImage->isAnim())
            {
                ParAnim* pa = (ParAnim*)_viewImage;
                if (pa->_a.loaded(_renderCount)) r = true;
            }
#endif            
        }
        return r;
    }


    void renderViewSecond()
    {
        // draw a full screen picture
        assert(_viewImage);

        if (_viewImage->isImg()) renderViewImageSecond((ParImg*)_viewImage);
#ifdef USESPINE        
        else if (_viewImage->isAnim()) renderViewAnimSecond((ParAnim*)_viewImage);
#endif        
    }

    void renderViewImageSecond(ParImg* pi)
    {
        // ensure loaded
        ImTex* tex = getLoadedTex(pi);
        bool ok = tex != 0;

        // we should not be called unless tex ready
        assert(ok);
        if (!ok) return; // emergency

        const TextFlowInfo& ti = _tbuild._textFlow._info;
            
        int wmax = _viewBox.width();
        int hmax = _viewBox.height();

        int hpad = ti._objPadBox._h + ti._objPadBox._y;
        int wpad = ti._objPadBox._w * 2;

        hmax -= hpad;
        wmax -= wpad;

        if (hmax <= 0 || wmax <= 0) return; // emergency

        int w = tex->_w;
        int h = tex->_h;
        if (w <= 0 || h <= 0) return; // emergency
        
        Boxf uv;
        Point2f swh = calculatePinch(wmax, hmax, w, h, uv);

        float sw = swh.x;
        float sh = swh.y;
        
        float padw = (_viewBox.width() - sw)/2;
        if (padw < 0) padw = 0;  // should not happen

        StrandCommon::Rect box(padw, ti._objPadBox._y, sw,  sh);
        
        box._y += _viewBasePos.y;
        int border = ti._objBorderSize;

        assert(_imMeta);
        ImageMeta* m = _imMeta->findOriginal(pi->_name);
        // NB: not dragging

        Colour tint = Colour(Colour::white);
        auto hit = renderImage(m, tex, ti, false, box,
                               IV2BTOP(uv),
                               IV2BBOT(uv),
                               border, tint);
        
        if (hit) _currentImageClick = hit;
    }

    void textChanged()
    {
        _textLayoutNeeded = true;
    }

protected:

    void _add(Par* p)  
    {
        // consume p
        _pars.push_back(p);
        _size += p->size();
        _trim();
        textChanged();
    }

    int imageCount() const
    {
        // count the number of images not including duplicates
        int cc = 0;
        for (Par* p : _pars)
        {
            if (p->isImg())
            {
                ParImg* pi = (ParImg*)p;
                if (!pi->_dup) ++cc;
            }
            else if (p->isAnim()) ++cc; // count animations too
        }
        return cc;
    }

    void _trim()
    {
        if (_maxText)
        {
            while (_size > _maxText)
            {
                //LOG1("text trim, size ", _size << " max:" <<_maxText);

                // this can also remove images, when they are
                // part of "old text".
                Pars::iterator it = _pars.begin();
                assert(it != _pars.end());
                Par* p = *it;

                // if the text is so long that it is still fresh
                // then we can't delete it.
                if (p->_fresh) break;
                
                _pars.pop_front();
                _size -= p->size();
                assert(_size >= 0);
                
                //LOG1("trimmed text to ", _size);
                delete p;
            }
        }
        
        while (_maxImg && imageCount() > _maxImg)
        {
            // remove the first image
            Pars::iterator it = _pars.begin();
            while (it != _pars.end())
            {
                if ((*it)->isObj())
                {
                    ParObj* pi = (ParObj*)(*it);
                    //LOG1("trimming image ", pi->_name);

                    _pars.erase(it);

                    // delete par but does not delete texture.
                    delete pi;
                    
                    break;
                }
                ++it;
            }

            // LRU image removal
            //texLoader->trim(_renderCount);
        }
    }
};

inline bool ImTextFont::clipped(const StrandCommon::Rect& box) const
{
    if (!box) return true;
    
    StrandCommon::Rect cr = ImText::getClipRect();
    const Point2f& pos = _host->_viewBasePos;

    StrandCommon::Rect b = box;
    b.moveBy(pos.x, pos.y);

    bool v = !cr.intersects(b);
    return v;
}

inline void ImTextFont::renderBackground(const ST::Region& r) const
{
    if (!r) return;
    assert(_host);
    
    ImGuiWindow* window = ImGui::GetCurrentWindow();
    const Point2f& pos = _host->_viewBasePos;
    Colour c(128U, 128U, 128U, 0.7f);
    ImU32 c32 = ColourToU32(c);

    for (int i = 0; i < r.numRects; ++i)
    {
        const RRect& ri = r.rects[i];
        ImVec2 pmin(pos.x + ri.x1, pos.y + ri.y1);
        ImVec2 pmax(pos.x + ri.x2, pos.y + ri.y2);
        window->DrawList->AddRectFilled(pmin, pmax, c32);
    }
}

inline void ImTextFont::render(const std::string& s,
                               const StrandCommon::Rect& box,
                               const TextProperties& tp) const
{
    // the flow area is notionally at the cursor position

    int sz = s.size();
    if (sz)
    {
        assert(_host);

        const char* t = s.c_str();
        const char* te = t + sz;
        const ImFont& f = font();
        
        Point2f pos = _host->_viewBasePos;
        pos.x += box._x;
        pos.y += box._y;

        Colour c;

        if (tp._disabled)
        {
            c = tp._hilite ? tp._disHiCol : tp._disCol;
        }
        else
        {
            c = tp._hilite ? tp._hiCol : tp._col;
        }

        ImU32 c32 = ColourToU32(c);

        ImDrawList* dl = ImGui::GetWindowDrawList();
        dl->AddText(&f, f.FontSize,
                    IVP(pos),
                    c32,
                    t, te);

        if (tp._underline)
        {
            ImVec2 tmin(pos.x, pos.y + box._h);
            ImVec2 tmax(pos.x + box._w, pos.y + box._h);
            dl->AddLine(tmin, tmax, c32, 1.0f);
        }
    }
}
