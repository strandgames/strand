@ctype mat4 hmm_mat4

@vs vs
uniform vs_params {
    vec2 scale;
    vec2 offset;
};

in vec2 pos;
in vec4 color0;
in vec2 texcoord0;
in vec3 color1;

out vec4 color;
out vec2 uv;
out vec3 tint;

void main() {
    gl_Position = vec4(pos * scale + offset, 0.0, 1.0);
    color = color0;
    uv = texcoord0;
    tint = color1;
}
@end

#pragma sokol @fs fs
uniform texture2D tex;
uniform sampler samp;

in vec4 color;
in vec2 uv;
in vec3 tint;
out vec4 frag_color;

void main() {
    frag_color = texture(sampler2D(tex, samp), uv) * color + vec4(tint, 0.0);
}
@end

@program scene vs fs
