//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#ifdef USESPINE

#include "spineinfo.h"
#include "jsonwalker.h"
#include "worldtime.h"

extern ImTexLoader* texLoader;

struct AnimInfo
{
    typedef std::string string;

    string              _name;
    string              _anim; // json or skel
    string              _atlas; // .atlas file
    string              _image;  // atlas image path, png or wepb

    // optional background image for animation
    // if supplied, animation is rendered over this image
    string              _backimage;

    string              _play; // anim to play
    bool                _loop = false;
    int                 _delay = 0;
    int                 _track = 0;
    bool                _append = false;
    bool                _done = false;
    bool                _large = false;

    enum Op
    {
        op_none = 0,
        op_show,
        op_hide,
    };

    // if op defined, "play" is not an animation but a Spine slot
    Op                  _op = op_none;

    bool validate()
    {
        // fill in missing elements
        bool r = !_name.empty() && !_anim.empty();
        if (r)
        {
            if (_atlas.empty()) _atlas = changeSuffix(_name, ".atlas");
            if (_image.empty()) _image = changeSuffix(_name, ".png");
        }
        return r;
    }

    friend std::ostream& operator<<(std::ostream& os, const AnimInfo& ai)
    {
        os << ai._name << ' ' << ai._play << " track:" << ai._track << " loop:" << ai._loop << " delay:" << ai._delay << " append:" << ai._append;
        return os;
    }

};

struct AnimState: public AnimInfo, public WorldTime
{
    typedef std::list<AnimInfo*> InfoQ;

    AnimInfo*           _ai = 0;            // current
    InfoQ               _aiq;
    
    ImTex*              _tex;
    const char*         _atlas;
    int                 _atlasz;
    const char*         _anim;
    int                 _animz;
    ImTex*              _texBackimage = 0;

    // these are the nominal dimensions from the anim file (when loaded)
    Point2f             _dim;

    bool                _begun = false;
    int                 _error = 0;
    bool                _playing = false;
    WTime               _tlast;
    bool                _active = false;
    bool                _static = false;  // static scene not playing but valid

    // some spine objects
    void*               _s_textureLoader = 0;
    void*               _s_renderer = 0;
    void*               _s_atlas = 0;
    void*               _s_skeldat = 0;
    void*               _s_skel = 0;
    void*               _s_statedat = 0;
    void*               _s_state = 0;

    float               _skel_x;
    float               _skel_y;
    float               _skel_w;
    float               _skel_h;

    ~AnimState()
    {
        while (pop()) ;
        setInfo(0);
    }

    bool pop()
    {
        // if present
        bool r = !_aiq.empty();
        if (r)
        {
            AnimInfo* ai = _aiq.front();
            _aiq.pop_front();
            assert(!ai->_done);
            assert(!_ai || _ai->_done);
            setInfo(ai);  // replace old one, assume done
        }
        return r;
    }

    string name() const
    {
        return _ai ? _ai->_name : "null";
    }

    string imageName() const
    {
        return _ai ? _ai->_image : string();
    }

    void setInfo(AnimInfo* ai)
    {
        // consume
        delete _ai;
        _ai = ai;
    }

    void mergeInfo(AnimInfo* ai)
    {
        if (_ai)
        {
            // copy over existing assets so the Op anim does not
            // have to specifiy them all.
            if (!_ai->_atlas.empty()) ai->_atlas = _ai->_atlas;
            if (!_ai->_image.empty()) ai->_image = _ai->_image;
            if (!_ai->_backimage.empty()) ai->_backimage = _ai->_backimage;
        }
        setInfo(ai);
    }

    void queueInfo(AnimInfo* ai)
    {
        // consume ai
        if (!_ai || _ai->_done) setInfo(ai);
        else
        {
            // queue animation until current over
            _aiq.push_back(ai);
        }
    }
    
    void preload(uint64 rcount)
    {
        // ASSUME valid
        if (!loaded(rcount))
        {
            assert(_ai);
            LOG2("anim loading ", name() << " atlas " << _ai->_image);

            // load the atlas image and also the anim data and atlas file
            // which are not images
            texLoader->getTex(_ai->_image, rcount);

            // if we have animation background, load it
            if (!_ai->_backimage.empty())
                texLoader->getTex(_ai->_backimage, rcount);

            texLoader->getTex(_ai->_image, rcount);
            texLoader->get(_ai->_anim, &_ai->_image, false); // json
            texLoader->get(_ai->_atlas, &_ai->_image, false);
        }
    }

    bool loaded(uint64 lru)
    {
        // are we loaded. also update lru
        assert(_ai);

        ImTexLoader::Rec* r = texLoader->getLoadedRec(_ai->_image, lru);

        _tex = 0;

        if (r)
        {
            //LOG1("anim, tex loaded ", _ai->_image);
            _tex = &r->_tex;
        }
        else
        {
            //LOG1("anim, tex not loaded ", _ai->_image);
        }

        if (!_atlas)
        {
            r = texLoader->find(_ai->_atlas);
            if (r && r->_data && r->_sz > 0)
            {
                _atlas = r->_data;
                _atlasz = r->_sz;
            }
            else
            {
                //LOG1("anim, atlas not loaded ", _ai->_atlas);
            }
        }

        if (!_anim)
        {
            r = texLoader->find(_ai->_anim);
            if (r && r->_data && r->_sz > 0)
            {
                _anim = r->_data;
                _animz = r->_sz;
            }
            else
            {
                //LOG1("anim, anim not loaded ", _ai->_anim);
            }
        }

        bool ok = (_tex && _atlas && _anim);

        if (ok && !_ai->_backimage.empty())
        {
            // if we're using a backimage check that too
            ImTexLoader::Rec* b = texLoader->getLoadedRec(_ai->_backimage,
                                                          lru);

            if (b)
            {
                _texBackimage = &b->_tex;
            }
            else
            {
                ok = false; // still waiting
                //LOG1("anim back image not loaded ", _ai->_backimage);
            }
        }

        if (ok)
        {
            if (!_dim)
            {
                // try to find the nominal dimensions from anim
                SpineInfo si;
                if (si.readFrom(_anim))
                {
                    LOG3("animation spine info ", si);
                    _dim = si.effectiveDim();
                }
                else
                {
                    LOG1("WARNING Can't find animation dimensions ", _ai->_name);
                }
            }
        }

        return ok;
    }

    void clear()
    {
        _tex = 0;
        _atlas = 0;
        _atlasz = 0;
        _anim = 0;
        _animz = 0;
        _texBackimage = 0;
    }
};

#else
// stubs
struct AnimInfo {};
struct AnimState: public AnimInfo
{
    void clear() {}
};

#endif
