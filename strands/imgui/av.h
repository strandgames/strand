//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#define KEY_EPS 0.0001

template<typename T> struct AV
{
    typedef double Time;

    struct V
    {
        Time    _t;
        T       _v;
    };
    
    typedef std::vector<V>  Vs;
    typedef typename Vs::iterator iterator;
    
    T   _v;
    Vs*  _vs = 0;

    Time _loopstart = 0;
    Time _loopend = 0;

    ~AV() { _purge(); }

    bool inLoop() const { return _loopstart != _loopend; }

    void clearLoop()
    {
        _loopstart = _loopend = 0;
    }

    int clearTo(const T& v)
    {
        // drop all data and assign a new default
        // clears any loop
        // return loop change

        int r = -inLoop();
        _purge();
        _v = v;
        return r;
    }

    int clearToCurrent(Time t)
    {
        return clearTo(at(t));
    }

    void setloop(Time t1, Time t2)
    {
        if (t2 > t1)
        {
            _loopstart = t1;
            _loopend = t2;
        }
    }

    bool hasFutureKeys(Time t) const
    {
        // any keys after time t ?
        bool res = false;
        if (_vs)
        {
            // anywhere after the start is in the loop
            // anywhere before the start has future keys
            res = inLoop();

            if (!res)
            {
                int z = _vs->size();
                assert(z > 0);

                // if last key is future, we have future key
                // otherwise not.
                res = ((*_vs)[z-1]._t > t);
            }
        }
        return res;
    }

    const V* lastKey() const
    {
        // or zero if no keys
        const V* vp = 0;
        if (_vs)
        {
            int n = _vs->size();
            if (n)
            {
                vp = &(*_vs)[n-1];
            }
        }
        return vp;
    }

    Time lastKeyTime() const
    {
        const V* vp = lastKey();
        return vp ? vp->_t : 0;
    }

    static T interpolate(T v1, T v2, float a)
    {
        if (a <= 0) return v1;
        if (a >= 1) return v2;
        return v1*(1 - a) + v2*a;
    }
    
    T at(Time t) const
    {
        T v;
        if (_vs)
        {
            int z = _vs->size();
            assert(z > 0);

            Time dl = _loopend - _loopstart;
            if (dl > 0)
            {
                if (t > _loopend)
                {
                    // map given time into loop segment
                    double q = floor((t - _loopstart)/dl);
                    t = t - q*dl;
                }
            }

            int i = _sup(t);
            if (i < 0) i = 0;
            const V& vi = (*_vs)[i];
            v = vi._v;
            if (i+1 < z)
            {
                // interpolate to the next key
                const V& vj = (*_vs)[i+1];
                float a = (t - vi._t)/(vj._t - vi._t);
                v = interpolate(vi._v, vj._v, a);
            }
        }
        else v = _v; // use default
        return v;
        
    }

    void set2l(Time t1, Time t2, T v, bool clearFrom)
    {
        if (clearFrom) clearToCurrent(t1);
        _set2(t1, t2, v);
    }

    void set4l(Time t1, T v1, Time t2, T v2, bool clearFrom)
    {
        if (clearFrom) clearToCurrent(t1);

        // clear all existing keys after t1
        _set(t1, v1);  
        _set(t2, v2);
    }

    int resetloop(Time t1, Time t2, float loop)
    {
        // remove any existing loop and set a loop t1 -> t2
        // return loop delta.
        
        int l1 = inLoop();

        clearLoop();

        if (loop < 0) t1 = t2 + loop;
        setloop(t1, t2);
        return inLoop() - l1;
    }
    
    void _clearFrom(int i)
    {
        assert(_vs);
        assert(i < (int)_vs->size());
        _vs->resize(i+1);
    }
    
    void _set2(Time t1, Time t2, T v)
    {
        // wish to change to value v at time t2 from the time t1
        // usually now.
        // need to anchor the current value at t1

        assert(t2 >= t1);
        
        // no anchor needed if the same
        if (t2 > t1)
        {
            // this will anchor a key if not present
            _set(t1, at(t1));
        }
        _set(t2, v);
    }
    
    int _set(Time t, T v)
    {
        // return index of key inserted or replaced
        int i;
        bool done = false;
        if (_vs)
        {
            i = _sup(t);
            if (i < 0)
            {
                // all keys bigger. needs to in at the start
                i = 0;
            }
            else
            {
                // i is the last key <= t
                V& vi = (*_vs)[i];
                if (vi._t == t)
                {
                    // adjust this key
                    //LOG1("replacing key at ", t << " " << vi._v << " with " << v);
                    vi._v = v;
                    done = true;
                }
                else
                {
                    // insert a new key after this key
                    ++i;
                    //_vs->insert(_vs->begin(), V{ t, v });
                }
            }
        }
        else
        {
            i = 0;
            _vs = new Vs;
        }

        if (!done) _vs->insert(_vs->begin() + i, V{ t, v });
        return i;
    }

    friend std::ostream& operator<<(std::ostream& os, const AV& av)
    {
#ifdef LOGGING        
        if (av._vs)
        {
            os << '{';
            int cc = 0;
            for (auto& vi : *av._vs)
            {
                if (cc++) os << ' ';
                os << '(' << vi._t << ',' << vi._v << ')';
            }
            os << '}';
        }
        else os << av._v;
#endif        
        return os;
    }

    void operator=(const T t) { _v = t; }
    operator T() const { return _v; }

private:

    int _sup(Time t) const
    {
        // return index of greatest key <= t, or -1 if all > or none
        assert(_vs);
        
        int sup = -1;
        int n = _vs->size();
        for (int i = 0; i < n; ++i)
        {
            V& vi = (*_vs)[i];
            if (vi._t > t) break;
            sup = i;
        }
        return sup;
    }


    void _purge()
    {
        clearLoop();
        delete _vs; _vs = 0;
    }

    // cannot be copied.
    AV& operator=(const AV& a) {}
};

typedef AV<float> AVF;
