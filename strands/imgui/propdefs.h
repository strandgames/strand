//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#define P_SCROLLBAR_SIZE  "ScrollbarSize"
#define P_WINDOWPADDING "WindowPadding"
#define P_FRAMEPADDING "FramePadding"
#define P_COMPACTWORDS "CompactWords"
#define P_COMPACTWORDS_DEF 1
#define P_COMPACTLINES "CompactLines"
#define P_COMPACTLINES_DEF 1

// picture padding
#define P_BOXPADTOP  "BoxPadTop"
#define P_BOXPADRIGHT "BoxPadRight"
#define P_BOXBORDER_SIZE  "BoxBorderSize"
#define P_BOXBORDER_SIZE_DEF 2
#define P_PICTURE_SIZE  "PictureSize"
// percentage of space available
#define P_PICTURE_SIZE_DEF  50
#define P_PICTURE_RES "PictureRes"
#define P_PICTURE_RES_DEF 0

// keyboard options
// size is in percent
#define P_KEYBOARD_SIZE "KeyboardSize"
#define P_DISABLE_KEYBOARD "DisableKeyboard"
#define P_DISABLE_KEYBOARD_DEF false

#define P_BOXPADRIGHT_DEF  12
#define P_BOXPADTOP_DEF  4

// for mobiles
#define P_ENABLE_COMMANDLINE "EnableCommandLine"
#define P_ENABLE_COMMANDLINE_DEF false

#define P_ENABLE_COMMANDCHOICES  "EnableCommandChoices"

#define P_MARGIN "Margin"

#define P_ENABLE_FULLBLANKS "EnableFullBlanks"

#define P_ENABLE_ROOMTITLE "EnableRoomTitle"
#define P_ENABLE_ROOMTITLE_DEF true

#define P_ENABLE_USERDEBUG "UserDebug"

// colours
#define P_TEXT_COL  "TextColour"
#define P_TEXT_COL_HI "TextColourHigh"

//audio
#define P_AUDIOLEVEL "AudioLevel"
#define P_AUDIOLEVEL_DEF 100

#define P_ENABLE_AUDIO "EnableAudio"
#define P_ENABLE_AUDIO_DEF true

// font & theme
#define P_FONTNAME   "FontName"
#define P_FONTSIZE   "FontSize"
#define P_THEMESTYLE "ThemeStyle"


