//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#ifdef USEMPG

#include <assert.h>
#include "logged.h"
#include "types.h"
#include "sokol_app.h"
#include "sokol_gfx.h"
#include "sokol_imgui.h"
#include "fetcher.h"
#include "mpeg.glsl.h"
#include "sokstate.h"

#define PL_MPEG_IMPLEMENTATION
#include "pl_mpeg.h"

extern Fetcher fetcher;
extern SOKState state;

#include "imtexloader.h"

extern ImTexLoader* texLoader;

void VideoState::Stream::initDecoder()
{
    assert(!plm);

    plm_buffer = plm_buffer_create_with_capacity(mpegBufSize);
    plm_buffer_set_load_callback(plm_buffer, &_plmpeg_load_callback, this);

    // buffer will be destroyed with plm
    plm = plm_create_with_buffer(plm_buffer, true);
    assert(plm);
    
    plm_set_video_decode_callback(plm, &_video_cb, this);

    // do not set video system to loop, do it ourselves
    //plm_set_loop(plm, _vi._loop);
    
    plm_set_audio_enabled(plm, 0); // FALSE

    // interval between starting the decoder and the first video callback
    _waitingForFrame = true;

    // track the start of decoding playout
    _beginTimer();
}

void VideoState::Stream::plmpeg_load_callback(plm_buffer_t* self)
{
    assert(self->discard_read_bytes);
    plm_buffer_discard_read_bytes(self);
            
    int wanted = self->capacity - self->length;
    if (!wanted) return;

    //LOG1("mpeg load callback ", _vi << " " << wanted);
    
    int a = 0;
    
    while (_dbuf && wanted > 0)
    {
        a = _dbuf->_size - _dpos;

        Buf* nb = _dbuf->_next;

        if (_vi._oneshot && !a)
        {
            _data = nb;  // can be null
            _size -= _dbuf->_size;
            LOG2("stream deleting buf ", _vi << " " <<_dbuf->_size);
            delete _dbuf;
            _dbuf = nb;

            // reset last
            if (!_dbuf) _dlast = &_data;
        }
        
        if (!a && nb)
        {
            _dbuf = nb;
            _dpos = 0;
            a = _dbuf->_size;
        }

        if (!a && _loaded && _vi._loop)
        {
            // at end of data, start over for loop
            assert(_data);
            _dbuf = _data;
            _dpos = 0;
            a = _dbuf->_size;            
            LOG2("stream data loop ", _vi);
        }

        if (!a) break;
                
        int r = u_min(wanted, a);

        //LOG1("mpeg load callback fed ", r << " leaving:" << _size);
                    
        uint8_t* dst = self->bytes + self->length;
        memcpy(dst, _dbuf->_data + _dpos, r);
        self->length += r;

        _dpos += r;
        wanted -= r;
    }

    if (wanted > 0)
    {
        LOG4("mpeg load callback did not fill buffer ", _vi << " " << -wanted);
    }

}

void VideoState::Stream::pollDecode()
{
    if (plm)
    {
        if (!_ended && plm_has_ended(plm))
        {
            if (!_loaded)
            {
                // we can't have ended if we havent finished loading
                // we have stalled.
                _stalled = true;

#ifdef LOGGING                
                _timer.stop();
                double dt = _timer.duration() - _playBeginTime;
                LOG1("mpg video stalled ", _vi << " at " << dt);
#endif                

                // XX reset this
                plm->has_ended = false;
            }
            else
            {
                if (_vi._loop)
                {
                    LOG2("stream playback loop ", _vi);
                    rewind();
                }
                else
                {
                    _ended = true;
                    _waitingForFrame = false; // should not be anyway.
                    LOG2("stream playback ended ", _vi);
                }
            }
        }

        if (!_ended)
        {
            //LOG1("polling stream ", _vi);
            plm_decode(plm, sapp_frame_duration());
        }
    }
}

void VideoState::Stream::updateTexture(int idx, plm_plane_t* plane)
{
    int w = plane->width;
    int h = plane->height;

    assert(_vi._tex);
    ImTex& tex = *_vi._tex;

    //LOG1("stream update texture ", tex._name << " " << idx);

    sg_image sgi;
    
    if (!idx)
    {
        if (tex._tid)
        {
            sgi.id = FROMTEXID(tex._tid);
        }
        else
        {
            tex.setDimensions(w, h, 1);

            // we will be inside imtexloader already waiting for tex
            texLoader->_trimPool(tex);

            sgi = tex.makeImage(0); // no data!
            if (sgi.id)
            {
                tex._tid = TOTEXID(sgi.id);
            }
        }
    }
    else
    {
        if (tex._tidvid[idx-1])
        {
            sgi.id = FROMTEXID(tex._tidvid[idx-1]);
        }
        else
        {
            ImTex t1;
            t1.setDimensions(w, h, 1);
            texLoader->_trimPool(t1);
            sgi = t1.makeImage(0); // no data!
            if (sgi.id)
            {
                tex._tidvid[idx-1] = TOTEXID(sgi.id);
            }
        }
    }

    if (sgi.id)
    {
        sg_image_data d2 = {};
        d2.subimage[0][0] = {
            .ptr = plane->data,
            .size = (size_t)(w * h)
        };
            
        sg_update_image(sgi, &d2);
    }
}

void VideoState::Stream::video_cb(plm_t* mpeg, plm_frame_t* frame)
{
    LOG4("mpeg video callback ", frame->time << " " << frame->width << "x" << frame->height);

    _waitingForFrame = false;
    
    updateTexture(0, &frame->y);
    updateTexture(1, &frame->cb);
    updateTexture(2, &frame->cr);
    
    assert(_vi._tex->isVideo());
}

void VideoState::Stream::_purgeDecoder()
{
    if (plm)
    {
        plm_destroy(plm);
        plm = 0;
        plm_buffer = 0; // destroyed by plm
    }

}

void VideoState::Stream::rewind()
{
    if (plm)
    {
        plm_rewind(plm);
        resetPos();
        _beginTimer(); // reset time playing

        // we have already textures, but the old picture
        // wait until new frames come out the decoder.
        _waitingForFrame = true;
    }
}

#endif // USEMPG
