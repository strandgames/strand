//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include "imgui.h"
#include "settings.h"
#include "logged.h"
#include "imcol.h"
#include "imstyle.h"
#include "strutils.h"

#define PRIVACY_POLICY "http://strandgames.com/legal/privacy.html"

// call this if need to relayout or redisplay
extern void styleChanged();
extern void EnableCommandChoices(bool);
extern std::string GameTitleAndVersion();
extern bool urlClicked(const std::string& url);
extern int ImageIdealSize();
extern bool VNMode();
extern bool isVirtualKeyboard;
extern bool isSmallScreen;
extern bool isMobile;
extern bool isWeb;
extern float choiceBoxScale;
extern void setMargin(int);
extern int marginTotal;
extern bool showRoomTitle();
extern std::string GetGameCredits();
extern std::string GetUserDebugStats();
extern int ImageMaxSettingsSize(bool* hasVersions);
extern bool showUserDebug;
extern void ChangeThemeStyle(const ThemeStyle* ts);
extern void SetAudioLevel(int percent);

void ApplyDefaultStyle()
{
    StyleColorsDark();
}

static void HelpMarker(const char* desc)
{
    ImGui::TextDisabled("(?)");
    if (ImGui::IsItemHovered(ImGuiHoveredFlags_DelayShort) && ImGui::BeginTooltip())
    {
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted(desc);
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

const ThemeStyle themeStyles[] =
{
    { "Dark", 0, StyleColorsDark },
    { "Light", 0, StyleColorsLight },
    { "Classic", 0, StyleColorsClassic },
    { "Purple", 0, StyleColorsPurple },
    { "Green", 0, StyleColorsGreen },
    { "Red", 0, StyleColorsRed },
};

#define MAX_STYLES 8

static const ThemeStyle* findThemeStyle(const char* name)
{
    for (int i = 0; i < ASIZE(themeStyles); ++i)
    {
        if (equalsIgnoreCase(themeStyles[i]._name, name))
            return themeStyles + i;
    }
    return 0;
}

void ChangeThemeStyle(const std::string& name)
{
    ChangeThemeStyle(findThemeStyle(name.c_str()));
}

void SettingsWindow(bool* settings_open)
{
    ImGuiWindowFlags flags = 0;
    
    bool fullScreen = isSmallScreen;
    
    flags |= ImGuiWindowFlags_NoSavedSettings;

    const ImGuiViewport* viewport = ImGui::GetMainViewport();
    
    if (fullScreen)
    {
        flags |= ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize;
        
        //ImGui::SetWindowFontScale(choiceBoxScale);
    
        ImGui::SetNextWindowPos(viewport->Pos);
        ImGui::SetNextWindowSize(viewport->Size);
    }
    else
    {
        float sc = 0.8f;
        ImVec2 pos = viewport->WorkPos;
        ImVec2 vz = viewport->WorkSize;
        pos.x += ((1-sc)*vz.x)/2;
        pos.y += ((1-sc)*vz.y)/2;    
        ImGui::SetNextWindowPos(pos, ImGuiCond_FirstUseEver);

        vz.x *= sc;
        vz.y *= sc;
        ImGui::SetNextWindowSize(vz, ImGuiCond_FirstUseEver);
    }

    std::string title = GameTitleAndVersion();
    if (title.empty()) title = "Settings";
    title += "###Settings";
    if (!ImGui::BeginPopupModal(title.c_str(), settings_open, flags)) return;

    //float width = ImGui::GetContentRegionAvail().x;
    ImGuiStyle& style = ImGui::GetStyle();

    //// OPTIONS
    ImGui::SeparatorText("General");
    bool check;

    if (!isSmallScreen)
    {
        check = PROPB(P_ENABLE_COMMANDCHOICES, true);
        if (ImGui::Checkbox("Enable Command Choices", &check))
        {
            EnableCommandChoices(check); // also sets property
        }
        ImGui::SameLine(); HelpMarker("Useful commands are offered as choices");
    }
    
    check = PROPB(P_ENABLE_COMMANDLINE, P_ENABLE_COMMANDLINE_DEF);
    if (ImGui::Checkbox("Enable Text Input", &check))
    {
        SPROP(P_ENABLE_COMMANDLINE, check);
        
        // NB: do not set this here, wait until dialog closed
        //forceCommandInput = check;
    }
    ImGui::SameLine(); HelpMarker("Show text input line");

    bool roomtitle = showRoomTitle();
    if (ImGui::Checkbox("Enable Room Title", &roomtitle))
    {
        SPROP(P_ENABLE_ROOMTITLE, roomtitle);
        styleChanged();
    }
    ImGui::SameLine(); HelpMarker("Show name of room at top");    

    if (!isSmallScreen)
    {
        // whole section is removed for mobile because layouts just
        // waste space, which mobile cannot afford.
        
        ImGui::SeparatorText("IF Layout");

        int mg = PROPI(P_MARGIN, marginTotal);
        int mx = u_max(mg*2, 128);
        //ImGui::SetNextItemWidth(width/2);
        if (ImGui::SliderInt("Margin", &mg, 0, mx))
        {
            SPROP(P_MARGIN, mg);
            setMargin(mg);
            styleChanged();
        }

        int ss = style.ScrollbarSize;
        if (ImGui::SliderInt("Scrollbar Size", &ss, 8, 64))
        {
            SPROP(P_SCROLLBAR_SIZE, ss);
            style.ScrollbarSize = ss;
            styleChanged();
        }
        //ImGui::SameLine(); HelpMarker("CTRL+click to input value.");

        int boxpadr = PROPI(P_BOXPADRIGHT, P_BOXPADRIGHT_DEF);
        if (ImGui::SliderInt("Picture Padding", &boxpadr, 0, 64))
        {
            SPROP(P_BOXPADRIGHT, boxpadr);
            styleChanged();
        
            // XX adjust pad top?
        }
    }

    if (!VNMode())
    {
        int bbz = PROPI(P_BOXBORDER_SIZE, P_BOXBORDER_SIZE_DEF);
        if (ImGui::SliderInt("Picture Border", &bbz, 0, 8))
        {
            SPROP(P_BOXBORDER_SIZE, bbz);
            styleChanged();
        }

        int pz = PROPI(P_PICTURE_SIZE, P_PICTURE_SIZE_DEF);
        if (ImGui::SliderInt("Picture Size (%)", &pz, 10, 90))
        {
            SPROP(P_PICTURE_SIZE, pz);
            styleChanged();
        }
    }

    bool versions;
    int maxw = ImageMaxSettingsSize(&versions);
    if (versions && maxw > 1000)   // missing meta?
    {
        int pz = ImageIdealSize();
        if (pz > maxw) pz = maxw;
        if (ImGui::SliderInt("Picture Resolution", &pz, 1000, maxw))
        {
            SPROP(P_PICTURE_RES, pz);
        }
    }

    // KEYBOARD 

    if (isVirtualKeyboard)
    {
        ImGui::SeparatorText("Keyboard Options");

        bool check = PROPB(P_DISABLE_KEYBOARD, P_DISABLE_KEYBOARD_DEF);
        if (ImGui::Checkbox("Disable Keyboard", &check))
        {
            SPROP(P_DISABLE_KEYBOARD, check);

            extern bool enableKeyboard;
            enableKeyboard = !check;
            
        }
        ImGui::SameLine(); HelpMarker("Hide virtual keyboard");

        int kz = PROPI(P_KEYBOARD_SIZE, 100);
        if (ImGui::SliderInt("Keyboard Size (%)", &kz, 50, 200))
        {
            SPROP(P_KEYBOARD_SIZE, kz);
            styleChanged();
        }
    }

    //// TEXT
    ImGui::SeparatorText("Text Options");

    if (!isSmallScreen)
    {
        Colour tc(PROPU(P_TEXT_COL, 0));
        if (!tc) tc = StyleColour(ImGuiCol_Text);

        float tcv[4] = { tc.redf(), tc.greenf(), tc.bluef(), tc.alphaf() };
        if (ImGui::ColorEdit4("Text Colour", tcv))
        {
            Colour c2(tcv[0], tcv[1], tcv[2], tcv[3]);
            SPROP(P_TEXT_COL, (int64)c2._argb);
            styleChanged();
        }
        ImGui::SameLine(); HelpMarker(
                                      "Click on the color square to open a color picker.\n"
                                      "Click and hold to use drag and drop.\n"
                                      "Right-click on the color square to show options.\n"
                                      "CTRL+click on individual component to input value.\n");
    }

    int compactWords = PROPI(P_COMPACTWORDS, P_COMPACTWORDS_DEF);
    if (ImGui::SliderInt("Compact Words", &compactWords, 0, 3))
    {
        SPROP(P_COMPACTWORDS, compactWords);
        styleChanged();
    }

    int compactLines = PROPI(P_COMPACTLINES, P_COMPACTLINES_DEF);
    if (ImGui::SliderInt("Compact Lines", &compactLines, 0, 5))
    {
        SPROP(P_COMPACTLINES, compactLines);
        styleChanged();
    }

    check = PROPB(P_ENABLE_FULLBLANKS, false);
    if (ImGui::Checkbox("Full Blanks", &check))
    {
        SPROP(P_ENABLE_FULLBLANKS, check);
        styleChanged();
    }
    ImGui::SameLine(); HelpMarker("Blank lines whole line height");

    // theme

    const char* themeNames[MAX_STYLES];
    int themeIdx = 0;
    int nt = ASIZE(themeStyles);
    std::string ts = PROPS(P_THEMESTYLE, "");  // might be empty
    
    for (int i = 0; i < nt; ++i)
    {
        themeNames[i] = themeStyles[i]._name;
        if (ts == themeNames[i]) themeIdx = i;
    }
    
    // Pass in the preview value visible before opening the combo (it could technically be different contents or not pulled from items[])
    const char* combo_preview_value = themeNames[themeIdx];
    ImGuiComboFlags tflags = 0;
    if (ImGui::BeginCombo("Theme", combo_preview_value, tflags))
    {
        for (int n = 0; n < nt; n++)
        {
            const bool is_selected = (themeIdx == n);
            if (ImGui::Selectable(themeNames[n], is_selected))
            {
                themeIdx = n;
                ChangeThemeStyle(themeNames[themeIdx]);
            }

            if (is_selected)
                ImGui::SetItemDefaultFocus();
        }
        ImGui::EndCombo();
    }

    //// AUDIO
    ImGui::SeparatorText("Audio");

    bool ae = PROPB(P_ENABLE_AUDIO, P_ENABLE_AUDIO_DEF);
    if (ImGui::Checkbox("Audio", &ae))
    {
        SPROP(P_ENABLE_AUDIO, ae);
    }
    
    int alev = PROPI(P_AUDIOLEVEL, P_AUDIOLEVEL_DEF);
    if (ImGui::SliderInt("Sound Volume (%)", &alev, 0, 120))
    {
        SetAudioLevel(alev);
    }

#if defined(LOGGING) || defined(USERDEBUG)
    
    ImGui::SeparatorText("Testing");

#if 0    
    int p1 = style.WindowPadding.x;
    if (ImGui::SliderInt("Window Padding", &p1, 0, 32))
    {
        SPROP(P_WINDOWPADDING, p1);
        style.WindowPadding = ImVec2(p1, p1);
        styleChanged();
    }

    int p2 = style.FramePadding.x;
    if (ImGui::SliderInt("Frame Padding", &p2, 0, 32))
    {
        SPROP(P_FRAMEPADDING, p2);
        style.FramePadding = ImVec2(p2, p2);
        styleChanged();
    }
#endif    

#if !defined(__ANDROID__)    
    bool v = TranscriptStarted();
    if (ImGui::Checkbox("Transcript", &v))
    {
        StartStopTranscript(v);

    }
#endif    

    bool ud = PROPB(P_ENABLE_USERDEBUG, false);
    if (showUserDebug) ud = true;
    if (ImGui::Checkbox("User Debug", &ud))
    {
        SPROP(P_ENABLE_USERDEBUG, ud);
        showUserDebug = ud;
    }

    if (ud)
    {
        std::string info = GetUserDebugStats();
        if (!info.empty()) ImGui::TextWrapped("%s", info.c_str());
    }

#endif  // USERDEBUG

    ImGui::SeparatorText("Info");

    if (ImGui::CollapsingHeader("About"))
    {
        std::string s = GetGameCredits();
        ImGui::TextWrapped("%s", s.c_str());
    }

    if (ImGui::CollapsingHeader("Privacy"))
    {
        if (ImGui::Button("Open Privacy Policy")) urlClicked(PRIVACY_POLICY);
    }

    // other options:
    // use screen width
    // disable pictures
    // sound on/off

    // font to store in save 

    // picture click min size percent
    // = below this size, clickables disabled
    
    ImGui::EndPopup();
}

