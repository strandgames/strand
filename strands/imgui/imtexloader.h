//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include <mutex>
#include <assert.h>
#include <list>
#include "imwebp.h"
#include "fda.h"
#include "logged.h"
#include "resample.h"
#include "scramble.h"
#include "region.h"
#include "imsampler.h"
#include "immeta.h"
#include "stats.h"
#include "imtex.h"

#ifdef USEMPG
#include "immpeg.h"
#endif

extern SamplerHolder samplerHolder;

#ifndef COOP
#include "threadpool.h"
#else
// forward
struct ThreadPool;
#endif

/*
#ifdef USE_FIBERS
#define LOAD_LOCK
#else
//#define LOAD_LOCK std::lock_guard<std::mutex> load_lock(_lock)

#define LOAD_LOCK \
    std::cout << "Load lock: " << std::this_thread::get_id() << "\n"; \
    std::lock_guard<std::mutex> load_lock(_lock)
#endif
*/

// fetcher takes care of calling on original thread!
#define LOAD_LOCK

#define TAG_IMTEX "ImTex, "

#define POOLSIZEMAX  512*1024*1024
#define POOLSIZEMAXWEBMOBILE  200*1024*1024
#define POOLSIZEMAXWEB  256*1024*1024

// value to bump max pool size. must be power of 2
#define POOLSIZEBUMP  (4*1024*1024)

extern std::string fullPath(const std::string&); // in main

struct PictureInfo
{
    typedef std::string string;

    string              _name;
    string              _tag;
    bool                _preload = false;
    bool                _large = false;   // fullscreen
    bool                _textWrap = false; // conform text around image mask
    int                 _sizeOverride = 0; // override percentage

    // in vn mode, this picture is to go into the text not the image area
    bool                _intext = false; 

#ifdef LOGGING
    friend std::ostream& operator<<(std::ostream& os, const PictureInfo& pi)
    {
        os << pi._name << ", tag:'" << pi._tag << '\'';
        if (pi._preload) os << ", preload";
        if (pi._large) os << ", large";
        if (pi._textWrap) os << ", textwrap";
        if (pi._sizeOverride) os << ", size:" << pi._sizeOverride;
        if (pi._intext) os << ", intext";
        return os;
    }
#endif    
};

struct ImageExtras
{
    typedef std::string string;
    
    struct ImageExtra
    {
        string          _name;
        ImageData       _icon;
    };

    static const int iconw = 256;
    static const int iconh = 192;
    std::list<ImageExtra>  _iextra;

    ImageExtra* find(const string& name)
    {
        for (auto& i : _iextra)
            if (i._name == name) return &i;

        return 0;
    }

    void add(const string& name,
             unsigned char* pix, int w, int h, int ch,
             bool icon)
    {
        // build extra information from image

        if (find(name)) return; // already present
        
        ImageData src;
        src.width = w;
        src.height = h;
        src.pixelStride = ch;
        src.lineStride = w*ch;
        src.data = pix;

        ImageExtra idst;
        idst._name = name;

        bool ok = false;

        if (icon)
        {
            if (makeIcon(idst, src)) ok = true;
            else
            {
                LOG1("problem iconisinng image ", name);
            }
        }

        if (ok) // if any valid
        {
            _iextra.push_back(idst);
        }
    }

    bool makeIcon(ImageExtra& idst, ImageData& src)
    {
        idst._icon.width = iconw;
        idst._icon.height = iconh;
        idst._icon.pixelStride = src.pixelStride;
        idst._icon.lineStride = idst._icon.width*src.pixelStride;
        idst._icon.data =
            new uint8_t[idst._icon.lineStride * idst._icon.height];

        bool ok = ResampleMix(&src, &idst._icon);
        if (!ok)
        {
            delete idst._icon.data;
            idst._icon.data = 0;
        }
        return ok;
    }

};

struct ImTexLoader
{
    typedef std::string string;

#ifdef USEMPG    
    typedef VideoState::Stream Stream;
    VideoState _vidState;
#else
    typedef void* Stream;
#endif

    //std::mutex  _lock;

    enum RecState
    {
        rec_new = 0,
        rec_waiting, // for load
        rec_loading,
        rec_loaded,
        rec_decoding,
        rec_decoded,
        rec_finished,
    };
    
    struct Rec
    {
        string          _name;
        bool            _failed = false;
        bool            _isImage = false;
        uint64          _lru = 0;  // this is set to the frame number
        RecState        _state = rec_new;

        // when we're not an image, this is the name
        // of the image that we support
        // when that image is purged, we are not needed
        string          _host;

        // this is set when we load and can be reset so that
        // we can handle one off operations after load
        bool            _justLoaded = false;
        ImTex           _tex;

        // acquired after loaded
        const char*     _data;
        int             _sz;

        Stream*         _stream = 0;  // data when video streaming

        // after loaded create a decoder state
        ImageDecodeState* _dstate;
        VideoInfo         _vi;

        // time at which we first wanted to render
        // when alpha > 0
        double            _firstRenderTime = 0;

        Rec(const string& name) : _name(name)
        {
            _init();
            _tex._name = name;
        }

        void purgeData()
        {
            delete _data;
            delete _dstate; 
            _init(); 
        }
        
        bool purge()
        {
            // remove the actual texture data (including video planes)
            _tex.destroy();
            destroyStream();
            purgeData();
            return true;
        }

        void destroyStream()
        {
#ifdef USEMPG            
            if (_stream)
            {
                LOG2("Destroy stream ", _stream->_vi);
                delete _stream;
                _stream = 0;
            }
#endif            
        }

        void updateLRU(uint64 lru)
        {
            assert(lru >= _lru);
            _lru = lru;        
        }

        void _init()
        {
            _data = 0;
            _sz = 0;
            _dstate = 0;
        }
        
    };
    
    typedef std::list<Rec>    Pool;
    typedef std::list<Rec>    Requests;
    
    Fetcher&            _fetcher;
    Pool                _pool;
    Requests            _req;
    ThreadPool*         _threads = 0;
    uint64              _renderCount;
    uint64              _poolMaxSize = 0; 

    // pool audit
    uint64              _poolTotalSize;
    int                 _poolCount;
    uint64              _poolTotalOld;
    int                 _poolCountOld;
    int                 _poolFailCount = 0; // alloc fails
    int                 _poolOversizeCount = 0; // olds ejected
    int                 _poolOversizedUsedCount = 0; // current pool ejected
    int                 _poolBumpCount = 0; // had to increase max pool
    string              _lastPoolSummary;
    
    ImageExtras         _iextras;
    const ImagesMeta*   _imMeta = 0;

    string              _maxLoadName;
    double              _maxLoadTime = 0;
    double              _maxLoadSize = 0;
    Stats               _loadStats;

    ImTexLoader(Fetcher& f) : _fetcher(f)
    {
#ifndef COOP
        _threads = new ThreadPool();
        int n = 4;
        if (_threads->start(n))
        {
            LOG3("Started image threadpool of ", n);
        }
        else
        {
            LOG1("WARNING: image threadpool only ", _threads->_threadCount << " instead of " << n);
        }
#endif        
    }

    ~ImTexLoader()
    {
        _purge();
#ifndef COOP
        delete _threads;
#endif        
    }

    void setRenderCount(uint64 rc)
    {
        _renderCount = rc;
#ifdef USEMPG
        _vidState._renderCount = rc;
#endif        
    }

    void _loadedData(Rec* r,
                    unsigned char* data,
                    unsigned int sz)
    {
        // ASSUME LOCK

        assert(r);
        assert(r->_state == rec_loading);
        assert(!r->_failed);

        // collect the data and go into loaded state
        r->_data = (char*)data;
        r->_sz = sz;
        r->_state = rec_loaded;

        if (r->_isImage)
        {
            //LOG1("image loaded ", name << " at " << _renderCount);
            createDecoder(*r);
        }
        else
        {
            // if we're not an image, then we are done.
            r->_state = rec_finished;
        }
    }

    void updateLoadStats(FetchInfo* fo, Rec* r)
    {
        if (fo)
        {
            double waitTime = 0;

            //LOG1("load stats ", fo->_name << " render:" << r->_firstRenderTime << " loaded:" << fo->_endTime);
            
            if (r->_firstRenderTime)
            {
                waitTime = fo->_endTime - r->_firstRenderTime;
                if (waitTime < 0) waitTime = 0;

#ifdef LOGGING                
                double dt = fo->duration();
                if (dt >= 1)
                {
                    LOG2("Load stats waited ", waitTime << " for load " << dt << " in " << fo->_name);
                }
#endif                
            }
            else
            {
                //LOG1("load stats not rendered ", fo->_name);
            }

            if (waitTime > _maxLoadTime)
            {
                _maxLoadTime = waitTime;
                _maxLoadName = fo->_name;
                _maxLoadSize = fo->_data.size();
            }
            
            // keep stats on load rate for all loads
            if (fo->_kps > 0) _loadStats.add(fo->_kps);
        }
        else
        {
            // otherwise reset stats
            _maxLoadName.clear();
            _maxLoadTime = 0;
            _maxLoadSize = 0;
            _loadStats.reset();
        }
    }

    void loaded(FetchInfo* fo)
    {
        LOAD_LOCK;

        assert(!fo->_cancel);

        Rec* r = _inRequest(fo->_name);
        assert(r);
            
        if (*fo)
        {
            // we are not streaming, the whole load is complete.
            assert(fo->_done);

            // do this before donate!
            updateLoadStats(fo, r);
            
            int sz;
            unsigned char* data = (unsigned char*)fo->donate(sz);
            _loadedData(r, data, sz);
        }
        else
        {
            LOG1(TAG_IMTEX "failed to load ", fo->_name);
            r->_failed = true;

            // leave fails in queue so we don't try to
            // load them again
        }
    }

#ifdef USEMPG
    void loading(FetchInfo* fo)
    {
        LOAD_LOCK;

        assert(!fo->_cancel);
        
        Rec* r = _inRequest(fo->_name);

        bool requesting = r != 0;

        if (!requesting)
        {
            // can have been moved to the pool (while playing).
            r = _find(fo->_name);
        }

        if (*fo)
        {
            if (!r)
            {
                // this could happen if we've been put into the pool while
                // still loading, then we walk away leaving the node LRU old
                // and subsequently trim the pool - but we're still loading!
                LOG2("imtex loading no stream cancelling ", fo->_name);

                // cancel any further pointless loading
                fo->_cancel = true;
                return;
            }

            assert((requesting && r->_state == rec_loading) ||
                   (!requesting && r->_state == rec_finished));
            
            // currently we only loadstream video
            assert(fo->_mType == MediaTraits::m_video);
            assert(r->_stream);
            
            int sz;
            unsigned char* d = (unsigned char*)fo->donate(sz);

            r->_stream->add(d, sz);

            LOG2("imtext loading video ", fo->_name << " " << sz << " total:" << r->_stream->_size);

            if (fo->_done)
            {
                // NB: do not record the load time for video
                // but it would go here.
                r->_state = rec_loaded;
                r->_stream->_loaded = true;  // mark all data loaded
            }
        }
        else
        {
            LOG1(TAG_IMTEX "failed to load ", fo->_name);
            if (r) r->_failed = true;
        }
    }

    void _pollStream(Rec& r)
    {
        // ASSUME LOCK
        // will create tex when ready
        r._stream->_poll();
    }

    void pollStream(Rec& r)
    {
        // called from render to continue stream when tex already created
        assert(r._stream);
        
        LOAD_LOCK;
        r._stream->_poll();
    }

    static void destroyStream(Rec* r)
    {
        if (r) r->destroyStream();
    }
#endif // USEMPG
    
    void _load(const string& path,
               const string* hostimage,
               bool mip)
    {
        // already queued (or failed)
        // ASSUME LOCK
        
        if (_inRequest(path)) return;
        
        Rec r(path);

        // if not supplied, we are an image or video
        r._isImage = (hostimage == 0);
        r._tex._useMipmaps = mip; 

        assert(r._isImage || !mip); // mip => image

        // items that are not images are associated with a "host image"
        // so they can be purged when the image is dumped
        if (hostimage) r._host = *hostimage;

        //LOG1("Queuing load of image ", path << " at " << _renderCount);
        r._state = rec_waiting;

        _req.emplace_back(r);
    }

    Rec* find(const string& path)
    {
        // public
        LOAD_LOCK;
        return _find(path);
    }
    
    Rec* _find(const string& path)
    {
        // ASSUME LOCK
        for (auto& i : _pool) if (i._name == path) return &i;
        return 0;
    }

    bool isLoaded(const string& name)
    {
        Rec* r = find(name);
        return (r && r->_isImage && r->_tex);
    }


    Rec* getLoadedRec(const string& name, uint64 lru)
    {
        // if loaded, get the tex and update the LRU
        Rec* r = find(name);
        if (r && r->_isImage && r->_tex.valid())
        {
            r->updateLRU(lru);
            return r;
        }
        return 0;
    }

    Rec* get(const string& path,
             const string* hostimage,
             bool mip)
    {
        /* if the texture is already in the pool, return it
         * otherwise perform a fetch and wait to be called later
         * hostimage is supplied for non images and is the primary image
         * for animation (ie the atlas).
         */

        LOAD_LOCK;

        Rec* r = _find(path);
        if (!r)
        {
            // not here, load it if not already queued
            _load(path, hostimage, mip);
        }
        return r;
    }
    
    Rec* getRec(const string& path, uint64 lru, bool mip)
    {
        // get tex and update LRU or start loading
        Rec* r = get(path, 0, mip);
        if (r && r->_isImage) r->updateLRU(lru);
        return r;
    }

    ImTex* getTex(const string& path, uint64 lru)
    {
        // NB: does not enable mipmap
        Rec* r = getRec(path, lru, false);
        if (r) return &r->_tex;
        return 0;
    }

    bool dropVideoTex(const string& path)
    {
        // if we have a loaded video texture, drop it
        Rec* r = find(path);
        bool v = (r && r->_isImage && r->_tex.isVideo());

        if (v)
        {
            LOG2("drop video texture ", path);
            _purgeRec(r);
        }
        return v;
    }

    void bindImTex(Rec& r, sg_image sgi)
    {
        if (sgi.id)
        {
            ImTex& it = r._tex;
            
            // drop data, decoder and pix but not TID
            simgui_image_desc_t foo{ sgi, samplerHolder.getSampler() };
            simgui_image_t si = simgui_make_image(&foo);
            it._tid = simgui_imtextureid(si);
        }
    }

    bool _pollDecoded(Rec& r)
    {
        // ASSUME LOCK
        assert(!r._failed);
        assert(r._dstate);
        assert(r._dstate->_pix);
        assert(r._tex._chans == 4);
        
        //LOG1("pollDecoded ", it._name << " w:" << it._w << " h:" << it._h << " chans:" << it._chans << " at " << _renderCount);

        assert(r._isImage);
        _trimPool(r._tex);

        sg_image sgi = r._tex.makeImage(r._dstate->_pix);
        bool res = sgi.id != 0;

        if (res)
        {
            bindImTex(r, sgi);
            r.purgeData();
            r._state = rec_finished;
        }
        else
        {
            //LOG1("WARNING poll decoder, failed to create image ", r._name);
            ++_poolFailCount;
            
            _trimOne(true);
            
            // function fails and is called again then the texture allocation
            // is re-attempted.
        }
        return res;

    }
    
    void _pollDecoder(Rec& r)
    {
        // ASSUME LOCK
        
        assert(r._dstate);
        assert(!r._failed);

        //TIMER("pollDecoder");

        bool done;

#ifdef COOP
        done = r._dstate->decodeStep();
#else
        //done = r._dstate->decodeAll();

        if (!r._dstate->_threadBegun)
        {
            r._dstate->_threadBegun = true;
            assert(_threads);

            //LOG1("enqueue ", r._name);
            _threads->enqueue(std::bind(&ImageDecodeState::decodeAllThread,
                                        r._dstate));
        }
        
        done = r._dstate->isDone();
#endif
        
        //LOG1("pollDecoder ", r._name << " " << done);
        if (done)
        {
            // done or failed
            if (!r._dstate->_failed)
            {
                //LOG1("pollDecoder decoded ", r._name << " at " << _renderCount);
                r._state = rec_decoded;
                
                int w = r._dstate->_w;
                int h = r._dstate->_h;
                int ch =r._dstate->_chans;
                r._tex.setDimensions(w, h, ch);
                // leave pix in the decoder

                bool makeIcon = false;
                
                _iextras.add(r._name, r._dstate->_pix, w, h, ch,
                             makeIcon);
            }                
            else
            {
                LOG1("pollDecoder ", r._name << " failed");
                
                r._failed = true;

                // drop data, decoder and pix
                r.purge();                
            }
        }
    }

    
    void poll()
    {
        UNUSED int dc = 0;
        UNUSED int cc = 0;
        
        LOAD_LOCK;
        
        for (auto& r : _req)
        {
            if (r._failed) continue;
            
            if (r._state == rec_waiting)
            {
                // keep issuing this start until accepted.

                // XX treat all non-images as small.
                // ie ASSUME they fit into the chunk buffer
                // XX this is a workaround for the itch cdn compression
                bool small = !r._isImage;
                UNUSED bool stream = false;

                if (!small && _imMeta)
                {
                    ImageMeta* im = _imMeta->find(r._name);
                    if (im)
                    {
                        if (im->_mType == MediaTraits::m_video)
                        {
                            // all videos stream small or not
                            stream = true;
                        }
                            
                        if (im->_fileSize && im->_fileSize < CHUNK_BUFFER)
                        {
                            //LOG1("tex loader using small fetch for ", r._name << " size " << im->_fileSize);
                            small = true;
                        }
                    }
                    else
                    {
                        LOG1("Warning: imtex, no meta for ", r._name);
                    }
                           
                }

#ifdef USEMPG
                if (stream)  // only true for video
                {
                    if (!r._stream)
                    {
                        r._vi._name = r._name;
                        r._vi._tex = &r._tex;
                        r._stream = new Stream(_vidState, r._vi);
                        r._stream->_timer.start();
                    }

                    if (!r._stream->_started)
                    {
                        int videoChunk = 512*1024;
                        r._state = rec_loading;

                        // stream video in chunks.
                        // `loading` will be called for each chunk until done
                        if (fetcher.start(r._name, _loading,
                                          small, videoChunk, this))
                        {

                            r._stream->_started = true;
                            LOG2(">>> tex starting load video ", r._stream->_vi);
                        }
                        else
                        {
                            r._state = rec_waiting;
                        }
                    }
                }
                else
#endif                        
                {
                    // not streaming. will call `loaded` only when finished

                    r._state = rec_loading;
                    
                    if (fetcher.start(r._name, _loaded, small, 0, this))
                    {

                        LOG2(">>> tex requesting file ", r._name << " at " << _renderCount);
                    }
                    else
                    {
                        r._state = rec_waiting;
                    }
                }
            }

#ifdef USEMPG            
            if (r._stream)
            {
                if (!r._tex)
                {
                    bool ready = r._state == rec_loaded;

                    // have we enough to start playing?
                    if (r._state == rec_loading && r._stream->_size >= 1024*1024)
                        ready = true;
                
                    if (ready)
                    {
                        LOG2("imtex video ready to stream ", r._name);

                        // initiate decoder
                        _pollStream(r);
                    }
                }
                else
                {
                    r._state = rec_finished;
                    LOG2("imtex video playing ", r._name);
                }
            }
#endif  // USEMPG
            
            if (r._state == rec_loaded && !r._stream)
            {
                assert(r._data && r._sz > 0);
                assert(r._dstate);
                
                //LOG1("tex loaded ", r._name << " at " << _renderCount);
                
                // move into decoding
                r._state = rec_decoding;
            }
            
            if (r._state == rec_decoding)
            {
#ifdef COOP
                // only allow one decoding per pol
                bool ok = !dc; ++dc;
                if (ok) _pollDecoder(r);
#else
                _pollDecoder(r);
#endif                
            }
            
            if (r._state == rec_decoded)
            {
                _pollDecoded(r);
            }
        }

        // move any finished requests to the pool
        for (Requests::iterator it = _req.begin(); it != _req.end();)
        {
            Rec& ri = *it;
            if (ri._state == rec_finished)
            {
                assert(!ri._failed);
                
                //LOG1("tex finished ", ri._name << " at " << _renderCount);

                ri._justLoaded = true;
                ri.updateLRU(_renderCount);

                _pool.push_back(ri); // makes copy

#ifdef USEMPG                
                if (ri._stream)
                {
                    Rec& j = _pool.back();
                    // XX fix stream reference to tex
                    j._stream->_vi._tex = &j._tex;
                    LOG2("imtex fix stream tex ", j._stream->_vi._tex->_name);
                }
#endif                

                // drop original request
                // all data cleared up now
                assert(!ri._dstate);
                it = _req.erase(it);

                ++cc;
            }
            else ++it;
        }

#ifdef LOGGING
        extern bool purgeTextures;
        if (purgeTextures)
        {
            purgeTextures = false;
            LOG2("PURGING all textures ", 0);
            while (_trimOne(false)) ;
        }
        
        // print out final pool size
        if (cc) _auditPool("loaded");
#endif


#if defined(LOGGING) || defined(USERDEBUG)
        _updatePoolSummary();
#endif        
    }

    void createDecoder(Rec& r)
    {
        assert(r._data && r._sz > 0);

        string suf = toLower(suffixOf(r._name));

        bool webp = (suf == ".webp");
        const unsigned char* dp = (const unsigned char*)r._data;

        if (webp)
        {
            //LOG4("decoding webp ", r._name);
            r._dstate = createWEBPDecodeState(dp, r._sz);
        }
        else
        {
            //LOG1("decoding image ", r._name);
            r._dstate = createSTBIDecodeState(dp, r._sz);
        }

        assert(r._dstate);
    }

    Rec* _inRequest(const string& path)
    {
        // ASSUME LOCK
        // are we in the request queue?
        for (auto& ri : _req)
            if (ri._name == path) return &ri;

        return 0;
    }

    void _purgeRec(Rec* rec)
    {
        UNUSED bool inuse = rec->_lru == _renderCount;

        string name = rec->_name;
        LOG2("Purging texture ", name << " inuse: " << inuse);
        
        rec->purge();  // destroy TID

        // drop image and its companions
        for (Pool::iterator it = _pool.begin(); it != _pool.end();)
        {
            Rec& ri = *it;
            if (ri._isImage)
            {
                if (ri._name == name) // our image?
                {
                    //LOG1("purging image ", ri._name);
                    it = _pool.erase(it);
                    continue;
                }
            }
            else
            {
                assert(!ri._host.empty());
                if (ri._host == name)
                {
                    LOG2("purging image companion ", ri._name);
                    ri.purgeData();
                    it = _pool.erase(it);
                    continue;
                }
            }
            ++it;
        }
    }

    bool _trimOne(bool force)
    {
        // ASSUME LOCK
        bool r = false;
        bool bv = false;
        Pool::iterator best;
        Pool::iterator fallback;
        bool fv = false;
        bool inuse = false;

        for (Pool::iterator it = _pool.begin(); it != _pool.end(); ++it)
        {
            Rec& ri = *it;            
            if (!ri._isImage) continue;  // choose only images

            assert(ri._lru > 0);

            // skip any currently loaded image (inuse)
            if (ri._lru == _renderCount)
            {
                if (!fv)
                {
                    // just pick first as fallback
                    fallback = it;
                    fv = true;
                }
                continue;
            }
            
            if (!bv || ri._lru < best->_lru)
            {
                if (!_inRequest(ri._name))
                {
                    best = it;
                    bv = true;
                }
            }
        }

        if (force && !bv && fv)
        {
            // use fallback
            best = fallback;
            bv = true;
            inuse = true;
        }
        
        if (bv)
        {
            if (inuse) ++_poolOversizedUsedCount;
            else ++_poolOversizeCount;

            _purgeRec(&*best);
        }
        return r;
    }

    void trimPool(const ImTex& tex)
    {
        // called from immpeg when creating images
        LOAD_LOCK;
        _trimPool(tex);
    }
    
    void _trimPool(const ImTex& tex)
    {
        assert(_poolMaxSize > 0);
        
        // ASSUME LOCK
        // add up current pool sizes
        _auditPool(0); // "trim");

        // how much do we have once this new texture is added?
        // NB: `r` not allocated a texture yet.
        uint rz = tex.allImageSize();

        bool toobig = _poolTotalSize + rz > _poolMaxSize;

        if (toobig)
        {
            LOG2("pool finding space for ", tex._name << " size(k):" << rz/1024);
            
            for (;;)
            {
                LOG2("Texture Pool frame:", _renderCount << " count:" <<  _poolCount + 1<< " size:" << (float)(_poolTotalSize +rz)/(1024*1024) << " olds:" << _poolCountOld << " oldSize:" << (float)_poolTotalOld/(1024*1024));

                toobig = _poolTotalSize + rz > _poolMaxSize;
                if (!toobig) break;
             
                // drop one texture
                bool v = _trimOne(false);
                if (!v)
                {
                    LOG2("No remaining old texture to purge ", 0);

                    if (_poolFailCount == 0)
                    {
                        // but we haven't been rejected yet by the OS
                        // perhaps the limit is too low
                        
                        uint64 m = _poolTotalSize + rz;

                        // round up to bump size (eg whole MBs)
                        m = (m + (POOLSIZEBUMP-1)) & ~(POOLSIZEBUMP-1);

                        LOG2("bumping pool max from ", _poolMaxSize << " to " << m);

                        _poolMaxSize = m;
                        ++_poolBumpCount;

                        // count this event as an oversize used, which it
                        // would have been if we didn't increase the pool size.
                        // This is so we know this is happening.
                        // NO. actually dont because it's confusing!
                        //++_poolOversizedUsedCount;
                        
                        break;
                    }
                    
                    v = _trimOne(true);
                }

                _auditPool("trimmed");

                if (!_poolCount) break; // emergency!
            }
        }

    }
    
    void _auditPool(const char* msg = 0)
    {
        // ASSUME LOCK
        _poolTotalSize = 0;
        _poolCount = 0;
        _poolTotalOld = 0;
        _poolCountOld = 0;

        for (auto& r : _pool)
        {
            if (r._isImage)
            {
                assert(r._tex);
                ++_poolCount;

                bool inuse = r._lru == _renderCount;
                uint sz = r._tex.allImageSize();

                if (msg)
                {
                    LOG4("Pool audit ", msg << ' ' << r._name << " size: " << (float)sz/(1024*1024) << " inuse:" << inuse << " lru:" << r._lru);
                }
                
                _poolTotalSize += sz;

                if (!inuse)
                {
                    ++_poolCountOld;
                    _poolTotalOld += sz;
                }
            }
        }

        if (msg)
        {
            LOG2("Texture pool audit ", msg << ' ' <<  _poolCount << " size:" << (float)_poolTotalSize/(1024*1024) << " olds:" << _poolCountOld << " oldSize:" << (float)_poolTotalOld/(1024*1024));
        }

    }

#if defined(USERDEBUG) || defined(LOGGING)
    void _updatePoolSummary()
    {        // ASSUME LOCK
        _auditPool(0);
        char buf[128];
        char* p = buf;
        p += sprintf(p, "%d/%dm", _poolCount, (int)(_poolTotalSize/(1024*1024)));
        if (_poolCount != _poolCountOld)
        {
            p += sprintf(p, " (%d/%dm)",
                         _poolCountOld, (int)(_poolTotalOld/(1024*1024)));
        }

        sprintf(p, " F:%d V:%d U:%d",
                _poolFailCount,
                _poolOversizeCount,
                _poolOversizedUsedCount);
        
        _lastPoolSummary = buf;
    }
#endif

private:

    static void _loaded(FetchInfo* fo, void* ctx)
    {
        assert(ctx);
        ImTexLoader* tl = (ImTexLoader*)ctx;
        tl->loaded(fo);
    }

#ifdef USEMPG    
    static void _loading(FetchInfo* fo, void* ctx)
    {
        assert(ctx);
        ImTexLoader* tl = (ImTexLoader*)ctx;
        tl->loading(fo);
    }
#endif    

    void _purge()
    {
        for (auto& i : _pool) i.purge();
        _pool.clear();
        _req.clear();
    }

};

