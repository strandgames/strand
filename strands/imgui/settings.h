//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include "props.h"
#include "propdefs.h"

struct ThemeStyle
{
    typedef void Fn1(ImGuiStyle*);
    typedef void Fn2();
    
    const char*  _name;
    Fn1*         _fn1;
    Fn2*         _fn2;
};

void SettingsWindow(bool* settings_open);
extern void StartStopTranscript(bool v);
extern bool TranscriptStarted();
extern void ChangeThemeStyle(const std::string& name);
extern void ApplyDefaultStyle();
    
