#include "net.h"

int main()
{
    Logged initLog;
    Logged::_logLevel = 3;  // XX

    NetworkWin net;

    Req* r = net.makeReq("update.strandgames.com:8080");
    if (r)
    {
        r->connect();
        r->send();
        r->receive();
        delete r;
    }
    
    return 0;
}
