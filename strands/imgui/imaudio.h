//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <mutex>
#include "utils.h"

// fibers do not need thread locks
#ifdef USE_FIBERS
#define AUDIO_LOCK(_s)
#else
#define AUDIO_LOCK(_s) std::lock_guard<std::mutex> audio_lock((_s)._lock)
#endif

// forward
struct AudioDecodeState;

struct AudioInfo
{
    // used to tranfser json parameters from the game
    typedef std::string string;

    string      _name;
    int         _dur = 1;
    int         _vol = 100;
    int         _chan = -1;  // not assigned
    bool        _preload = false;

    friend std::ostream& operator<<(std::ostream& os, const AudioInfo& ai)
    {
        return os << ai._name << " vol:" << ai._vol << " dur:" << ai._dur << " chan:" << ai._chan;
    }
};

struct AudioState
{
    typedef std::string string;
    
    struct Buf
    {
        // does not know how many channels.
        // if stereo, samples are interleaved
        float*  _f = 0;
        int     _size;  // number of samples
        Buf*    _next = 0;
        
        ~Buf() { delete _f; }
    };

    int          _output_sample_rate;
    int          _output_channels;
    unsigned int _gen = 0;
    int         _cacheSize = 8;
    std::mutex  _lock;
    float       _currentGain = 1;

    ~AudioState() { _purge(); }

    enum State
    {
        tr_notloaded = 0,
        tr_loading,
        tr_loaded,
    };

    struct Track
    {
        string      _name;
        float       _level = 1;
        int         _channels = 1;
        int         _audioChannel = 0;

        Buf*        _bufs = 0;
        Buf**       _tail = &_bufs;

        int         _repeats = 0;
        State       _state = tr_notloaded;
        int         _gen = 0;

        // do not play yet!
        bool        _preload = false;

        // if we can't decode mark as failed
        // keep this track as a marker to prevent
        // further decode attempts
        bool        _failed = false;

        // not owned
        AudioDecodeState* _decoder = 0;

        Track(const string& name) : _name(name) {}
        Track(const char* name) : _name(name) {}

        ~Track() { clear(); }

        int size() const
        {
            // total samples per channel
            int t = 0;
            Buf* b = _bufs;
            while (b)
            {
                t += b->_size;
                b = b->_next;
            }
            return t/_channels;
        }


        Buf* _add(int n)
        {
            Buf* b = new Buf;
            b->_size = n * _channels;
            b->_f = new float[b->_size];
            *_tail = b;
            _tail = &b->_next;
            return b;
        }

        void addBlank(int n)
        {
            // n frames
            Buf* b = _add(n);

            // size is total samples, not per channel
            memset(b->_f, 0, b->_size * sizeof(float));
        }

        void addMono(int n, float* data)
        {
            if (!n) return;
            
            Buf* b = _add(n);

            if (_channels == 1)
            {
                memcpy(b->_f, data, n*sizeof(*data));
            }
            else
            {
                assert(_channels == 2);
                float* s = data;
                float* d = b->_f;
                while (n)
                {
                    // double up
                    --n;
                    *d++ = *s;
                    *d++ = *s++;
                }
            }
        }

        void addStereo(int n, float* ldata, float* rdata)
        {
            if (!n) return;
            
            Buf* b = _add(n);

            if (_channels == 1)
            {
                // mix down to mono
                float* d = b->_f;
                for (int i = 0; i < n; ++i)
                    *d++ = ((*ldata++) + (*rdata++))/2;
            }
            else
            {
                assert(_channels == 2);

                // interleave data
                float* d = b->_f;
                while (n)
                {
                    --n;
                    *d++ = *ldata++;
                    *d++ = *rdata++;
                }
            }
        }

        void clear()
        {
            // delete all bufs in track
            while (_bufs)
            {
                Buf* b = _bufs->_next;
                delete _bufs;
                _bufs = b;
            }
            _tail = &_bufs;
        }
    };

    Track* findTrack(const string& name) const
    {
        for (auto ti : _tracks)
            if (ti->_name == name) return ti;
        return 0;
    }
    
    Track* internTrack(const AudioInfo& ai)
    {
        Track* ti = findTrack(ai._name);

        if (!ti)
        {
            // allocate it
            ti = new Track(ai._name);
            ti->_channels = _output_channels;

            //LOG1("new audio track for ", ai);

            // ai chan is -1 when not assigned
            // zero is default channel unless assigned
            int chan = 0;
            if (ai._chan > 0) chan = ai._chan;
            
            ti->_audioChannel = chan;
            _tracks.push_back(ti);
        }
        return ti;
    }

    struct Cursor
    {
        Track*        _track;
        Buf*          _buf;  // current buf
        int           _pos;  // samples offset into current buf

        float         _fade = 1;
        float         _fadeRate;

        Cursor(Track* t)
        {
            _track = t;
            reset();
        }

        void reset()
        {
            _buf = _track->_bufs;
            _pos = 0;
            resetFade();
        }

        const string& name() const { return _track->_name; }

        bool streaming() const
        {
            return _track->_state == AudioState::tr_loading;
        }

        bool valid() const
        {
            // parked at end of buf is not valid
            return _buf && _pos < _buf->_size;
        }

        void startFade(int n)
        {
            // fade over n samples
            _fadeRate = 1.0f/n;
            _fade -= _fadeRate;
        }

        void resetFade()
        {
            _fade = 1;
        }

        bool atStart() const
        {
            // have we not played any yet?
            return !_pos && _buf == _track->_bufs;
        }

        void bump()
        {
            if (_buf)
            {
                if (!_fade)
                {
                    // faded out
                    _buf = 0;
                }
                else
                {
                    _pos += _track->_channels;
                    if (_pos >= _buf->_size)
                    {
                        if (_buf->_next)
                        {
                            _buf = _buf->_next;
                            _pos = 0;
                        }
                        else
                        {
                            // at end, do we repeat?
                            if (_track->_repeats > 0)
                            {
                                --_track->_repeats;
                                reset(); // start over
                            }
                        }
                    }
                }
            }
        }

        float at(int i = 0)
        {
            assert(valid());
            assert(_pos + i < _buf->_size);
            
            // do not clamp here. will be clamped in mix
            float v = _buf->_f[_pos + i] * _track->_level * _fade;

            // are we fading out?
            if (!i && _fade < 1)
            {
                _fade -= _fadeRate;
                if (_fade < 0) _fade = 0;
            }
            
            return v;
        }

        void applyGainClamp(float g, int n)
        {
            // n is number of samples
            assert(!_buf->_next); // assume all in one buf
            assert(n <= _buf->_size);
            float* p = _buf->_f;
            while (n)
            {
                --n;
                float v = *p * g;
                if (v < -1) v = -1; else if (v > 1) v = 1;
                *p++ = v;
            }
        }

#define MIX(_dc, _v) _dc->_buf->_f[_dc->_pos] += _v
#define MIX2(_dc, _vl, _vr)  \
        float* v = _dc->_buf->_f + _dc->_pos; \
        v[0] += _vl;    \
        v[1] += vr

        /*
        void mix2(float vl, float vr)
        {
            assert(valid());
            assert(_pos + 1 < _buf->_size);

            float* p = _buf->_f + _pos;
#if 0
            // widen stereo
            // but destroys mid range
            float w = 0.7f*2;  // widen factor (0-1)*2

            float m = (vl + vr)/2;  // mid and spread
            float s = (vl - vr)/2;

            m *= 2-w;
            s *= w;

            // widen
            p[0] += m + s;
            p[1] += m - s;
#else            
            // NB: do not clamp here either, later there is a master level 
            p[0] += vl;
            p[1] += vr;
#endif            
        }
        */
    };

    typedef std::vector<Track*>   Tracks;
    Tracks                _tracks;
    std::vector<Cursor*>  _playing;

    void touchTrack(Track* t)
    {
        // update the generation count on this track each time we play
        t->_gen = ++_gen;
    }
    
    Cursor* _playTrack(Track* t)
    {
        // ASSUME not already playing
        // ASSUME LOCK
        
        Cursor* c = 0;

        bool ok = (t->_state == tr_loaded);

        if (t->_state == tr_loading)
        {
            // 20 seconds of buffered data loaded?
            float dt = (float)t->size()/_output_sample_rate;
            ok = dt >= 20;

            if (ok)
            {
                LOG2("streaming track ", t->_name << " with " << dt);
            }
        }

        if (ok)
        {
            c = new Cursor(t);
            assert(c->valid());
            _playing.push_back(c);
        }
        return c;
    }
    
    static int dub(Cursor* dc, Cursor* sc, int n)
    {
        // n frames

        int schan = sc->_track->_channels;
        int dchan = dc->_track->_channels;

        int m = n;

        while (m)
        {
            if (!sc->valid() || !dc->valid()) break;

            --m;

            if (dchan == 1)
            {
                float v = sc->at();
                if (schan == 2) v = (v + sc->at(1))/2;
                MIX(dc, v);
                //dc->mix(v);

            }
            else
            {
                assert(dchan == 2);

                float vl = sc->at();
                float vr = schan == 2 ? sc->at(1) : vl;

                MIX2(dc, vl, vr);
                //dc->mix2(vl, vr);
            }

            sc->bump();
            dc->bump();
        }

        // how many did we actually dub (per channel)
        return n - m;
    }

    void dubPlaying(Cursor* dc, int n, float gain)
    {
        // function called from `audio_stream_cb` in a separate
        // thread. Must protect the playing array
        AUDIO_LOCK(*this);
        
        auto it = _playing.begin();

        // number of samples per channel played <= n
        int nmax = 0;

        while (it != _playing.end())
        {
            Cursor* ci = *it;
            bool ok = ci->valid();

            if (!ok && ci->streaming())
            {
                // possibly recover from stall
                // if new data has been added, there will be a next buf
                // that we can go to.
                ci->bump();
                ok = ci->valid();
            }
            
            if (ok)
            {
                dc->reset();

                // dub up to n samples per channel into dest cursor
                int m = dub(dc, ci, n);
                if (m > nmax) nmax = m;
            }
            
            if (ci->valid())
            {
                // keep in use
                touchTrack(ci->_track);
                ++it;
            }
            else
            {
                // played out
                if (ci->streaming())
                {
                    LOG1("audio STALL ", ci->name());
                    ++it;
                }
                else
                {
                    LOG2("finished playing ", ci->name());
                    it = _playing.erase(it);
                    delete ci;

                    // delete old track data out of cache
                    cleanTracks();
                }
            }
        }

        // apply global gain
        if (nmax)
        {
            // how many total samples to apply gain in output
            int ns = nmax*dc->_track->_channels;
            dc->applyGainClamp(gain, ns);
        }
        
    }

    Cursor* _trackPlaying(Track* t) const
    {
        // ASSUME LOCK
        for (auto ci : _playing)
            if (ci->_track == t) return ci;
        return 0;
    }

    /*
    int trackPlayingCount() const
    {
        return _playing.size();
    }
    */

    Track* oldestTrack() const
    {
        // find oldest loaded track
        Track* told = 0;
        for (Track* ti : _tracks)
        {
            if (ti->_state == tr_loaded)
            {
                if (!told || ti->_gen < told->_gen) told = ti;
            }
        }
        return told;
    }

    int countLoadedTracks() const
    {
        int cc = 0;
        for (Track* ti : _tracks)
            if (ti->_state == tr_loaded) ++cc;

        return cc;
    }
    
    void cleanTracks()
    {
        for (;;)
        {
            int tc = countLoadedTracks();

            if (tc <= _cacheSize) break;
            
            Track* t = oldestTrack();

            assert(t);
            if (_trackPlaying(t))
            {
                LOG1("Audio WARNING: cannot purge playing track ", t->_name);
                break;
            }
            else
            {
                ::erase(_tracks, t);
                delete t;
            }
        }
    }

    void stopLoadingTracks()
    {
        // stop any tracks not yet started to load or ones in the process of
        // loading.
        // leave playing tracks and those already loaded but not playing.
        auto it = _tracks.begin();
        while (it != _tracks.end())
        {
            Track* ti = *it;
            if (ti->_state == tr_loading || ti->_state == tr_notloaded)
            {
                it = _tracks.erase(it);
                delete ti;
            }
            else ++it;
        }
    }

    int _fadeTracks(Track* tr = 0)
    {
        // fade a playing track or all playing tracks.

        // XXX NOT ANYMORE:
        // when fading all, they must be channel <= 0;
        // why did this not fade all tracks??
        // because otherwise stop_audio doesnt stop audio!
        
        // ASSUME LOCk
        int cc = 0;

        auto it = _playing.begin();
        while (it != _playing.end())
        {
            Cursor* ci = *it;

            bool hit = false;

            if (!tr)
            {
#if 1
                hit = true;
#else                
                // all tracks, providing they are channel undefined
                // or zero
                if (ci->_track)
                {
                    hit = ci->_track->_audioChannel == 0;
                }
#endif                
            }
            else
            {
                // must match track. channel irrelevant
                hit = tr == ci->_track;
            }

            if (hit)
            {
                ++cc;
                if (ci->atStart())
                {
                    // no need to fade out if not actually playing.
                    it = _playing.erase(it);
                    delete ci;
                    continue;
                }
                else
                {
                    // this will fade out then drop the cursors and also the tracks
                    // fade over 1/2 second
                    ci->startFade(_output_sample_rate/2);
                }
            }
            ++it;
        }
        return cc;
    }
    
    void stopAllPlaying()
    {
        AUDIO_LOCK(*this);

        // then fade all playing tracks, remain in cache
        // NB: does not stop channels > 0
        _fadeTracks();

        stopLoadingTracks();
    }

    void stopTrack(const string& name)
    {
        // ASSUME LOCK
        Track* t = findTrack(name);
        if (t) _stopTrack(t);
    }

    bool _stopOtherChannelTracks(const AudioInfo& ai)
    {
        // return true if track erased
        // ASSUME LOCK
        for (auto ti : _tracks)
        {
            if (ti->_audioChannel == ai._chan && ti->_name != ai._name)
            {
                if (_stopTrack(ti))
                {
                    // was erased
                    return true;
                }
            }
        }
        return false;
    }

    void stopOtherChannelTracks(const AudioInfo& ai)
    {
        // ASSUME LOCK
        while (_stopOtherChannelTracks(ai)) ;
    }

    void audioCallback(float* buffer, int num_frames, int num_channels)
    {
        //LOG1("audio_stream_cb ", num_frames << " chans:" << num_channels);

        Track tr("out");
        tr._channels = num_channels;
        tr.addBlank(num_frames);
            
        Cursor dc(&tr);

        //float gain = PROPI(P_AUDIOLEVEL, P_AUDIOLEVEL_DEF)/100.0f;
    
        dubPlaying(&dc, num_frames, _currentGain);
        memcpy(buffer, tr._bufs->_f, num_frames * num_channels * sizeof(float));
    }

                                      
private:

    bool _stopTrack(Track* t)
    {
        // fade if playing, otherise erase if not loaded
        // otherwise it remains in cache
        // return true if track erased
        // ASSUME LOCk
        bool r = false;
        if (!_fadeTracks(t))
        {
            if (t->_state != tr_loaded)
            {
                // if this is loading, then the load will be cancelled
                // in the callback
                ::erase(_tracks, t);
                delete t;
                r = true;
            }
        }
        return r;
    }

    void _purge()
    {
        ::purge(_playing);
        ::purge(_tracks);
    }
};

struct AudioDecodeState
{
    typedef std::string string;
    
    // this data is not owned by this state
    unsigned char* _data = 0;
    int _dsize = 0;

    string _name;
    AudioState::Track* _track = 0;
    
    stb_vorbis* _vorb = 0;

    bool _started = false;
    bool _failed = false;

    int  _vused = 0;
    int  _verror = 0;
    int  _totalSamples = 0;
    int  _channels = 0;
    int  _sampleRate = 0;

    static const int abufz = 16*1024; // samples
    float sbufL[abufz];
    float sbufR[abufz];

    AudioDecodeState(const string& name, AudioState::Track* t)
        : _name(name), _track(t) {}

    ~AudioDecodeState() { _purge(); }

    void setData(const unsigned char* d, int sz)
    {
        assert(d && sz);
            
        _data = (unsigned char*)d;
        _dsize = sz;
    }
    
    bool decodeStep()
    {
        // return true if more to do
        bool r = !_failed;

        if (r)
        {
            r = _decodeStep();
        }

        return r;
    }

    bool _decodeStep()
    {
        // return true if more to do
        bool res = false;
        
        assert(!_failed);
        
        if (!_started && _dsize > 0)
        {
            assert(!_vorb);
            _vorb = stb_vorbis_open_pushdata(_data,
                                             _dsize,
                                             &_vused,
                                             &_verror,
                                             0);

            if (_vorb)
            {
                stb_vorbis_info vi = stb_vorbis_get_info(_vorb);
                _channels = vi.channels;
                _sampleRate = vi.sample_rate;
                LOG2("decode audio ", _name << " nchan:" << _channels << " sample rate:" << _sampleRate);

                _started = true;

                if (_channels < 1 || _channels > 2)
                {
                    LOG2("audio output channels not supported ", _channels);
                    _failed = true;
                    
                    _purge();  // drop vorb
                }
            }
            else
            {
                if (_verror != VORBIS_need_more_data) _failed = true;
            }
        }

        // collect a bunch of output at a time rather than emitting
        // each frame as a new packet

        int sused = 0;
        float** out;
        int samples = 0;
        
        if (_vorb) for (;;) 
        {
            int channels;
            
            samples = 0;

            int r = _dsize - _vused;
            if (r <= 0) break;

            int u = stb_vorbis_decode_frame_pushdata(_vorb,
                                                     _data + _vused,
                                                     r,
                                                     &channels,
                                                     &out,
                                                     &samples);

            //LOG1("audiodecode ", _name << " r=" << r << " u=" << u << " samples=" << samples);

            // more to do unless no progress and no samples
            res = (u || samples);

            if (!res) break;

            // should not change through decoding
            assert(!samples || channels == _channels);

            _vused += u;

            if (sused + samples > abufz)
            {
                // exceeded buffer, commit what we have
                break;
            }

            if (samples > 0)
            {
                // channels is 1 or 2
                memcpy(sbufL + sused, out[0], samples*sizeof(float));
                if (channels == 2)
                {
                    memcpy(sbufR + sused, out[1], samples*sizeof(float));
                }                    
                sused += samples;
            }
        }

        if (sused)
        {
            assert(_track);

            //LOG1("decoded audio chunk of samples, ", sused);

            if (_channels == 1) _track->addMono(sused, sbufL);
            else
            {
                // channels == 2
                _track->addStereo(sused, sbufL, sbufR);
            }
            _totalSamples += sused;
        }

        // what about the last batch of samples?
        if (samples > 0)
        {
            //LOG1("decoded audio extra samples, ", samples);
            if (_channels == 1) _track->addMono(samples, out[0]);
            else
            {
                // channels == 2
                _track->addStereo(samples, out[0], out[1]);
            }
            _totalSamples += sused;                
        }
        
        return res;
    }

    void _purge()
    {
        if (_vorb)
        {
            stb_vorbis_close(_vorb);
            _vorb = 0;
        }
    }
};
