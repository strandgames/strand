//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once
// set chunk_size to 0 for no chunks
//#define CHUNK_SIZE 2*1024*1024
#define CHUNK_BUFFER 3*1024*1024

#include <string>
#include "growbuf.h"
#include "sokol_fetch.h"
#include "scramble.h"
#include "strutils.h"
#include "worldtime.h"
#include "mediatype.h"
#include "fd.h"

// forward
struct Fetcher;
struct FetchInfo;

extern WorldTime worldTime;

extern std::string fullPath(const std::string&); // in main


extern void sokol_log(const char* tag,
                      uint32_t log_level,
                      uint32_t log_item_id, 
                      const char* message_or_null, 
                      uint32_t line_nr, 
                      const char* filename_or_null,
                      void* user_data);

struct FetchTraits: public MediaTraits
{
    typedef void fetch_done(FetchInfo*, void* ctx);
};

struct FetchInfo: public FetchTraits
{
    Fetcher*        _host;
    string          _name;
    string          _fullPath; // when fetching files
    GrowString      _data;
    bool            _failed = false;
    bool            _softfail = false;

    fetch_done*     _doneFn = 0;
    void*           _doneCtx = 0;
    bool            _stream = false; // call function as data loads
    bool            _done = false;
    bool            _cancel = false;  // mark to cancel

    char            _chunk[CHUNK_BUFFER];
    sfetch_handle_t _handle;

    // timing
    double           _startTime;
    double           _endTime;
    float           _kps = 0; // k-bytes per second

    ScrambleState*  _ss = 0;
    MediaType       _mType = m_void;
    
    FetchInfo(Fetcher* f, const string& name) : _host(f), _name(name)
    {
        _mType = mediaSuffix(suffixOf(_name));
    }

    ~FetchInfo()
    {
        DescrambleEnd(_ss);
    }

    operator bool() const { return !_failed; }
    
    void notify()
    {
        if (_doneFn) (*_doneFn)(this, _doneCtx);                
    }

    char* donate(int& sz)
    {
        assert(!_failed);
        
        sz = _data.size();
        _data.add(0); // ensure zero terminate not included in size
        return _data.donate();
    }

    double duration() const
    {
        return _endTime - _startTime;
    }
};

struct Fetcher: public FetchTraits
{
    static void _fetch_cb(const sfetch_response_t* r)
    {
        FetchInfo* fo = *(FetchInfo**)r->user_data;
        assert(fo->_host);
        fo->_host->fetch_cb(r, fo);
    }

    void fetch_cb(const sfetch_response_t* r, FetchInfo* fo)
    {
        // if we're done, stop the timer ASAP.
        if (r->finished) fo->_endTime = worldTime.duration();

        if (r->dispatched)
        {
            LOG2("fetch dispatch binding buffer ", r->path);
            //sfetch_bind_buffer(r->handle, sfetch_range_t{ (void*)fo->_chunk, CHUNK_BUFFER });
        }
        else if (r->fetched)
        {
            unsigned int sz = r->data.size;
            UNUSED uint off = r->data_offset;
            string fn = filenameOf(r->path);

            if (!r->finished)
            {
                LOG3("fetching ", r->path << " " << sz  << " at " << off << " fail:" << r->failed);
            }

            assert(!fo->_cancel);
            
            if (sz)
            {
                unsigned char* d = (unsigned char*)r->data.ptr;

                if (!fo->_data.size())
                {
                    assert(!fo->_ss);
                    ScrambleState* ss = DescrambleStart(d, sz, fn.c_str()); // &sz
                    if (DescrambleOK(ss))
                    {
                        //LOG3("Fetch descramble start ", sz);
                        fo->_ss = ss;
                    }
                    else
                    {
                        DescrambleEnd(ss);
                    }
                }
                else
                {
                    if (fo->_ss)
                    {
                        //LOG3("fetch descramble next ", sz);
                        DescrambleNext(fo->_ss, d, sz);
                    }
                }

                fo->_data.append((char*)d, sz);

                // if finished will notify below
                if (fo->_stream && !r->finished)
                {
                    fo->notify();

                    if (fo->_cancel)
                    {
                        // wish to cancel.
                        // this will call handler again with finished and
                        // failed.
                        _cancel(fo);
                    }
                }
            }
        }
        else if (r->failed && !fo->_cancel)
        {
            // we are called as fail after a cancel, but it is not
            // really a fail.
            fo->_failed = true;

            if (!fo->_softfail)
            {
                LOG1("fetch '", r->path << "' failed with " << r->error_code);
                switch (r->error_code)
                {
                case SFETCH_ERROR_FILE_NOT_FOUND:
                    LOG1("fetch, file not found '", r->path << "'");
                    LOG1("exists file says ", FD::existsFile(r->path));
                    break;
                case SFETCH_ERROR_BUFFER_TOO_SMALL:
                    LOG1("fetch, buffer too small ", r->path);
                    break;
                case SFETCH_ERROR_INVALID_HTTP_STATUS:
                    LOG1("fetch, invalid http status ", r->path);
                    break;
                case SFETCH_ERROR_NO_BUFFER:
                    LOG1("fetch, no buffer allocated ", r->path);
                    break;
                case SFETCH_ERROR_UNEXPECTED_EOF:
                    LOG1("fetch, unexpected EOF ", r->path);
                    break;
                }
            }
        }

        if (r->finished)
        {
            // call even if we failed
            // can also be canceled.
            // caller should extract data via donate
            fo->_done = true;

            if (fo->_cancel)
            {
                // do not call notify if cancelled
                // nor adjust load timings
                LOG1("fetch ", r->path << " cancelled");
            }
            else
            {
                // find data rate
                double dt = fo->duration(); // seconds
                if (dt > 0)
                {
                    // calculate the loading average K bytes per second
                    int z = fo->_data.size();
                    fo->_kps = (z/dt)/1024.0f;
                }

                LOG3("fetch ", r->path << " finished " << fo->_data.size() << " in " << dt << " rate(k/s):" << fo->_kps);

                // this might mark for cancel, but we are already done
                fo->notify();
            }
            
            // clean up
            delete fo;
        }
    }

    FetchInfo* start(const string& path, fetch_done* cb,
                     bool small,
                     int streamz = 0,
                     void* ctx = 0)
    {
        // if streamz, this is the size of the stream buffer
        // which must be less than the CHUNK_BUFFER
        // if streamz, fetch_done will be called as data loads

        FetchInfo* fo = new FetchInfo(this, path);
        fo->_doneFn = cb;
        fo->_doneCtx = ctx;
        fo->_stream = streamz > 0; // call as data loads
        fo->_fullPath = fullPath(path);
        
        sfetch_request_t fr = {};
        fr.path = fo->_fullPath.c_str();
        fr.callback = _fetch_cb;
        fr.user_data = sfetch_range_t{ (void*)&fo, sizeof(fo) };
        fr.buffer = sfetch_range_t{ (void*)fo->_chunk, CHUNK_BUFFER };

        if (small)
        {
            //assert(!streamz);  // can be small videos
            
            // fetch file in one go and assume it will fit in buffer.
            // this will not issue a HEAD request
            fr.chunk_size = 0;
        }
        else
        {
            // streams will issue a HEAD request and fetch in chunks

            int chunk = CHUNK_BUFFER;
            if (streamz > 0 && streamz < chunk) chunk = streamz;
            fr.chunk_size = chunk;
        }

        // start tracking time taken to load
        fo->_startTime = worldTime.duration();
        
        fo->_handle = sfetch_send(&fr);
        bool v = sfetch_handle_valid(fo->_handle);
        if (!v)
        {
            // this will happen if the request pool is too small
            LOG1("WARNING: fetch start ", fr.path << " failed");
            delete fo;
            fo = 0;
        }

        if (v)
        {
            LOG2("fetch start ", fr.path << (streamz ? " streaming" : ""));
        }
        
        return fo;
    }

    static void poll()
    {
        sfetch_dowork();
    }

    static bool valid()
    {
        return sfetch_valid();
    }

    void _cancel(FetchInfo* fo)
    {
        if (sfetch_handle_valid(fo->_handle))
        {
            LOG1("fetch issue cancel ", fo->_name);
            sfetch_cancel(fo->_handle);
        }
    }

    static void init(const char* msg = 0)
    {
        // need to init context in each thread
        sfetch_desc_t fdesc = {};
        //fdesc.max_requests = 256; // default 128 is fine
        fdesc.num_channels = 1;
        fdesc.num_lanes = 4; // max network requests at once
#ifdef LOGGING    
        fdesc.logger.func = sokol_log;
#endif
        sfetch_setup(&fdesc);
        LOG2("Initialised fetcher ", (msg ? msg : ""));
    }

};
