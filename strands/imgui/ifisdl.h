//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include <iostream>
#include <stdio.h>
#include <functional>
#include <chrono>
#include <thread>
#include <mutex>
#include <string>
#include "hunkfile.h"  // inlude first to use real files!
#include "fda.h"
#include "ifi.h"
#include "ifihost.h"
#include "logged.h"
#include "imtext.h"
#include "wordstat.h"
#include "reporter.h"
#include "vn.h"

#define CHOICE_TEXTXX

#define META_PREFIX "meta_"

//name of the save hunk for game data
#define SAVE_HUNK_STRAND "strand"
#define SAVE_HUNK_PROPS "props"

#define AUTOSAVE_FILENAME  "autosave.sav"
#define TRANSCRIPT_FILENAME  "transcript.txt"

#define SLEEP std::this_thread::sleep_for(std::chrono::milliseconds(10))


extern void EnableVNMode(bool);
extern void EnableCommandInput();

struct SDLHandler: public IFIHandler
{
    /* this is the UI handler not the client handler.
     * which is KLClient and KLHandler
     * processing here is on the GUI hosting side not the game side
     */
    
    IFIHost*        _host;
    ChoicesInfo*    _choice = 0;

    typedef std::function<void(const char*, int id)> TextEmitter;
    TextEmitter     _emitter;

    string          _title;

    // have we received the meta data from the game
    // usually sent at start
    bool            _receivedGameMeta = false;

    string          _requestSaveFile;
    string          _requestLoadFile;
    Roster          _roster;

    // only created if we are reporting
    Reporter*       _reporter = 0;

    SDLHandler(IFIHost* host) : _host(host) {}
    ~SDLHandler()
    {
        delete _choice;
        delete _reporter;
    }

    void _emit(const string& s, int id = 0)
    {
        _emit(s.c_str(), id);
    }

    void _emit(const char* s, int id = 0)
    {
        assert(_emitter && s);
        if (*s || id) _emitter(s, id);
    }

    bool ifiText(const string& s) override
    {
        _emit(s);
        return true;
    }

    bool ifiTextFormatted(const TextF& t) override
    {
        //LOG1("GOT formatted text ", t);
        _emit(t._text, t._id);
        return true;
    }
    
    bool ifiMoves(int moveCount) override
    {
        std::cout << "current move count " << moveCount << std::endl;
        return true;
    }

    string prompt() const
    {
        return getProp(IFI_PROMPT).toString();
    }

    void emitPostSave(bool v)
    {
        // send a message to the engine to indicate save worked or not.
        GrowString js;
        buildJSONStart(js);        

        JSONWalker::addBoolValue(js, IFI_POSTSAVE, v);
        buildJSONEnd();

        // send
        _host->eval(js.start());
    }

    void emitPostLoad(bool v)
    {
        // send a message to the engine to indicate load worked or not.
        GrowString js;
        buildJSONStart(js);        

        JSONWalker::addBoolValue(js, IFI_POSTLOAD, v);
        buildJSONEnd();

        // send
        _host->eval(js.start());
    }

    bool ifiSaveData() override
    {
        // when the game issues save

        //LOG1(">>> ifisaveData ", 0);
        
        // XX ignore any given filename
        extern void browserSave();
        browserSave();
        return true;
    }

    bool ifiRestartResponse() override
    {
        // when game issues "restart:1"
        LOG2("IFI response got restart ", 1);

        extern void browserRestart();
        browserRestart();

        return true;
    }


    static string autoSaveFilename()
    {
        // the prefix is now the android saveBaseFile
        return AUTOSAVE_FILENAME;
#if 0        
#ifdef __ANDROID__
        const char* ap = android_internal_datapath();
        return makePath(ap, AUTOSAVE_FILENAME);
#else        
        return AUTOSAVE_FILENAME;
#endif
#endif        
    }

    static string transcriptFilename()
    {
        return TRANSCRIPT_FILENAME;
#if 0        
#ifdef __ANDROID__
        const char* ap = android_internal_datapath();
        return makePath(ap, TRANSCRIPT_FILENAME);
#else        
        return TRANSCRIPT_FILENAME;
#endif
#endif        
    }

    bool ifiSave(const uchar* data, int size, const string& name) override
    {
        // ifiSaveDataResponse is handled in the ifihandler, which
        // calls this to actually perform the save to file.
        // the name is optionally provided in the json.
        // NB: do not own `data`
        // NB: data can be empty and size == 0. this happens when save failed.

        // any suggested name?
        string f = name;
        
        // otherwise invent our own
        if (f.empty()) f = _requestSaveFile;
        
        // ensure we use a save suffix
        f = changeSuffix(f, ".sav");

        bool r = size > 0;
        if (r)
        {
            assert(*data);
        
            string path = makePath(saveBaseDir, f);
            LOG2("ifiSave ", path << " size:" << size);

            HunkFile hf(path);
            HunkHead hh((uchar*)data, size); // not owned
        
            string title = getGameTitle();
            if (title.empty())
            {
                LOG2("warning: unknown game title ", name);
            }

            // add type=strand so we know its for the strand backend
            // and use the title for the name so that save games must match
            hf.add(SAVE_HUNK_STRAND, title, hh);
            
            // now add UI info into the save
            string uiprops = Props::get().toString();

            // might not be any!
            if (!uiprops.empty())
            {
                //LOG3("saving props '", uiprops << '\'');
                hf.add(SAVE_HUNK_PROPS, "ui", HunkHead(uiprops));
            }

            r = hf.write();
            if (r)
            {
                LOG2("saved '", title << "' in " << path);

                // XX defined in main. This syncs the persistent files
                syncFilesystem();
            }
            else
            {
                LOG1("ifiSave, cannot write to file '", path << "'\n");
            }
        }

        // clear request name in case anyone wants to know the save
        // is done (or failed).
        _requestSaveFile.clear();

        if (filenameOf(f) != autoSaveFilename()) emitPostSave(r);

        return r;
    }

    bool ifiLoadData(const string& s) override
    {
        // called directly from requestLoad.
        // we load from file and send it to the back end.

        // can also be called from the game with a filename or blank.
        
        string f = s;

        // otherwise invent our own
        if (f.empty()) f = _requestLoadFile;

        if (f.empty())
        {
            // if blank need to ping back to the UI load dialog
            extern void browserLoad();
            browserLoad();
            return true; // handled
        }
        
        // ensure we use a save suffix
        f = changeSuffix(f, ".sav");

        // use the save path prefix because its in the same place
        string path = makePath(saveBaseDir, f);

        LOG2("Loading save ", path);

        HunkFile hf(path);
        HunkInfo* hi = 0;

        bool r = hf.read();
        if (r)
        {
            string title = getGameTitle();
            
            // find any hunk for the strand backend (should be just one)
            hi = hf.find(SAVE_HUNK_STRAND);

            if (hi)
            {
                // is it for this game?
                r = equalsIgnoreCase(hi->_name, title);
                if (!r)
                {
                    LOG1("ERROR, save game is for game ", hi->_name);
                    r = false;
                }
                
            }
            if (!hi)
            {
                LOG1("ERROR, not a strand save game ", path);
                r = false;
            }
        }

        if (r)
        {
            // are there any UI settings
            string uipname("ui");
            HunkInfo* uii = hf.find(SAVE_HUNK_PROPS, &uipname);
            if (uii)
            {
                // NB: zero terminated
                //LOG1("restoring UI props; '", (const char*)uii->_head._data << "'");
                bool r1 = Props::get().fromString((const char*)uii->_head._data);
                if (r1)
                {
                    //LOG1("restored props; ", Props::get().toString());
                }
                else
                {
                    LOG1("Warning: cannot restore UI settings ", 0);
                }
            }
        }

        if (r)
        {
            LOG2("loaded save '", hi->_name << "' from " << path);
            
            GrowString js;
            buildJSONStart(js);        

            // Note: there will be a zero on the end of the data
            JSONWalker::addStringValue(js, IFI_LOADDATA, (char*)hi->_head._data);
            buildJSONEnd();

            //LOG1("ifiLoadData, ", js.start());
            assert(_host);

            // send
            _host->eval(js.start());
            _host->syncRelease(); // refresh when not in sync already
        }

        if (!r)
        {
            LOG1("IFI loadGame, unable to load '", path << "'");

            // only need to send if failed!
            // if it worked, we have sent the data and that data needs
            // to be restored before we know if it has worked.
            emitPostLoad(false);
        }
        
        return r;
    }

    bool ifiResume(bool v) override
    {
        //LOG1(">>>>> UI IFI resume ", v);

        string path = makePath(saveBaseDir, autoSaveFilename());

        if (FDFile::existsFile(path))
        {
            LOG2("IFI resume, autosave present, ", path);

            // trigger UI dialog to resume
            extern bool can_resume;
            can_resume = true;
        }
        else
        {
            LOG2("IFI resume, autosave not present, ", path);
        }
        
        return true;
    }

    bool ifiTitleTextResponse(const string& s) override
    {
        // in game title such as location 
        // LOG3("got title ", s);
        _title = s;
        return true;
    }

    bool ifiPeopleResponse(const string& js) override
    {
        // eg [{"id":[],"name":"witch"}]
        
        LOG3("got people ", js);

        bool r = true;
        _roster.clear();

        JSONWalker jw(js);
        for (jw.beginArray();!jw._error && !jw._end;jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st) break; // bad json

            if (!isObject)
            {
                LOG3("Bad people list ", st);
                continue;
            }

            // expect a list of persons
            string person;
            if (jw.atObj(person))
            {
                for (JSONWalker jw(person); r && jw.nextKey(); jw.next())
                {
                    const char* st = jw.checkValue(isObject);
                    if (!st) { r = false; break; } // bad json

                    if (!isObject)
                    {
                        var v = jw.collectValue(st);
                        if (jw._key == IFI_NAME) // person name
                        {
                            string name = v.toString();
                            if (!name.empty())
                            {
                                Person p;
                                p._name = name;
                                _roster.push_back(p);
                            
                                //LOG1("added to roster: ", name);
                            }
                        }
                    } 
                }
            }
        }
        return r;
    }

    bool ifiMetaResponse(const string& js) override
    {
        LOG3("Got meta ", js);

        // note we've received meta data which contains hints
        // for the UI.
        _receivedGameMeta = true;
            
        for (JSONWalker jw(js); jw.nextKey(); jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st) break; // bad json

            if (!isObject)
            {
                // throw all the meta data into the handler propset
                var v = jw.collectValue(st);
                if (v)
                {
                    setProp(string(META_PREFIX) + jw._key, v);
                    //LOG1("adding meta prop ", string(META_PREFIX) + jw._key << " = " << v);
                }
            }
        }

        // meta data can contain: `layout:vn` 
        string lo = getGameLayout();
        if (lo == "vn") EnableVNMode(true); 

        // from the metadata send stored in IFI props and got here
        bool ui_textinput = getGameUITextInput();
        if (ui_textinput) EnableCommandInput();
        
        return true;
    }

    static void _ifiReport_fetch_cb(FetchInfo* fo, void* ctx)
    {
        //LOG1("ifi report, got callback!", 0);
    }

    bool ifiReport(const string& js) override
    {
        // report will be an object. for example
        // { "score": "brochure rope" }
        //LOG3("got ifi report ", js);

#ifdef REPORTING        
        GrowString js1;
        if (JSONWalker::reopenJSON(js1, js))
        {
            // we can no add more json to js1
            string title = getGameTitle();
            if (!title.empty())
                JSONWalker::addStringValue(js1, IFI_TITLE, title);

            string version = getGameVersion();
            if (!version.empty())
                JSONWalker::addStringValue(js1, IFI_VERSION, version);

            js1.add('}');
            js1.add(0);
            
            // encode js into a URL compatible string
            string s64 = Reporter::encode(js1);

            /*
            LOG3("got ifi report ", js1 << " as " << s64);

            // check we decode correctly
            string s64d = Reporter::decode(s64);
            assert(s64d == (const char*)js1);
            */

#ifdef __EMSCRIPTEN__

            // need https within browser
            string path = "https://update.strandgames.com/info/" + s64;
            
            // use fetcher for web reporting. mark small
            fetcher.start(path, _ifiReport_fetch_cb, true);
#else

            // send to civet server directly
            string path = "http://update.strandgames.com:8080/info/" + s64;            
            //  desktops use cpp-http lib va reporter.
            if (!_reporter) _reporter = Reporter::createReporter();
            _reporter->sendAsync(path);
#endif 

        }
#endif  // REPORTING  
        
        return true;
    }

    bool ifiSoundResponse(const string& js) override
    {
        AudioInfo ai;
        
        for (JSONWalker jw(js); jw.nextKey(); jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st) break; // bad json
            
            if (!isObject)
            {
                // throw all the meta data into the handler propset
                var v = jw.collectValue(st);
                if (jw._key == IFI_CHANNEL) ai._chan =  v.toInt();
                else if (jw._key == IFI_DURATION) ai._dur = v.toInt();
                else if (jw._key == IFI_NAME) ai._name = v.toString();
                else if (jw._key == IFI_VOLUME) ai._vol = v.toInt();
                else if (jw._key == IFI_PRELOAD) ai._preload = v.isTrue();
            }
        }

        LOG2("Got audio ", js << " " << ai);
        
        if (!ai._name.empty() && ai._vol >= 0) play_audio(ai);
        else
        {
            stop_audio();
        }
            
        return true;
    }

    // functions to get meta data folded into properties
#define GETGAMEPROP_STR(_fn, _prop)                     \
    string getGame ## _fn()                             \
    {                                                   \
        var v = _props.find(META_PREFIX _prop);         \
        return v ? v.toString() : string();             \
    }

#define GETGAMEPROP_BOOL(_fn, _prop)                    \
    bool getGame ## _fn()                               \
    {                                                   \
        return _props.find(META_PREFIX _prop).isTrue(); \
    }    

    GETGAMEPROP_STR(Title, IFI_TITLE);
    GETGAMEPROP_STR(Version, IFI_VERSION);
    GETGAMEPROP_STR(Font, IFI_FONT);
    GETGAMEPROP_STR(Layout, IFI_LAYOUT);
    GETGAMEPROP_BOOL(UITextInput, IFI_UI_TEXTINPUT);
    GETGAMEPROP_STR(Credits, IFI_CREDITS);
    GETGAMEPROP_STR(IconName, IFI_ICONNAME);

    bool ifiPictureResponse(const string& js) override
    {
        PictureInfo pi;

        if (!js.size()) return true; // ignore
        if (js[0] == '{')
        {
            // json
            //LOG2("Got picture request ", js);

            for (JSONWalker jw(js); jw.nextKey(); jw.next())
            {
                bool isObject;
                const char* st = jw.checkValue(isObject);
                if (!st) break; // bad json
            
                if (!isObject)
                {
                    // throw all the meta data into the handler propset
                    var v = jw.collectValue(st);
                    if (jw._key == IFI_NAME) pi._name = v.toString();
                    else if (jw._key == IFI_IMGTAG)
                        pi._tag = toLower(trim(v.toString()));
                    else if (jw._key == IFI_PRELOAD) pi._preload = v.isTrue();
                    else if (jw._key == IFI_LARGE) pi._large = v.isTrue();
                    else if (jw._key == IFI_TEXTWRAP) pi._textWrap = v.isTrue();
                    else if (jw._key == IFI_SIZE) pi._sizeOverride = v.toInt();
                    else if (jw._key == IFI_INTEXT) pi._intext = v.isTrue();
                }
            }
        }
        else
        {
            // assume simple file name
            pi._name = js;
        }

        if (!pi._name.empty())
        {
            LOG2("Got picture ", pi);
            show_picture(pi);
        }

        return true;
    }

    bool ifiAssetResponseVars(const VarList& vs) override
    {
        //LOG1("#### Asset Response Vars: ", vs);
        show_asset(vs);
        return true;
    }

    bool ifiAnimateResponse(const string& js) override
    {
#ifdef USESPINE        
        if (js[0] == '{')
        {
            // json
            LOG2("Got animation request ", js);

            AnimInfo* ai = new AnimInfo();

            for (JSONWalker jw(js); jw.nextKey(); jw.next())
            {
                bool isObject;
                const char* st = jw.checkValue(isObject);
                if (!st) break; // bad json
            
                if (!isObject)
                {
                    // throw all the meta data into the handler propset
                    var v = jw.collectValue(st);
                    if (jw._key == IFI_NAME)
                    {
                        ai->_anim = v.toString();

                        // no suffix on anim name
                        ai->_name = changeSuffix(ai->_anim, 0);
                    }
                    else if (jw._key == IFI_ATLAS) ai->_atlas = v.toString();
                    else if (jw._key == IFI_IMAGE) ai->_image = v.toString();
                    else if (jw._key == IFI_PLAY) ai->_play = v.toString();
                    else if (jw._key == IFI_LOOP) ai->_loop = v.isTrue();
                    else if (jw._key == IFI_DELAY)
                    {
                        ai->_delay = v.toInt();

                        // even for zero delay, this means append
                        ai->_append = true;
                    }
                    else if (jw._key == IFI_SHOW) ai->_op = AnimState::op_show;
                    else if (jw._key == IFI_HIDE) ai->_op = AnimState::op_hide;
                    else if (jw._key == IFI_LARGE) ai->_large = v.isTrue();
                    else if (jw._key == IFI_TRACK) ai->_track = v.toInt();
                    else if (jw._key == IFI_BACKIMAGE)
                        ai->_backimage = v.toString();
                }
            }

            extern void play_animation(AnimInfo* ai);

            // validate will fill in unspecified fields with defaults
            if (ai->validate()) play_animation(ai);  // consumes ai
            else delete ai;
        }
        return true; // handled
#else
        return false;
#endif        
    }

    /////

    void presentChoices()
    {
        if (_choice)
        {
            int cc = 0;
            for (auto& i : _choice->_choices)
            {
                string s = "(";
                ++cc;
                s += std::to_string(cc);
                s += ") ";
                s += i._text._text;
                s += '\n';
                _emit(s);
            }
        }
    }

#ifdef IFI_HANDLE_CHOICE
    bool ifiHandleChoicesInfo(ChoicesInfo* ci) override
    {
        assert(ci);
        assert(ci != _choice);

        // remove any pending
        if (_choice) delete _choice;

        // we keep the donated choices given to us, and delete it later
        _choice = ci;

#ifdef CHOICE_TEXT        
        _emit("\n");

        if (_choice->_header)
        {
            _emit(_choice->_header._text);
            _emit("\n");
        }
        presentChoices();
#endif        

        // signal handled
        return true;
    }
#endif // IFI_HANDLE_CHOICE    

    bool flush()
    {
        // wait for input
        bool done;

        for (;;)
        {
            done = !_host->sync();
            if (done) break;
            _host->release();
            break;
        }
        return done;
    }

    void sendInput()
    {
        /// call sctx yieldcmd
        std::string s = gui_input_pump();
        if (!s.empty())
        {
            bool done = false;

            if (_choice)
            {
                int nc = _choice->size();
                int chosen = 0;
                const char* p = s.c_str();

                // parse choice number
                while (u_isdigit(*p))
                    chosen = chosen*10 + (*p++ - '0');

                if (chosen > 0 && chosen <= nc)
                {
                    //LOG1(">>> pump got choice ", chosen);

                    // have accepted a value
                    ChoiceList::iterator it = _choice->_choices.begin();
                    if (chosen > 1) std::advance(it, chosen-1);

                    ChoiceInfo ci = *it; // copy

                    if (ci._response[0] == '{')
                    {
                        // response is JSON, so just use it.
                        LOG4("IFI choice, sending direct response ", ci._response);
                        _host->eval(ci._response.c_str());
                        done = true;
                    }
                }

                // clear any choices after input
                delete _choice; _choice = 0;
            }
            
            if (!done)
            {
                GrowString js;
                buildJSONStart(js);
                buildCmdJSON(s.c_str());
                buildJSONEnd();
                _host->eval(js.start());
            }
        }
    }

    bool pumpfn()
    {
        flush();

        //static int cc; LOG1("pumpfn! ", ++cc);
        sendInput();
        return true;
    }
};

struct History
{
    typedef std::string string;
    static const int hSize = 10;
    
    string      _h[hSize];
    int         _pos = 0;
    int         _cur = 0;

    void add(const char* s)
    {
        // reject lines that are just a number
        // as these are choices
        // also rejects empty lines
        const char* e = s;
        while (u_isdigit(*e)) ++e;
        if (!*e) return; 
        
        if (_pos >= hSize)
        {
            memcpy(_h, _h+1, sizeof(string));
            --_pos;
        }
        _h[_pos++] = s;
        _cur = _pos;
    }

    const char* up() 
    {
        if (_cur) return _h[--_cur].c_str();
        return 0;
    }

    const char* down()
    {
        if (_cur < _pos) return _h[_cur++].c_str();
        return 0;
    }

    void clear()
    {
        _pos = 0;
        _cur = 0;
    }
    
};

struct Transcript
{
    typedef std::string string;

    string      _name;
    size_t      _size;
    int         _error = 0;

    virtual ~Transcript() {}

    virtual bool open(const string& name) = 0;
    virtual void close() = 0;
    virtual bool write(const char* s, size_t sz) = 0;
    virtual bool write(const char c) = 0;
    virtual string getAll() = 0;
    
    bool write(const string& t) { return write(t.c_str(), t.size()); }
    bool write(const char* s) { return write(s, strlen(s)); }
    
};

struct TranscriptFile: public Transcript
{
    FILE*       _fp = 0;

    ~TranscriptFile()
    {
        close();
    }

    bool isOpen() const { return _fp != 0; }

    bool open(const string& name) override
    {
        close();
        _name = name;
        _size = 0;
        _fp = fopen(name.c_str(), "w");
        return _fp != 0;
    }

    void close() override
    {
        if (_fp)
        {
            fclose(_fp);
            _fp = 0;
        }
    }

    bool write(char c) override
    {
        bool r = isOpen() && !_error;
        if (r)
        {
            r = fputc(c, _fp) != EOF;
            if (r) ++_size;
            else _error = 1;
        }
        return r;
    }
    
    bool write(const char* s, size_t sz) override
    {
        bool r = isOpen() && !_error;

        if (r)
        {
            //LOG1("transcript: ", s);
            r = fwrite(s, 1, sz, _fp) == sz;
            if (r) _size += sz;
            else _error = 1;
        }
        return r;
    }

    string getAll() override
    {
        assert(!isOpen());

        string all;
        if (_size)
        {
            FILE* fp = fopen(_name.c_str(), "r");
            if (fp)
            {
                all.resize(_size);
                fread((char*)all.data(), 1, _size, fp);
                fclose(fp);
                
            }
        }
        return all;
    }

    
};

struct StrandCtx
{
    typedef std::string string;
    
    IFI*        ifi = 0;

    string configdir = ".";
    const char* datadir = "."; // default;
    const char* story = "story"; 

    IFIHost host;
    SDLHandler h;
    IFI::Ctx   ifiCtx;

    char            _guiInputBuf[256];
    bool            _guiInputReady = false;
    History         _hist;
    ImText          _mainText;
    bool            _loading = false;

    int             _loadSz = 0;
    char*           _loadData = 0;

    Transcript*     _transcript = 0;
    WordStat        _wstat;

    // optional metadata for all images
    ImagesMeta      _imMeta;
    std::mutex      _lock;

    VNMan           _vn;
    int             _inputCounter = 0;

    // used for aspect ratio
    int             _appWidth = 0;
    int             _appHeight = 0;

    Roster&         roster() { return h._roster; }

    void resetAll()
    {
        _hist.clear();
        _mainText.clear();

        // drop the client
        if (ifi)
        {
            LOG2("deleting ifi client", "");
            delete ifi;
            ifi = 0;
        }

        h.resetAll();
    }

    static void _fetch_done(FetchInfo* fo, void* ctx)
    {
        StrandCtx* sctx = (StrandCtx*)ctx;
        sctx->fetchLoaded(fo);
    }

    void fetchLoaded(FetchInfo* fo)
    {
        string name = fo->_name;
        
        if (*fo)
        {
            LOG2("loaded ", name);
            _loadData = fo->donate(_loadSz);
        }
        else
        {
            LOG1("FAILED loading ", name);
        }
        
        _loading = false;
    }

    char* fetchLoad(const char* name, int& size)
    {
        // synchronous load
        // this is only used for files loaded via game callback
        // eg. story.stz and not including init.kl

        assert(!_loading);

#ifndef COOP
        if (!fetcher.valid())
        {
            // need to initialise in the thread if needed
            fetcher.init("ifisdl");
        }
#endif

        string fname = fullPath(name);
        
        FDLoad fd(fname, false); // do not convert dos lines!
        if (fd)
        {
            //LOG3("fetchLoad local ", fname);
            unsigned int sz = fd._size;
            unsigned char* d = fd.donate();

            // omit label check;
            int sc = DescrambleAll(d, sz); // return scramble code

            bool notScrambled = sc == SCRAMBLE_FAIL_ID;
            bool ok = sc == 0 || notScrambled;

            if (ok)
            {
#ifdef KEYONLY
                // prevent loading on non-scrambled stories
                if (notScrambled) ok = false;
#else              
                if (notScrambled)
                {
                    LOG2("Loaded local ", fname << " size " << sz);
                }
                else
                {
                    LOG2("Descrambled local ", fname << " size:" << sz);
                }
#endif                

                size = _loadSz = sz;
                _loadData = (char*)d;
            }

            if (!ok)
            {
                LOG2("Load local descramble failed ", fname);
                delete d;
            }
        }
        else
        {
            //LOG3("fetchLoad fetcher ", name);
            
            // these are loads made by the engine
            // mark them as small since it does not load media
            if (!fetcher.start(name, _fetch_done, true, 0, this))
            {
                LOG1("ERROR fetchload start failed ", name);
            }
            else
            {
                _loading = true;
                _loadSz = 0;
                _loadData = 0;

                while (_loading)
                {
                    if (ifiCtx._p) (ifiCtx._p)();
                    else
                    {
                        SLEEP;
                        fetcher.poll(); // pump
                    }
                    size = _loadSz;
                }
            }
        }
        
        return _loadData;
    }

    StrandCtx() : h(&host)
    {
        _mainText.setImagesMeta(&_imMeta);
        _vn.setImagesMeta(&_imMeta);

        using namespace std::placeholders;
#ifdef COOP
        // when using threads, there is no pump
        ifiCtx._p = std::bind(&SDLHandler::pumpfn, &h);
#endif        
        ifiCtx._loader = std::bind(&StrandCtx::fetchLoad, this, _1, _2);
    }

    const char* getTitle() const { return h._title.c_str(); }

    void sendCmd(const char* s)
    {
        // command `s' but sometimes we wish to display this instead
        // For example, choices are entered as numbers, but we want to
        // display the actual choice text.
        
        // mark text as no longer fresh
        _mainText.seenAllText(false);
        
        // drop out of any image focus
        _mainText.setViewImage(0);  // cancel

        // and cancel any visual zoom
        _vn.resetZoomVN();

        // count the total number of commands sent
        incInputCounter();
        
        strcpy(_guiInputBuf, s);

        // add to command history unless special command
        if (*s != '_')
            _hist.add(_guiInputBuf);
        
        _guiInputReady = true;
    }

    bool yieldCmd(string& cmd)
    {
        // called from strand pumpfn for input
        bool r = false;

        // do we have a click command from the text?
        cmd = getClickCommand();  
        
        if (!cmd.empty())
        {
            clearClickCommand();
            
            _mainText.seenAllText(true);
            //_mainText.disableToCurrentPage(true);
            
            r = true;

            // XX HACK
            AudioInfo ai;
            ai._name = "audio/click.ogg";
            ai._vol = 50;
            ai._chan = 9;
            play_audio(ai);
        }
        else if (_guiInputReady)
        {
            r = true;
            cmd = _guiInputBuf;
            _guiInputReady = false;
        }
        return r;
    }

    bool init(SDLHandler::TextEmitter& e, const char* storyname)
    {
        assert(!ifi);
        
        if (storyname && *storyname) story = storyname;

        // this creates KLClient
        ifi = IFI::create(&ifiCtx);
        if (!ifi)
        {
            LOG1("Failed to create IFI ", story);
            return false;
        }

        h.setProp(IFI_CONFIGDIR, configdir);
        h.setProp(IFI_DATADIR, datadir);
        h.setProp(IFI_STORY, story);

        h._emitter = e;

        host.setHandler(&h);
        host.setIFI(ifi);

        // initial json tells back-end various directories & story
        GrowString js;
        h.buildJSONStart(js);
        h.buildInitialJSON();
        h.buildJSONEnd();
    
        // plug the host handler into the client
        ifi->setEmitter(&IFIHost::emitter, &host);
    
        // start the back-end
        static const char* argv[4];
        argv[0] = "moon";
        argv[1] = "-e";
        argv[2] = js.start();
        argv[3] = 0;

        LOG3("IFIConsole, sending boot json, ", js.start());
        ifi->start(3, (char**)argv);

        // perform initial sync to allow game to start
        if (!host.sync())
        {
            LOG1("IFIConsole, client not running; ", js.start());
            delete ifi;
            return -1;
        }
    
        host.release();
        h._startDone = true;

        // we guarantee the back-end will receive some prologue json
        // *before* any commands. This allows the back-end to get ready
        // after it has been started, but *also* to allow the back-end to
        // hold off from properly starting until the first eval (if necessary).
        h.buildJSONStart(js);
        h.buildPrologueJSON();
        h.buildJSONEnd();

        LOG3("IFIConsole, sending start json, ", js.start());        
        if (host.eval(js.start()))
        {
            // allow it to process
            host.syncRelease();
        }

        /* some games wait until they've received a {begin:true} code */
        const char* beginjs = "{\"" IFI_BEGIN "\":true}";
        LOG3("Sending ifi begin, ", beginjs);
        if (host.eval(beginjs)) host.syncRelease();
        return true;
    }

    void finish()
    {
        // will stop client thread and release engine
        delete ifi;

        // finish any remaining output
        host.drainQueue();

        LOG3("IFIConsole, ", "finished");
    }

    void autoSave()
    {
        LOG2("auto save ", _inputCounter);
        requestSave(SDLHandler::autoSaveFilename());
    }

    void incInputCounter()
    {
        ++_inputCounter;

        if (_inputCounter % 5 == 0)
        {
            // once we start auto-saving we continue
            extern bool startUnsaved;
            startUnsaved = false;

            autoSave();
        }
    }

    bool isSaveClear() const
    {
        return h._requestSaveFile.empty();
    }

    void requestSave(const string& name)
    {
        if (!isSaveClear())
        {
            LOG1("WARNING save not clear ", h._requestSaveFile);
        }
        
        h._requestSaveFile = name;
        
        LOG2("UI requesting save, ", h._requestSaveFile);

        GrowString js;
        h.buildJSONStart(js);
        JSONWalker::addBoolValue(js, IFI_SAVEDATA, true);
        h.buildJSONEnd();

        // send to back end via host.
        // goes to KL ifiSaveData which sends data back to front-end
        // arrives in ifiSave here
        host.eval(js.start());
    }

    bool requestLoad(const string& name)
    {
        LOG2("UI requesting load, ", name);

        h._requestLoadFile = name;

        // call our handler directly, this will load and
        // send data to backend
        return h.ifiLoadData(name);
    }

    void requestRestart()
    {
        LOG2("UI requesting restart ", 0);

        GrowString js;
        h.buildJSONStart(js);
        JSONWalker::addBoolValue(js, IFI_RESTART, true);
        h.buildJSONEnd();

        // send to back end via host
        host.eval(js.start());
    }

    void requestSubcommand(const string& term)
    {
        LOG2("UI requesting subcommand, ", term);

        GrowString js;
        h.buildJSONStart(js);
        JSONWalker::addStringValue(js, IFI_SUBCOMMAND, term);
        h.buildJSONEnd();

        // send to back end via host
        host.eval(js.start());
    }

    void enableCommandChoices(bool v)
    {
        //LOG1("enable command choices ", v);

        // This feature controls whether commands get elevated to choice

        // XX NB: these are terms directly in core.str
        const char* term = v ? "CHOICE_OPT_ON" : "CHOICE_OPT_OFF";
        requestSubcommand(term);
    }

    bool restoreImageMeta(const char* data)
    {
        bool r = _imMeta.restoreFromJson(data);

        if (r)
        {
            //LOG1("loaded images meta ", _imMeta);
        }
        else
        {
            LOG1("Failed to parse image meta json ", data);
        }
        return r;
    }

    string getClickCommand()
    {
        // from either main text or VN
        string s = _mainText.getClickCommand(); 
        if (s.empty())
            s = _vn.getClickCommand();

        // otherwise vn click waits for next time
        return s;
    }

    void clearClickCommand()
    {
        // from either main text or VN
        _mainText.clearClickCommand();
        _vn.clearClickCommand();
    }

    void setAppSize(int w, int h)
    {
        _appWidth = w;
        _appHeight = h;
        //LOG1("change app size ", w << "x" << h);
        _vn.setAppSize(w, h);
    }

};
