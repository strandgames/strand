//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <iostream>
#include <vector>
#include <list>
#include <assert.h>
#include <math.h>

#include "textflowi.h"
#include "utf8.h"
#include "utils.h"
#include "strutils.h"
#include "logged.h"
#include "ti.h"

struct Range
{
    int         _start;
    int         _end;

    Range() : _start(0), _end(0) {}
    Range(int s, int e) : _start(s), _end(e) {}

    bool isEmpty() const { return _start >= _end; }
    int size() const { return _end - _start; }

    Range intersection(const Range& other) const
    {
        return Range(u_max(_start, other._start), u_min(_end, other._end));
    }

    bool intersects(const Range& r) const
    { return _start < r._end && r._start < _end; }

    bool contains(int p) const {return p >= _start && p < _end; }

    void operator+=(int d)
    { _start += d; _end += d; }

    void operator-=(int d)
    { _start -= d; _end -= d; }

    Range operator+(int d) const
    {
        Range r(*this);
        r += d;
        return r;
    }

    friend std::ostream& operator<<(std::ostream& os, const Range& r)
    { return os << '[' << r._start << '-' << r._end << ']'; }
};

struct TextTraits
{
    typedef StrandCommon::Rect          TextBox;
    typedef Range                       TextRange;
    typedef std::string                 string;

    // layout of part of a section
    struct TextLayout
    {
        ~TextLayout()
        {
            //delete _boxMaskLayout;
        }
        
        TextRange           _span;
        TextBox             _box;

        int                 _line = 0; // not yet assigned
        int                 _page = 0;

        // sections can be disabled, eg not fresh
        // but pagination can split diabled within a section
        // down to the layout.
        bool                _disabled = false;

        // when not disabled scroll means to scroll and not
        // paginate. this is used after clicks that release "more"
        bool                _scroll = false;

        // number of newlines on end of box
        int                 _newlines = 0;

        // was this text originally followed by a space (not newline)
        bool                _endSpace = false;

        // eg hyphen
        bool                _softBreak = false;

        // when we layout using an object mask, this is translated
        // to screenspace, translated and padded.
        // keep a copy here
        // NB: region owned
        // XX do we need to keep this anymore??
        //ST::Region*         _boxMaskLayout = 0;

        bool valid() const { return !_span.isEmpty(); }

        friend std::ostream& operator<<(std::ostream& os, const TextLayout& l)
        { return os << '{' << l._span << ':' << l._box << '}'; }
    };

    typedef std::list<TextLayout>       layoutT;
};


struct TextSection: public TextTraits
{
    /* run of text with the same set of properties */
    
    TextSection() {}
    TextSection(const string& s, const TextProperties& p)
        : _text(s), _props(p) {}

    // length of text in bytes
    int       size() const { return _text.size(); }

    StrandCommon::Rect      extent() const
    {
        // find layout box for whole section 
        StrandCommon::Rect box;
        for (auto& l : _layout)
            if (l._line) box = box.combine(l._box);
        
        return box;
    }

    StrandCommon::Rect      extent(int maxpage) const
    {
        // find layout box for whole section 
        StrandCommon::Rect box;
        for (auto& l : _layout)
            if (l._line && l._page <= maxpage) box = box.combine(l._box);
        
        return box;
    }

    const TextLayout* firstLayout() const
    {
        if (_layout.empty()) return 0;
        return &_layout.front();
    }

    TFont& font() { return *_props._font; }

    string              _text;
    TextProperties      _props;

    // for links etc.
    string              _altText;

    // opaque reference back to the originator of this text or object
    void*               _ref = 0;

    // if we are an object, this is the box
    // with and height are the extent and (x,y) is any offset
    StrandCommon::Rect  _boxPos;
    BoxAlign            _boxAlignV;
    BoxAlign            _boxAlignH;
    float               _boxScale;  // screenspace scaling of original

    // if our box object has a custom shape
    // NB: this points into the metadata
    const ST::Region*   _boxMask = 0;

    // when calculated, this is the layout of `text' in this section
    layoutT             _layout;

    friend std::ostream& operator<<(std::ostream& os, const TextSection& s)
    { return os << s._text; }
};


struct TextFlow: public TextTraits
{
    /* text layout engine for a set of sections */
    
    typedef std::list<TextSection*>     Sections;

    ~TextFlow() { _purge(); }

    bool isEmpty() const { return _sections.empty(); }

    void addText(const string& t, const TextProperties& tp, void* ref)
    {
        //LOG1("addText '", t << "' " << tp);
        assert(t.size() > 0);
        TextSection* ts = new TextSection(t, tp);
        ts->_ref = ref;
        _sections.push_back(ts);
    }

    void addTextAlt(const string& t, const TextProperties& tp,
                    const string& alt, void* ref)
    {
        TextSection* ts = new TextSection(t, tp);
        ts->_altText = alt;
        ts->_ref = ref;
        _sections.push_back(ts);
        //LOG1("addTextAlt '", ts->_text << "' alt: " << ts->_altText);
    }

    void addObject(const StrandCommon::Rect& pos, void* ref,
                   BoxAlign ah, BoxAlign av,
                   float scale,
                   bool disabled,
                   const ST::Region* mask)
    {
        TextSection* ts = new TextSection(string(), TextProperties());
        assert(pos);
        assert(!mask || !mask->isEmpty());

        ts->_boxPos = pos;
        ts->_boxAlignH = ah;
        ts->_boxAlignV = av;
        ts->_ref = ref;
        ts->_boxMask = mask;    // not owned
        ts->_boxScale = scale;
        ts->_props._canhover = true;
        ts->_props._disabled = disabled;
        _sections.push_back(ts);
    }

    void setRegion(const ST::Region& r)
    {
        // set the region to flow text.
        _region = r;
    }

#if 0    
    void setBackgroundRegion(const ST::Region* r)
    {
        if (r)
        {
            _regionBackground = *r;
            //LOG1("set background region to ", _regionBackground);
        }
        else
        {
            // otherwise clear it
            _regionBackground.reset();
        }
    }
#endif    

    void clear()
    {
        _purge();
    }

    struct iterator
    {
        iterator(TextFlow& tf) : _host(tf) { _init(); }
        bool    valid() const { return _s != 0; }

        operator bool() const { return valid(); }
        
        void operator=(const iterator& i)
        {
            _host = i._host;
            _si = i._si;
            _li = i._li;
            _s = i._s;
            _cc = i._cc;
        }

        bool operator==(const iterator& i) const
        { return (_s == i._s && _li == i._li); }

        bool operator!=(const iterator& i) const
        { return (_s != i._s || _li != i._li); }
        

        // Assume valid
        TextLayout&             layout() { return *_li; }
        TextSection*            section() const { return _s; }
        int                     count() const { return _cc; }

        // return current span
        TextRange              span() const
        {
            return (*_li)._span + _cc;
        }

        char                   spanLastChar() const
        {
            return _s->_text[(*_li)._span._end-1];
        }

        void next()
        {
            if (_s)
            {
                if (++_li == _s->_layout.end())
                    _bumpS();
            }
        }

        void nextSection()
        {
            if (_s) _bumpS();
        }
        
        string getText() const
        {
            return _host.getTextInRange(span());
        }

        int top() const
        {
            //return logical top
            // ASSUME valid
            
            TextLayout& tl = *_li;
            int y = tl._box._y;
            if (_s->_boxPos)
            {
                // adjust for box y top padding
                y -= _s->_boxPos._y;
            }
            assert(y >= 0);
            return y;
        }

        int bottom() const
        {
            TextLayout& tl = *_li;
            return tl._box.bottom();
        }

    private:

        void _bumpS()
        {
            // move to next section
            _cc += _s->size();
            _s = 0;
            while (++_si != _host._sections.end())
            {
                TextSection* s = *_si;
                _li = s->_layout.begin();                        
                if (_li != s->_layout.end())
                {
                    _s = s;
                    break;
                }
            }
        }
            
        void _init()
        {
            _cc = 0;
            _s = 0;
            _si = _host._sections.begin();
            _initL();
        }
            
        void _initL()
        {
            if (_si != _host._sections.end())
            {
                _s = *_si;
                _li = _s->_layout.begin();
                if (_li == _s->_layout.end()) _s = 0;
            }                
        }

        TextFlow&                               _host;
        Sections::iterator                      _si;
        TextSection::layoutT::iterator          _li;
        TextSection*                            _s;
        int                                     _cc;
    };

    string getText() const
    {
        string s;
        for (auto si : _sections) s += si->_text;
        return s;
    }

    int size() const
    {
        int sz = 0;
        for (auto si : _sections) sz += si->size();
        return sz;
    }

    string getTextInRange(const TextRange& range) const
    {
        string t;
        if (!range.isEmpty())
        {
            int cc = 0;
            for (auto s : _sections)
            {
                int c1 = s->size();
                TextRange span(cc, cc + c1);
                TextRange cr = span.intersection(range);
                if (!cr.isEmpty())
                {
                    cr -= cc;
                    t += s->_text.substr(cr._start, cr.size());
                }
                cc += c1;
            }            
        }
        return t;
    }

    TextSection* getSectionAt(int x, int y) 
    {
        TextSection* s = 0;
        for (iterator it(*this); it; it.next())
        {
            //TextSection* s = it.section();
            const TextLayout& l = it.layout();
            if (l._box.within(x, y))
            {
                s = it.section();
                break;
            }
        }
        return s;
    }

    void performLayout()
    {
        //TIMER("performlayout");
        
        // first clear old layout
        for (auto si : _sections) si->_layout.clear();
        _makeWordBoxes();
        
        _performLayout();

        //dumpLayout();
    }

    void reperformLayout()
    {
        for (iterator it(*this); it; it.next())
        {
            TextLayout& tl = it.layout();
            tl._box._x = 0;
            tl._box._y = 0;
            tl._line = 0;
            tl._page = 0;
        }
        
        _performLayout();
    }

    StrandCommon::Rect extent() const
    {
        // extent up to and including the current render page
        StrandCommon::Rect r;
        for (auto si : _sections)
            r = r.combine(si->extent(_currentRenderPage));

        if (r)
        {
            //expand by any pad bottom to include padding below objects
            r._h += _info._objPadBox._h;

            // additional black space to be added
            // this is used to make the extent a specific size
            // eg dialog view
            r._h += _footpadding;

            // any top space considered part of our extent
            if (r._y > 0)
            {
                r._h += r._y;
                r._y = 0;
            }
        }
        return r;
    }

    void renderPages()
    {
        // render text on currentRenderPage
        // ignore box objects
        
        TextSection* slast = 0;

        for (iterator it(*this); it;)
        {
            TextSection* s = it.section();
            const TextLayout& l = it.layout();

            // ignore box objects
            bool ok = !s->_boxPos;

            // render all pages up to current
            if (l._page > _currentRenderPage) ok = false;
            
            if (ok)
            {
                if (s != slast)
                {
                    // apply clipping to whole sections
                    slast = s;
                    auto srect = s->extent();
                    if (s->font().clipped(srect))
                    {
                        //LOG1("### clip text '", s->_text << "'");
                        it.nextSection();
                        continue;
                    }
                }

                if (l._line)
                {
                    TextProperties tp = s->_props;
                    if (l._disabled) tp._disabled = true;

                    // NB: if scroll, leave look like enabled

                    if (_vnmode && tp._disabled)
                    {
                        // consider fading text. text fade
                        // The text box (lbox) is relative to the current
                        // text box view (viewbox) which has already been
                        // adjusted for scroll-y.
                        if (_textOverlap > 0)
                        {
                            // centrey of box from top of text box
                            int boxy = (l._box.top() + l._box.bottom())/2;
                            float ty = boxy - _viewBox[PY1];
                            if (ty < 0) ty = 0;

                            //LOG1("overlap ", textOverlap << " text box dy " << ty);
                            float dov = _textOverlap - ty;
                            if (dov > 0)
                            {
                                dov = dov/_textOverlap; // overlap fraction

                                //LOG1("overlap ", dov << " alpha " << 1 - dov);
                                dov = 1 - dov*dov;
                                
                                // text overlaps, fade it
                                tp._disCol = tp._disCol.mixAlpha(dov);
                            }
                            
                        }
                    }
                    
                    s->font().render(it.getText(), l._box, tp);
                }
            }
            it.next();
        }
    }

    static int regionMinx(const ST::Region& r)
    {
        int minx = -1;
        
        int nt = r.size();
        if (nt > 0)
        {
            minx = r[0].x1;
            for (int i = 1; i < nt; ++i)
            {
                const RRect& ri = r[i];
                if (ri.x1 < minx) minx = ri.x1;
            }
        }
        return minx;
    }

    static int regionMinxOver(const ST::Region& r, int m)
    {
        // min x of all boxes > m
        
        int minx = m;
        
        int nt = r.size();
        for (int i = 0; i < nt; ++i)
        {
            const RRect& ri = r[i];
            if (ri.x1 > m && (minx == m || ri.x1 < minx)) minx = ri.x1;
        }
        return minx;
    }

    static int regionMaxxUnder(const ST::Region& r, int m)
    {
        int maxx = m;
        
        int nt = r.size();
        for (int i = 0; i < nt; ++i)
        {
            const RRect& ri = r[i];
            if (ri.x2 < m && (maxx == m || ri.x2 > maxx)) maxx = ri.x2;
        }
        return maxx;
    }

    int skipSpaceNL(Utf8& u)
    {
        // skip whitespace but find out if it contains a newline
        int nl = 0;
        for (;;)
        {
            uint c = u.get();
            if (!c) break;
            if (Utf8::isNL(c)) ++nl;
            else if (!Utf8::_isspace(c))
            {
                u.backup();
                break;
            }
        }
        return nl;
    }

    int lineHeight(TFont* f) const
    {
        assert(f);
        
        int th = ROUND(f->getHeight());

        th -= _info._squeezeLines;
        if (th < 2) th = 2;  // min gap
        
        return th;
    }

    int blankHeight(int th, int n) const
    {
        // calculate height for `n'` lines each of `th'
        
        if (_info._enableFullBlanks) return th*n;
        
        int h = 0;
        if (n)
        {
            h = th;

            if (n > 1)
            {
                // make the gap smaller
                h += (n-1)*(th/2);
                //LOG1("blank height ", h << " n=" << n);
            }
        }
        return h;
    }

    int lineHeight(TextSection* s) const
    {
        return lineHeight(s->_props._font);
    }

    void _makeWordBoxes()
    {
        // break each section into breakable words
        // NB: need to add break/nobreak flags for the words

        for (auto si : _sections)
        {
            if (si->_boxPos)
            {
                TextLayout tlay;
                
                // initially, the box is empty
                si->_layout.push_back(tlay);
                continue;
            }
            
            const char* s0 = si->_text.c_str();
            
            Utf8 us(s0);
            for (;;)
            {
                int newlines;
                bool endspace = false;
                bool softbreak = false;
                
                newlines = skipSpaceNL(us);

                if (newlines)
                {
                    // started with newlines.
                    // need to add a special box for this
                    TextLayout tlay;
                    
                    // if we ended with a newline then remember
                    // the end of the logical line
                    tlay._newlines = newlines;
                    si->_layout.push_back(tlay);
                }

                newlines = 0;

                Utf8 ue = us;
                Utf8 unext;
                uint c;

                while ((c = ue.get()) != 0)
                {
                    if (Utf8::_isspace(c))
                    {
                        ue.backup();
                        unext = ue;
                        
                        // eat any whitespace
                        newlines = skipSpaceNL(unext);
                        if (!newlines) endspace = true;
                        break;
                    }
                    else if (c == '-')
                    {
                        // separate word boxes around hyphen
                        unext = ue;
                        softbreak = true;
                        break;
                    }
                }

                int len = ue._s - us._s;
                if (len > 0)
                {
                    TextLayout tlay;
                    uint tpos = us._s - s0;
                    tlay._span = TextRange(tpos, tpos + len);

                    string ti = si->_text.substr(tpos, len);

                    int tw = ROUND(si->font().getStringWidth(ti));
                    int th = lineHeight(si);

                    // LOG1("string width of '", ti << "' " << tw);

                    // initially the box is just a size but no position
                    tlay._box = TextBox(0, 0, tw, th);

                    //int ah = ROUNDUP(si->font().getAscent());
                    //tlay._baseLine = ah;

                    // if we ended with a newline then remember
                    // the end of the logical line
                    tlay._newlines = newlines;
                    tlay._endSpace = endspace;
                    tlay._softBreak = softbreak;
                    si->_layout.push_back(tlay);
                }

                if (!c) break;
                us = unext;
            }
        }
    }

    int findLineStartX(int x, int y, int h)
    {
        int sx;
        int yh = y + h - 1; 

        for (;;)
        {
            RRect* rp;
            bool r = _region.pointInside(x, y, &rp);

            if (r)
            {
                sx = x;
            }
            else
            {
                // is y in the region?
                if (rp && rp->y1 <= y && rp->y2 > y)
                {
                    if (rp->x1 > x)
                    {
                        sx = rp->x1;
                        r = true;
                    }
                }
            }

            if (!r)
            {
                sx = -1;
                break; // fail
            }

            // sx is a valid start point to the right of (x,y)

            // bottom of rect with (sx,y)
            int y2 = rp->y2;
            
            if (y2 > yh) break; // bottom contains height, so ok

            // need to locate from a new point just outside
            // of current rect, but inside our height
            x = sx;
            y = y2;
        }

        return sx;
    }
    
    bool _performLayoutObject(iterator ij, int x, int y)
    {
        // place a box
        bool fits = false;
        
        TextSection* s = ij.section();
        TextLayout& l = ij.layout();
        
        assert(s->_boxPos);

        // the box includes borders
        int bw = s->_boxPos._w;
        int bh = s->_boxPos._h;

        //int border = _info._objBorderSize;
        int topPad = _info._objPadBox._y;
        int botPad = _info._objPadBox._h;
        int rpad = _info._objPadBox._w;

        const RRect& bounds = _region.extent();

        if (bw + rpad > bounds.width() || bh + topPad + botPad > bounds.height())
        {
            LOG2("WARNING: box cannot fit in bounds, size ", s->_boxPos);
            l._box.clear();
            assert(!s->extent());
            return false;
        }

        // we are an object
        int bx;
        int by = y + s->_boxPos._y ; // add any box y displacement

        by += topPad; // and any top padding
        
        assert(s->_boxAlignH == boxalign_left || s->_boxAlignH == boxalign_right);
        // find place for box
        while (by + bh + botPad <= bounds.y2)
        {
            //LOG1("trying box, size ", s->_boxPos << " at " << by);

            // horizontal slice
            ST::Region br(RRect(bounds.x1, by, bounds.width(), bh));
            ST::Region slice;
            ST::Region::IntersectRegion(&slice, &_region, &br);

            int minx = bounds.x1 - 1;
            int maxx = bounds.x2 + 1;

            while (!fits)
            {
                if (s->_boxAlignH == boxalign_left)
                {
                    // try moving right along slice for next min
                    // must be > minx
                    bx = regionMinxOver(slice, minx);

                    if (bx <= minx) break; // no more
                                
                    minx = bx;

                    bx += s->_boxPos._x; // offset?
                    fits = slice.contains(RRect(bx, by, bw, bh));
                }
                else 
                {
                    // boxalign_right

                    // move left along slice for next max
                    // must be < maxx
                    bx = regionMaxxUnder(slice, maxx);
                    if (bx >= maxx) break;

                    maxx = bx;

                    bx -= s->_boxPos._w;
                    fits = slice.contains(RRect(bx, by, bw, bh));
                }
            }

            // the box will remain empty if it didnt fit
            if (fits)
            {
                l._box._x = bx;
                l._box._y = by;
                l._box._w = bw;
                l._box._h = bh;
                l._line = _currentLine;
                l._page = _currentLayoutPage;
                l._disabled = s->_props._disabled;
                l._scroll = s->_props._scroll;

                // arrange the box mask to flush left
                if (s->_boxMask && s->_boxAlignH == boxalign_left)
                {
                    // this is the mask from meta
                    //assert(!l._boxMaskLayout);
                    
                    ST::Region* r = new ST::Region;

                    // need to scale to screenspace
                    s->_boxMask->scaled(*r, s->_boxScale, s->_boxScale);

                    r->fillHoles();

                    // move to layout box position
                    r->translate(bx, by);

                    // widen left to zero
                    r->expandMinMax(0,0);

                    //LOG1("box mask left ", *r);

                    // add add padding to space the text
                    r->expandBy(rpad, 0);

                    //LOG1("performlayout using image mask ", r);
                    _region -= *r;

                    // given to layout and deleted later
                    //l._boxMaskLayout = r; // consume
                    delete r;
                }
                else
                {
                    // remove the placed area from the text region
                    RRect bi(bx, by, bw + rpad, bh);
                    _region -= bi;
                }
                break;
            }

            
            // move down to another slice
            by += lineHeight(s); // XX move whole lines??
            bx = bounds.x1;
        }
                    
        if (fits)
        {
            //LOG1("box fitted, size ", s->_boxPos);
        }
        else
        {
            l._box.clear();
            assert(!s->extent());
            LOG2("WARNIG: box did not fit, size ", s->_boxPos);
        }
        
        return fits;
    }

    void _performLayoutObjectVN(iterator ij, int x, int y)
    {
        TextSection* s = ij.section();
        TextLayout& l = ij.layout();
        
        assert(s->_boxPos);

        // box includes borders
        int bw = s->_boxPos._w;
        int bh = s->_boxPos._h;

        l._box._x = x;
        l._box._y = y;
        l._box._w = bw;
        l._box._h = bh;
        l._line = _currentLine;
        l._page = _currentLayoutPage;
        l._disabled = s->_props._disabled; // XX are these ever disabled?
        l._scroll = s->_props._scroll;
        
        //LOG1("layout VN object at ", l._box);
    }

#ifdef LOGGING
    void dumpLayout()
    {
        LOG2("text layout in ", _region);
        iterator it(*this);
        while (it)
        {
            TextSection* s = it.section();
            const TextLayout& l = it.layout();
            if (!s->_boxPos)
            {
                StrandCommon::Rect srect;
                string si = it.getText();
                LOG2(si, ' ' << l._box << " line:" << l._line << " page:" << l._page);
            }
            else
            {
                LOG2("box ", l._box << " line:" << l._line << " page:" << l._page);
            }
            it.next();
        }
    }
#endif

    void _performLayout()
    {
        _currentLayoutPage = 1;

        // clear any footer padding, which if needed, is added after layout
        _footpadding = 0;

        // lines start from 1, zero means not placed
        // subsequent pages continue the line number, not reset it.
        _currentLine = 1;

        // vn mode will make multiple pages, otherwise we'll get just one
        while (!_performLayoutPage())
        {
            ++_currentLayoutPage;

            //LOG1("performlayout, doing page ", _currentLayoutPage);

            // emergency!
            if (_currentLayoutPage > 10)
            {
                LOG2("performlayout, too many pages ", _currentLayoutPage);
                break;
            }
        }

        // remember how many pages
        _pageCount = _currentLayoutPage;

        //LOG1("performlayout page count ", _pageCount);
    }

    bool _performLayoutPage()
    {
        // return true if we layout all the text.
        // false if we run out of space. But in that case, some of the
        // text will be laid out. Those have been assigned a line number.
        
        bool res = true;

        // assume we have word boxes
        RRect bounds = _region.extent();

        int x = bounds.x1;
        int y = bounds.y1;

        // remember the bottom of the last object placed
        int lastObjBaseY = -1;  // not set
        
        bool fits;

        iterator it(*this);

        // find the first non layout word.
        // this is when we are laying out a subsequent page
        while (it.valid())
        {
            TextLayout& tl = it.layout();
            if (!tl._line) break;
            it.next();
        }

        // all vertical objects are on page 1
        if (_currentLayoutPage == 1)
        {
            // make an initial pass to place all vertically aligned boxes
            // these will be totally ignored by the text placement loop
            iterator ij = it;

            while (ij.valid())
            {
                TextSection* s = ij.section();

                if (s->_boxPos)
                {
                    //if (!_vnmode)
                    {
                        if (s->_boxAlignV == boxalign_top)
                        {
                            // will fit at top and remove chunk from region
                            _performLayoutObject(ij, x, y);
                        }
                    }
                }
                ij.next();
            }
        }

        // at the start of a line
        while (it.valid())
        {
            TextSection* s = it.section();
            TextLayout& l = it.layout();
            
            if (s->_boxPos) // an object
            {
                // ignore all boxes vertical aligned in the main loop
                // and all images in vn mode as these are done above
                //if (!_vnmode && s->_boxAlignV == boxalign_none)
                if (s->_boxAlignV == boxalign_none)
                {
                    // when placing an object, check to see if we have
                    // placed text already to cover it and Y is further down
                    // than the bottom of the previous object.
                    // otherwise we have to move to at least the bottom of the
                    // previous object for both text placement and the new obj

                    if (lastObjBaseY > 0)
                    {
                        // add bottom pad
                        int y1 = lastObjBaseY + _info._objPadBox._h;
                        if (y < y1) y = y1;
                    }
                
                    if (_performLayoutObject(it, x, y))
                        lastObjBaseY = l._box.bottom();
                }
                
                it.next();
                continue;
            }

            int th = lineHeight(s);

            if (!l._box)
            {
                if (l._newlines)
                {
                    // this is a pure newline box
                    x = bounds.x1;
                    y += blankHeight(th, l._newlines); 
                    ++_currentLine;
                }

                l._line = _currentLine;
                l._page = _currentLayoutPage;
                l._disabled = s->_props._disabled;
                l._scroll = s->_props._scroll;
                
                it.next();
                continue;
            }
            
            assert(s->_text.size());
            assert(l._box);

            int x1 = findLineStartX(x, y, th);

            if (x1 < x)
            {
                // we have run out of space.
                // end this page layout.
                //LOG1("cannot start line at ", x << "," << y);
                res = false;
                break;
            }

            x = x1;
            
            iterator iok = it;
            bool canbreak = false;
            int newlines = 0;

            while (it.valid())
            {
                TextLayout& l = it.layout();
                TextSection* s = it.section();

                if (s->_boxPos)
                {
                    //if (_vnmode || s->_boxAlignV != boxalign_none)
                    if (s->_boxAlignV != boxalign_none)
                    {
                        // ignore all vert aligned boxes as if not here
                        it.next();
                        continue; 
                    }
                    
                    // any objects on the line mean the end of this fitting
                    break;
                }

                // set to a possible location
                l._box._x = x;
                l._box._y = y;
                // NB: line is set later, when we know it fits

                // newlines have empty boxes
                fits = !l._box;

                if (!fits)
                {
                    RRect bi(l._box._x, l._box._y, l._box._w, l._box._h);
                    fits = _region.contains(bi);
                }

                if (fits)
                {
                    l._line = _currentLine;
                    l._page = _currentLayoutPage;
                    l._disabled = s->_props._disabled;
                    l._scroll = s->_props._scroll;
                    
                    if (l._endSpace || l._softBreak)
                    {
                        // this fits and ends in a space. So we can definitely
                        // break here if the next text does not fit
                        iok = it;
                        canbreak = true;
                    }
                }
                else
                {
                    // if we had a breakable word that fitted, go back to that
                    // and continue from the one following.
                    // otherwise go back to the start of the line and re-fit.
                    it = iok;
                    if (canbreak)
                    {
                        it.next();
                    }
                    
                    // see if there is more x space further right
                    canbreak = false;
                    assert(l._box._w > 0);

                    // XX there could be more space before the end of this word
                    x = findLineStartX(x + l._box._w, y, th);
                    if (x < 0) break; // no more space

                    //LOG1("trying further right at ", x << "," << y << " for " << getTextInRange(it.span()));
                    continue; // try again
                }

                it.next();
                
                newlines = l._newlines;
                if (newlines) break;

                x += l._box._w;

                if (l._endSpace)
                {
                    // NB: this used to be rounddown, but that causes
                    // words to be too close on small horizontal resolutions
                    int spacew = ROUND(s->font().getSpaceWidth());
                    spacew -= _info._squeezeWords;
                    if (spacew < 2) spacew = 2;  // min gap
                    x += spacew;
                }
            }

            if (!newlines) newlines = 1;

            // next line
            int objy = 0;
            int dy = blankHeight(th, newlines);
            int y1 = y + dy;

            if (lastObjBaseY > 0)
                objy = lastObjBaseY + _info._objPadBox._h; // bottom pad

            if (newlines > 1)
            {
                // when we have a blank line (or more), always start
                // from below the last object
                if (y < objy)
                {
                    if (objy > y1) y1 = objy;
                }
            }
            else if (objy)
            {
                if (y < lastObjBaseY && y1 >= lastObjBaseY)
                {
                    // text has caught up with the bottom of the previous
                    // object. Do we need padding?
                    //int d = _info._objPadBox._h - (y1 - lastObjBaseY);

                    // ensure at least pad h
                    //if (d > 0) y1 += d;
                    if (objy > y1) y1 = objy;
                }
            }

            x = bounds.x1;
            y = y1;

            // always count actual lines and not blanks
            ++_currentLine;
        }

        return res;
    }

    void _disableToPage(int page)
    {
        // seen all text up to and including `page'
        // mark layouts disabled
        // NB: the containing sections may not be all disabled

        iterator it(*this);
        while (it.valid())
        {
            TextFlow::TextLayout& tl = it.layout();
            if (tl._page > page) break; // done
            tl._disabled = true;
            it.next();
        }
    }

    void disableToCurrentPage()
    {
        _disableToPage(_currentRenderPage);
    }

    struct Pager
    {
        enum break_type
        {
            break_void = 0,
            break_blank,
            break_para,  // newline
            break_obj,
            break_count,
        };

        struct breaker
        {
            break_type  _type = break_void;
            iterator    _it;
            int         _y;

            breaker(break_type t, iterator& it, int y)
                : _type(t), _it(it), _y(y) {}

#ifdef LOGGING            
            friend std::ostream& operator<<(std::ostream& os, const breaker& b)
            {
                switch (b._type)
                {
                case break_void:
                    os << "void";
                    break;
                case break_blank:
                    os << "blank at '" << b._it.getText() << "'";
                    break;
                case break_para:
                    os << "para at '" << b._it.getText() << "'";
                    break;
                case break_obj:
                    os << "obj";
                    break;
                }
                return os << " at " << b._y;
            }
#endif            
        };

        TextFlow&       _host;
        std::vector<breaker>   _breaks;

        Pager(TextFlow& tf) : _host(tf) {}

        void clear()
        {
            _breaks.clear();
        }

        breaker* find(break_type t)
        {
            int n = _breaks.size();
            for (int i = 0; i < n; ++i)
                if (_breaks[i]._type == t) return &_breaks[i];
            
            return 0;
        }
        
        void intern(const breaker& b)
        {
            //LOG1("pager break ", b);
            
            breaker* bp = find(b._type);

            if (bp) *bp = b;
            else _breaks.push_back(b);
        }

        breaker* findBestBreak(int ptop, int vh)
        {
            breaker* best = 0;
            int ymin = 0;
            
            for (breaker& bi : _breaks)
            {
                int y = bi._it.top();
                if ((!best || y > best->_y) && y >= ymin)
                {
                    TextSection* ts = bi._it.section();
                    if (ts->_boxPos)
                    {
                        // make sure we never break within the vspan
                        // of an object.
                        ymin = bi._it.bottom();
                    }
                    else
                    {
                        // do not break at a picture otherwise we get
                        // picture without any subsequent text.
                        best = &bi;
                    }
                }
            }

            if (best)
            {
                // insist it must be at least half a page
                int y = best->_it.top();
                int dy = y - ptop;
                assert(dy >= 0);
                if (dy*2 < vh)
                {
                    //LOG1("rejecting page break ", *best);
                    best = 0;
                }
            }
            
            return best;
        }
        
    };

    typedef Pager::breaker breaker;
    typedef Pager::break_type break_type;

    bool paginate(int vh, int pageh)
    {
        // fit into either `vh` or a number of `pageh` pages.
        // return true if needed to paginate
        // if `pageh == 0` just do paginate check
        
        assert(vh >= 0);
        assert(!pageh || pageh >= vh);

        int freshy = -1;
        int freshline = 1;
        
        int pagelim = u_max(vh, pageh);
        
        // find freshy
        iterator ij(*this);
        while (ij.valid())
        {
            TextSection* ts = ij.section();
            if (!ts->_boxPos)  
            {
                TextFlow::TextLayout& tl = ij.layout();

                // box valid if not pure newline
                // skip past if scroll
                if (!tl._disabled && !tl._scroll && tl._box)
                {
                    int y = ij.top(); // logical top
                    
                    if (freshy < 0 || y < freshy)
                    {
                        freshy = y;
                        freshline = tl._line;
                    }

#if  0
                    if (freshy >= 0)
                    {
                        LOG2("freshy ", y << " at '" << (ts->_boxPos ? "box" : ij.getText()) << "'");
                    }
#endif                    

                    if (freshy >= 0 && tl._line > freshline) break; // early out
                }
            }
            ij.next();
        }

        int ptop = freshy;
        int page = 1;
        bool overflow = false;
        int lastline = 0;
        
        Pager pager(*this);
        
        iterator it(*this);
        iterator lastlineit = it;
                
        bool started = false;
        while (it.valid())
        {
            TextFlow::TextLayout& tl = it.layout();
            TextSection* ts = it.section();

            if (!started)
            {
                if (!tl._disabled && !tl._scroll)
                {
                    lastline = tl._line;
                    lastlineit = it;
                    started = true;
                }
            }

            if (started)
            {
                int y = it.top();
                StrandCommon::Rect b = tl._box;
                // pure newline can occur before top of page
                if (y >= ptop)
                {
                    int dy = b.bottom() - ptop;

                    if (dy > vh)
                    {
                        // page has exceeded view with choices+input
                        // but not necessarily the "more" page.
                        overflow = true;
                        if (!pageh) return true; // needs paging
                    }

                    if (lastline != tl._line)
                    {
                        lastline = tl._line;
                        if (!ts->_props._underline) lastlineit = it;
                    }

                    if (dy <= pagelim)
                    {
                        // same page
                        // are we a break candidate?

                        if (ts->_boxPos)
                        {
                            pager.intern(breaker(Pager::break_obj, it, y));
                        }
                        else if (tl._newlines > 0)
                        {
                            pager.intern(breaker((tl._newlines > 1 ? Pager::break_blank : Pager::break_para), it, y));
                        }
                    }
                    else
                    {
                        // next page
                        ++page;
                        overflow = false;

                        breaker* best = pager.findBestBreak(ptop, pageh);
                        if (best)
                        {
                            //LOG1("breaking page at ", *best);

                            // best will previously have page assigned
                            // next after best will be assigned next page

                            it = best->_it;
                            it.next();

                            // logical top of next item
                            if (it.valid()) ptop = it.top(); 
                            pager.clear();
                            continue;
                        }
                        else
                        {
                            pager.clear();
                            if (ts->_props._underline)
                            {
                                assert(lastlineit);
                                it = lastlineit;

                                // go back to line start and reassign
                                // from new page
                                
                                ptop = it.top(); // logical top of next item

                                // will redo `it` and assign new page
                                // then continue
                                continue;
                            }

                            ptop = y;
                        }
                    }
                }
                tl._page = page;
            }
            it.next();
        }

        if (overflow)
        {
            // this happens when we have no more text but we need to
            // display "continue" otherwise the choices wont fit.
            ++page;
            //LOG1("paginate more overflow ", page);
        }
        
        _pageCount = page;
        //LOG1("paginated pages ", _pageCount);

        return page > 1;
    }

    bool onLastPage() const
    {
        // turns out it CAN be > .
        // this happens when resizing the window
        return _currentRenderPage >= _pageCount;
    }

    void bumpCurrentPage()
    {
        assert(_currentRenderPage < _pageCount);
        ++_currentRenderPage;
    }

    void resetCurrentPage()
    {
        _currentRenderPage = 1;
        _pageCount = 1;
    }
    
    void _purge()
    {
        ::purge(_sections);
    }
    
    // layout region for text flow
    ST::Region          _region;

    // all text split into sections
    Sections            _sections;

    TextFlowInfo        _info;

    // to be added to the bottom of the extent
    int                 _footpadding;

    // valid during layout and also render
    int                 _currentLayoutPage;
    int                 _currentRenderPage = 1;
    int                 _pageCount;

    // only valid whilst in layout
    int                 _currentLine;

    bool                _vnmode;

    // text area. used only for text box fade
    Boxf                _viewBox;

    // only used in render to fade text overlapping pictures
    float               _textOverlap;
};

struct TextFlowBuilder: TextTraits
{
    TextFlow            _textFlow;

    TextProperties      _props;
    TextProperties      _propsBold;
    TextProperties      _propsItalic;
    TextProperties      _propsBoldItalic;
    TextProperties      _propsLink;


    typedef bool renderStartFn();
    typedef bool renderEndFn();
    std::function<renderStartFn> _renderStartSignal;
    std::function<renderEndFn> _renderEndSignal;

    typedef void renderObjFn(TextSection*);
    std::function<renderObjFn> _renderObj;

    void        fontSizeChanged()
    {
        if (_props._font) _props._font->_spaceWidth = 0;
        if (_propsBold._font) _propsBold._font->_spaceWidth = 0;
        if (_propsItalic._font) _propsItalic._font->_spaceWidth = 0;
        if (_propsLink._font) _propsLink._font->_spaceWidth = 0;
    }

    void        setFont(TFont* f)
    {
        // when changed all set to the same
        fontSizeChanged();
        
        _props._font = f;
        _propsBold._font = f;
        _propsItalic._font = f;
        _propsBoldItalic._font = f;
        _propsLink._font = f;
    }

    void        setFontItalic(TFont* f)
    {
        fontSizeChanged();
        _propsItalic._font = f;
    }
    
    void        setFontBold(TFont* f)
    {
        fontSizeChanged();
        _propsBold._font = f;
    }
            
    void        setFontLink(TFont* f)
    {
        fontSizeChanged();
        _propsLink._font = f;
    }

    void        setColour(Colour c, Colour hi)
    {
        _props._col = c;
        _propsBold._col = c;
        _propsItalic._col = c;
        _propsBoldItalic._col = c;
        _propsLink._col = c;

        _props._hiCol = hi;
        _propsBold._hiCol = hi;
        _propsItalic._hiCol = hi;
        _propsBoldItalic._hiCol = hi;
        _propsLink._hiCol = hi;
    }

    void        setDisabledColour(Colour c, Colour hi)
    {
        _props._disCol = c;
        _propsBold._disCol = c;
        _propsItalic._disCol = c;
        _propsBoldItalic._disCol = c;
        _propsLink._disCol = c;

        _props._disHiCol = hi;
        _propsBold._disHiCol = hi;
        _propsItalic._disHiCol = hi;
        _propsBoldItalic._disHiCol = hi;
        _propsLink._disHiCol = hi;
    }

    void        setLinkColor(Colour c, Colour hicol)
    {
        //LOG1("Setting link colour ", c);
        _propsLink._col = c;
        _propsLink._hiCol = hicol;
    }
    
    void clear()
    {
        _textFlow.clear();
    }

    int nominalLineHeight() const
    {
        // return the height of the default font
        // ASSUME font set 
        return _textFlow.lineHeight(_props._font);
    }

    void addObject(const StrandCommon::Rect& pos,
                   void* ref, BoxAlign ah, BoxAlign av,
                   float scale,
                   bool disabled,
                   const ST::Region* mask)
    {
        _textFlow.addObject(pos, ref, ah, av, scale, disabled, mask);
    }

    void addMarkdown(const string& s, TextProperties* tprops, void* ref)
    { addMarkdown(s.c_str(), tprops, ref); }

#define MERGE_TPROPS(_pp)               \
    TextProperties _tp = _pp;           \
    _tp._disabled = disabled;           \
    _tp._scroll = scroll;               \
    _tp._hilite = hilite
    
    void addMarkdown(const char* s, TextProperties* tprops, void* ref)
    {
        // use the standard `props`, but take certain overrides from
        // `tprops' if given. like disabled & hilite
        
        Utf8 us(s);
        Utf8 ue(s);

        Utf8 unext;
        bool endspan = false;

        TextProperties* pp = &_props; // default
        TextProperties* ppnext = pp;

        bool hilite = tprops ? tprops->_hilite : false;
        bool disabled = tprops ? tprops->_disabled : false;
        bool scroll = tprops ? tprops->_scroll : false;

        //LOG1("addMarkdown '", AddEscapes(AddCodes(s)) << "'");

        bool    inItalic = false;
        bool    inBold = false;
        
        for (;;)
        {
            uint c = ue.get();
            if (!c)
            {
                endspan = true;
            }
            else if (c == '!')
            {
                // ![](picture) ?
                if (*ue == '[')
                {
                    const char* s1 = ::parseMarkdownLink(ue._s);
                    if (s1 != ue._s)
                    {
                        // we have a valid link syntax
                        // skip all picture links in output
                        unext = s1;
                        endspan = true;
                    }
                }
            }
            else if (c == '[')
            {
                // [](link) ?
                ue.backup(); // onto '['
                
                string head, tail;
                const char* s1 = ::parseMarkdownLink(ue._s, &head, &tail);

                if (s1 != ue._s)
                {
                    // we have a valid link syntax
                    unext = s1;

                    // if a space follows the link (usual) then add it
                    // to the link text.
                    while (Utf8::_isspace(*unext))
                        Utf8::appendU(head, unext.get());
                    
                    // send what we have and continues from unext
                    int len = ue._s - us._s;
                    if (len) 
                    {
                        string s1 = string(us._s, len);
                        MERGE_TPROPS(*pp);
                        _textFlow.addText(s1, _tp, ref);
                    }
                    
                    // continue from unext
                    ue = us = unext;

                    // add head of link
                    MERGE_TPROPS(_propsLink);
                    _textFlow.addTextAlt(head, _tp, tail, ref);
                    
                }
                else ue.get(); // over '['
            }
            else if (c == '*')
            {
                // *markdown*
                unext = ue;
                endspan = true;

                // allow **
                if (*unext == '*') unext.get();

                inBold = !inBold;
                ppnext = inBold ? &_propsBold : &_props;
            }
            else if (c == '_')
            {
                // _markdown_
                unext = ue;
                endspan = true;

                // allow __
                if (*unext == '_') unext.get();

                inItalic = !inItalic;
                ppnext = inItalic ? &_propsItalic : &_props;
            }

            if (endspan)
            {
                if (c) ue.backup(); // onto the char that ended span
                
                // [us,eu] is fine to add
                int len = ue._s - us._s;
                if (len) 
                {
                    string s1 = string(us._s, len);

                    // append any following space into this span
                    if (c)
                    {
                        while (Utf8::_isspace(*unext))
                            Utf8::appendU(s1, unext.get());
                    }
                    
                    MERGE_TPROPS(*pp);
                    _textFlow.addText(s1, _tp, ref);
                }

                // continue from unext
                ue = us = unext;

                pp = ppnext;
                endspan = false;
            }

            if (!c) break;
        }
    }

    //void setRegion(const ST::Region& r, const ST::Region* rbackground = 0)
    void setRegion(const ST::Region& r)
    {
        _textFlow.setRegion(r);

        // sets or clears
        //_textFlow.setBackgroundRegion(rbackground);
    }

    void performLayout()
    {
        _textFlow.performLayout();
    }

    void reperformLayout()
    {
        // assume word boxes already present and region ok
        _textFlow.reperformLayout();
    }

    StrandCommon::Rect extent() const
    {
        return _textFlow.extent();
    }

    TextSection* updateHover(int x, int y)
    {
        TextSection* s = _textFlow.getSectionAt(x, y);

        for (auto si : _textFlow._sections)
        {
            if (si->_props._canhover)
            {
                si->_props._hilite = (s == si);
            }
        }
        return s;
    }

    TextSection* findFirstObject() const
    {
        for (auto si : _textFlow._sections)
            if (si->_boxPos) return si;

        return 0;
    }
    
    void renderPages()
    {
        // render all pages up to current render page.
        
        bool ok = true;
        int page = _textFlow._currentRenderPage;

        if (_renderStartSignal) ok = _renderStartSignal();

        if (ok)
        {
            if (_renderObj)
            {
                for (TextFlow::iterator it(_textFlow); it; it.nextSection())
                {
                    TextSection* s = it.section();
                    if (s->_boxPos)
                    {
                        const TextLayout* l = s->firstLayout();
                        bool objok = l != 0;

                        if (objok)
                        {
                            objok = (l->_page <= page);
                            if (objok) _renderObj(s);
                        }
                    }
                }
            }
            
            _textFlow.renderPages();

            if (_renderEndSignal) _renderEndSignal();
        }
    }
};


