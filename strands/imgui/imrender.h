//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include <assert.h>
#include "immeta.h"
#include "logged.h"
#include "box.h"
#include "colour.h"
#include "pbase.h" // atsimplename
#include "sokstate.h"
#include "dl.h"

#define IV2(_b) ImVec2((_b).width(),(_b).height())
#define IV2BTOP(_b) ImVec2((_b)[PX1], (_b)[PY1])
#define IV2BBOT(_b) ImVec2((_b)[PX2], (_b)[PY2])

#define IVP(_p) ImVec2(_p.x, _p.y)

#define MAX_ZOOM_FACTOR 5

extern void ResetPinch();
extern float GetPinch();
extern bool PinchActive();
extern SOKState state;

static inline bool intersecty(const Point2f& a, const Point2f& b, float y,
                              Point2f& ip)
{
    float miny, maxy;
    if (a.y <= b.y)
    {
        miny = a.y;
        maxy = b.y;
    }
    else
    {
        miny = b.y;
        maxy = a.y;
    }

    // if our y intersect is outside the line or on the ends
    // then we dont intersect.
    if (y <= miny || y >= maxy) return false;

    float dy = b.y - a.y;
    float dx = b.x - a.x;

    float x1, y1;
    if (dx)
    {
        if (dy)
        {
            float m = dy/dx;  // y = mX + c. c = y - mx.
            float c = a.y - m*a.x;
            x1 = (y - c)/m;
            y1 = m*x1 + c;
        }
        else
        {
            // line is horizontal
            x1 = a.x;
            y1 = y;

        }
    }
    else
    {
        // line is vertical
        x1 = a.x;
        y1 = y;
    }

#ifdef LOGGING
    if (y1 < miny || y1 > maxy)
    {
        LOG2("bad instersect ", y1 << " outside [" << miny << "," << maxy << "]");
        //assert(y1 >= miny && y1 <= maxy);
    }
#endif

    if (y1 < miny) y1 = miny;
    else if (y1 > maxy) y1 = maxy;

    ip = Point2f(x1, y1);

    return true;
}

static inline float hypot(const Point2f& a, const Point2f b)
{
    double dx = a.x - b.x;
    double dy = a.y - b.y;
    return (float)sqrt(dx*dx + dy*dy);
}

struct ImRender
{
    typedef std::string string;

    // set when we click on a command link
    // this is picked up by yielded
    string      _clickCommand;

    Point2f     _viewBasePos;

    bool        _dragging = false;
    float       _mouseWheel;
    ImVec2      _viewDrag0;
    ImVec2      _viewDrag1;

    ~ImRender()
    {
        clearRI();
    }

    void resetZoom()
    {
        _mouseWheel = 0;
        _viewDrag0 = _viewDrag1 = ImVec2(0,0);
        ResetPinch();
        mouseDragSum(0, 0);
    }

    struct Rotation
    {
        // base object of rotation
        bool    _baseSet = false;
        Boxf    _baseBox;
        Point2f _baseOrigin;

        operator bool() const { return _a != 0; }

        Point2f rotate(const Point2f& p) const
        { return p.rotate(_cos_a, _sin_a, _baseOrigin); }

        void setBase(const Boxf& box)
        {
            _baseSet = true;
            _baseBox = box;
            _baseOrigin = box.centre();
        }

        void setAngle(double a)
        {
            _a = a;
        }

        void init()
        {
            _cos_a = cos(_a);
            _sin_a = sin(_a);
            _baseSet = false;
        }

    private:

        double  _a = 0;
        double  _cos_a, _sin_a;
    };

    struct PinchZoom
    {
        Point2f _orig;
        float   _zoom;
        float   _dxi; // internal pre-scaled versions
        float   _dyi;
        float   _dx;
        float   _dy;
        string  _lastBasename;

        PinchZoom() { clear(); }

        bool valid() const
        {
            return _zoom > 1;
        }

        void clear()
        {
            _zoom = 1;
            _dx = _dy = 0;
            _dxi = _dyi = 0;
            _orig.x = _orig.y = 0;
        }

        operator bool() const { return valid(); }

        void apply(Boxf& pos) const
        {
            if (valid())
            {
                pos.scaleOrigin(Point2f(_zoom,_zoom),_orig);
                pos.move(_dx, _dy);
            }
        }

        void apply(Point2f& pt) const
        {
            if (valid())
            {
                pt = pt.scaleOrigin(_zoom, _orig);
                pt.x += _dx;
                pt.y += _dy;
            }
        }

        void applyInv(Point2f& pt) const
        {
            if (valid())
            {
                pt.x -= _dx;
                pt.y -= _dy;
                pt = pt.scaleOrigin(1/_zoom, _orig);
            }
        }

        friend std::ostream& operator<<(std::ostream& os, const PinchZoom& pz)
        {
            os << "zoom:" << pz._zoom << " dx:" << pz._dx << " dy:" << pz._dy;
            return os;
        }
    };

    void renderRect(Boxf box, Colour c)
    {
        if (!c.transparent())
        {
            box.move(_viewBasePos);
            ImDrawList* dl = ImGui::GetWindowDrawList();
            dl->AddRectFilled(IV2BTOP(box),
                              IV2BBOT(box),
                              ColourToU32(c));
        }
    }

    void renderGradient(Boxf box, Colour c1, Colour c2)
    {
        box.move(_viewBasePos);
        uint a = ColourToU32(c1);
        uint b = ColourToU32(c2);
        ImDrawList* dl = ImGui::GetWindowDrawList();
        dl->AddRectFilledMultiColor(IV2BTOP(box),
                                    IV2BBOT(box),
                                    a, a, b, b);
    }

    struct RenderInfo: public DL<RenderInfo>
    {
        RenderInfo(ImRender* host) : _host(host) {}

        ImRender* _host;
        Boxf    _box;
        ImTex*  _tex;
        Boxf    _uv;
        Colour  _tint;
        float   _taperMin;
        Colour  _taperColour; // at the bottom of taper (if tapered)
        Rotation* _rot = 0;
        int     _overlayMode;
        float   _overlayFactor;
        float   _overlayIntensity;

        // valid only in render taint. no need to copy
        Point2f _viewOffset;

        void copy(const RenderInfo& ri)
        {
            _box = ri._box;
            _tex = ri._tex;
            _uv = ri._uv;
            _tint = ri._tint;
            _taperMin = ri._taperMin;
            _taperColour = ri._taperColour;
            _rot = ri._rot;
            _overlayMode = ri._overlayMode;
            _overlayFactor = ri._overlayFactor;
            _overlayIntensity = ri._overlayIntensity;
        }
    };


    DL<RenderInfo>::List        _usedRi;
    DL<RenderInfo>::List        _spareRi;

    RenderInfo* allocRI()
    {
        RenderInfo* ri;
        if (!_spareRi.empty())
        {
            ri = _spareRi.first();
            assert(ri);
            ri->remove(); // from spares
        }
        else
        {
            ri = new RenderInfo(this);
        }
        _usedRi.add(ri);
        return ri;
    }

    void freeRI(RenderInfo* ri)
    {
        assert(ri);
        ri->remove();  // from used
        _spareRi.add(ri); // onto spare
    }

    void clearRI()
    {
        assert(_usedRi.empty());
        while (!_spareRi.empty()) delete _spareRi.first();
    }

    static void setCol(float* d, const float* s)
    {
        d[0] = s[0];
        d[1] = s[1];
        d[2] = s[2];
        d[3] = s[3];
    }
    
    static void setCol(float* d, const ImVec4& s)
    {
        setCol(d, (const float*)&s);
    }

#ifdef USEMPG
    static bool applyTex(int slot, ImTex* tex)
    {
        sg_image sgi;

        int w = tex->_w;
        int h = tex->_h;

        if (!slot)
        {
            sgi.id = FROMTEXID(tex->_tid);
        }
        else
        {
            sgi.id = FROMTEXID(tex->_tidvid[slot-1]);

            // XXX hack
            // ASSUME colour planes are quarter size
            w /= 2;
            h /= 2;
        }

        if (sgi.id)
        {
            //LOG1("video tex id ", sgi.id);

            // NB: sampler is already assigned
            state.mpg.bind.fs.images[slot] = sgi;
            state.mpg.image_attrs[slot].width = w;
            state.mpg.image_attrs[slot].height = h;
        }
        return sgi.id != 0;
    }

    static void render_video(const ImDrawList* dl, const ImDrawCmd* cmd)
    {
        static const uint16_t indices12[12] = {
            0, 1, 2,  0, 2, 5, 5,2,3, 5,3,4
        };

        static const uint16_t indices6[6] = {
            0, 1, 2,  0, 2, 3,
        };
        
        assert(cmd);
        auto ri = (RenderInfo*)cmd->UserCallbackData;
        assert(ri);

        // tint col
        ImVec4 col = ColourToImVec4(ri->_tint);

        // taint col
        // alpha value is mix factor
        ImVec4 bottaint = ColourToImVec4(ri->_taperColour);

        // starts with background with zero alpha
        ImVec4 toptaint = bottaint;
        toptaint.w = 0; // zap alpha, do not pre-multiply = no mix

        float dpi = app_dpi_scale;
        int cx, cy, cw, ch;

        /*
        cx = (int) cmd->ClipRect.x*dpi;
        cy = (int) cmd->ClipRect.y*dpi;
        cw = (int) (cmd->ClipRect.z - cmd->ClipRect.x)*dpi;
        ch = (int) (cmd->ClipRect.w - cmd->ClipRect.y)*dpi;;
        */

        // this version clips to the outer window
        // allowing the image to overlap the choice box etc.
        ImVec2 cmin = dl->GetClipRectMin();
        ImVec2 cmax = dl->GetClipRectMax();

        cx = cmin.x*dpi;
        cy = cmin.y*dpi;
        cw = (cmax.x - cmin.x)*dpi;
        ch = (cmax.y - cmin.y)*dpi;

        bool ok = applyTex(0, ri->_tex) && applyTex(1, ri->_tex) && applyTex(2, ri->_tex);

        if (ok)
        {
            //LOG1("render video ", ri->_tex->_name);

            sg_apply_scissor_rect(cx, cy, cw, ch, true);
            //sg_apply_viewport(cx, cy, cw, ch, true);

            //LOG1(TAG_IMTEXT "view x:", cx << " y:" << cy << " w:" << cw << " h:" << ch << " skw:" << pa->_a._skel_w << " skh:" << pa->_a._skel_h << " dpi: " << dpi);
            mpg_vertex v[MAX_MPG_VERTS];

            Boxf& uv = ri->_uv;
            float ux1 = uv[PX1];
            float uy1 = uv[PY1];
            float ux2 = uv[PX2];
            float uy2 = uv[PY2];

            const Boxf& box = ri->_box;

            float x1 = box[PX1];
            float y1 = box[PY1];
            float x2 = box[PX2];
            float y2 = box[PY2];

            v[0].x = x1; v[0].y = y1;
            v[0].u = ux1; v[0].v = uy1;

            v[1].x = x2; v[1].y = y1;
            v[1].u = ux2; v[1].v = uy1;

            v[2].x = x2; v[2].y = y2;
            v[2].u = ux2; v[2].v = uy2;

            v[3].x = x1; v[3].y = y2;
            v[3].u = ux1; v[3].v = uy2;

            for (int i = 0; i < 4; ++i)
            {
                setCol(v[i].col, col);
                setCol(v[i].tcol, toptaint);
            }

            sg_range sgi;
            sgi.ptr = indices6;
            int nv = 4;
            int ni = 6;
            
            if (splitTaperRect(v, *ri, bottaint))
            {
                nv = 6;
                ni = 12;
                sgi.ptr = indices12;
            }

            sg_range sgv;
            sgv.ptr = v;
            sgv.size = nv*sizeof(mpg_vertex);
            sgi.size = ni*sizeof(uint16_t);

            sg_update_buffer(state.mpg.bind.vertex_buffers[0], &sgv);
            sg_update_buffer(state.mpg.bind.index_buffer, &sgi);

            sg_apply_pipeline(state.mpg.pip);
            sg_apply_bindings(&state.mpg.bind);

            mpg_fs_params_t fs_params;
            fs_params.mode = ri->_overlayMode;
            fs_params.factor = ri->_overlayFactor;
            fs_params.intensity = ri->_overlayIntensity;
            sg_range sgr1 = SG_RANGE(fs_params);
            sg_apply_uniforms(SG_SHADERSTAGE_FS, SLOT_mpg_fs_params, &sgr1);

            ImGuiIO* io = &ImGui::GetIO();
            mpg_vs_params_t vs_params;
            vs_params.disp_size[0] = io->DisplaySize.x;
            vs_params.disp_size[1] = io->DisplaySize.y;
            sg_apply_uniforms(SG_SHADERSTAGE_VS, 0, SG_RANGE_REF(vs_params));

            sg_draw(0, ni, 1);

        }

        ri->_host->freeRI(ri);
    }
#endif // USEMPG


    static void interpCol(float* d, float* c1, float* c2, float a)
    {
        for (int i = 0; i < 4; ++i) d[i] = c1[i]*(1.0f-a) + a*c2[i];
    }

    static bool splitTaperRect(base_vertex* v, RenderInfo& ri,
                               const ImVec4& taint)
    {
        float taper = ri._taperMin;
        bool split = taper > 0 && taper < 1;

        /*
         * box original 0,1,2,3 is split so that 2,5 are added

          0                  1
           +-----------------+
           |                 |
           |                 |
           |                 |
           |                 |
           |                 |
         5 +-----------------+2
           |                 |
           |                 |
           |                 |
         4 +-----------------+3

         */

        if (split)
        {
            float h = v[3].y - v[0].y;
            float uh = v[3].v - v[0].v;
            float boxtapery = h*taper + v[0].y;
            float uvtapery = uh*taper + v[0].v;

            // move down one
            // 2->3, 3->4
            v[4] = v[3]; v[3] = v[2];

            // set bottom taint to background colour
            setCol(v[3].tcol, taint);
            setCol(v[4].tcol, taint);

            // add new vert 2
            //v[2].x = v[1].x;
            v[2].y = boxtapery;
            //v[2].u = v[1].u;
            v[2].v = uvtapery;

            interpCol(v[2].col, v[1].col, v[3].col, taper);
            setCol(v[2].tcol, v[1].tcol);  // tint starts from here

            // add last vert 5
            v[5].x = v[0].x; v[5].y = boxtapery;
            v[5].u = v[0].u; v[5].v = uvtapery;
            interpCol(v[5].col, v[0].col, v[4].col, taper);
            setCol(v[5].tcol, v[0].tcol);  // tint starts from here
        }
        return split;
    }

#ifdef USETAINT

    static bool splitTaperQuad(taint_vertex* v, RenderInfo& ri,
                               const ImVec4& taint)
    {
        float taper = ri._taperMin;
        bool split = taper > 0 && taper < 1;

        if (split)
        {
            float bh = ri._box.height();

            // NB: box here has not been offset and needs correction
            float boxtapery = bh*taper + (ri._box[PY1] + ri._viewOffset.y);
            Point2f e, f;

            // these have had offset added
            Point2f a(v[0].x, v[0].y);
            Point2f b(v[1].x, v[1].y);
            Point2f c(v[2].x, v[2].y);
            Point2f d(v[3].x, v[3].y);
            
            split = intersecty(b, c, boxtapery, e) &&
                intersecty(a, d, boxtapery, f);

            if (split)
            {
                float l1 = hypot(b, e)/bh;
                float l2 = hypot(a, f)/bh;

                //assert(l1 >= 0 && l1 <= 1);
                //assert(l2 >= 0 && l2 <= 1);

                Boxf& uv = ri._uv;
                float uy1 = uv[PY1];
                float uy2 = uv[PY2];

                float ul1 = (uy2 - uy1)*l1 + uy1;
                float ul2 = (uy2 - uy1)*l2 + uy1;

                // move down one
                // 2->3, 3->4
                v[4] = v[3]; v[3] = v[2];

                // set bottom taint to background colour
                setCol(v[3].tcol, taint);
                setCol(v[4].tcol, taint);

                // add new vert 2
                v[2].x = e.x; v[2].y = e.y;
                v[2].v = ul1;

                interpCol(v[2].col, v[1].col, v[3].col, l1);
                setCol(v[2].tcol, v[1].tcol);

                // add last vert 5
                v[5].x = f.x; v[5].y = f.y;
                v[5].u = v[0].u; v[5].v = ul2;
                
                interpCol(v[5].col, v[0].col, v[4].col, l2);
                setCol(v[5].tcol, v[0].tcol);
            }
        }

        return split;
    }

    static void render_taint(const ImDrawList* dl, const ImDrawCmd* cmd)
    {
        static const uint16_t indices12[12] = {
            0, 1, 2,  0, 2, 5, 5,2,3, 5,3,4
        };

        static const uint16_t indices6[6] = {
            0, 1, 2,  0, 2, 3,
        };
        
        assert(cmd);
        auto ri = (RenderInfo*)cmd->UserCallbackData;
        assert(ri);

        // tint col
        ImVec4 col = ColourToImVec4(ri->_tint);

        // taint col
        // alpha value is mix factor
        ImVec4 bottaint = ColourToImVec4(ri->_taperColour);

        // starts with background with zero alpha
        ImVec4 toptaint = bottaint;
        toptaint.w = 0; // zap alpha, do not pre-multiply = no mix

        float dpi = app_dpi_scale;
        int cx, cy, cw, ch;

#if 0
        // this version clips to the current window
        cx = (int) cmd->ClipRect.x*dpi;
        cy = (int) cmd->ClipRect.y*dpi;
        cw = (int) (cmd->ClipRect.z - cmd->ClipRect.x)*dpi;
        ch = (int) (cmd->ClipRect.w - cmd->ClipRect.y)*dpi;;

#else
        // this version clips to the outer window
        // allowing the image to overlap the choice box etc.
        ImVec2 cmin = dl->GetClipRectMin();
        ImVec2 cmax = dl->GetClipRectMax();

        cx = cmin.x*dpi;
        cy = cmin.y*dpi;
        cw = (cmax.x - cmin.x)*dpi;
        ch = (cmax.y - cmin.y)*dpi;
#endif

        sg_apply_scissor_rect(cx, cy, cw, ch, true);
        //sg_apply_viewport(cx, cy, cw, ch, true);

        //Boxi clipbox(cx, cy, cx + cw, cy + ch);
        //LOG1("render_taint ", ri->_box << " clip " << clipbox);

        ImTex* tex = ri->_tex;
        assert(tex);

        sg_image sgi;
        sgi.id = tex->getImageTID(); // actual image tid not simgui version

        bool ok = sgi.id != 0;
        if (ok)
        {
            sg_bindings& bind = state.taint.bind;
            bind.fs.images[SLOT_taint_tex] = sgi;

            Boxf& uv = ri->_uv;
            float ux1 = uv[PX1];
            float uy1 = uv[PY1];
            float ux2 = uv[PX2];
            float uy2 = uv[PY2];

            taint_vertex sv[6];
            // NB: coords set later
            sv[0].u = ux1; sv[0].v = uy1;
            sv[1].u = ux2; sv[1].v = uy1;
            sv[2].u = ux2; sv[2].v = uy2;
            sv[3].u = ux1; sv[3].v = uy2;

            for (int i = 0; i < 4; ++i)
            {
                setCol(sv[i].col, col);
                setCol(sv[i].tcol, toptaint);
            }

            sg_range sgi;
            sgi.ptr = indices6;

            int nv = 4;
            int ni = 6;

            Boxf box = ri->_box;
            Rotation& rot = *ri->_rot;
            
            if (rot && rot._baseSet)
            {
                // apply rotate to non-offset box, adding offset to results
                Point2f& off = ri->_viewOffset;
                Point2f a = rot.rotate(box.topleft()) + off;
                Point2f b = rot.rotate(box.topright()) + off;
                Point2f c = rot.rotate(box.bottomright()) + off;
                Point2f d = rot.rotate(box.bottomleft()) + off;

                sv[0].x = a.x; sv[0].y = a.y;
                sv[1].x = b.x; sv[1].y = b.y;
                sv[2].x = c.x; sv[2].y = c.y;
                sv[3].x = d.x; sv[3].y = d.y;

                if (splitTaperQuad(sv, *ri, bottaint))
                {    
                    // for now we always have 2 extra verts
                    nv = 6;
                    ni = 12;
                    sgi.ptr = indices12;
                }
            }
            else
            {
                // apply view offset and set coordinates
                box.move(ri->_viewOffset);
        
                float x1 = box[PX1];
                float y1 = box[PY1];
                float x2 = box[PX2];
                float y2 = box[PY2];
                
                sv[0].x = x1; sv[0].y = y1;
                sv[1].x = x2; sv[1].y = y1;
                sv[2].x = x2; sv[2].y = y2;
                sv[3].x = x1; sv[3].y = y2;

                if (splitTaperRect(sv, *ri, bottaint))
                {
                    nv = 6;
                    ni = 12;
                    sgi.ptr = indices12;
                }
            }

            sg_range sgv;
            sgv.ptr = sv;
            sgv.size = nv*sizeof(taint_vertex);
            sgi.size = ni*sizeof(uint16_t);

            assert(!sg_query_buffer_will_overflow(bind.vertex_buffers[0], sgv.size));
            bind.vertex_buffer_offsets[0] =
                sg_append_buffer(bind.vertex_buffers[0], &sgv);

            bind.index_buffer_offset =
                sg_append_buffer(bind.index_buffer, &sgi);


            sg_apply_pipeline(state.taint.pip);
            sg_apply_bindings(&state.taint.bind);

            ImGuiIO* io = &ImGui::GetIO();
            taint_vs_params_t vs_params;
            vs_params.disp_size[0] = io->DisplaySize.x;
            vs_params.disp_size[1] = io->DisplaySize.y;
            sg_apply_uniforms(SG_SHADERSTAGE_VS, 0, SG_RANGE_REF(vs_params));

            sg_draw(0, ni, 1);
        }

        ri->_host->freeRI(ri);
    }
#endif // USESTRAND

    bool renderImageVN(RenderInfo& ri)
    {
        // return true unless transparent

        // should all be valid if we get here
        assert(ri._box && ri._uv && ri._tex && ri._rot);

        ImU32 col = ColourToU32(ri._tint);
        if (!(col & IM_COL32_A_MASK)) return false;

        ImDrawList* dl = ImGui::GetWindowDrawList();

#ifdef USEMPG
        if (ri._tex->isVideo())
        {
            RenderInfo* rt = allocRI();
            rt->copy(ri);
            rt->_box.move(_viewBasePos); // offset
            dl->AddCallback(render_video, rt);
            return true;
        }
#endif

#ifdef USETAINT
        RenderInfo* rt = allocRI();
        rt->copy(ri);
        rt->_viewOffset = _viewBasePos;
        dl->AddCallback(render_taint, rt);
#else

        bool taper = ri._taperMin > 0 && ri._taperMin < 1;

        // colour at bottom (when tapering)
        ImU32 bcol = ColourToU32(ri._taperColour);

        Rotation& rot = *ri._rot;
        Boxf& uv = ri._uv;
        Boxf box = ri._box;

        if (rot && rot._baseSet)
        {
            //LOG1("VN renderImageRotated ", tex->_name);

            // clip to base rotating area
            Boxf cb = rot._baseBox;
            cb.move(offset);
            ImGui::PushClipRect(IV2BTOP(cb), IV2BBOT(cb), true);

            float ux1 = uv[PX1];
            float uy1 = uv[PY1];
            float ux2 = uv[PX2];
            float uy2 = uv[PY2];

            Point2f a = rot.rotate(box.topleft()) + offset;
            Point2f b = rot.rotate(box.topright()) + offset;
            Point2f c = rot.rotate(box.bottomright()) + offset;
            Point2f d = rot.rotate(box.bottomleft()) + offset;
            Point2f e, f;
            float bh = box.height();

            if (taper)
            {
                float boxtapery = bh*ri._taperMin + (box[PY1] + offset.y);
                taper = intersecty(b, c, boxtapery, e) &&
                intersecty(a, d, boxtapery, f);
            }

            if (taper)
            {
                float l1 = hypot(b, e)/bh;
                float l2 = hypot(a, f)/bh;

                //assert(l1 >= 0 && l1 <= 1);
                //assert(l2 >= 0 && l2 <= 1);

                l1 = (uy2 - uy1)*l1 + uy1;
                l2 = (uy2 - uy1)*l2 + uy1;

                ImTextureID tid = ri._tex->_tid;
                bool push_texture_id = tid != dl->_CmdHeader.TextureId;
                if (push_texture_id) dl->PushTextureID(tid);

                //float uvtapery = (uy2 - uy1)*ri._taperMin + uy1;

                dl->PrimReserve(12, 6);  // 12 index, 6 verts
                dl->PrimQuadUV(IVP(a),
                               IVP(b),
                               IVP(e),
                               IVP(f),
                               ImVec2(ux1, uy1),
                               ImVec2(ux2, uy1),
                               //ImVec2(ux2, uvtapery),
                               //ImVec2(ux1, uvtapery),

                               ImVec2(ux2, l1),
                               ImVec2(ux1, l2),
                               col);

                // need 6 more index and 2 more verts
                ImDrawVert* v = dl->_VtxWritePtr;
                v->pos = IVP(c); // vert c, bottom right
                v->uv = ImVec2(ux2,uy2);
                v->col = bcol;

                v++;

                // bottom left
                v->pos = IVP(d);
                v->uv = ImVec2(ux1, uy2);
                v->col = bcol;

                dl->_VtxWritePtr += 2;

                unsigned int i = dl->_VtxCurrentIdx;
                ImDrawIdx* ip = dl->_IdxWritePtr;

                // already added two triangles forming rectangle
                // a,b,c,d (clockwise).
                // add triangles: FEC and FCD
                *ip++ = i-1; // f
                *ip++ = i-2; // e
                *ip++ = i; // c
                *ip++ = i-1; // f
                *ip++ = i; // c
                *ip = i+1; // d

                dl->_VtxCurrentIdx += 2;
                dl->_IdxWritePtr += 6;

                if (push_texture_id) dl->PopTextureID();
            }
            else
            {
                dl->AddImageQuad(ri._tex->_tid,
                                 IVP(a),
                                 IVP(b),
                                 IVP(c),
                                 IVP(d),

                                 ImVec2(ux1, uy1),
                                 ImVec2(ux2, uy1),
                                 ImVec2(ux2, uy2),
                                 ImVec2(ux1, uy2),

                                 col);
            }

            ImGui::PopClipRect();
        }
        else
        {
            // non-rotating

            box.move(offset);

            if (!taper)
            {
                // no rotate, no taper
                dl->AddImage(ri._tex->_tid,
                             IV2BTOP(box),
                             IV2BBOT(box),
                             IV2BTOP(uv),
                             IV2BBOT(uv),
                             col);
            }
            else
            {
                // no rotate but taper

                ImVec2 img_pos = IV2BTOP(box);
                ImVec2 img_bot = IV2BBOT(box);

                ImVec2 uv0 = IV2BTOP(uv);
                ImVec2 uv1 = IV2BBOT(uv);

                ImTextureID tid = ri._tex->_tid;
                bool push_texture_id = tid != dl->_CmdHeader.TextureId;
                if (push_texture_id) dl->PushTextureID(tid);

                dl->PrimReserve(12, 6);
                float boxtapery = (img_bot.y - img_pos.y)*ri._taperMin + img_pos.y;
                float uvtapery = (uv1.y - uv0.y)*ri._taperMin + uv0.y;

                // add four points ending in y=boxtapery
                // top left, top right, bottom right, bottom left
                // later need to add two more vertices
                dl->PrimRectUV(img_pos, ImVec2(img_bot.x,boxtapery),
                               uv0, ImVec2(uv1.x,uvtapery), col);

                // need 6 more index and 2 more verts
                ImDrawVert* v = dl->_VtxWritePtr;
                v->pos = img_bot; // vert e, bottom right
                v->uv = uv1;
                v->col = bcol;

                v++;

                // bottom left
                v->pos = ImVec2(box[PX1], box[PY2]);  // vert f
                v->uv = ImVec2(uv0.x, uv1.y);
                v->col = bcol;

                dl->_VtxWritePtr += 2;

                unsigned int i = dl->_VtxCurrentIdx;
                ImDrawIdx* ip = dl->_IdxWritePtr;

                // already added two triangles forming rectangle
                // a,b,c,d (clockwise).
                // add triangles: DCE and DEF.
                *ip++ = i-1; // d
                *ip++ = i-2; // c
                *ip++ = i; // e
                *ip++ = i-1; // d
                *ip++ = i; // e
                *ip = i+1; // f

                dl->_VtxCurrentIdx += 2;
                dl->_IdxWritePtr += 6;

                if (push_texture_id) dl->PopTextureID();
            }
        }
#endif // USETAINT
        return true;
    }

    Point2f getAdjustedMousePos() const
    {
        // mouse position relative to view
        ImVec2 cp = ImGui::GetMousePos();
        Point2f pos1(cp.x, cp.y);
        return pos1 - _viewBasePos;
    }

    struct HitClick
    {
        ImageMeta::EltClick*  _elt = 0;

        // position in the same space as the elt poly
        Point2f               _pos;

        operator bool() const { return _elt != 0; }

        void clear() { _elt = 0; }
    };

    HitClick hitPathVN(const ImageMeta::Elts& elts,
                       const RenderInfo& ri,
                       PinchZoom& pz)
    {
        ImageMeta::EltClick* hit = 0;

        // adjust for the fact we haven't yet adjusted the polygon offset
        Point2f mpos = getAdjustedMousePos();

        // the polys are already scaled by update world into pixel space
        // but not yet adjusted by pinch-zoom.
        // apply inverse pinch-zoom to map mouse into pixel poly space
        pz.applyInv(mpos);

        // first go through and see if we are inside any shape
        for (auto ei : elts)
        {
            assert(ei->_type == ImageMeta::m_elt_click);
            auto ec = (ImageMeta::EltClick*)ei;

            if (ec->_active)
            {
                for (auto& pi : ec->_polys._polys)
                {
                    if (pi.contains(mpos))
                    {
                        if (!hit || ec->_z > hit->_z) hit = ec;
                        break;
                    }
                }
            }

            // some paths are shown all the time (eg signposts)
            // render those here
            bool show = ec->_show;

            // override to display all paths regardless
            if (ImGui::IsKeyDown(ImGuiKey_Insert)) show = true;

            if (show)
            {
                // polys already in pixel space (and rotated).
                // need to apply final pinch-zoom and draw.
                renderPathVN(ec, pz, ri._box, 0.7f);
            }
        }
        return HitClick{hit, mpos};
    }


    void renderPathVN(ImageMeta::EltClick* ec, PinchZoom& pz,
                      const Boxf& clip,
                      float fade = 0)
    {
        Point2f offset = _viewBasePos;

        ImDrawList* dl = ImGui::GetWindowDrawList();

        assert(ec->_active);

        Box cb = clip;
        cb.move(offset);
        ImGui::PushClipRect(IV2BTOP(cb), IV2BBOT(cb), true);

        for (auto& pi : ec->_polys._polys)
        {
            int n = pi.size();
            if (n)
            {
                Colour bcol = pi._col;
                ImDrawFlags flags = 0;
                ImVec2* points = new ImVec2[n];

                if (fade) bcol = bcol.fade(fade);

                // scale the points
                for (int i = 0; i < n; ++i)
                {
                    Point2f p = pi._points[i];
                    pz.apply(p);
                    p += offset;
                    points[i].x = p.x;
                    points[i].y = p.y;
                }

                dl->AddPolyline(points, n,
                                ColourToU32(bcol),
                                flags,
                                pi._strokeW);
                delete [] points;
            }
        }

        ImGui::PopClipRect();
    }


#if 0
    ImageMeta::EltClick*
    renderPathsVN(const ImageMeta::Elts& elts, PinchZoom& pz)
    {
        ImageMeta::EltClick* hit = 0;

        Point2f offset = _viewBasePos;

        bool showAll = false;

        // override to display all paths regardless
        if (ImGui::IsKeyDown(ImGuiKey_Insert)) showAll = true;

        // adjust for the fact we havent yet adjusted the polygon offset
        Point2f mpos = getAdjustedMousePos();

        // there can be more than one hit
        // later we choose the highest Z.
        std::list<ImageMeta::EltClick*> hitlist;

        // the polys are already scaled by update world.

        // map any view zoom into poly space
        pz.applyInv(mpos);

        // first go through and see if we are inside any shape
        for (auto ei : elts)
        {
            assert(ei->_type == ImageMeta::m_elt_click);
            auto ec = (ImageMeta::EltClick*)ei;
            assert(ec->_active);

            for (auto& pi : ec->_polys._polys)
            {
                if (pi.contains(mpos))
                {
                    hitlist.push_back(ec);
                    break;
                }
            }
        }

        // find the biggest Z
        for (auto hi : hitlist)
            if (!hit || hi->_z > hit->_z) hit = hi;

        ImDrawList* dl = ImGui::GetWindowDrawList();
        for (auto ei : elts)
        {
            auto ec = (ImageMeta::EltClick*)ei;
            bool hilite = false;
            bool show = false;

            if (ec->_show) show = true;  // clickable part of UI
            if (showAll || ec == hit) { show = true; hilite = true; }

            if (show)
            {
                for (auto& pi : ec->_polys._polys)
                {
                    int n = pi.size();
                    if (n)
                    {
                        Colour bcol = pi._col;
                        ImDrawFlags flags = 0;
                        ImVec2* points = new ImVec2[n];

                        if (!hilite) bcol = bcol.fade(0.7f);

                        // scale the points
                        for (int i = 0; i < n; ++i)
                        {
                            Point2f p = pi._points[i];
                            pz.apply(p);
                            p += offset;
                            points[i].x = p.x;
                            points[i].y = p.y;
                        }

                        dl->AddPolyline(points, n,
                                        ColourToU32(bcol),
                                        flags,
                                        pi._strokeW);
                        delete [] points;
                    }
                }
            }
        }
        return hit;
    }
#endif

    static StrandCommon::Rect getClipRect()
    {
        // is the mouse within the current clip region
        ImGuiWindow* iw = ImGui::GetCurrentWindow();
        int cx = (int) iw->ClipRect.Min.x;
        int cy = (int) iw->ClipRect.Min.y;
        int cw = (int) iw->ClipRect.Max.x - cx;
        int ch = (int) iw->ClipRect.Max.y - cy;
        return StrandCommon::Rect(cx, cy, cw, ch);

        //bool r = rr.within(mp.x, mp.y);
        //LOG1("render clip ", rr << ", mouse  " << mp.x << "," << mp.y << " in view " << r);
    }


#if 0
    void renderRegion(const ST::Region& r, int border, Colour bcol)
    {
        for (int i = 0; i < r.numRects; ++i)
        {
            const RRect& ri = r.rects[i];
            int x = ri.x1;
            int y = ri.y1;
            int w = ri.width();
            int h = ri.height();

            x += _viewBasePos.x;
            y += _viewBasePos.y;

            const StrandCommon::Rect b(x, y, w, h);
            renderBorder(border, b, bcol);
        }
    }
#endif

    static void renderBorder(int border,
                             const StrandCommon::Rect& box, Colour bcol)
    {
        // the box top left has already been adjusted by the cursor pos
        // the box includes the border
        if (border)
        {
            float thickness = (float)border/2;
            ImVec2 b_pos(box._x, box._y);
            ImVec2 b_bot = b_pos;
            b_bot.x += box._w;
            b_bot.y += box._h;

            b_pos.x += thickness;
            b_pos.y += thickness;
            b_bot.x -= thickness;
            b_bot.y -= thickness;

            //LOG1("render border ", box);

            ImDrawList* dl = ImGui::GetWindowDrawList();
            //int flags = ImDrawListFlags_AntiAliasedLines;
            dl->AddRect(b_pos,
                        b_bot,
                        ColourToU32(bcol),
                        0.0f,
                        0, // flags
                        border);
        }
    }

    static ImageMeta::EltClick*
    renderImage(const ImageMeta* m,
                ImTex* tex,
                const TextFlowInfo& ti,
                bool hilite,
                StrandCommon::Rect box, ImVec2 uv0, ImVec2 uv1,
                int border,
                Colour tint)
    {
        // this is used to render an image within a flow or full-screen
        // return whether an item was clicked

        // clipped?
        if (!tex || !getClipRect().intersects(box)) return 0;

        // if the image has clickables, consider indicating this
        // and remembering what was clicked
        bool imageClickable = m && m->hasClickables();

        //int border = ti._objBorderSize;
        Colour bcol;

        // if the image has clickables, indicate this with a different
        // colour border
        if (imageClickable)
        {
            bcol = hilite ? ti._objBorderColHi : ti._objBorderCol;
        }
        else
        {
            // if not clickable indicate with a plain border colour
            bcol = hilite ? StyleColour(ImGuiCol_Text) : ti._objBorderCol;
        }

        // apply tint alpha to border
        float a = tint.alphaf();
        if (a < 1) bcol = bcol.mixAlpha(a);

        renderBorder(border, box, bcol);

        // layout box includes border within which, we place the image
        box._x += border;
        box._y += border;
        box._w -= border*2;
        box._h -= border*2;

        ImVec2 img_pos(box._x, box._y);
        ImVec2 img_bot(box._x + box._w, box._y + box._h);

        ImDrawList* dl = ImGui::GetWindowDrawList();
        dl->AddImage(tex->_tid,
                     img_pos,
                     img_bot,
                     uv0, uv1,
                     ColourToU32(tint));

        ImageMeta::EltClick* hit = 0;

        // NB: if dragging `m` is usually passed as 0 so we dont click
        if (imageClickable)
        {
            // need to adjust the image box by the zoom and pan factor
            Point2f top(box._x, box._y);
            Point2f sz(box._w, box._h);

            sz.x /= (uv1.x - uv0.x);
            sz.y /= (uv1.y - uv0.y);
            top.x -= uv0.x * sz.x;
            top.y -= uv0.y * sz.y;
            hit = renderPaths(m, top, sz);
        }
        return hit;
    }

    static ImageMeta::EltClick*
    renderPaths(const ImageMeta* m, Point2f top, Point2f sz)
    {
        // NB: not VN

        // return image clicked
        ImageMeta::EltClick* hit = 0;

        bool showAll = false;

#ifdef LOGGING
        // override to display all paths regardless
        if (ImGui::IsKeyPressed(ImGuiKey_Insert)) showAll = true;
#endif

        if (m)
        {
            //ImGuiWindow* window = ImGui::GetCurrentWindow();

            ImVec2 cp = ImGui::GetMousePos();

            // map position into poly coord space
            Point2f pos1((cp.x - top.x)/sz.x, (cp.y - top.y)/sz.y);

            // there can be more than one hit
            // later we choose the highest Z.
            std::list<ImageMeta::EltClick*> hitlist;

            // first go through as see if we are inside any shape
            for (auto ei : m->_elts)
            {
                if (ei->_type == ImageMeta::m_elt_click)
                {
                    auto ec = (ImageMeta::EltClick*)ei;

                    // non active clicks are, eg a text area
                    if (ec->_active)
                    {
                        for (auto& pi : ec->_polys._polys)
                        {
                            if (pi.contains(pos1))
                            {
                                hitlist.push_back(ec);
                                break;
                            }
                        }
                    }
                }
            }

            // find the biggest Z
            for (auto hi : hitlist)
                if (!hit || hi->_z > hit->_z) hit = hi;

            ImDrawList* dl = ImGui::GetWindowDrawList();
            for (auto ei : m->_elts)
            {
                if (ei->_type == ImageMeta::m_elt_click)
                {
                    auto ec = (ImageMeta::EltClick*)ei;

                    bool hilite = false;
                    bool show = false;

                    if (ec->_show) show = true;  // clickable part of UI
                    if (showAll || ec == hit) { show = true; hilite = true; }

                    // never show non-active areas
                    if (!ec->_active) show = false;

                    if (show)
                    {
                        for (auto& pi : ec->_polys._polys)
                        {
                            int n = pi.size();
                            if (n)
                            {
                                Colour bcol = pi._col;
                                ImDrawFlags flags = 0;
                                ImVec2* points = new ImVec2[n];

                                if (!hilite) bcol = bcol.fade(0.7f);

                                // scale the points
                                for (int i = 0; i < n; ++i)
                                {
                                    Point2f p = pi[i];
                                    p *= sz;
                                    p += top;
                                    points[i].x = p.x;
                                    points[i].y = p.y;
                                }

                                dl->AddPolyline(points, n,
                                                ColourToU32(bcol),
                                                flags,
                                                pi._strokeW);
                                delete [] points;
                            }
                        }
                    }
                }
            }
        }
        return hit;
    }

    void setClickCommand(const string& url)
    {
        //LOG1("click command ", url);
        _clickCommand = url;
    }

    const string& getClickCommand() const
    {
        return _clickCommand;
    }

    void clearClickCommand()
    {
        _clickCommand.clear();
    }

    bool linkClickedLabel(const string& label)
    {
        // normally the label is the name of an object term
        // this is converted to a term ref (eg _FOO)
        // which causes the game to "x foo"
        string cl = trim(label);
        bool ok = !cl.empty();
        if (ok)
        {
            // formats allowed:
            // foo -> _FOO
            // get wombat -> get wombat
            // (foo bar) -> foo bar

            bool plain = false;
            char c = cl[0];
            if (c == '(')
            {
                // interior is treated as plain
                // XX lose ()
                plain = true;
            }
            else
            {
                // plain means we don't convert into a term
                plain = true;

                // multiple tokens are interpreted as plain
                // but also things that do look like terms
                // so any spaces or http://whatever will not be
                // simple and therefore not plain.
                // anything starting with underscore also wont be simple.

                int l = ST::Traits::atSimpleName(cl.c_str());
                if (l == (int)cl.size())
                {
                    //LOG1("click label simple '" , cl << "'");
                    plain = false;
                }
            }

            if (!plain)
            {
                // otherwise assume we refer to a term
                // uppercase and add underscore
                cl = ::toUpper(cl);
                if (cl[0] != '_') cl = '_' + cl;
            }

            setClickCommand(cl);
        }
        return ok;
    }

    static bool mouseDragSum(int b, ImVec2* last)
    {
        bool r = false;
        static ImVec2 base[3];

        if (!last)
        {
            // clear internal base offset
            memset(base, 0, sizeof(base));
        }
        else
        {
            ImVec2 dd = ImGui::GetMouseDragDelta(b);

            if (dd.x || dd.y)
            {
                last->x = base[b].x - dd.x/3000;
                last->y = base[b].y - dd.y/3000;
                r = true;
            }
            else
            {
                base[b] = *last;
            }
        }
        return r;
    }

    bool hoverOK()
    {
        return ImGui::IsWindowHovered(ImGuiHoveredFlags_RootAndChildWindows);
    }

    void calculatePinchZoom(const Boxf& pos, PinchZoom& pz, bool canChange)
    {
        // version used for VN images
        // if canChange is false, just maintain clipping

        if (canChange)
        {
            ImGuiIO& io = ImGui::GetIO();

            if (hoverOK()) _mouseWheel += io.MouseWheel;

            mouseDragSum(0, &_viewDrag0);
            mouseDragSum(1, &_viewDrag1);

            pz._dxi = -_viewDrag0.x;
            pz._dyi = -_viewDrag0.y;

            //LOG1("Mouse wheel:", _mouseWheel);
            //LOG1("Drag:", pz._dx << "x" << pz._dy);

            pz._zoom = _mouseWheel/100;
            pz._zoom -= _viewDrag1.y*3; // add in right mouse move & scale

            // include any pinch zoom
            pz._zoom += GetPinch()/1000;  // scale

            if (pz._zoom <= 0)
            {
                // when zoomed out, reset all bases
                pz._zoom = 0;
                resetZoom();
            }
            else if (pz._zoom > MAX_ZOOM_FACTOR-1) pz._zoom = MAX_ZOOM_FACTOR-1;

            pz._zoom = pz._zoom + 1;
        }

        pz._dx = pz._dxi * pos.width() * pz._zoom;
        pz._dy = pz._dyi * pos.height() * pz._zoom;

        //LOG1("Zoom ", pz._zoom);

        Boxf posz = pos;
        pz.apply(posz);

        if (posz[PX1] > pos[PX1]) pz._dx -= posz[PX1] - pos[PX1];
        else if (posz[PX2] < pos[PX2]) pz._dx += pos[PX2] - posz[PX2];

        if (posz[PY1] > pos[PY1]) pz._dy -= posz[PY1] - pos[PY1];
        else if (posz[PY2] < pos[PY2]) pz._dy += pos[PY2] - posz[PY2];

    }

    Point2f calculatePinch(int wmax, int hmax, int w, int h, Boxf& uv)
    {
        // this old version is used for the text zoom view
        ImGuiIO& io = ImGui::GetIO();
        _mouseWheel += io.MouseWheel;

        mouseDragSum(0, &_viewDrag0);
        mouseDragSum(1, &_viewDrag1);

        float dx = _viewDrag0.x;
        float dy = _viewDrag0.y;

        //ImGui::Text("Mouse wheel: %.1f", mouseWheel);
        //ImGui::Text("Drag: %.1f, %.1f", dd.x, dd.y);

        float zoom = _mouseWheel/200;

        if (_viewDrag1.y > 0) _viewDrag1.y = 0;
        zoom -= _viewDrag1.y*3; // add in right mouse move & scale

        // include any pinch zoom
        zoom += GetPinch()/1000;  // scale

        if (zoom <= 0)
        {
            // when zoomed out, reset all bases
            zoom = 0;
            resetZoom();
        }
        else if (zoom > 0.99f) zoom = 0.99f;  // [0,99%] clamp max

        float zw = 1 - zoom;  // [0,1]
        float tx = zoom/2; // (1 - zw)/2; // [0,.5]
        float ty = zoom/2;

        if (tx + dx < 0) dx = -tx;
        if (tx + zw + dx > 1) dx = 1 - (tx + zw);
        tx += dx;

        if (ty + dy < 0) dy = -ty;
        if (ty + zw + dy > 1) dy = 1 - (ty + zw);
        ty += dy;

        float bx = tx + zw;
        float by = ty + zw;

        _viewDrag0.x = dx;
        _viewDrag0.y = dy;

        float sc = 1/zw;
        float sw = w*sc;
        float sh = h*sc;
        if (sw > wmax)
        {
            sw = wmax;
            sh = (float)h/w * sw;
        }

        if (sh > hmax)
        {
            sh = hmax;
            sw = (float)w/h * sh;
        }

        if (zoom)
        {
            float vw = wmax - sw;
            if (vw > 0)
            {
                // least of what we have and what we can use
                float uv = u_min(vw/(sw*sc), 1-zw);
                if (uv > 0)
                {
                    // want to give to both sides the same
                    float t = u_min(tx, 1 - bx);

                    // but make sure we have some
                    t = u_min(t, uv/2);

                    // allocate common amount
                    tx -= t;
                    bx += t;

                    // any left?
                    t = uv - 2*t;

                    // if so, then one side must be depleted, give to other.
                    if (!tx) bx += t; else tx -= t;

                    sw += uv*(sw*sc);
                    assert(sw <= wmax);
                }
            }

            vw = hmax - sh;
            if (vw > 0)
            {
                float uv = u_min(vw/(sh*sc), 1-zw);
                if (uv > 0)
                {
                    // want to give to both sides the same
                    float t = u_min(ty, 1 - by);

                    // but make sure we have some
                    t = u_min(t, uv/2);

                    // allocate common amount
                    ty -= t;
                    by += t;

                    // any left?
                    t = uv - 2*t;

                    if (!ty) by += t; else ty -= t;

                    sh += uv*(sh*sc);
                    assert(sh <= hmax);
                }
            }
        }

        uv = Boxf(tx, ty, bx, by);
        return Point2f(sw, sh);
    }
};
