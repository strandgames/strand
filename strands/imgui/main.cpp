//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#define OLDMENU

#define SOKOL_IMPL

#ifdef __APPLE__
#include <mach-o/dyld.h>
#include <limits.h>
#include "TargetConditionals.h"
#endif

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/fiber.h>
#endif

#ifdef COOP
#define USE_FIBERS
#endif

// fibers do not need thread locks
#ifdef USE_FIBERS
#define SCTX_LOCK(_s)
#else
#define SCTX_LOCK(_s) std::lock_guard<std::mutex> sctx_lock(_s->_lock)
#endif

#define MAX_ACCEPTABLE_LOADTIME 2

// most are 800
#define IMAGE_IDEAL_SIZE_WEB_MOBILE 1000

// standard web is 1600
// if hi res is included, people are often 2000 high
// so make sure we dont load those unless hi res is requested.
#define IMAGE_IDEAL_SIZE_WEB 1800

#define IMAGE_IDEAL_SIZE_ANDROID 1800

// initially when slow
#define IMAGE_IDEAL_SIZE_SLOW 3000

// ideal size for desktops
#define IMAGE_IDEAL_SIZE_NORMAL 4000

#define FONT_SIZE_DEFAULT 23
#define MARGIN_SIZE_DEFAULT 8

#define LOG_FILENAME_DEFAULT  "strand.log"

#include "sokol_app.h"
#include "sokol_gfx.h"
#include "sokol_time.h"
#include "sokol_glue.h"
#include "imgui.h"
#include "sokol_imgui.h"
#include "sokol_audio.h"
#include "sokol_log.h"
#include "sokol_args.h"
#include "sokstate.h"

#ifdef USESPINE
// generated shader for animation
#include "imgui-anim.glsl.h"
#endif

#ifdef USEMPG
#include "mpeg.glsl.h"
#endif

#ifdef USETAINT
#include "taint.glsl.h"
#endif

#define STB_VORBIS_HEADER_ONLY
#include "stb_vorbis.c"

#include <vector>
#include <sstream>
#include "fda.h"
#include "logged.h"
#include "imaudio.h"
#include "settings.h"
#include "scramble.h"
#include "IconsMaterialDesign.h"
#include "imsampler.h"
#include "worldtime.h"
#include "box.h"
#include "strands.h"

static AudioState audioState;

#ifdef USE_DEMO
static bool show_demo_window = false;
#endif


typedef std::string string;

struct FontFileInfo
{
    string              _name;
    string              _displayName;
    
    bool                _bold = false;
    bool                _italic = false;

    bool                _light = false;

    // we can be regular bold or regular italic
    bool                _regular = false;

    // we are an icon font, not a text font
    bool                _icons = false;

    ImFont*             _imFont = 0;

    // font data loaded before creating font
    bool                _requested = false;
    bool                _loading = false;  // or loaded

    // this is not deleted so we can switch back to an old font
    char*               _data = 0;
    int                 _dsize = 0;

    FontFileInfo(const std::string& name) : _name(name)
    {
        _displayName = getDisplayName(_name);
    }

    string stemName() const
    {
        // take the first word before hyphen as the font base stem name
        size_t e = _displayName.find('-');
        if (e != std::string::npos)
        {
            return _displayName.substr(0, e);
        }
        return _displayName;
    }

    static string getDisplayName(const string& name)
    {
        // name without suffix
        // eg Roboto-Regular
        string n = changeSuffix(name, 0);

        // remove any trailing numbers
        // eg Roboto-Regular-2
        const char* s = n.c_str();
        const char* e = s + strlen(s);
        while (e != s && u_isdigit(e[-1])) --e;  // trim numbers
        if (e != s && e[-1] == '-') --e; // trim hyphen
        if (e != s) n = n.substr(0, e-s);

        // XX apparently we must keep the hyphens or imgui doesnt like it.
        //n = replaceAll(n, '-', ' ');
        return n;
    }

    bool isBase() const
    {
        // a base font is one that's a family root and not a bold
        // or italic version.
        // so base includes regular, light, medium etc
        return !_icons && !_bold && !_italic;
    }

    void validate()
    {
        // fill in missing defaults
        
        // expect either light or regular.
        // light => not regular, else regular
        // icons are not regular
        _regular = !_icons && !_light;
    }

    friend std::ostream& operator<<(std::ostream& os, const FontFileInfo& f)
    {
        os << f._name;
        if (!f._icons)
        {
            os << " bold:" << f._bold << " italic:" << f._italic;
        }
        else os << " icons";
        return os;
    }
};

static std::vector<FontFileInfo> fontFiles;


static bool show_quit_dialog = false;
static bool quitting = false;
static bool show_restart_dialog = false;

bool can_resume = false; // set if there is an auto-save from last time

// if true, we show dialog when `can_resume` otherwise we just resume.
static bool enable_resume_dialog = false;

// dialog showing
static bool show_resume_dialog = false;

static bool show_settings_window = false;
static bool html5_ask_leave_site = false;
static bool strand_enabled = false;
static bool show_strand = true;
static bool fontChanged = false; 
static bool fontSizeChanged = false;
static float fontSize = FONT_SIZE_DEFAULT; 
static bool fontSizeChosen = false;
bool enableKeyboard = true;
static bool forceKb = false;
bool forceCommandInput = false;
static int androidDPI = 0;
float app_dpi_scale = 1;
float fakeDpi = 0; // not set
static bool disableMipmaps = false; // command line option

// margin = current margin each side
// marginCalculated = last total margin we calculated ideally
// marginTotal = current total margin
static int marginMin = 16; // for non-mobile
static int margin = MARGIN_SIZE_DEFAULT; // non-mobile will override
static int marginCalculated;
int marginTotal;

static const char* requestFontList;
static const char* requestImageMeta;
static const char* requestGameIcon;
static std::string requestStoryName;
static sg_pass_action pass_action;
static bool haveFonts = false;
static int imageMetaLoaded = 0; // 1 = ok, -1 = fail
static int fontListLoaded = 0;  // 1 = ok, -1 = fail
bool enablePictureClickables = true;

// resize will recalculate ideal font size
static bool enableDynamicTextSize = false;

// used to calculate ideal font size
Boxf approxTextArea;

// base path of all file loading
static std::string fileBaseDir;
static std::string saveBaseDir;

#if defined(__EMSCRIPTEN__) || defined(__ANDROID__)
static bool fullscreen = true;  // NB: android ignores this anyway
#else
static bool fullscreen = false;
#endif

static bool expectSave;
static bool expectLoad;
static bool saveEnabled = true;
static bool loadEnabled = true;
static bool restartEnabled = true;
static bool needOpenBrowser;
bool startUnsaved = true; // start of game no save yet.

// used when we want to delay user input until media played etc.
static bool suspendInput = false;

void SetSuspendInput(bool v)
{
    //LOG1("setting suspend input ", v);
    suspendInput = v;
}

// are we a mobile, native or web 
bool isMobile;
bool isWeb;
static bool isDynamicSize = true;  // whether we autosize fonts, not web mobile
bool isVirtualKeyboard; // if mobile
bool isSmallScreen; // if mobile (no scrollbar)
float choiceBoxScale = 1;
static bool asRelease = false; // act like release mode

#ifdef LOGGING
// for debugging. set this and it will simulate running out of
// texture memory for the next image
bool noTextureMemory = false;
bool purgeTextures = false;
#endif

// debug features
static bool disableText = false;
bool showUserDebug = false;

// this manages samplers
SamplerHolder samplerHolder;


#include "imgui_internal.h"
#include <stdio.h>

#include "fetcher.h"

Fetcher fetcher;

struct PushIconFont
{
    bool        _pushed = false;

    void        push()
    {
        for (auto& fi : fontFiles)
        {
            if (fi._icons)
            {
                assert(fi._imFont);
                ImGui::PushFont(fi._imFont);
                _pushed = true;
                break;
            }
        }
    }

    void        pop()
    {
        if (_pushed)
        {
            ImGui::PopFont();
            _pushed = false;
        }
    }

    static void fixme()
    {
        // XXX big hairy steaming great hack !!
        auto w = ImGui::GetCurrentWindow();
        int z = w->DrawList->_TextureIdStack.Size;
        if (!z)
        {
            auto font = ImGui::GetDefaultFont();
            w->DrawList->PushTextureID(font->ContainerAtlas->TexID);
        }
    }

    PushIconFont()  { push(); }
    ~PushIconFont()  { pop(); }
};


#include "imtexloader.h"
#include "imcol.h"
#include "keyboard.h"
#include "imfilebrowser.h"

// forward
std::string gui_input_pump();
void play_audio(const AudioInfo&);
void stop_audio();
void show_picture(PictureInfo&);
void show_asset(const VarList&);
void syncFilesystem();
static void adjustIdealFontSize();

#ifdef USESPINE
#include "sspine.h"
#endif

#include "ifisdl.h"


static Keyboard vKeyboard;
ImTexLoader* texLoader;
WorldTime worldTime;
static double lastFrameTime;
static double lastClearTime;
static Stats fps;

#define FPS_TARGET  30

// forward
static ImGui::FileBrowser* fbrowser;
static StrandCtx* sctx;

SOKState state;

#define VNMODE (sctx->_vn._enabled)

void StrandWindow(bool* open, uint rc);
void StrandInit(const char*);

#ifdef USE_FIBERS
#define ASTACK_SZ 1024*16
#define FIBSTACK_SZ 1024*64

typedef void FiberFn(void*);

struct Fiber
{
    emscripten_fiber_t  _context;
    char                _asyncify_stack[ASTACK_SZ];
    alignas(16) char    _stack[FIBSTACK_SZ];

    void init(em_arg_callback_func entry, void *arg)
    {
        emscripten_fiber_init(&_context,
                              entry, arg,
                              _stack, sizeof(_stack),
                              _asyncify_stack, sizeof(_asyncify_stack));
    }
};

struct FiberState
{
    emscripten_fiber_t _main;
    char _asyncify_stack[ASTACK_SZ];
    Fiber fibers[1];  // only need one

    void init()
    {
        // turn the current context into a "main" fiber
        emscripten_fiber_init_from_current_context(&_main,
                                                   _asyncify_stack,
                                                   sizeof(_asyncify_stack));
    }

    void yieldToMain(int f = 0)
    {
        // yield current fiber  to main
        emscripten_fiber_swap(&fibers[f]._context, &_main);
    }

    void yieldTo(int f = 0)
    {
        // yield main to fiber f
        emscripten_fiber_swap(&_main, &fibers[f]._context);
    }

    void run(FiberFn* fn, int f = 0)
    {
        fibers[f].init(fn, 0);
    }

};

static FiberState fiberState;

static void strandFiber(void* arg)
{
    if (strand_enabled)
    {
        LOG2("starting strand fiber ", requestStoryName);

        // this will yield and not come back until strand is finished.
        StrandInit(requestStoryName.c_str());

        LOG2("finished strand fiber ", requestStoryName);

        // strand is done, so disable.
        strand_enabled = false;
    }

    // if we return, switch back to main
    fiberState.yieldToMain();
}

#endif // USE_FIBERS


string gui_input_pump()
{
    // called from strand fiber pumpfn to poll for input
    string s;

    // if there's a command, return it, otherwise yield
    UNUSED bool v = sctx->yieldCmd(s);

#ifdef COOP    
    if (!v) fiberState.yieldToMain();
#endif
    
    return s;
}


static void strand_pump()
{
    if (strand_enabled)
    {
#ifdef COOP
        // fiber switch to strand, from main frame loop
        fiberState.yieldTo();
#else
        sctx->h.sendInput();
        sctx->h.flush();
#endif        
    }
}

static void sendCmd(const char* s)
{
    SCTX_LOCK(sctx);
    sctx->sendCmd(s);
}

static void addText(const char* s, bool hilite)
{
    // ASSUME LOCK
    int sz = strlen(s);
    if (sz)
    {
        sctx->_mainText.add(s, hilite);

        if (sctx->_transcript)
        {
            char last = s[sz-1];

            // do not include link tails
            string s1 = RemoveMarkdown(s, false);  
            sctx->_transcript->write(s1);
            if (last != '\n') sctx->_transcript->write('\n');
        }
    }
}

static void textReceiver(const char* s, int id)
{
    // called from SDKHandler textemitter
    // the id can be used to pass in the type of text.
    // currently id=1 => command echo
    SCTX_LOCK(sctx);

    int hilite = false;

    bool commandEcho = id == TEXTF_ID_ECHO;
    
    // hilight the echo
    if (commandEcho) hilite = true;

    if (id == TEXTF_ID_REPLACE)
    {
        sctx->_mainText.replace(0);
    }

    addText(s, hilite);

    if (!id)
    {
        while (*s) sctx->_wstat.learnWords(*s++);
    }
}


static void layoutChanged()
{
    sctx->_mainText.textChanged();
}

static void forceWorldUpdate()
{
    assert(sctx);
    sctx->_vn.forceWorldUpdate();
}

void styleChanged()
{
    layoutChanged();

    // theme changed
    vKeyboard.styleChanged(); // new colours

    // and keyboard within file browser
    assert(fbrowser);
    fbrowser->styleChanged();

    // the window background is passed to the VN manager so it can
    // fade the pictures according to the background
    assert(sctx);
    sctx->_vn.backgroundColour(StyleColour(ImGuiCol_WindowBg));

    // needs to recalculate use of background colour
    forceWorldUpdate();
}

void EnableCommandChoices(bool v)
{
    //LOG1("EnableCommandChoices ", v);
    if (SPROP(P_ENABLE_COMMANDCHOICES, v))
        sctx->enableCommandChoices(v);
}

bool VNMode()
{
    return VNMODE;
}

void EnableVNMode(bool on)
{
    // called ifiMetaResponse when "layout:vn" specified in meta data
    if (VNMODE != on)
    {
        VNMODE = on;
        styleChanged();
    }
}

void EnableCommandInput()
{
    // only enable, do not disable. called when game overrides
    // called also from init for debug
    //LOG1("EnableCommandInput ", 1);
    SPROP(P_ENABLE_COMMANDLINE, true);
    forceCommandInput = true;
}

void StrandInit(const char* story)
{
    using namespace std::placeholders;  
    SDLHandler::TextEmitter e = std::bind(textReceiver, _1, _2);
    if (!fileBaseDir.empty()) sctx->configdir = fileBaseDir;

    // XX does not use DPI
    // this is only used for aspect ratios
    sctx->setAppSize(sapp_width(), sapp_height());

    // reset initially as we are not updating the size
    sctx->_vn._world._needEval = false;
    
    sctx->init(e, story);

    // XX are we too early for this?
    // NB: on by default, only need to do anything if already off
    bool cc = PROPB(P_ENABLE_COMMANDCHOICES, true);
    if (!cc) EnableCommandChoices(cc);
}

static std::string lastInputBuf;  
static int lastSuggest; // last suggestion index
static int lastInputEnd; // length of input text without last word
static WordStat::WordList suggestedWords;

static int inputCallback(ImGuiInputTextCallbackData* data)
{
    bool doCompletion =
        data->EventFlag == ImGuiInputTextFlags_CallbackCompletion;

    bool doHistoryUp = data->EventFlag == ImGuiInputTextFlags_CallbackHistory
        && data->EventKey == ImGuiKey_UpArrow;
    
    bool doHistoryDown = data->EventFlag == ImGuiInputTextFlags_CallbackHistory
        && data->EventKey == ImGuiKey_DownArrow;

    if (doCompletion)
    {
        if (!data->BufTextLen) doHistoryUp = true;
        else
        {
            // same buffer means more suggestions
            bool same = (lastInputBuf == data->Buf);

            if (!same)
            {
                // update buffer for further suggestions next time
                lastSuggest = 0;
                lastInputBuf = data->Buf;

                // backup to find length before last word
                int pos = data->BufTextLen;
                do
                {
                    --pos;
                    if (data->Buf[pos] == ' ') break;

                } while (pos > 0);
                lastInputEnd = pos;

                suggestedWords.clear();

                SCTX_LOCK(sctx);
                sctx->_wstat.suggestCompletion(data->Buf, suggestedWords);
            }
            
            int sz = suggestedWords.size();
            if (lastSuggest >= sz) lastSuggest = 0; // cycle

            if (lastSuggest < sz)
            {
                //LOG("suggest ", suggestedWords[lastSuggest] << " len:" << lastInputEnd);
                
                data->DeleteChars(0, data->BufTextLen);

                if (lastInputEnd)
                {
                    // +1 to get space from original string
                    data->InsertChars(0, lastInputBuf.c_str(),
                                      lastInputBuf.c_str() + lastInputEnd + 1);
                }
                
                data->InsertChars(data->CursorPos, suggestedWords[lastSuggest++].c_str());

                // text with suggestion becomes last buf so to match next time.
                // but leave length as before so to change last word.
                lastInputBuf = data->Buf;
            }
        }
    }
    
    if (doHistoryUp)
    {
        SCTX_LOCK(sctx);
        const char* s = sctx->_hist.up();
        if (s)
        {
            data->DeleteChars(0, data->BufTextLen);
            data->InsertChars(0, s);
        }
        //data->SelectAll();
    }
    
    if (doHistoryDown)
    {
        SCTX_LOCK(sctx);
        const char* s = sctx->_hist.down();
        if (s)
        {
            data->DeleteChars(0, data->BufTextLen);
            data->InsertChars(0, s);
            //data->SelectAll();
        }
    }
    return 0;
}

static FontFileInfo* findFontInfo(const std::string& name)
{
    // find the font info for font `name`
    // which can be a file path
    // or it can be a display name given to the font
    // but the case has to agree

    // remove any path and convert to display name if not already
    std::string fn = FontFileInfo::getDisplayName(filenameOf(name));

    for (auto& ffi : fontFiles)
    {
        if (ffi._displayName == fn) return &ffi;
    }
    return 0;
}

static FontFileInfo* findBestFont(const std::string& name)
{
    // find the closest font for `name`.
    FontFileInfo* best = 0;
    FontFileInfo* fallback = 0;

    std::string pat = toLower(name);
    
    for (auto& fi : fontFiles)
    {
        if (!fi.isBase()) continue; // must be a base

        // keep track of a fallback base find in case we don't find it.
        if (!fallback) fallback = &fi;

        // see if pattern occurs in the font file name we have
        if (findFirst(toLower(fi._name), pat))
        {
            best = &fi;
            break;
        }
    }

    if (!best) best = fallback;
    return best;
}

static void selectFont(FontFileInfo* ffi)
{
    // when chosen from a menu or restored from save
    if (ffi)
    {
        fontSizeChosen = false; // allow size to be calculated
        ffi->_requested = true;
        SPROP(P_FONTNAME, ffi->_name);
    }
}

static void selectFontSize(int fz)
{
    if (fontSize != fz)
    {
        fontSize = fz;
        fontSizeChosen = true; // remember we manually changed it.
        fontSizeChanged = true;

        // update font size in properties so to save
        SPROP(P_FONTSIZE, fz); // int
    }
}

static void selectFont(const std::string& name)
{
    // select by name from save game properties
    
    //LOG1("selecting font ", name);
    FontFileInfo* ffi = findFontInfo(name);
    if (ffi)
    {
        LOG2("located selected font ", *ffi);
        selectFont(ffi);
    }
    else
    {
        LOG1("Waring: selected font not found ", name);
    }
}

static int validateFontSize(int fz)
{
    if (fz > 72) fz = 72;
    else if (fz < 12) fz = 12;
    return fz;
}

#ifdef OLDMENU
static void fontMenuMobile()
{
    int fz = fontSize;
    ImVec2 sz(2*fontSize, 0);
    if (ImGui::Button("+", sz))
    {
        fz = validateFontSize(fz+1);
        selectFontSize(fz);
    }
    if (ImGui::Button("-", sz))
    {
        fz = validateFontSize(fz-1);
        selectFontSize(fz);
    }
}

static void fontMenuDesktop()
{
    float h = ImGui::GetFrameHeightWithSpacing();
    float w = 20*fontSize;
    if (w < 400) w = 400;
    ImGui::BeginChild("fontchooser", ImVec2(w,h), false);

    ImGui::SetNextItemWidth(w/2);
    ImFont* font_current = ImGui::GetFont();

    // the debug name is the same as the display name
    if (ImGui::BeginCombo("##fontcombo", font_current->GetDebugName()))
    {
        for (uint i = 0; i < fontFiles.size(); ++i)
        {
            FontFileInfo& ffi = fontFiles[i];
            if (ffi.isBase())
            {
                if (ImGui::Selectable(ffi._displayName.c_str()))
                {
                    selectFont(&ffi);
                    break;
                }
            }
        }
        ImGui::EndCombo();
    }
    ImGui::SameLine();

    ImGui::SetNextItemWidth(w/4);
    int fz = fontSize;
    if (ImGui::InputInt("##Size", &fz, 1, 0))
    {
        fz= validateFontSize(fz);
        selectFontSize(fz);
    }
    ImGui::EndChild();
}
#endif // OLDMENU

#ifdef __EMSCRIPTEN__
EM_JS(void, call_js_args, (const char* fname, int fnamez, const char* mime, int mimez), {
    offerDownload(UTF8ToString(fname, fnamez), UTF8ToString(mime, mimez));
});

EM_JS(int, call_js_is_mobile, (void), {
  if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
  {
      return 1;
  }

  // try workaround for iOS
  let r = [
           'iPad Simulator',
           'iPhone Simulator',
           'iPod Simulator',
           'iPad',
           'iPhone',
           'iPod'
           ].includes(navigator.platform)
      // iPad on iOS 13 detection
      || (navigator.userAgent.includes("Mac") && "ontouchend" in document);

  return r;
});


EM_JS(void, call_js_console, (const char* c_msg), {
        var msg = UTF8ToString(c_msg);
        console.log("#### MESSAGE:" + msg);
    });

void exportTranscript()
{
    if (sctx->_transcript)
    {
        SCTX_LOCK(sctx);
        
        //const std::string& fname = sctx->_transcript->_name;
        std::string mime = "text/plain";
        std::string content = sctx->_transcript->getAll();
        LOG2("exporting transcript size, ", content.size());
        if (!content.empty())
        {
            call_js_args(content.c_str(), content.size(), mime.c_str(), mime.size());
        }
    }
}
#else
void exportTranscript() {}
#endif

static ImVec2 getViewSize()
{
    ImVec2 v = ImGui::GetContentRegionAvail();
    
    //LOG1("viewsize ", a << " x:" << v.x << " y:" << v.y);
    return v;
}

static Point2f getAppSize()
{
    return Point2f(sapp_width()/app_dpi_scale,
                   sapp_height()/app_dpi_scale);
}

static Point2 getScreenSize()
{
    Point2 sz;
#ifdef __WIN32
    sz.x = GetSystemMetrics(SM_CXSCREEN);
    sz.y = GetSystemMetrics(SM_CYSCREEN);
#endif

#ifdef __APPLE__
    auto id = CGMainDisplayID();
    sz.x = CGDisplayPixelsWide(id);
    sz.y = CGDisplayPixelsHigh(id);    
#endif

    // linux?
    
    
    return sz;
}

static ImVec2 mainViewportSize()
{
    const ImGuiViewport* v = ImGui::GetMainViewport();
    //LOG1("viewport size ", v->Size.x << "," << v->Size.y);
    return v->Size;
}

static void FBrowserOpen()
{
    // ASSUME `fbrowser`
    assert(fbrowser);
    
    ImVec2 p = mainViewportSize();
    float sx = 0.90f;
    float sy = 0.90f;
    float w = p.x * sx;
    float h = p.y * sy;
    float x = (p.x - w)/2;
    float y = (p.y - h)/2;
    //LOG1("browser window ", x << "," << y << " (" << w << "x" << h << ")");
    fbrowser->SetWindowSize(w, h);
    fbrowser->SetWindowPos(x, y);
    fbrowser->_showKeyboard = isVirtualKeyboard || forceKb;
    fbrowser->Open();
}

static ImVec2 adjustForPadding(const ImVec2& view)
{
    const ImGuiStyle& style = ImGui::GetStyle();
    return ImVec2(view.x - style.WindowPadding.x*2,
                  view.y - style.WindowPadding.y*2);
}

void setMargin(int m)
{
    // also called from settings
    marginTotal = marginCalculated = m;
    margin = m/2;
}

static void makeChoice(int i)
{
    IFIHandler::ChoicesInfo* ch = sctx->h._choice;
    assert(ch);

    // a click on an outline can overlap the choice box.
    // clear any image (or text) click.
    sctx->clearClickCommand();

    ch->_selected = i;
    char buf[8];
    sprintf(buf, "%d", i+1);
    //LOG1("making choice ", buf);
    sendCmd(buf);
}

static bool HoverOK()
{
    bool h = ImGui::IsWindowHovered(ImGuiHoveredFlags_RootAndChildWindows);
    return h;
}

static bool continueKeys()
{

    bool r = HoverOK() &&
        (ImGui::IsKeyPressed(ImGuiKey_PageDown, false) ||
         ImGui::IsKeyPressed(ImGuiKey_DownArrow, false) ||
         ImGui::IsKeyPressed(ImGuiKey_Enter, false) ||
         ImGui::IsKeyPressed(ImGuiKey_Space, false) ||
         ImGui::IsKeyPressed(ImGuiKey_Escape, false));

    return r;
}

bool urlClicked(const string& url)
{
    bool r = startsWith(url, "http://") || startsWith(url, "https://");

    if (r)
    {
        LOG2("link clicked ", url);
            
#ifdef __EMSCRIPTEN__
            /* Implementation in pre.J's */
        EM_ASM({ if(window["open_url"]) window.open_url($0, $1) }, url.c_str(), url.length());
#else
        extern void OsOpenInShell(const char* path);
        OsOpenInShell(url.c_str());
#endif
    }
    return r;
}

bool showRoomTitle()
{
    bool v = VNMODE ? false : P_ENABLE_ROOMTITLE_DEF;
    
    // override preference
    if (PROPDEF(P_ENABLE_ROOMTITLE))
    {
        v = PROPB(P_ENABLE_ROOMTITLE, P_ENABLE_ROOMTITLE_DEF);
    }
    return v;
}

string GetGameCredits()
{
    // get and cache text string for GUI
    static string credits;
    static bool creditsDone = false;

    if (!creditsDone)
    {
        // credits do not change. get and keep
        creditsDone = true;
        credits = RemoveMarkdown(sctx->h.getGameCredits());
    }

    return credits;
}


void ChangeThemeStyle(const ThemeStyle* ts)
{
    if (ts)
    {
        SPROP(P_THEMESTYLE, ts->_name);

        if (ts->_fn1) (*ts->_fn1)(0);
        if (ts->_fn2) (*ts->_fn2)();
        styleChanged();
    }
}

void browserLoad()
{
    // also called from ifiLoadData when no filename given
    assert(fbrowser);

    expectLoad = true;
    fbrowser->SetTitle("Load Game");
    
    int bflags = ImGuiFileBrowserFlags_CloseOnEsc;
    fbrowser->SetFlags(bflags);
    needOpenBrowser = true;
}

void browserSave()
{
    assert(fbrowser);

    expectSave = true;
    fbrowser->SetTitle("Save Game");

    int bflags = ImGuiFileBrowserFlags_CloseOnEsc |
        //ImGuiFileBrowserFlags_CreateNewDir |
        ImGuiFileBrowserFlags_EnterNewFilename;

    fbrowser->SetFlags(bflags);
    needOpenBrowser = true;
}

void browserRestart()
{
    show_restart_dialog = true; // are you sure?
}

static void releaseMore()
{
    // when continuing from a "more"
    assert(!sctx->_mainText.onLastPage());
    
    // mark sections seen up to (and including)
    //the old render page
    sctx->_mainText.disableToCurrentPage();
    sctx->_mainText._tbuild._textFlow.bumpCurrentPage();
}

#if !defined(__ANDROID__)
bool TranscriptStarted()
{
    return sctx->_transcript;
}

void StartStopTranscript(bool v)
{
    if (!v && sctx->_transcript)
    {
        SCTX_LOCK(sctx);
        LOG2("Closing Transcript, ", sctx->_transcript->_name);

        sctx->_transcript->close();
        exportTranscript();
                    
        delete sctx->_transcript;
        sctx->_transcript = 0;
    }
                
    if (v && !sctx->_transcript)
    {
        Transcript* t = new TranscriptFile;
        string tFilename = SDLHandler::transcriptFilename();
        string path = makePath(saveBaseDir, tFilename);
        path = FD::incrementalFilename(path);
        if (t->open(path))
        {
            sctx->_transcript = t;
            LOG2("Starting transcript ", path);
        }
        else
        {
            delete t;
            LOG1("Failed to open transcript, ", path);
        }
    }
}
#endif // !android

static void scalePadding()
{
    ImVec2 p = ImGui::GetStyle().FramePadding;
    p.y *= choiceBoxScale;
    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, p);
    
    //const ImGuiStyle& style = 
    //style.FramePadding.y = pad.y * choiceBoxScale;    
}

static void unscalePadding()
{
    ImGui::PopStyleVar(1);
    //style.FramePadding.y = pad.y;
}

string GameTitleAndVersion()
{
    // also used in settings
    string s = sctx->h.getGameTitle();
    if (!s.empty())
    {
        string ver = sctx->h.getGameVersion();
        if (!ver.empty())
        {
            s += ' ';
            s += ver;
        }
    }
    return s;
}

inline static bool equal(const ImVec4& a, const ImVec4& b)
{
    return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w;
}

#ifdef OLDMENU
static float calcIconMenuWidth(const char* icons)
{
    PushIconFont pf;

    Utf8 u8(icons);

    // number of icon characters
    int n = u8.length();

    float w = 0;
    
    if (n > 0)
    {
        // all the icons next to each other but no separator
        w = ImGui::CalcTextSize(icons).x;

        const ImGuiStyle& style = ImGui::GetStyle();
        w += IM_TRUNC(style.ItemSpacing.x*2.0f + 0.5f) * n; // leave a space after the last one
    }
    
    return w;
}
#endif

typedef void preloadCB(FetchInfo* fo, void* ctx);
typedef void preloadData(const char* name, const char* data, int sz);

std::string fullPath(const std::string& p)
{
    return makePath(fileBaseDir, p);
}

static void preloadFile(const char*& name, preloadCB* cb, preloadData* pd)
{
    // load either locally or fetch
    // NB: if pd==0, forced to fallback to fetch
    if (name && pd)
    {
        // look locally
        FDLoad fd(fullPath(name), true);
        if (fd)
        {
            (*pd)(name, (const char*)fd._data, fd._size);
            name = 0;  // found
        }
    }

    // otherwise fetch
    // these files are all small
    if (name && fetcher.start(name, cb, true)) name = 0;
}

static void gameicon_loaded_data(const char* name,
                                 const char* data, int size)
{
    LOG2("Game Icon loaded ", name << " size:" << size);
    string suf = suffixOf(name);

    ImageDecodeState* decoder = 0;

    if (equalsIgnoreCase(suf, ".webp"))
    {
        // now need to decode full image.
        // used to be just
        // ok = WebpGetInfo(fd, fd._size, &w, &h, &chans);
        decoder = createWEBPDecodeState((const unsigned char*)data, size);
    }
    else
    {
        decoder = createSTBIDecodeState((const unsigned char*)data, size);
    }
    
    if (decoder)
    {
        if (decoder->decodeAll() && !decoder->_failed)
        {
            LOG2("Decoded icon ", name << " size " << decoder->_w << "x" << decoder->_h);

            unsigned int psize = decoder->_w * decoder->_h * decoder->_chans;
            assert(psize > 0);

            sg_image_desc img_desc = {};
            img_desc.width = decoder->_w;
            img_desc.height = decoder->_h;
            img_desc.pixel_format = SG_PIXELFORMAT_RGBA8;
            img_desc.data.subimage[0][0].ptr = decoder->_pix;
            img_desc.data.subimage[0][0].size = psize;

            // fill out values needed for mipmap since it doesn't fill defaults
            img_desc.type = SG_IMAGETYPE_2D;
            img_desc.num_slices = 1;
            img_desc.num_mipmaps = 1;
            img_desc.sample_count = 1;

            MipMapMaker mm;
            sg_image_desc d = mm.prepare_mipmaps(&img_desc);

            static const int iconSizes[] =
            {
                16, 32, 64, 128, 256,
            };

            sapp_icon_desc icon_desc = {};

            int icc = 0;
            for (int i = 0; i < ASIZE(iconSizes); ++i)
            {
                int x = iconSizes[i];
                //LOG1("making icon data ", x);

                size_t dsize = x*x*4;
                bool found = false;
                for (int j = SG_MAX_MIPMAPS-1; j >= 0; --j)
                {
                    if (d.data.subimage[0][j].size == dsize)
                    {
                        //LOG("located data for icon ", x);
                        found = true;

                        icon_desc.images[icc].width = x;
                        icon_desc.images[icc].height = x;
                        icon_desc.images[icc].pixels.ptr = d.data.subimage[0][j].ptr;
                        icon_desc.images[icc].pixels.size = dsize;
                        assert(icon_desc.images[icc].pixels.ptr);
                        ++icc;
                        break;
                    }
                }

                if (!found)
                {
                    LOG1("WARNING: did not find icon data ", x);
                }
            }

            if (icc > 0)
            {
                LOG2("making icon with images ", icc);
                sapp_set_icon(&icon_desc);
            }
        }
        else
        {
            LOG1("unable to decode icon image ", name);
        }
    }

    delete decoder;
}

static void gameicon_loaded(FetchInfo* fo, void* ctx)
{
    if (*fo)
    {
        int sz;
        char* data = fo->donate(sz);
        gameicon_loaded_data(fo->_name.c_str(), data, sz);
        delete data;
    }
    else
    {
        LOG1("game icon failed to load ", fo->_name);
    }
}

void StrandWindow(bool* strand_open, uint64 renderCount)
{
    ImGuiWindowFlags flags = 0;

    flags |= ImGuiWindowFlags_MenuBar;
    flags |= ImGuiWindowFlags_NoCollapse;
    flags |= ImGuiWindowFlags_NoScrollbar;

    double timeNow = worldTime.duration();

    // windowed    
    //ImGui::SetNextWindowSize(ImVec2(800, 720), ImGuiCond_FirstUseEver);
    
    // act fullscreen regardless.
    // when running in a sokol main window, need to "fullscreen" within it.
    flags |= ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoTitleBar;
    
    bool use_work_area = true;
    const ImGuiViewport* viewport = ImGui::GetMainViewport();
    ImGui::SetNextWindowPos(use_work_area ? viewport->WorkPos : viewport->Pos);
    ImGui::SetNextWindowSize(use_work_area ? viewport->WorkSize : viewport->Size);
    const ImGuiStyle& style = ImGui::GetStyle();
    ImVec2 pad = style.FramePadding;
    ImVec2 spacing = style.ItemSpacing;
    ImVec2 wpad = style.WindowPadding;
    
    static ImVec2 lastViewSize;

    ImVec2 vz = mainViewportSize();
    if (vz.x > 0)
    {
        float dx = vz.x - lastViewSize.x;
        float dy = vz.y - lastViewSize.y;

        if (fabsf(dx) >= 4 || fabsf(dy) >= 4) // choke back
        {
            lastViewSize = vz;

            if (isDynamicSize)
            {
                if (enableDynamicTextSize) adjustIdealFontSize();
                else
                {
                    marginTotal += dx;

                    // do not expand margin past calculated value
                    // allow user to widen screen and lengthen lines
                    int m = u_min(marginCalculated, marginTotal);
                    margin = u_max(m/2, marginMin);
                }
            }

            //LOG1("viewport changed ", vz.x << "x" << vz.y);

            // relayout & scroll
            layoutChanged();
        }
    }

    static bool textinputfocus = false;
    static bool gameTitleUpdated = false;
    static string gameTitleAndVersion = "Strand";
    static string gameIconName;

    int showRoomTitle = P_ENABLE_ROOMTITLE_DEF;  // true

    if (VNMODE)
    {
        // default to hide for vnmode
        showRoomTitle = false;

        // but if taller than wider, then allow
        if (vz.y > vz.x) showRoomTitle = true;
    }
    
    // override preference
    if (PROPDEF(P_ENABLE_ROOMTITLE))
    {
        showRoomTitle = PROPB(P_ENABLE_ROOMTITLE, P_ENABLE_ROOMTITLE_DEF);          }

    if (!gameTitleUpdated)
    {
        // keep polling for the title
        string t = GameTitleAndVersion();
        if (!t.empty())
        {
            gameTitleUpdated = true;
            gameTitleAndVersion = t;
            sapp_set_window_title(gameTitleAndVersion.c_str());
        }
    }

    if (gameIconName.empty())
    {
        gameIconName = sctx->h.getGameIconName();
        if (!gameIconName.empty())
        {
            LOG2("Got icon name ", gameIconName);
            requestGameIcon = gameIconName.c_str();

            // force fetch load
            preloadFile(requestGameIcon, gameicon_loaded, 0);
        }
    }

    ImGui::Begin(gameTitleAndVersion.c_str(), 0, flags);

    ImVec2 viewsz = getViewSize();

#ifndef OLDMENU
    if (ImGui::BeginMenuBar())
    {
        PushIconFont pf;
                        
        bool mok = strand_enabled;

        if (ImGui::MenuItem(ICON_MD_REFRESH, 0, false, mok))
            sendCmd("look");
        else if (!isSmallScreen && mok && ImGui::IsItemHovered(ImGuiHoveredFlags_DelayNormal))
        {
            pf.pop();
            ImGui::SetTooltip("Refresh");
            pf.push();
        }

        if (ImGui::MenuItem(ICON_MD_STAR_BORDER, 0, false, mok))
            sendCmd("inventory");
        else if (!isSmallScreen && mok && ImGui::IsItemHovered(ImGuiHoveredFlags_DelayNormal))
        {
            pf.pop();
            ImGui::SetTooltip("Inventory");
            pf.push();
        }

        if (ImGui::BeginMenu(ICON_MD_MORE_HORIZ)) // three dots "..."
        {
            pf.pop();

            // this fixes the stack imbalance so we can use the base font
            pf.fixme();

            if (ImGui::MenuItem("Undo", 0, false, mok)) sendCmd("_undo");

            ImGui::Separator();

            if (ImGui::MenuItem("Text Size +", 0, false, mok))
            {
                int fz = fontSize;
                ImVec2 sz(2*fontSize, 0);
                fz = validateFontSize(fz+1);
                selectFontSize(fz);
            }

            if (ImGui::MenuItem("Text Size -", 0, false, mok))
            {
                    int fz = fontSize;
                    ImVec2 sz(2*fontSize, 0);
                    fz = validateFontSize(fz-1);
                    selectFontSize(fz);
            }
            
            if (ImGui::MenuItem("Settings")) show_settings_window = true;

            ImGui::Separator();
            
            if (ImGui::MenuItem("Load", (!isMobile ? "Ctrl+L" : ""), false, loadEnabled && !suspendInput))
                browserLoad();
            
            if (ImGui::MenuItem("Save", (!isMobile ? "Ctrl+S" : ""), false, saveEnabled && !suspendInput))
                browserSave();

            if (ImGui::MenuItem("Restart", 0, false, restartEnabled && !suspendInput))
                browserRestart();

            ImGui::Separator();
            
            if (ImGui::MenuItem("Quit")) show_quit_dialog = true;

            ImGui::EndMenu();            
        }

        ImGui::EndMenuBar();
    }
    
#else   // OLDMENU

    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("Menu"))
        {
            if (ImGui::MenuItem("Load", (!isMobile ? "Ctrl+L" : ""), false, loadEnabled && !suspendInput))
                browserLoad();

            if (ImGui::MenuItem("Save", (!isMobile ? "Ctrl+S" : ""), false, saveEnabled && !suspendInput))
                browserSave();
            
            if (ImGui::MenuItem("Restart", 0, false, restartEnabled && !suspendInput))
                browserRestart();

            ImGui::Separator();

            if (ImGui::MenuItem("Settings")) show_settings_window = true;

            ImGui::Separator();

            if (ImGui::MenuItem("Quit")) show_quit_dialog = true;

            ImGui::EndMenu();
        }

        if (isSmallScreen)
        {
            fontMenuMobile();
        }
        else
        {
            if (ImGui::BeginMenu("Font"))
            {
                fontMenuDesktop();
                ImGui::EndMenu();
            }
        }

        // calculate icons menu width

        static const char* iconMenu =
#ifdef USE_DEMO            
            ICON_MD_ADB
#endif            
            ICON_MD_STAR_BORDER
            ICON_MD_REFRESH
            ICON_MD_UNDO
            ;

        // XX calculation a hack.
        float iw = calcIconMenuWidth(iconMenu);
        float rp = viewsz.x - iw;
        
        float px = ImGui::GetCursorPosX();
        bool showIcons = px <= rp;

        if (showIcons)
        {
            ImGui::SetCursorPosX(rp);
            PushIconFont pf;
                        
#ifdef USE_DEMO        
            if (ImGui::MenuItem(ICON_MD_ADB)) show_demo_window = true;
#endif

            bool mok = strand_enabled;
            if (ImGui::MenuItem(ICON_MD_STAR_BORDER, 0, false, mok))
                sendCmd("inventory");
            else if (!isSmallScreen && mok && ImGui::IsItemHovered(ImGuiHoveredFlags_DelayNormal))
            {
                pf.pop();
                ImGui::SetTooltip("Inventory");
                pf.push();
            }

            if (ImGui::MenuItem(ICON_MD_REFRESH, 0, false, mok))
                sendCmd("look");
            else if (!isSmallScreen && mok && ImGui::IsItemHovered(ImGuiHoveredFlags_DelayNormal))
            {
                pf.pop();
                ImGui::SetTooltip("Refresh");
                pf.push();
            }
            
            if (ImGui::MenuItem(ICON_MD_UNDO, 0, false, mok)) sendCmd("_undo");
            else if (!isSmallScreen && mok && ImGui::IsItemHovered(ImGuiHoveredFlags_DelayNormal))
            {
                pf.pop();
                ImGui::SetTooltip("Undo");
                pf.push();
            }
        }
        ImGui::EndMenuBar();
    }
#endif  // OLDMENU

    // if shown
    ImVec2 titleBoxSz(viewsz.x, ImGui::GetTextLineHeight() + wpad.y*2);

    // parser input (usually desktop), non-vn mode
    // NB: this can be overridden by the game
    bool inputActive = !isVirtualKeyboard && !VNMODE;

    if (forceCommandInput) inputActive = true; // menu override

    int cby = 0;
    int txy = viewsz.y;

    if (showRoomTitle)
    {
        // reduce text space by the title bar
        txy -= titleBoxSz.y + spacing.y;
    }

    // we have a concept called the page size which is the space available
    // assuming no choice box or input keyboard. used for "continue" cases.
    int txpagey = txy;
    int choiceLines = 0;
    bool choiceContinue = false;

    // if we have choices, calculate size of choice box and reduce text size
    IFIHandler::ChoicesInfo* ch = sctx->h._choice;
    if (ch)
    {
        // choice box can switch off input
        if (!ch->_textinput) inputActive = false;

        // choice can override for special inputs needed by the game.
        if (ch->_textinputforce) inputActive = true;

        choiceLines = ch->size();

        // detect pure "continue" and convert into a "continue" page.
        if (choiceLines == 1 && !inputActive)
        {
            // have just one choice with no text input
            // and the choice is just continue
            const string& ct = ch->_choices[0]._text._text;
            if (equalsIgnoreCase(ct, "continue")) choiceContinue = true;
        }

        // this was from when choice boxes can have a header
        // not sure if this is ever used anymore.
        if (ch->_header) ++choiceLines;
    }

    // adjust space for choices height
    if (choiceLines > 0)
    {
        // size of radio button
        
        // font size + pad*2 + spacing
        float lh = ImGui::GetTextLineHeight()
            + pad.y*2*choiceBoxScale + spacing.y;
        cby = lh*choiceLines;
        cby += wpad.y*2; 
        txy -= cby + spacing.y;
    }

    // when the game is over, hide the input line
    if (!strand_enabled) inputActive = false;

    bool showKeyboard = inputActive && enableKeyboard
        && (isVirtualKeyboard|| forceKb);
    ImVec2 keyboardSz(0,0);
    ImVec2 inputBoxSz;

    // size of input line and also "more" button line
    float inputy = ImGui::GetTextLineHeight()
        + wpad.y*2 + pad.y*2*choiceBoxScale;

    // if we are paging, then "more" is same height as input box
    txpagey -= inputy + spacing.y;

    if (inputActive)
    {
        if (showKeyboard)
        {
            vKeyboard.reset();
        
            // default to zero meaning we have not set a size manually
            int kz = PROPI(P_KEYBOARD_SIZE, 0);
            if (kz > 0)
            {
                vKeyboard.setScale((float)kz/100);
            }

            // calculate scale to fit the view, unless scale already set
            ImVec2 cr1 = adjustForPadding(viewsz);
            keyboardSz = vKeyboard.calculateSize(cr1, 25); // 25% max
            keyboardSz.y += pad.y*2 + spacing.y;
        }
        inputBoxSz = ImVec2(viewsz.x, keyboardSz.y + inputy);
        txy -= inputBoxSz.y + spacing.y;
    }

    ImVec2 choiceBoxSz(viewsz.x, cby);

    if (txpagey < txy)
    {
        // this can only happen if there are no choices and no input
        // so that the space with "continue" is less than the normal text
        // space.
        // when this happens, reduce space otherwise we can't put "continue".
        txy = txpagey;
    }

    // txy is client area when choices drawn (and possible input)
    // txpagey is client area when no choices drawn and space for "continue"

    // This is the size for the text window
    Boxf clientBox(0, 0, viewsz.x, txy);

    // the size of the text window when we have "continue".
    Boxf clientPage(0, 0, viewsz.x, txpagey);

    // the text/picture overlap is computed from the "appsize" which 
    // should be the visible area
    sctx->setAppSize(viewsz.x, txpagey);

    // title box
    if (showRoomTitle)
    {
        const char* titleText = sctx->getTitle();
        ImGui::BeginChild("Title", titleBoxSz, true, ImGuiWindowFlags_NoScrollbar);

        int ind = margin;

        if (ind) ImGui::Indent(ind);

        // calculate the colour for the header text
        static ImVec4 oldtc; 
        static ImVec4 headertc;

        ImVec4 tc = ImGui::GetStyleColorVec4(ImGuiCol_ButtonHovered);
        if (!equal(tc, oldtc))
        {
            oldtc = tc;
            
            // calculate our tc
            Colour tcc(tc.x, tc.y, tc.z);
            Colour tcc2 = tcc.brighter(1.3f);
            headertc = ColourToImVec4(tcc2);
        }
        

#if defined(USERDEBUG) || defined(LOGGING)

        // show normal header line unless we're going to show stats
        // AND we're mobile. in which case we show stats instead
        // because the line is too short.
        if (!(texLoader && showUserDebug && isSmallScreen))
        {
            ImGui::TextColored(headertc, "%s", titleText);            
        }

        // show texture memory stats on top line
        if (texLoader && showUserDebug)
        {
            char buf[128];
            int fr = 1.0/fps.average();
            int frmin = 1.0/fps.maximum();
            double frstd = fps.stdDeviation()*1000;
            sprintf(buf, "%d(%d,%.1f) ", fr, frmin, frstd);
            strcat(buf, texLoader->_lastPoolSummary.c_str());
            
            float w = ImGui::CalcTextSize(buf).x + pad.x;
            float wmax = ImGui::GetContentRegionAvail().x;
            ImGui::SameLine(wmax - w);
            ImGui::TextColored(headertc, "%s", buf);
        }
#else
        ImGui::TextColored(headertc, "%s", titleText);
#endif        

        if (ind) ImGui::Unindent(ind);

        ImGui::EndChild();  // end titlebox
    }

    // show page is set when we are showing a page with "continue"
    // and we're not the last page.
    bool showpage = false;
    bool showmore = false;
    static bool updatescroll = false;

#ifdef LOGGING    
    // special key to hide text so we can take image screenshots
    static bool disableKeyDown = false;
    bool ddown = ImGui::IsKeyPressed(ImGuiKey_Pause);
    if (ddown != disableKeyDown)
    {
        disableKeyDown = ddown;
        if (ddown)
        {
            disableText = !disableText; // toggle
            if (!disableText) updatescroll = true;
        }

    }

    // debug key to force texture allocation to fail
    ddown = ImGui::IsKeyPressed(ImGuiKey_F12);
    if (ddown && !noTextureMemory)
    {
        LOG2("DEBUG testing texture memory limit ", 0);
        noTextureMemory = true;

        // also chuck all non-essential textures?
        //purgeTextures = true;
    }
#endif

#if defined(LOGGING) || defined(USERDEBUG)
    static bool memKeyDown = false;
    bool md = ImGui::IsKeyPressed(ImGuiKey_F9);
    if (md != memKeyDown)
    {
        memKeyDown = md;
        if (md) showUserDebug = !showUserDebug;
    }
#endif

    bool hover = HoverOK();
    
    if (txy > 0)
    {
        // main client area
        // mobile has no scrollbar
        bool hasScrollbar = !isSmallScreen && !disableText;
        bool vnmode = VNMODE;

        Boxf textBox;
        Boxf cr1;

        bool needUpdate = sctx->_mainText._textLayoutNeeded;

        {
            SCTX_LOCK(sctx); 

            showpage = !needUpdate && !sctx->_mainText.onLastPage();
            
            if (vnmode)
            {
                const Boxf& uview = showpage ? clientPage : clientBox;
                sctx->_vn.updateRoot(clientPage, uview, renderCount);
                sctx->_vn.updateWorld(timeNow);

                // fetch vn layout text box
                auto* tn = sctx->_vn._world._textArea;
                if (tn) textBox = tn->_pos;
                else LOG1("VN mode WARNING no text box ", 0);
            }

            if (!textBox)
            {
                // by default we fill the client area with text box
                // and for non-vn
                textBox = showpage ? clientPage : clientBox;
            }

            approxTextArea = textBox; // used when changing font

            // calculate the text box client area
            ImGuiStyle& style = ImGui::GetStyle();
            cr1 = textBox;
            cr1.expand(-wpad.x, -wpad.y);
        
            // adjust for any scrollbar
            if (hasScrollbar) cr1.expandRight(-style.ScrollbarSize);
        
            {
                //SCTX_LOCK(sctx); // lock while we prepare text
                sctx->_mainText._vnmode = VNMODE;
                sctx->_mainText._renderCount = renderCount;
                sctx->_mainText.setViewBox(cr1);
                sctx->_mainText._margins = margin;
                sctx->_mainText.setRoster(&sctx->roster());

                // first pass. if we needed to layout, then something has
                // changed and we need to scroll to end
                bool changed = sctx->_mainText.renderFirst();

                if (changed)
                {
                    updatescroll = true;
                    if (sctx->_mainText.paginateNeeded())
                    {
                        Boxf b = clientPage;
                        if (vnmode)
                        {
                            // perform world update again with page box
                            // because the text box may now be different
                            sctx->_vn.updateRoot(clientPage, clientPage,
                                                renderCount);
                            sctx->_vn.updateWorld(timeNow);
                            
                            // new text area 
                            auto* tn = sctx->_vn._world._textArea; 
                            if (tn) b = tn->_pos;
                        }

                        b.expand(-wpad.x, -wpad.y);
                        sctx->_mainText.paginate(b); // set pagecount
                    }
                }
            }

            showpage = !sctx->_mainText.onLastPage();

            if (showpage)
            {
                if (vnmode)
                {
                    auto* tn = sctx->_vn._world._textArea; 
                    if (tn) textBox = tn->_pos;
                }

                cr1 = textBox;
                cr1.expand(-wpad.x, -wpad.y);                
            }
        }

        if (vnmode)
        {
            // make a container for the client area
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,0));
            ImGui::BeginChild("client",
                              (showpage ? IV2(clientPage) : IV2(clientBox)),
                              false,
                              ImGuiWindowFlags_NoScrollbar);
            ImGui::PopStyleVar(1);
        }
        
        if (updatescroll)
        {
            updatescroll = false;
            int vh = cr1.height();
            //LOG1("scroll view, cry ", cr1.y << " pagey:" << crpage.y << " viewy " << viewsz.y);
            int scrolly = sctx->_mainText.calculateScroll(vh);

            if (scrolly)
            {
                //LOG1("scrolling by", scrolly);
                ImGui::SetNextWindowContentSize(ImVec2(0, vh + scrolly));
                ImGui::SetNextWindowScroll(ImVec2(0,scrolly));
            }
        }

        // full screen image?
        bool showingImage = sctx->_mainText._viewImageReady;

        int f = hasScrollbar ?
            ImGuiWindowFlags_AlwaysVerticalScrollbar :
            ImGuiWindowFlags_NoScrollbar;

        // disable scrollbar
        if (showingImage)
        {
            // add back the space for scrollbar that's not there
            if (hasScrollbar) cr1.expandRight(style.ScrollbarSize);
            sctx->_mainText.setViewBox(cr1);
            f = ImGuiWindowFlags_NoScrollbar;
        }

        if (vnmode)
        {
            SCTX_LOCK(sctx);
            sctx->_vn.render(timeNow);
            
            // only set this in vnmode because otherwise we're not
            // creating the client container window
            ImGui::SetCursorPos(ImVec2(textBox[0], textBox[1]));
        }

        bool textBorder = !VNMODE;

        ImGui::PushStyleColor(ImGuiCol_ScrollbarBg, ImVec4(0, 0, 0, 0));
        ImGui::BeginChild("Text", IV2(textBox), textBorder, f);
        
#ifdef LOGGING
        /*
        ImVec2 cr = ImGui::GetContentRegionAvail();
        if (cr.x != cr1.x || cr.y != cr1.y)
        {
            LOG2("text content region calculated wrong (", cr1.x << "," << cr1.y << ") actual (" << cr.x << "," << cr.y << ")");
        }
        */
#endif

        //ImGuiWindow* window = ImGui::GetCurrentWindowRead();            
        //LOG1("window scroll:", window->Scroll.y);

        // second pass, the actual render
        if (!disableText)
            sctx->_mainText.renderSecond(sctx->_vn._world._textOverlap);

        // clicked on a url?
        // if so, clear it, otherwise it is picked up in `yieldCmd`
        string c = sctx->getClickCommand(); 
        if (urlClicked(c)) sctx->clearClickCommand();

        if (!showpage && hover)
        {
            // scroll up and down with keyboard, but not during "more"

            // NB: no longer allow arrows to scroll text
            // only page up and down.
            // this is so arrows can scroll choices
            
            int dy = 0;

            if (ImGui::IsKeyDown(ImGuiKey_PageDown)) dy = 10;
            else if (ImGui::IsKeyDown(ImGuiKey_PageUp)) dy = -10;
            //else if (ImGui::IsKeyDown(ImGuiKey_DownArrow)) dy = 1;
            //else if (ImGui::IsKeyDown(ImGuiKey_UpArrow)) dy = -1;

            if (dy)
            {
                assert(sctx->_mainText.onLastPage());
                float fy = ImGui::GetFontSize()*2;
                float y = ImGui::GetScrollY();
                y += dy*fy;
                ImGui::SetScrollY(y);
            }
        }

        ImGui::EndChild(); // text
        ImGui::PopStyleColor(1);

        if (vnmode)
            ImGui::EndChild(); // client

        showmore = showpage || choiceContinue;

        // disable save/load during more (allow for choice/choiceContinue)
        saveEnabled = !showmore;
        loadEnabled = !showmore;
        restartEnabled = !showmore;

        if (showmore && !suspendInput)
        {
            // ensure we're not opening a file browser
            needOpenBrowser = false;
            expectLoad = false;
            expectSave = false;
            
            ImVec2 moreBoxSz(viewsz.x, inputy);

            // vn mode page option
            int f = ImGuiWindowFlags_NoScrollbar;
            ImGui::BeginChild("Page", moreBoxSz, true, f);

            scalePadding();
            bool ent = continueKeys();
            bool b = ImGui::Button("Continue", ImVec2(-1,-1));
            unscalePadding();
            
            if (b || ent)
            {
                sctx->_mainText.setViewImage(0); // release any zoom image
                if (showpage)
                {
                    releaseMore();
                    updatescroll = true;
                }
                else if (choiceContinue)
                {
                    // simulate choosing the one continue option.
                    makeChoice(0);
                }
            }
            ImGui::EndChild();        
        }
    }

    // choice box
    if (cby > 0 && !showmore && !suspendInput)
    {
        int f = ImGuiWindowFlags_NoScrollbar;
        ImGui::BeginChild("choice", choiceBoxSz, true, f);

        //  this feature is not used anymore, but might be reinstated.
        if (ch->_header)
        {
            ImGui::TextColored(ImVec4(1,1,0,1), "%s",
                               ch->_header._text.c_str());
        }

        int n = ch->size();
        assert(n > 0);

        if (ch->_selected < 0 && !inputActive)
        {
            // none have focus, default to first one
            ch->_selected = 0;
            //const std::string& t = ch->_choices[0]._text._text;
            //setF(t.c_str());
        }

        if (!textinputfocus && hover)
        {
            // allow arrows to cycle choices
            if (ImGui::IsKeyPressed(ImGuiKey_DownArrow, false))
            {
                // if -1, then 0
                ch->_selected = (ch->_selected + 1) % n;
            }
            else if (ImGui::IsKeyPressed(ImGuiKey_UpArrow, false))
            {
                // if -1, then 0
                if (ch->_selected < 0) ch->_selected = 0;
                else ch->_selected = (ch->_selected - 1 + n) % n;
            }
        }

        bool accept = false;

        scalePadding();
        
        for (int i = 0; i < n && !accept; ++i)
        {
            const std::string& t = ch->_choices[i]._text._text;
            
            if (ImGui::RadioButton(t.c_str(), ch->_selected == i))
            {
                ch->_selected = i;
                accept = true;
            }

            if (ImGui::IsKeyPressed(ImGuiKey_Enter, false)
                && hover
                && ch->_selected >= 0)
            {
                accept = true;
                //LOG1(">>> choice selected ", ch->_selected);
            }
        }

        unscalePadding();

        if (accept)
        {
            assert(ch->_selected >= 0 && ch->_selected < n);
            makeChoice(ch->_selected);
        }

        ImGui::EndChild();
    }

    static bool claimed = false;

    if (cby) claimed = false;

    if (inputActive && !showmore && !suspendInput)
    {
        // input box
        static char buf1[256];
        int f = ImGuiWindowFlags_NoScrollbar;
        ImGui::BeginChild("Input", inputBoxSz, true, f);
        ImGui::SetNextItemWidth(inputBoxSz.x - pad.x*2);

        bool reclaim_focus = false;

        int tf =
            ImGuiInputTextFlags_CallbackCompletion
            | ImGuiInputTextFlags_CallbackHistory
            | ImGuiInputTextFlags_EnterReturnsTrue
            //| ImGuiInputTextFlags_CallbackEdit
            ;

        scalePadding();

        std::string pr = sctx->h.prompt();
        pr += " type here";
        if (ImGui::InputTextWithHint("InputText",
                                     pr.c_str(), buf1, sizeof(buf1),
                                     tf, inputCallback))
        {
            if (*buf1) // prevent repeating blanks
            {
                lastInputBuf.clear();
            
                sendCmd(buf1);
                *buf1 = 0; // clear
            }
            reclaim_focus = true;
        }

        unscalePadding();

        // when we go from choices to no choices
        // claim the focus back to the input line.
        //if (cby) claimed = false;

        if (!cby)
        {
            // claim focus just the once 
            if (!claimed)
            {
                reclaim_focus = true;
                claimed = true;
            }
        }

        if (reclaim_focus)
            ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget

        textinputfocus = ImGui::IsItemActive();

        if (showKeyboard)
        {
            ImVec2 cr = adjustForPadding(viewsz);
            float pad = (cr.x - keyboardSz.x)/2;
            if (pad > 0) ImGui::Indent(pad);
            vKeyboard.show();
            if (pad > 0) ImGui::Unindent(pad);
        }
        
        ImGui::EndChild(); // input box
    }

    ImGui::End();
}

int ImageMaxSettingsSize(bool* hasVersions)
{
    int w = 0;

    auto mi = sctx->_imMeta.findBiggestImage(hasVersions);
    if (mi)
    {
        w = mi->_w;
        
        // round up to 1000s
        w = (w + 999)/1000;
        w *= 1000;
    }
    return w;
}

int ImageIdealSize()
{
    // in settings?
    int idealSize = PROPI(P_PICTURE_RES, P_PICTURE_RES_DEF);

    if (!idealSize)
    {
        if (isMobile && isWeb)
        {
            // smallest
            idealSize = IMAGE_IDEAL_SIZE_WEB_MOBILE;
        }
        else if (isMobile)
        {
            // android
            idealSize = IMAGE_IDEAL_SIZE_ANDROID;
        }
        else if (isWeb)
        {
            idealSize = IMAGE_IDEAL_SIZE_WEB;
        }
    }
    
    // there could be bigger images available
    if (!idealSize)
        idealSize = IMAGE_IDEAL_SIZE_NORMAL;
    
    return idealSize;
}

std::string GetUserDebugStats()
{
    std::string s;

    char buf[128];

    if (androidDPI)
    {
        sprintf(buf, "dpi:%d/%.1f w:%d h:%d f:%d m:%d",
                androidDPI,
                app_dpi_scale,
                sapp_width(),
                sapp_height(),
                (int)fontSize,
                (int)margin);
    }
    else
    {
        sprintf(buf, "dpi:%.1f w:%d h:%d f:%d m:%d",
                app_dpi_scale,
                sapp_width(),
                sapp_height(),
                (int)fontSize,
                (int)margin);
    }
            
    s += buf;
                
    if (texLoader)
    {
        std::stringstream ss;
        double t = texLoader->_maxLoadTime;

        if (t > 0)
        {
            double z = texLoader->_maxLoadSize/1024.0; // kb
            sprintf(buf, "\nMaxt time:%.1f Size:%.1f", t, z);
            ss << buf;
            ss << texLoader->_loadStats;
            s += ' ';
            s += ss.str();
        }
    }
    return s;
}

string selectPictureAlternative(const string& name)
{
    // NB: can be a video
    
    int idealSize = ImageIdealSize();
    
    if (texLoader)
    {
        double t = texLoader->_maxLoadTime;

        if (t > MAX_ACCEPTABLE_LOADTIME)
        {
            //LOG1("Max image load ", texLoader->_maxLoadName << " time:" << t << " for size " << texLoader->_maxLoadSize << "Kb");
            //LOG3("load Stats ", texLoader->_loadStats);

            // too slow

            // clear load stats between resolution adjustments
            texLoader->updateLoadStats(0, 0);
            
            int newSize = idealSize;

            if (!newSize)
            {
                // if not set, select nominal slow size
                newSize = IMAGE_IDEAL_SIZE_SLOW;
            }
            else
            {
                newSize -= 1000;
            }

            // prevent too low
            if (newSize < 1000) newSize = 1000;

            if (newSize != idealSize)
            {
                LOG1("Image loading too slow, " << t << " adjusting to ", newSize << " for " << name);

                idealSize = newSize;
                SPROP(P_PICTURE_RES, idealSize);
            }
        }
    }

    const ImageMeta* im = sctx->_imMeta.findBestVersion(name, idealSize);
    if (im && im->_name != name)
    {
        LOG2("show_picture replacing ", name << " with " << im->_name);
        return im->_name;
    }
    return name;
}

void show_picture(PictureInfo& pnfo)
{
    string name = pnfo._name;
    pnfo._name = selectPictureAlternative(name);
    
    SCTX_LOCK(sctx);

    if (!VNMODE || pnfo._intext)
    {
        sctx->_mainText.addImage(pnfo);
    }

    if (VNMODE && !pnfo._intext && !pnfo._preload)
    {
        VarList vs;
        vs.add(IFI_RESOURCE, name);
        vs.add(IFI_NAME, name);
        sctx->_vn.updateNode(vs);
    }
}

void show_asset(const VarList& vs)
{
    SCTX_LOCK(sctx);    
    sctx->_vn.updateNode(vs);
}

void SetAudioLevel(int percent)
{
    // XX gain can be > 100%
    
    if (percent < 0) percent = 0;

    int gain = PROPI(P_AUDIOLEVEL, P_AUDIOLEVEL_DEF);
    if (gain != percent)
    {
        SPROP(P_AUDIOLEVEL, percent);
    }
    
    audioState._currentGain = percent/100.0f;
}

void play_audio(const AudioInfo& ai)
{
    // vol is a percentage
    // duration; -1 = loop, 0 = stop this sound

    bool ae = PROPB(P_ENABLE_AUDIO, P_ENABLE_AUDIO_DEF);
    if (ae)
    {
        AUDIO_LOCK(audioState);

        if (!ai._dur)
        {
            LOG2("request to stop audio ", ai._name);
            audioState.stopTrack(ai._name);
        }
        else
        {
            LOG2("request to play audio ", ai);

            if (ai._chan > 0)
            {
                // stop any tracks on same channel with a different name
                // NB: except channel 0
                audioState.stopOtherChannelTracks(ai);
            }
            
            AudioState::Track* tr = audioState.internTrack(ai);

            // either set or reset the cache state
            
            auto cursor = audioState._trackPlaying(tr);
            if (cursor)
            {
                LOG2("audio already playing ", ai._name);
                if (ai._chan > 0)
                {
                    // start from beginning if this is not a background sound
                    cursor->reset();
                }
            }
            else
            {
                // NB: track could be already loading
                
                tr->_preload = ai._preload;

                // XX hack
                // when we play another background loop, stop current one.
                // and all other playing sounds.
                // NB: chan < 0 => default
                if (!tr->_preload && ai._dur < 0 && ai._chan <= 0)
                    audioState._fadeTracks();

                // vol 0, means 100%
                int vol = ai._vol;
                int dur = ai._dur;
                if (vol <= 0) vol = 100;
                if (vol != 100) tr->_level = vol/100.0f;

                if (dur != 1)
                {
                    tr->_repeats = dur < 0 ? 1000 : dur-1; // XX
                }

                // if in cache, play it now
                if (!tr->_preload && audioState._playTrack(tr))
                {
                    LOG2("audio in cache ", ai._name);
                }
            }
        }
    }
}


void play_animation(AnimInfo* ai)
{
#ifdef USESPINE
    LOG2("play_animation ", *ai);

    // no point in loading otherwise
    SCTX_LOCK(sctx);
    sctx->_mainText.setAnimation(ai);
#endif    
}

static int calculateIdealFontSize(int* margin, int forcesize)
{
    // return ideal font size unless `forcesize` is set for a given
    // font size
    // also calculate the (total) margin for this size

    // this is not usually exact as we calculate the size before areas
    // are set up.
    assert(approxTextArea);
    float w = approxTextArea.width(); 
    float h = approxTextArea.height();

    //LOG1("Idealfontsize, area ", w << 'x' << h);
    int cpl = 60;

#define GR ((52.0f-32.0f)/(360-440))
#define CR (52.0f - 360*GR)

    if (isMobile && !isWeb)
    {
        // android
        if (androidDPI > 300)
        {
            // apply a reduction with DPI
            // 360 ~ 52
            // 440 ~ 32
            int cpl1 = ROUND(androidDPI * GR + CR);
            //LOG1(">>>> CPL1 calculated at ", cpl1 << " GR:" << GR << " CR:" << CR);
            if (cpl1 < 30) cpl1 = 30;  // limit eg for 480
            if (cpl1 < cpl) cpl = cpl1;
        }
    }
    
    float fh = forcesize;
    float aspect = 0.5f; // nominal char w/h

    float fakemargin = marginMin*2;
    int m = fakemargin;
    if (w > fakemargin)
    {
        bool hlimited = false;

        if (!fh)
        {
            float fw = (w - fakemargin)/cpl;
            fh = fw/aspect;

            int lines = 16;
            if (VNMODE) lines = 13; 

            // based on wanted lines, this will be the max font height
            int maxfonth = h/lines;
            if (fh > maxfonth)
            {
                hlimited = true;
                fh = maxfonth;
            }
        }
        
        m = u_max(w - fh*aspect*cpl, fakemargin);

        if (m > fakemargin)
        {
            float fmm = m/fakemargin;

            // if we had to shrink font to accommodate lines
            // then relax the margin so we can have more words per line
            // even if this is more than the ideal length
            if (hlimited && fmm > 4)
            {
                m = fakemargin*4;
            }

            //LOG1("calculated margin is fake * ", fmm << " relaxed to " << m/fakemargin);
        }
        
        LOG2("Calculated ideal font size ", fh << " margin:" << m << " in " << w << "x" << h);
    }

    if (!forcesize && fh < 10)
    {
        // emergency maximize all space
        fh = 10;
        m = 0;
    }

    assert(m >= 0);
    *margin = m;

    //LOG1("Calculated ideal font size for area ", approxTextArea << " font:" << fh << " margin:" << m);
    
    return fh;
}


static void setFontConfigSize(ImFontConfig& cfg, float size, bool icons)
{
    // XX adjust icons a bit smaller.
    //if (icons) size = size*3/4;
    
    cfg.SizePixels = size;

    float dpi = app_dpi_scale;
    if (dpi > 1)
    {
        LOG3("font raster density ", dpi);
        cfg.RasterizerDensity = dpi;
    }

    if (icons)
    {
        cfg.PixelSnapH = true;
        cfg.GlyphMinAdvanceX = size;

        // XX hack otherwise icons appear at top of line
        //cfg.GlyphOffset = ImVec2(0,size/4);
    }
}

static void adjustIdealFontSize()
{
    // this also adjusts margin
    
    int mjt = 0;
    int fz = 0;

    // force it to be the size chosen
    if (fontSizeChosen) fz = fontSize;
    
    fz = calculateIdealFontSize(&mjt, fz);

    assert(fz);

    if (fz != fontSize)
    {
        //LOG1(">> font size changed from ", fontSize << " to " << fz);
        fontSize = fz;
        fontSizeChanged = true;
    }

    if (!fontSizeChosen)
    {
        // maintain the total and the actual each side
        // the total is used to accumulate relative change
        setMargin(mjt);
    }
}

static void makeFonts()
{
    // create font texture for the custom font
    if (fontChanged || fontSizeChanged)
    {
        sctx->_mainText.fontSizeChanged();
        layoutChanged();

        auto& io = ImGui::GetIO();

        simgui_image_t si = simgui_image_from_imtextureid(io.Fonts->TexID);
        //LOG1("font deleting old atlas tex ", FROMTEXID(io.Fonts->TexID));
        simgui_image_desc_t idesc = simgui_query_image_desc(si);
        sg_destroy_image(idesc.image);
        simgui_destroy_image(si);

        // need to do this because we want to regenerate the atlas
        io.Fonts->ClearTexData();
        //io.Fonts->Build();

        // when font size changes, this can cause another
        // font size change!
        if (isDynamicSize && !fontSizeChanged) adjustIdealFontSize();

        fontChanged = false;
        fontSizeChanged = false;
        
        for (int i = 0; i < io.Fonts->Fonts.Size; ++i)
        {
            ImFontConfig& ci = io.Fonts->ConfigData[i];

            // recover from the name of the font
            FontFileInfo* fi = findFontInfo(ci.Name);
            assert(fi);
            setFontConfigSize(ci, fontSize, fi->_icons);
            LOG2("setting font size ", ci.Name << " to " << fontSize);
        }


        unsigned char* font_pixels;
        int font_width, font_height;
        io.Fonts->GetTexDataAsRGBA32(&font_pixels, &font_width, &font_height);

        //LOG1("font atlas size ", font_width << "," << font_height);
        
        //printf("font w:%d, h:%d\n", font_width, font_height);
        sg_image_desc img_desc = { };
        img_desc.width = font_width;
        img_desc.height = font_height;
        img_desc.pixel_format = SG_PIXELFORMAT_RGBA8;
        img_desc.data.subimage[0][0].ptr = font_pixels;
        img_desc.data.subimage[0][0].size = font_width * font_height * 4;
        sg_image sgi = sg_make_image(&img_desc);
		
        simgui_image_desc_t foo{ sgi, samplerHolder.getSampler() };
        io.Fonts->TexID = simgui_imtextureid(simgui_make_image(&foo));

        // so we can click on words 
        io.MouseDragThreshold = fontSize;
    }
}


static FontFileInfo* findFontBoldFor(FontFileInfo* fi)
{
    // return the corresponding bold font.
    
    assert(!fi->_bold);
    assert(fi->isBase() || fi->_italic); 

    bool isLight = fi->_light;
    bool isItalic = fi->_italic;

    FontFileInfo* best[3] = { 0,0,0 };
    
    std::string sname = fi->stemName(); // fontStem(fi->_name);
    for (auto& ffi : fontFiles)
    {
        if (ffi._italic != isItalic) continue; // if italic, must agree
        
        if (ffi.stemName() == sname)
        {
            // light => light/bold, regular or regular/bold
            // regular => bold

            if (isLight)
            {
                // first choice
                if (ffi._bold && ffi._light)
                {
                    best[0] = &ffi;
                    break;
                }

                // second choice
                if (!ffi._bold && ffi._regular) best[1] = &ffi;
                
                // third choice
                if (ffi._bold && ffi._regular) best[2] = &ffi;
            }
            else
            {
                if (ffi._bold && ffi._regular)
                {
                    best[0] = &ffi;
                    break;
                }
            }
        }
    }

    // best choice
    for (int i = 0; i < 3; ++i) if (best[i]) return best[i];
    return 0;
}

static FontFileInfo* findFontItalicFor(FontFileInfo* fi)
{
    assert(!fi->_italic);
    assert(fi->isBase());
    
    bool isLight = fi->_light;
    bool isBold = fi->_bold;

    FontFileInfo* best[2] =  { 0, 0 };
    
    std::string sname = fi->stemName();
    for (auto& ffi : fontFiles)
    {
        if (ffi._bold != isBold) continue; // if were bold need italic bold

        if (!ffi._italic) continue; // need some form of italic regardless
        
        if (ffi.stemName() == sname)
        {
            // light => light/italic, regular/italic
            // regular => regular/italic
            if (isLight)
            {
                if (ffi._light) { best[0] = &ffi; break; }
                if (ffi._regular) best[1] = &ffi;
            }
            else
            {
                if (ffi._regular) { best[0] = &ffi; break; }
            }
        }
    }

    for (int i = 0; i < 2; ++i) if (best[i]) return best[i];
    return 0;
}

#define BASEFONT "merriweather-light"


#define ICON_FONT_NONE 0
#define ICON_FONT_FA 1
#define ICON_FONT_MATERIAL 2

static int isIconFont(const std::string& name)
{
    // table ordering must correspond with definitions above.
    int v = 0;
    
    static const char* names[] =
    {
        "fa-", // fontawesome
        "MaterialIcons", // google
    };

    for (int i = 0; i < ASIZE(names); ++i)
    {
        if (findFirst(name, names[i]))
        {
            v = i+1;
            break;
        }
    }

    //LOG1("isIconFont ", name << " = " << v);

    return v;
}

static void choose_basefont()
{
    // select the default game font.
    // NB: the game has to be running for this to ask it
    
    // do we have a meta suggestion?
    string gamefont = sctx->h.getGameFont();

    // otherwise use our own suggestion
    if (gamefont.empty())
    {
        gamefont = BASEFONT;
        LOG2("game does not suggest a font, fallback to ", gamefont);
    }

    FontFileInfo* basefont = findBestFont(gamefont);

    LOG3("basefont: ", (basefont ? basefont->_name : "NONE"));

    if (basefont)
    {
        // kick off loading of the first font
        basefont->_requested = true;
    }
}

static void font_list_loaded_data(const char* name,
                                  const char* fdata, int sz)
{
    fontListLoaded = 1; // success
    
    // expect fontlist.txt in unix format (not DOS)
    std::vector<std::string> fonts;
    split(fonts, fdata, '\n');

    // the font list is the file names of the fonts without any path.
    for (uint i = 0; i < fonts.size(); ++i)
    {
        std::string& fname = fonts[i];
        std::string f = toLower(fname);

        // ignore any non font files
        if (suffixOf(f) != ".ttf") continue;
        
        FontFileInfo ffi(fname);
        if (f.find("bold") != std::string::npos) ffi._bold = true;
        if (f.find("italic") != std::string::npos) ffi._italic = true;
        if (f.find("light") != std::string::npos) ffi._light = true;
        if (f.find("regular") != std::string::npos) ffi._regular = true;

        // use original name including case
        if (isIconFont(fname))
        {
            ffi._icons = true;

            // request all icon fonts to preload
            ffi._requested = true;
        }
        
        ffi.validate();
        fontFiles.push_back(ffi);
    }

#ifdef LOGGING
    LOG2("Loaded font list ", fontFiles.size());
    int cc = 0;
    for (auto& i : fontFiles)
    {
        ++cc;
        LOG2(cc, " " << i);
    }
#endif
}


static void font_list_loaded(FetchInfo* fo, void* ctx)
{
    if (*fo)
    {
        int sz;
        char* fdata = fo->donate(sz);
        font_list_loaded_data(fo->_name.c_str(), fdata, sz);
        delete fdata;
    }
    else
    {
        // failed
        fontListLoaded = -1;

        LOG1("WARNING: Missing Font list file: ", fo->_name);
        abort();
    }
}

static const ImWchar* GetGlyphRangesIFI(FontFileInfo* fi)
{
    int icon = isIconFont(fi->_name);
    
    static const ImWchar ranges[] =
    {
        0x0020, 0x00FF, // Basic Latin + Latin Supplement
        0x2013, 0x2014, // long hyphens
        0x2018, 0x2019, // apostrophes left & right
        0x201C, 0x201D, // open and close quote
        0x2022, 0x2022, // bullet
        0x2026, 0x2026, // ellipsis
        0
    };

    static const ImWchar fa_ranges[] =
    {
        0xf0e2, 0xf0e2,  // ICON_FA_ARROW_ROTATE_LEFT
        0xf021, 0xf021,  // ICON_FA_ARROWS_ROTATE
        0
    };


    static const ImWchar material[] =
    {
        0xe166, 0xe166,  // ICON_MD_UNDO
        0xe5d3, 0xe5d3,   // ICON_MD_MORE_HORIZ
        0xe5d5, 0xe5d5,  // ICON_MD_REFRESH

        //0xe1a1, 0xe1a1,  // ICON_MD_INVENTORY_2
        0xe60e, 0xe60e,  // ICON_MD_ADB
        0xe83a, 0xe83a,   //ICON_MD_STAR_BORDER
        
        0xe316, 0xe317,  // ICON_MD_KEYBOARD_ARROW_UP, ICON_MD_KEYBOARD_BACKSPACE
        0xe31b, 0xe31c,  // ICON_MD_KEYBOARD_RETURN, ICON_MD_KEYBOARD_TAB
        0xe256, 0xe256,  // ICON_MD_SPACE_BAR
        0
    };
    
    switch (icon)
    {
    case ICON_FONT_FA:
        return fa_ranges;
    case ICON_FONT_MATERIAL:
        return material;
    }

    return ranges;
}


static bool font_loaddata(FontFileInfo* fi)
{
    ImGuiIO& io = ImGui::GetIO();

    assert(fi->_data && fi->_dsize);
    assert(!fi->_imFont);

    const ImWchar* gr = GetGlyphRangesIFI(fi);

    bool ok = gr != 0;

    if (ok)
    {
        ImFontConfig cfg;
        cfg.FontDataOwnedByAtlas = false; // we own the data
        setFontConfigSize(cfg, fontSize, fi->_icons);
        ImFormatString(cfg.Name, IM_ARRAYSIZE(cfg.Name), "%s",
                       fi->_displayName.c_str());

        LOG2("installing font ", fi->_name << " size:" << fontSize << " cfg:" << cfg.Name);

        ImFont* imf =
            io.Fonts->AddFontFromMemoryTTF((void*)fi->_data, fi->_dsize,
                                           fontSize, &cfg, gr);

        ok = imf != 0;
        if (ok) fi->_imFont = imf;
    }
    
    if (!ok)
    {
        LOG1("no glyphs loaded for font ", fi->_name);
    }
    
    return ok;
}

static void font_loaded(FetchInfo* fo, void* ctx)
{
    if (*fo)
    {
        FontFileInfo* fi = findFontInfo(fo->_name);
        assert(fi);
        assert(!fi->_data);

        // we keep the data once loaded
        fi->_data = fo->donate(fi->_dsize);
    }
    else
    {
        LOG1("failed to load font ", fo->_name);
    }
}

static void imagemeta_loaded_data(const char* name,
                                  const char* data, int sz)
{
    imageMetaLoaded = 1;  /// whether or not it restores.
            
    LOG2("ImageMeta json loaded ", sz);
    SCTX_LOCK(sctx);        
    if (sctx->restoreImageMeta(data))
    {

        // give the meta also to the tex loader
        // this is used to determine the file size and how to fetch
        assert(texLoader);
        texLoader->_imMeta = &sctx->_imMeta;
    }
}

static void imagemeta_loaded(FetchInfo* fo, void* ctx)
{
    if (*fo)
    {
        int sz;
        char* data = fo->donate(sz);
        imagemeta_loaded_data(fo->_name.c_str(), data, sz);
        delete data;
    }
    else
    {
        // failed to load, but is not required
        imageMetaLoaded = -1;
    }
}

void stop_audio()
{
    LOG2("stop_audio ", 1);
    audioState.stopAllPlaying();
}

static bool audio_decode(const string& name,
                         const unsigned char* data,
                         int sz,
                         bool all)
{
    assert(data && sz);

    // each time we are called the same data extends
    // `all` true if all data loaded, otherwise streaming
    // ASSUME lock
    
    bool r = false;

    //LOG1("audio decode ", name << " " << sz);

    // ASSUME LOCK
    
    AudioState::Track* tr = audioState.findTrack(name);
    if (tr)
    {
        assert(tr->_state == AudioState::tr_loading);

        if (!tr->_decoder)
            tr->_decoder = new AudioDecodeState(name, tr);
        
        tr->_decoder->setData(data, sz);
        while (tr->_decoder->decodeStep()) ;

        //int total = tr->_decoder->_totalSamples;
        //LOG4("audio ", name << " decoded " << total << " samples.");

        if (all)
        {
            tr->_state = AudioState::tr_loaded;
            LOG2("audio load finished ", name);
        }

        r = !tr->_decoder->_failed;

        AUDIO_LOCK(audioState);
        
        auto cursor = audioState._trackPlaying(tr);

        if (r && !cursor)
        {
            // update LRU on newly loaded track
            audioState.touchTrack(tr);

            // ok if we're only to preload, otherwise play track
            if (!tr->_preload)
            {
                // can play a track that is still loading
                // providing enough has loaded
                audioState._playTrack(tr);
            }
        }

        if (all || !r)
        {
            delete tr->_decoder;
            tr->_decoder = 0;
        }
    }
    return r;
}

static void audio_loaded(FetchInfo* fo, void* ctx)
{
    if (*fo)
    {
        int sz = fo->_data.size();
        char* fdata = fo->_data.start();
        string name = fo->_name;

        assert(!fo->_cancel);

        // not done if streaming
        bool all = fo->_done;

        //LOG1("Loading audio ", name << " data size:" << sz);
        //AUDIO_LOCK(audioState);
        if (!audio_decode(name, (const unsigned char*)fdata, sz, all))
        {
            // cancel any loading
            // this will happen if the data doesn't decode
            // OR the track is gone because the audio is cancelled.

            // mark for cancel.
            fo->_cancel = true;
        }
    }
}

static bool getFontData(FontFileInfo* fi)
{
    assert(fi);
    
    if (!fi->_data && !fi->_loading)
    {
        // expect all fonts to be in "fonts" subdir
        std::string fpath = "fonts/" + fi->_name;

        FDLoad fd(fullPath(fpath));
        if (fd)
        {
            // mark as loading. if this fails to load we don't
            // keep trying to load
            fi->_loading = true;
            fi->_dsize = fd._size;
            fi->_data = (char*)fd.donate();
        }
        else
        {
            // mark fonts as "small" to load at once
            if (fetcher.start(fpath, font_loaded, true)) fi->_loading = true;
        }
    }

    return fi->_data != 0;
}

static void clearFonts()
{
    ImGuiIO& io = ImGui::GetIO();
    io.Fonts->Clear();
    for (auto& fi : fontFiles) fi._imFont = 0;
}
                          
static void loadFonts()
{
    FontFileInfo* rf = 0;

    // work on the icon fonts first
    for (auto& fi : fontFiles)
    {
        if (fi._requested)
        {
            rf = &fi;            
            if (fi._icons) break;
        }
    }

    if (!rf) return; 

    if (rf->isBase())
    {
        // check for data present, missing font, load data or fetch data
        bool fl = getFontData(rf);

        FontFileInfo* fb = findFontBoldFor(rf);
        bool flb = !fb || getFontData(fb);
        
        FontFileInfo* fi = findFontItalicFor(rf);
        bool fli = !fi || getFontData(fi);

        //LOG1("reqesting font ", rf->_name << " bold:" << (fb ? fb->_name : "none") << " italic:" << (fi ? fi->_name : "none"));

        // bold and italic do not have to exist
        if (fl && flb && fli)
        {
            clearFonts();

            sctx->_mainText.setFont(0);
            sctx->_mainText.setBoldFont(0);
            sctx->_mainText.setItalicFont(0);

            if (font_loaddata(rf))
            {
                LOG2("setting font regular ", rf->_name);
                sctx->_mainText.setFont(rf->_imFont);
            }

            if (fb && font_loaddata(fb))
            {
                LOG2("setting font bold ", fb->_name);
                sctx->_mainText.setBoldFont(fb->_imFont);
            }
            
            if (fi && font_loaddata(fi))
            {
                LOG2("setting font italic ", fi->_name);
                sctx->_mainText.setItalicFont(fi->_imFont);
            }

            // reload icons
            for (auto& fi : fontFiles)
                if (fi._icons) font_loaddata(&fi);

            // clear down now loaded
            rf->_requested = false;

            // signal to makefonts
            fontChanged = true;

            // now we have loaded at least one base font, we can render
            haveFonts = true;
        }
    }
    else
    {
        // non base fonts like icons
        assert(rf->_icons);

        bool fl = getFontData(rf);        

        if (fl)
        {
            LOG2("loaded icon font ", rf->_name);
            rf->_requested = false;
        }
    }
}

static void pollAudio()
{
    // load the audio in small chunks to work over web network
    const int audioChunk = 512*1024;
    
    //AUDIO_LOCK(audioState);
    for (auto ti : audioState._tracks)
    {
        if (ti->_state == AudioState::tr_notloaded)
        {
            ti->_state = AudioState::tr_loading;

            // streaming
            if (!fetcher.start(ti->_name, audio_loaded,
                              false, audioChunk))
            {
                LOG1("pollaudio fetcher cannot start ", ti->_name);
                ti->_state = AudioState::tr_notloaded;
            }
        }
    }
}

static void audio_stream_cb(float* buffer, int num_frames, int num_channels)
{
    audioState.audioCallback(buffer, num_frames, num_channels);
}

#ifdef __EMSCRIPTEN__
void syncFilesystem()
{
    LOG2("syncing filesystem", "");
    
  // sync from memory state to persisted and then
  // run 'success'
  EM_ASM(
         FS.syncfs(false,function (err) {
            if (err) console.log("failed to sync filesystem");
    });
  );
}

void initFilesystem()
{
    EM_ASM(
           FS.mkdir('/savegames');
           FS.mount(IDBFS, {}, '/savegames');

           // sync from persisted state into memory 
            FS.syncfs(true, function (err) {
            if (err)
                console.log("failed to init filesystem");
               });
           );
}
#else
void initFilesystem() {}
void syncFilesystem() {}
#endif

#ifdef LOGGING
void sokol_log(const char* tag,                // e.g. 'sfetch'
               uint32_t log_level,             // 0=panic, 1=error, 2=warn, 3=info
               uint32_t log_item_id,           // SFETCH_LOGITEM_*
               const char* message_or_null,    // a message string, may be nullptr in release mode
               uint32_t line_nr,               // line number in sokol_fetch.h
               const char* filename_or_null,   // source filename, may be nullptr in release mode
               void* user_data)
{
    const char* msg = message_or_null;
    if (!msg) msg = "";
    LOG2("sokol_log ", tag << " msg:" << msg);
}
#endif

static void enableMobile()
{
    isMobile = true;
    
    if (isMobile) isVirtualKeyboard = true; // for now
    if (isMobile) isSmallScreen = true; // for now

    if (isWeb && isMobile)
    {
        // web mobile force no font auto size
        isDynamicSize = false;
    }
    
    if (isMobile && !isWeb)
    {
        // android/ios ?
        choiceBoxScale = 4;
        marginMin = MARGIN_SIZE_DEFAULT;
    }

    //if (isMobile) enablePictureClickables = false;
}

void init()
{
    // setup sokol-gfx and sokol-time
    sg_desc desc = { };
    desc.environment = sglue_environment();
#ifdef LOGGING
    desc.logger.func = sokol_log;
#endif    
    sg_setup(&desc);
    
    saudio_desc adesc = {};
    adesc.sample_rate = 44100; // also default
    adesc.num_channels = 2; // stereo
#ifdef COOP    
    adesc.buffer_frames = 4*1024;  // default is 2048, make bigger
#else
    adesc.buffer_frames = 2048; 
#endif    
    adesc.stream_cb = audio_stream_cb;  // 0 for push model
    adesc.user_data = (void*)&audioState;
    
#ifdef LOGGING    
    adesc.logger.func = sokol_log;
#endif    
    saudio_setup(&adesc);

    // actual
    audioState._output_sample_rate = saudio_sample_rate();
    audioState._output_channels = saudio_channels();

    int gain = PROPI(P_AUDIOLEVEL, P_AUDIOLEVEL_DEF);
    SetAudioLevel(gain);

    fetcher.init("main");

    // setup sokol-imgui, but provide our own font
    simgui_desc_t simgui_desc = {};
    simgui_desc.no_default_font = true;
    //simgui_desc.sample_count = 0;
    //simgui_desc.image_pool_size = 256;
    simgui_desc.disable_windows_resize_from_edges = true;

#ifdef LOGGING    
    simgui_desc.logger.func = sokol_log;
#endif
    
    simgui_setup(&simgui_desc);

    // find and keep the operating DPI
    app_dpi_scale = fakeDpi;  // given by option override
    if (!app_dpi_scale)
        app_dpi_scale = sapp_dpi_scale();
    
    //LOG1("simgui_desc sample_count ", simgui_desc.sample_count);
    //LOG1("simgui_desc image_pool_size ", simgui_desc.image_pool_size);

    bool mobile = false;
    
#ifdef __EMSCRIPTEN__
    isWeb = true;
    mobile = (call_js_is_mobile() != 0);
#endif

#ifdef __ANDROID__
    mobile = true;
    androidDPI = android_dpi();
#endif

    if (mobile) enableMobile();
    
    // file with list of fonts on server
    requestFontList = "fontlist.txt";

    // request meta data for all images
    requestImageMeta = "imagemeta.json";
    
    // configure Dear ImGui with our own embedded font
    //    auto& io = ImGui::GetIO();
    
    //ImFontConfig fontCfg;
    //fontCfg.FontDataOwnedByAtlas = false;
    //fontCfg.OversampleH = 2;
    //fontCfg.OversampleV = 2;
    //fontCfg.RasterizerMultiply = 1.5f;
    //fontCfg.PixelSnapH = true; 
    
    //io.Fonts->AddFontFromFileTTF("fonts/Roboto-Regular.ttf", fontSize);
    //io.Fonts->AddFontFromFileTTF("fonts/Merriweather-Light.ttf", fontSize);

    // initial clear color
    sg_pass_action pa = {};
    sg_color_attachment_action& pa1 = pa.colors[0];
    pa1.load_action = SG_LOADACTION_CLEAR;
    pa1.clear_value = sg_color{0.45f, 0.55f, 0.6f, 1.0f};
        
    initFilesystem(); // emscripten

    fbrowser = new ImGui::FileBrowser();

    // make file browser simpler for mobile
    if (isMobile) fbrowser->_simple = true;
    fbrowser->SetPwd(saveBaseDir);
    fbrowser->SetTypeFilters({ ".sav" });

    ImGuiIO& io = ImGui::GetIO();
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.ConfigInputTextCursorBlink = false;
    //io.ConfigWindowsMoveFromTitleBarOnly = true;

    ImGuiStyle& style = ImGui::GetStyle();
    
    // set initial style values. these are updated
    // by settings directly.
    style.ScrollbarSize = PROPI(P_SCROLLBAR_SIZE, 24);
    style.ScrollbarRounding = 8;
    style.ChildRounding = 4.0f;
    style.ItemSpacing.y = 2.0f;

    int wpad = PROPI(P_WINDOWPADDING, 4);
    int fpad = PROPI(P_FRAMEPADDING, 2);
    style.WindowPadding = ImVec2(wpad,wpad);
    style.FramePadding = ImVec2(fpad,fpad);

    io.IniFilename = 0;
    style.HoverDelayNormal = 1.0f;
    
    ApplyDefaultStyle();

    texLoader = new ImTexLoader(fetcher);
    texLoader->_poolMaxSize = POOLSIZEMAX;

    sctx = new StrandCtx;

    if (isWeb)
    {
        texLoader->_poolMaxSize = POOLSIZEMAXWEB;

        // NB: already disabled. see `allowMipmaps`
        // but we might one day allow it!
        sctx->_vn._enableMipmaps = false;

        if (isMobile)
        {
            // trim things more for mobile web
            texLoader->_poolMaxSize = POOLSIZEMAXWEBMOBILE;
        }
    }

    if (isSmallScreen)
    {
        // expand touch events
        style.TouchExtraPadding = ImVec2(8,8);

        // make pictures bigger by default on small screens
        SPROP_DEFAULT(P_PICTURE_SIZE, 60);
    }

    if (disableMipmaps) sctx->_vn._enableMipmaps = false;

#ifdef LOGGING
    // debug builds start with command line if not mobile
    if (!isMobile && !asRelease) EnableCommandInput();

    // command line option to switch off (eg for screenshots)
#endif        

    forceCommandInput = PROPB(P_ENABLE_COMMANDLINE,
                              P_ENABLE_COMMANDLINE_DEF);

#ifdef USESPINE    
    static sg_buffer_desc sgb = {};

    // supply data later
    sgb.size = MAX_SVERTICES*sizeof(SVertex);
    sgb.usage = SG_USAGE_STREAM;
    sgb.label = "spine vertices";
    state.anim.bind.vertex_buffers[0] = sg_make_buffer(&sgb);

    static sg_buffer_desc sgb2 = {};
    sgb2.type = SG_BUFFERTYPE_INDEXBUFFER;
    sgb2.size = (MAX_SVERTICES*3/2)*sizeof(unsigned short);
    sgb2.usage = SG_USAGE_STREAM;
    sgb2.label = "spine indices";
    state.anim.bind.index_buffer = sg_make_buffer(&sgb2);

    static sg_pipeline_desc sgpip = {};
    sgpip.layout.attrs[ATTR_spine_vs_pos].format = SG_VERTEXFORMAT_FLOAT2;
    sgpip.layout.attrs[ATTR_spine_vs_color0].format   = SG_VERTEXFORMAT_FLOAT4;
    sgpip.layout.attrs[ATTR_spine_vs_texcoord0].format = SG_VERTEXFORMAT_FLOAT2;
    sgpip.layout.attrs[ATTR_spine_vs_color1].format = SG_VERTEXFORMAT_FLOAT3;

    const sg_shader_desc* sd = spine_scene_shader_desc(sg_query_backend());
    assert(sd);
    sgpip.shader =  sg_make_shader(sd);
    sgpip.index_type = SG_INDEXTYPE_UINT16;
    sgpip.depth.compare = SG_COMPAREFUNC_LESS_EQUAL;
    sgpip.depth.write_enabled = true;
    sgpip.cull_mode = SG_CULLMODE_BACK;

    sg_blend_state& bs = sgpip.colors[0].blend;
    bs.enabled = true;
    bs.src_factor_rgb = SG_BLENDFACTOR_SRC_ALPHA;
    bs.dst_factor_rgb = SG_BLENDFACTOR_ONE_MINUS_SRC_ALPHA;
    bs.src_factor_alpha = SG_BLENDFACTOR_ONE;
    bs.dst_factor_alpha = SG_BLENDFACTOR_ZERO;
    state.anim.pip = sg_make_pipeline(&sgpip);

    // set up things spine needs to load etc.
    SSpineCtx* ssc = SSpineCtx::get();
    ssc->_loader = texLoader;
#endif // USESPINE

#ifdef USEMPG
    {
        sg_buffer_desc vb = {};
        //vb.data = SG_RANGE(vertices);
        vb.size = MAX_MPG_VERTS*sizeof(mpg_vertex);
        vb.usage = SG_USAGE_STREAM;
        vb.label = "mpeg vertices";
        state.mpg.bind.vertex_buffers[0] = sg_make_buffer(&vb);

        sg_buffer_desc vi = {};
        vi.type = SG_BUFFERTYPE_INDEXBUFFER;
        vi.usage = SG_USAGE_STREAM;
        //vi.data = SG_RANGE(indices);
        vi.size = MAX_MPG_INDEX*sizeof(uint16_t);
        vi.label = "mpeg indices";
        state.mpg.bind.index_buffer = sg_make_buffer(&vi);

        sg_pipeline_desc d2 = {};
        auto& a1 = d2.layout.attrs;
        a1[ATTR_mpg_vs_pos].format = SG_VERTEXFORMAT_FLOAT2;
        a1[ATTR_mpg_vs_texcoord].format = SG_VERTEXFORMAT_FLOAT2;
        a1[ATTR_mpg_vs_color0].format = SG_VERTEXFORMAT_FLOAT4;
        a1[ATTR_mpg_vs_tcol0].format = SG_VERTEXFORMAT_FLOAT4;

        d2.shader = sg_make_shader(mpg_mpeg_shader_desc(sg_query_backend()));
        d2.index_type = SG_INDEXTYPE_UINT16;
        d2.cull_mode = SG_CULLMODE_NONE;
        d2.depth = {
            .compare = SG_COMPAREFUNC_LESS_EQUAL,
            .write_enabled = true
        };

        d2.colors[0] = {
            .write_mask = SG_COLORMASK_RGB,
            .blend = {
                .enabled = true,
                .src_factor_rgb = SG_BLENDFACTOR_SRC_ALPHA,
                .dst_factor_rgb = SG_BLENDFACTOR_ONE_MINUS_SRC_ALPHA,
            },
        };
                    
        state.mpg.pip = sg_make_pipeline(&d2);
        state.mpg.bind.fs.samplers[SLOT_mpg_smp] = samplerHolder.getSampler();
    }
#endif  // USEMPG

#ifdef USETAINT
    {

        // supply data later
        sg_buffer_desc vb = {};
        //vb.data = SG_RANGE(state.taint.sverts);
        vb.size = MAX_S_VERTS*sizeof(taint_vertex);
        vb.usage = SG_USAGE_STREAM;
        vb.label = "taint vertices";
        state.taint.bind.vertex_buffers[0] = sg_make_buffer(&vb);

        sg_buffer_desc vi = {};
        vi.type = SG_BUFFERTYPE_INDEXBUFFER;
        vi.usage = SG_USAGE_STREAM;
        //vi.data = SG_RANGE(indices);
        vi.size = MAX_S_INDEX*sizeof(uint16_t);

        vi.label = "taint indices";
        state.taint.bind.index_buffer = sg_make_buffer(&vi);

        sg_pipeline_desc d2 = {};
        auto& a1 = d2.layout.attrs;
        a1[ATTR_taint_vs_pos].format = SG_VERTEXFORMAT_FLOAT2;
        a1[ATTR_taint_vs_texcoord0].format = SG_VERTEXFORMAT_FLOAT2;
        a1[ATTR_taint_vs_color0].format = SG_VERTEXFORMAT_FLOAT4;
        a1[ATTR_taint_vs_tcol0].format = SG_VERTEXFORMAT_FLOAT4;

        d2.shader = sg_make_shader(taint_strand_shader_desc(sg_query_backend()));
        d2.index_type = SG_INDEXTYPE_UINT16;
        d2.cull_mode = SG_CULLMODE_NONE;
        d2.depth = {
            .compare = SG_COMPAREFUNC_LESS_EQUAL,
            .write_enabled = true
        };

        d2.colors[0] = {
            .write_mask = SG_COLORMASK_RGB,
            .blend = {
                .enabled = true,
                .src_factor_rgb = SG_BLENDFACTOR_SRC_ALPHA,
                .dst_factor_rgb = SG_BLENDFACTOR_ONE_MINUS_SRC_ALPHA,
            },
        };
                    
        state.taint.pip = sg_make_pipeline(&d2);
        state.taint.bind.fs.samplers[SLOT_taint_smp] = samplerHolder.getSampler();
    }
#endif    
    
    if (show_strand)
        strand_enabled = true; // start from main loop

    // initiate loading of files needed before we can run game.
    preloadFile(requestFontList, font_list_loaded, font_list_loaded_data);
    preloadFile(requestImageMeta, imagemeta_loaded, imagemeta_loaded_data);

#ifdef USE_FIBERS
    fiberState.init();
    fiberState.run(strandFiber); // this calls StrandInit
#else
    LOG2("starting strand thread ", requestStoryName);
    StrandInit(requestStoryName.c_str());
#endif    

}

static bool confirmDialog(bool* show,
                          const char* title,
                          const char* question,
                          const char* yes,
                          const char* no)
{
    if (*show)
    {
        ImGui::OpenPopup(title);
        *show = false;
    }
     
    bool confirm = false;        
    if (ImGui::BeginPopupModal(title,
                               nullptr,
                               ImGuiWindowFlags_AlwaysAutoResize))
    {
        ImGui::Text("%s", question);
        ImGui::Separator();

        float w = ImGui::CalcTextSize(yes).x;
        if (ImGui::Button(yes, ImVec2(w*2, 0)))
        {
            confirm = true;
            ImGui::CloseCurrentPopup();
        }
        
        ImGui::SetItemDefaultFocus();
        ImGui::SameLine();
        if (ImGui::Button(no, ImVec2(-1, 0))) ImGui::CloseCurrentPopup();
        ImGui::EndPopup();
    }
    return confirm;
}

static void performRequestLoad(const string& f)
{
    if (sctx->requestLoad(f))
    {
        // restore certain prop settings

        // is a font selected?
        string fn = PROPS(P_FONTNAME, "");
        if (!fn.empty()) selectFont(fn);

        int fz = PROPI(P_FONTSIZE, 0);
        if (fz) selectFontSize(fz);

        // what about theme?
        string ts = PROPS(P_THEMESTYLE, "");
        if (!ts.empty()) ChangeThemeStyle(ts);

        // restore audio gain
        int gain = PROPI(P_AUDIOLEVEL, P_AUDIOLEVEL_DEF);
        SetAudioLevel(gain);

        // and what about the command line?
        forceCommandInput = PROPB(P_ENABLE_COMMANDLINE,
                                  P_ENABLE_COMMANDLINE_DEF);
        
        startUnsaved = false; // no longer at start
    }
}

void frame()
{
    fetcher.poll();

    // wait until we have these before we can run the game
    // or they do not load
    if (!imageMetaLoaded || !fontListLoaded) return;

    static uint64 renderCount;
    ++renderCount;
        
    loadFonts();

    // switch to strand, which will produce output and/or yield back
    strand_pump();

    // poll after strand so to pick up audio requests quicker
    pollAudio();

    // poll after strand to pick up requests quicker
    // poll texture loading using the LAST rendercount
    // this is what all the existing textures will be stamped with for LRU
    // from the last render
    texLoader->poll();
    texLoader->setRenderCount(renderCount);

    // wait until base font loaded before render 
    if (!haveFonts)
    {
        static bool chosenFonts = false;
        
        // wait until the game is running AND we've received the game meta data
        if (sctx->h._receivedGameMeta)
        {
            if (!chosenFonts)
            {
                chosenFonts = true;

                // this will select a font,
                // request it to load then eventually set `haveFonts`
                choose_basefont();
            }

            if (!approxTextArea)
            {
                // need a basis for calculating the ideal font size
                // BEFORE we render
                // we can use the app size as a base and manually
                // run a world update to find the calculated text box
                // based on the app size.
                // this wont be the same as the value once the game
                // starts, but hopefully the font size will be close.
                Point2f appsize = getAppSize();
                approxTextArea[PX2] = appsize.x;
                approxTextArea[PY2] = appsize.y;

                if (VNMODE)
                {
                    //sctx->setAppSize(appsize.x, appsize.y);
                    sctx->_vn._world.updateRoot(approxTextArea, approxTextArea);
                    sctx->_vn.updateWorld(worldTime.duration());
                        
                    auto* tn = sctx->_vn._world._textArea;
                    if (tn) approxTextArea = tn->_pos;
                }

                //LOG1("approx Text Area ", approxTextArea);
                if (!approxTextArea)
                {
                    LOG2("world ", sctx->_vn._world);
                    assert(approxTextArea);
                }
            }
        }
        return;  // cant render yet
    }

    // cant build fonts until we have them (above)
    // however, cant render without fonts
    makeFonts();
    
#ifdef LOGGING
    // see if we want to save
    ImGuiIO& io = ImGui::GetIO();
    if (io.WantSaveIniSettings)
    {
        //LOG3("want to save ini file ", io.IniSavingRate);
        io.WantSaveIniSettings = false;
    }
#endif

    simgui_frame_desc_t dframe;
    dframe.width = sapp_width();
    dframe.height = sapp_height();

    dframe.delta_time = sapp_frame_duration();
    //dframe.delta_time = 1/20.0; // XX 20 FPS
    dframe.dpi_scale = app_dpi_scale;
    simgui_new_frame(dframe);

    if (show_strand) StrandWindow(&show_strand, renderCount);

    static bool settings_shown = false;
    static bool waiting_for_close = false;

    if (show_settings_window)
    {
        ImGui::OpenPopup("###Settings");
        show_settings_window = false;
        settings_shown = true;
        waiting_for_close = true;
    }

    if (isMobile && !isWeb)
        ImGui::PushStyleVar(ImGuiStyleVar_ScrollbarSize, fontSize);
    
    SettingsWindow(&settings_shown);

    if (isMobile && !isWeb) ImGui::PopStyleVar(1);

    if (!settings_shown && waiting_for_close)
    {
        // when dialog closes, we retreive things that we cant change
        // whilst it is running
        waiting_for_close = false;
        forceCommandInput = PROPB(P_ENABLE_COMMANDLINE,
                                  P_ENABLE_COMMANDLINE_DEF);
    }
    
#ifdef USE_DEMO    
    if (show_demo_window)
        ImGui::ShowDemoWindow(&show_demo_window);
#endif

    assert(fbrowser);

    if (needOpenBrowser)
    {
        needOpenBrowser = false;
        FBrowserOpen();
    }
        
    fbrowser->Display();
    if (fbrowser->HasSelected())
    {
        std::string f = fbrowser->GetSelected().string();
        //LOG1("selected ", f);
            
        if (expectSave)
        {
            sctx->requestSave(f);

            // may be at start but expect auto-save to follow
            startUnsaved = false; 
        }

        if (expectLoad)
        {
            performRequestLoad(f);
        }
               
        fbrowser->ClearSelected();
        expectSave = false;
        expectLoad = false;
    }

    if (show_quit_dialog && !startUnsaved && !quitting)
    {
        // auto-save
        sctx->autoSave();
    }

    if (!quitting && confirmDialog(&show_quit_dialog,
                      "Quit Game?",
                      "Do you really want to quit?\n",
                      "Yes",
                      "No"))
    {
        quitting = true;
    }

    if (quitting && sctx->isSaveClear())
    {
#ifndef __ANDROID__
        // no transcript on android
        StartStopTranscript(false);
#endif        
        sapp_quit();
        
#ifdef __ANDROID__
        android_finish();
#endif        
    }

    if (confirmDialog(&show_restart_dialog,
                      "Restart Game?",
                      "Start over. Are you sure?",
                      "Yes", "No"))
    {
        sctx->requestRestart();
    }

    if (can_resume)
    {
        // trigger dialog
        can_resume = false;

        if (enable_resume_dialog)
        {
            show_resume_dialog = true;
        }
        else
        {
            performRequestLoad(SDLHandler::autoSaveFilename());
        }
    }

    if (confirmDialog(&show_resume_dialog,
                      "Resume Game?",
                      "There is an auto-save.\nDo you want to contiue?",
                      "Yes", "No"))
    {
        performRequestLoad(SDLHandler::autoSaveFilename());
    }

    // the sokol_gfx draw pass
    sg_pass pass = {};
    pass.action = pass_action;
    pass.swapchain = sglue_swapchain();
    sg_begin_pass(&pass);
    
    simgui_render();
    sg_end_pass();
    sg_commit();

    // try to stabalise the time taken in the frame loop
    // and also limit CPU usage.
    // we can't put a delay between calculation and render so it must
    // be after everything (or before).

    double timeNow = worldTime.duration();

    if (lastFrameTime > 0)
    {
        double dt = timeNow - lastFrameTime;

#if 0        
#ifndef __EMSCRIPTEN__
        // target ms - actual ms
        int spare = (int)(1000.0/FPS_TARGET - 1000*dt);
        if (spare > 10) // at least 10ms spare
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(spare));
            timeNow = worldTime.duration();
            dt = timeNow - lastFrameTime;
        }
#endif
#endif        

        if (timeNow - lastClearTime > 5)
        {
            fps.reset();
            lastClearTime = timeNow;
        }

        fps.add(dt);
    }
    
    lastFrameTime = timeNow;
}

void cleanup(void)
{
    delete sctx; sctx = 0;
    delete texLoader; texLoader = 0;
    delete fbrowser; fbrowser = 0;
    
    LOG2("fetch shutdown ", 1);
    sfetch_shutdown();
    LOG2("audio shutdown ", 1);
    saudio_shutdown();

    // this causes it to crash. not sure why
    //LOG1("sg shutdown ", 1);
    //sg_shutdown();

    samplerHolder._purge();
    
    LOG2("gui shutdown ", 1);
    simgui_shutdown();
    LOG2("args shutdown ", 1);
    sargs_shutdown();
    LOG2("done shutdown ", 1);
}

struct PinchInfo
{
    bool _active = false;
    ImVec2 _start[2];
    ImVec2 _pos[2];
    bool   _ended[2];
    float _distStart;
    float _dist;
    float _distBase;

    void reset()
    {
        // this doesnt deactivate when pinch goes back to zero
        //_active = false;
        _dist = _distBase = 0;
        _distStart = 0;
    }

    static float dist(const ImVec2& a, const ImVec2& b)
    {
        float dx = a.x - b.x;
        float dy = a.y - b.y;
        return sqrtf(dx*dx + dy*dy);
    }

    void begin()
    {
        // called when we first get two points
        _distStart = dist(_start[0], _start[1]);
        _distBase = _dist;
        _ended[0] = _ended[1] = false;
        _active = true;
    }

    void ended(int i)
    {
        _ended[i] = true;
        if (_ended[0] && _ended[1]) _active = false;
    }

    void moved()
    {
        if (_active)
        {
            float d = dist(_pos[0], _pos[1]);
            float dd = d - _distStart;
            _dist = _distBase + dd;
        }
        //LOG1("pinch ", _dist);
    }

};

static PinchInfo thePinch;

void ResetPinch()
{
    thePinch.reset();
}

float GetPinch()
{
    return thePinch._dist;
}

bool PinchActive()
{
    return thePinch._active;
}

static void handleTouch(const sapp_event* e, PinchInfo& pi)
{
    const char* typestr = 0;

    switch (e->type)
    {
    case SAPP_EVENTTYPE_TOUCHES_BEGAN:
        typestr = "begin";

        if (e->num_touches == 2)
        {
            for (int i = 0; i < 2; ++i)
            {
                const sapp_touchpoint& tp = e->touches[i];
                pi._start[i] = ImVec2(tp.pos_x, tp.pos_y);
            }
        }
        break;
    case SAPP_EVENTTYPE_TOUCHES_MOVED:
        typestr = "moved";

        if (e->num_touches == 2)
        {
            for (int i = 0; i < 2; ++i)
            {
                const sapp_touchpoint& tp = e->touches[i];
                pi._pos[i] = ImVec2(tp.pos_x, tp.pos_y);
            }
        }
        break;
    case SAPP_EVENTTYPE_TOUCHES_ENDED:
        typestr = "ended";
        break;
    case SAPP_EVENTTYPE_TOUCHES_CANCELLED:
        typestr = "cancelled";
        break;
    }

#if 0
    // enable touch debugging
    if (typestr)
    {
        int n = e->num_touches;
        for (int i = 0; i < n; ++i)
        {
            const sapp_touchpoint& tp = e->touches[i];
            int id = tp.identifier;
            int x = tp.pos_x;
            int y = tp.pos_y;
            bool changed = tp.changed;

            LOG2("touch", i << " " << typestr << " (" << x << "," << y << ") " << id << " " << changed);
        }
    }
#endif    

    if (typestr)
    {
        if (e->num_touches == 2)
        {
            if (e->type == SAPP_EVENTTYPE_TOUCHES_BEGAN) pi.begin();
            else if (e->type == SAPP_EVENTTYPE_TOUCHES_MOVED) pi.moved();
        }
        
        if (e->type == SAPP_EVENTTYPE_TOUCHES_ENDED) pi.ended(e->num_touches-1);
    }
}

void input(const sapp_event* e)
{
    if (e->type == SAPP_EVENTTYPE_QUIT_REQUESTED)
    {
        show_quit_dialog = true;
        sapp_cancel_quit();
    }
    else
    {
#ifdef USE_DEMO
        if (e->type == SAPP_EVENTTYPE_KEY_DOWN &&
            e->key_code == SAPP_KEYCODE_F1) show_demo_window = true;
#endif            
        handleTouch(e, thePinch);
        simgui_handle_event(e);
    }
}

extern "C" void reload_story(const char* name)
{
    // call on main fiber
    strand_enabled = false;
    
    LOG2("reload_story ", name);
    
    sctx->resetAll();  // drop IFI client

    requestStoryName = name;

#ifdef USE_FIBERS
    fiberState.run(strandFiber);
#endif    

    // runfiber will run from the main loop once enabled
    strand_enabled = true;
}

static bool hasStoryFile(const string& dir)
{
    // XX ASSUME story is either "story.stz" or "story.str"
    // this is used for locating the story directory when we don't know
    bool v = FD::existsFile(makePath(dir, "story" STORY_COMPRESSED_SUFFIX)) ||
        FD::existsFile(makePath(dir, "story" STORY_SUFFIX));

    LOG2("looking for story in '", dir << "' found:" << v);
    return v;
}

#ifdef __APPLE__
static string appleBundlePath()
{
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef resourcesURL = CFBundleCopyBundleURL(mainBundle);
    CFStringRef str = CFURLCopyFileSystemPath(resourcesURL,
                                              kCFURLPOSIXPathStyle );
    CFRelease(resourcesURL);
    
    char path[PATH_MAX];
    CFStringGetCString( str, path, FILENAME_MAX, kCFStringEncodingASCII );
    CFRelease(str);
    
    LOG2("bundle path '", path << '\'');
    return path;
}
#endif

static string exeDirectory()
{
    // try to find the directory of the current exe
    // if not known return empty string.
    // we dont need to know this for ios or android since the
    // assets and resources are in a known relative location
    string d;

    char path[1024];
    UNUSED uint32_t size = sizeof(path);
    
#ifdef __APPLE__
    // this is needed when the exe is inside a bundle
    if (!_NSGetExecutablePath(path, &size))
    {
        // the path has the exe name on the end so remove it.
        d = pathOf(path);
    }
#endif // __APPLE__

#ifdef _WIN32
    if (GetModuleFileName(0, path, size))
    {
        //LOG1("windows exe path '", path << '\'');
        d = pathOf(path);
    }
#endif

    return d;
}

static string findFileBase()
{
    // try to find the base directory for resources
    // look for the story file

#ifdef __ANDROID__
    // this is inside the asset pack for android and not
    // a normal file.
    return ".";
#endif    

    // first try the exe dir if known
    string edir = exeDirectory();
    if (!edir.empty() && hasStoryFile(edir)) return edir;

    string d;
    
#ifdef __APPLE__
    // check the bundle path resources separately
    // when we are sandboxed the bundle is not in the same location
    // as the exe and is also read-only
    d = appleBundlePath();
    if (!d.empty())
    {
        d = makePath(d, "Contents/Resources");
        if (hasStoryFile(d)) return d;
    }
#endif

    // try the current working dir
    d = ".";
    if (hasStoryFile(d)) return d;

    // otherwise look in a number of places relative to either the exe dir
    // or the current dir.
    if (!edir.empty()) d = edir;

    // or current dir
    static const char* places[] =
    {
#if TARGET_OS_IPHONE
        "strandbin",
#endif
#ifdef __APPLE__
        "../Resources",  // OSX app
#endif
    };

    for (int i = 0; i < ASIZE(places); ++i)
    {
        string d1 = makePath(d, places[i]);
        if (hasStoryFile(d1)) return d1;
    }

    return "";
}

static string findSaveBase()
{
    string d;
    
#ifdef __ANDROID__
    d = android_internal_datapath();
#elif __EMSCRIPTEN__
    auto p = std::filesystem::current_path();
    p /= "savegames";
    d = p.c_str();
#elif __WIN32
    d = exeDirectory();
#elif __APPLE__
    // OSX has to save in current dir since it is sandboxed
    d = ".";
#endif
    return d;
}

sapp_desc sokol_main(int argc, char* argv[])
{
    Logged initLog;
    
    sargs_desc adesc = {};
    adesc.argc = argc;
    adesc.argv = argv;
    adesc.max_args = 32;
    sargs_setup(&adesc);

    const char* story = 0;

    int optionWidth = 0;
    int optionHeight = 0;

#ifdef __EMSCRIPTEN__
    Logged::_logLevel = 3;  // XX

    for (int i = 0; i < sargs_num_args(); i++)
    {
        const char* a = sargs_key_at(i);
        const char* v = sargs_value_at(i);
        if (!strcmp(a, "story")) story = v;
    }
#else

    float dpioption = 0;
    bool enableLog = false;

#ifdef __ANDROID__
    Logged::_logLevel = 3;  // XX
#endif    

    for (int i = 1; i < argc; ++i)
    {
        const char* a = argv[i];
        if (a[0] == '-')
        {
            if (!u_stricmp(argv[i], "-log")) enableLog = true;
            else if (!strcmp(a, "-f")) fullscreen = true;
            else if (!strcmp(a, "-d") && i < argc-1)
            {
                int d = atoi(argv[++i]);
                LOG2("Setting log level to ", d);
                Logged::_logLevel = d;
            }
            else if (!u_stricmp(a, "-story") && i < argc-1) story = argv[++i];
            else if (!strcmp(a, "-w") && i < argc-1)
                optionWidth = atoi(argv[++i]);
            else if (!strcmp(a, "-h") && i < argc-1)
                optionHeight = atoi(argv[++i]);
            else if (!u_stricmp(a, "-mobile")) enableMobile();
            else if (!u_stricmp(a, "-android"))
            {
                // fake android for screenshots
                enableMobile();

                androidDPI = 420; // default for screenshots
                if (i < argc-1)
                {
                    const char* d = argv[i+1];
                    if (u_isdigit(*d))
                    {
                        androidDPI = atoi(d);
                        ++i; // accept
                    }
                }
            }
            else if (!u_stricmp(a, "-dpi") && i < argc-1)
            {
                dpioption = atof(argv[++i]);
            }
            else if (!u_stricmp(a, "-fakedpi") && i < argc-1)
            {
                // use for testing to override actual.
                fakeDpi = atof(argv[++i]);
            }
            else if (!u_stricmp(a, "-asrelease"))
            {
                // act like release mode.
                // eg for screenshots
                asRelease = true;
            }
            else if (!u_stricmp(a, "-nomipmaps"))
            {
                disableMipmaps = true;
            }
            else
            {
                LOG2("unrecognised option ", a);
            }
        }
    }

    if (dpioption > 0)
    {
        // used only for adjusting w & h to actual pixels
        // for screenshots
        // put the DPI option AFTER
        //
        // divide out the w and h options
        optionWidth /= dpioption;
        optionHeight /= dpioption;
    }

    for (int i = 1; i < argc; ++i)
    {
        const char* a = argv[i];
        if (a[0] != '-')
        {
            // take the first non-option as the base dir
            if (fileBaseDir.empty())
            {
                if (FD::existsDir(a))
                {
                    fileBaseDir = a;
                    LOG2("setting base path to ", fileBaseDir);
                }
                else
                {
                    LOG("base path not a directory '", a << "'");
                }
            }
        }
    }

    if (fileBaseDir.empty())
    {
        fileBaseDir = findFileBase();
        
        if (!fileBaseDir.empty())
        {
            LOG2("Locating base path at '", fileBaseDir << "'");
        }
    }

    if (enableLog)
    {
        string logname = fullPath(LOG_FILENAME_DEFAULT);
        initLog.openFile(logname.c_str());
    }

    saveBaseDir = findSaveBase();

    // otherwise we save in the same place.
    // the above will be set by desktops to the exe dir
    // mobiles will save in
    if (saveBaseDir.empty()) saveBaseDir = fileBaseDir;

    if (!saveBaseDir.empty())
    {
        LOG2("Locating save path at '", saveBaseDir << "'");
    }

#endif // !__EMSCRIPTEN__

    if (optionWidth > 0 && optionHeight > 0)
    {
        LOG2("Optional size ", optionWidth << "x" << optionHeight);
    }
    else
    {
        // clear down otherwise
        optionHeight = optionWidth = 0;
    }

    // have a "story" arg?
    if (story) requestStoryName = story;

    sapp_desc desc = { };
    desc.init_cb = init;
    desc.frame_cb = frame;
    desc.event_cb = input;
    desc.cleanup_cb = cleanup;

    Point2 screenxy = getScreenSize();
    Point2 appxy;
    
    // calculate ideal default window size
    // ratio 7/10. 
    //

    int maxw = screenxy.x*0.90;
    
    appxy.y = screenxy.y*0.9;
    appxy.x = (appxy.y * 7)/10;

    if (appxy.x > maxw)
    {
        appxy.x = maxw;
        appxy.y = (appxy.x*10)/7;
    }

    if (!appxy.x || !appxy.y)
    {
        // guess!
        appxy.x = 840;
        appxy.y = 1200;
    }

    //LOG1(">>>> app size ", screenxy << " app " << appxy);
    
    if (optionWidth > 0)
    {
        desc.width = optionWidth;
        desc.height = optionHeight;
    }
    else
    {
        desc.width = appxy.x;
        desc.height = appxy.y;
    }
    
    desc.fullscreen = fullscreen;

    // renders full size on Hi DPI screens
    desc.high_dpi = true;
    desc.html5_ask_leave_site = html5_ask_leave_site;
    desc.ios_keyboard_resizes_canvas = false;
    desc.icon.sokol_default = true;
    //desc.enable_clipboard = true;
    desc.window_title = "Strand";

    // use main loop
    desc.html5_use_emsc_set_main_loop = true;
    
    // Windows
    //desc.win32_console_attach = false;
    //desc.win32_console_create = false;
    
    return desc;
}

/*
 IOS does not have a shell, so we must find another way to open a link
 */
#ifndef __EMSCRIPTEN__

#ifdef __APPLE__
#include "TargetConditionals.h"
#endif

void OsOpenInShell(const char* path)
{
    UNUSED char command[256];
    
#if TARGET_OS_IPHONE
    // iOS ?
#elif defined(_WIN32)
    // Note: executable path must use  backslashes! 
    ::ShellExecuteA(NULL, "open", path, NULL, NULL, SW_SHOWDEFAULT);

#elif defined(__APPLE__)
    // OSX
    const char* open_executable = "open";
    snprintf(command, 256, "%s \"%s\"", open_executable, path);
    system(command);
#elif defined(__ANDROID__)
    android_java_call(JAVA_CALL_ID_URL, path);
#elif defined(__linux__) // NB android defines linux also!
    // linux
    const char* open_executable = "xdg-open";
    snprintf(command, 256, "%s \"%s\"", open_executable, path);
    system(command);

#elif defined(__EMSCRIPTEN__)
    // this is not handled here. see `urlClicked`.
#else
    #error OsOpenInShell unknown OS
    
#endif
}

#endif // !__EMSCRIPTEN__


