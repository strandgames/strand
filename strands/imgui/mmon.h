//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

struct MouseMonitor
{
    bool   _down = false;
    bool   _canClick = false;

    // test this for a click
    bool   _click = false;
    
    void update()
    {
        bool down = ImGui::IsMouseDown(0);

        _click = false;

        if (down)
        {
            
            if (!_down)
            {
                _down = true;
                _canClick = true;
            }
            else if (_canClick)
            {
                auto& io = ImGui::GetIO();
                float d = io.MouseDownDuration[0];
                if (d >= 0.3f) _canClick = false;
                if (ImGui::IsMouseDragging(0)) _canClick = false;
            }
        }
        else
        {
            if (_canClick) _click = true;
            _canClick = false;
            _down = false;
        }
    }
};
