//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#ifdef REPORTING

// android => linux
#if (defined(__APPLE__) || defined(__linux__)) && !defined(__ANDROID__)
#include <sys/sysctl.h>
#endif

#include "reporter.h"
#include "pack7.h"
#include "base64.h"
#include "lz.h"
#include "logged.h"

#if !defined(__EMSCRIPTEN__) && !defined(NOHTTP)

#include <iostream>
#include <future>
#include <thread>
#include <chrono>
#include <vector>

#define CPPHTTPLIB_NO_EXCEPTIONS

#include "httplib.h"
#include "urlparse.h"

struct ReporterImp: public Reporter
{

    typedef std::future<bool> ReqResult;
    typedef std::vector<ReqResult> ReqResults;

    ReqResults          _reqResults;

    ReporterImp() {}

    ~ReporterImp() { _drain(); }

    void sendAsync(const string& url) override
    {
        _drain();

        LOG2("reporter sending ", url);
        ReqResult f = std::async(std::launch::async,
                                 &ReporterImp::_send, this, url);
        _reqResults.push_back(std::move(f));
    }


private:

    string osVersionString()
    {
        string vs;
#ifdef __APPLE__

        /*
        Darwin version to OS X release:

        24.x.x  15
        23.5.x  14.5 2019
        17.x.x. macOS 10.13.x High Sierra
        16.x.x  macOS 10.12.x Sierra
        15.x.x  OS X  10.11.x El Capitan
        14.x.x  OS X  10.10.x Yosemite
        13.x.x  OS X  10.9.x  Mavericks
        12.x.x  OS X  10.8.x  Mountain Lion
        11.x.x  OS X  10.7.x  Lion
        10.x.x  OS X  10.6.x  Snow Leopard
        9.x.x  OS X  10.5.x  Leopard
        8.x.x  OS X  10.4.x  Tiger
        7.x.x  OS X  10.3.x  Panther
        6.x.x  OS X  10.2.x  Jaguar
        5.x    OS X  10.1.x  Puma
        */
        
        char buf[512];
        size_t size = sizeof(buf);
        if (!sysctlbyname("kern.osrelease", buf, &size, NULL, 0))
        {
            vs = buf;
        }
#endif        
        return vs;
    }

    bool _send(const string& urlreport)
    {
        bool r = false;

        URLParser urlp(urlreport);
        
        httplib::Client cli(urlp._domain, urlp._port);

        string osname;
        
#if defined(_WIN32)
        osname = "Windows";
#elif defined(__ANDROID__)
        osname = "Android";
#elif defined(__APPLE__)
        osname = "OSX";
#elif defined(__linux__)
        osname = "Linux";
#else
        osname = "Strand";
#endif

        httplib::Headers headers;
        string vs = osVersionString();
        if (!vs.empty())
        {
            osname += " ";
            osname += vs;
        }
        
        headers.insert(std::pair("User-Agent", osname));

        if (auto res = cli.Get(urlp._file, headers))
        {
            LOG3("status:", res->status);
            LOG3("content-type:", res->get_header_value("Content-Type"));
            LOG3("body: ", res->body);
            r = true;
        }
        else
        {
            LOG2("reporter url ", urlp << " error code: " << res.error());
        }
        return r;
    }

    void _drain()
    {
        auto it = _reqResults.begin();

        while (it != _reqResults.end())
        {
            if (it->wait_for(std::chrono::seconds(0)) ==
                std::future_status::ready)
            {
                //LOG1("Reporter: future done", 0);
                it = _reqResults.erase(it);
            }
            else ++it;
        }
    }

};

Reporter* Reporter::createReporter()
{
    return new ReporterImp();
}

#endif // !__EMSCRIPTEN__

std::string Reporter::encode(const char* js)
{
    // encode js into a URL compatible string
    unsigned int jz = strlen(js);

    int version = 0;
    string s64;

    // first pack to find the size
    unsigned int packz;
    unsigned char* d = pack7(js, packz);

    Compress c;
    
    if (c.compress2((const unsigned char*)js, jz))
    {
        if (c._outsz < packz)
        {
            // compress beats pack
            version = 1;

            // first two bytes will be uncompessed size
            s64 = base64_encode(c._outbuf, c._outsz);
        }
    }

    //LOG1("reporter pack saved ", jz - packz << " lz saved " << jz - c._outsz);

    if (!version)
    {
        // use pack
        s64 = base64_encode(d, packz);
    }

    delete d;
    
    // put a digit on at the start to indicate version
    string s1;
    s1 += (char)('0' + version);
    s1 += s64;
    return s1;
}

std::string Reporter::decode(const string& s64)
{
    // check we decode correctly
    string s;
    unsigned int sz = s64.size();
    if (sz)
    {
        const char* sp = s64.c_str();
        
        // version is the first character
        int version = *sp - '0';

        string s64d = b64decode(sp+1, sz-1);

        if (version == 0)
        {
            char* s1 = unpack7((unsigned char*)s64d.c_str(), s64d.size());
            s = s1;
            delete s1;
        }
        else if (version == 1)
        {
            Compress c;
            const unsigned char* sp = (const unsigned char*)s64d.c_str();

            // first two chars are the uncompressed size;
            unsigned int a = *sp++;
            unsigned int b = *sp++;

            unsigned int usize = (b << 16) + a;
            unsigned int sz = s64d.size() - 2;
            
            c.decompress(sp, sz, usize);
            s = (char*)c._outbuf; // has a zero;
        }
    }
    return s;
}


#endif // REPORTING

