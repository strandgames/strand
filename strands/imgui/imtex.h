//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include <assert.h>
#include "logged.h"

#define TOTEXID(_id)  ((void*)(uintptr_t)(_id))
#define FROMTEXID(_id) ((uint32_t)(uintptr_t)(_id))

struct MipMapMaker
{
    unsigned char* target = 0;
    unsigned int _newImageSize = 0;

    ~MipMapMaker()
    {
        delete target;
    }

    sg_image_desc prepare_mipmaps(const sg_image_desc* desc_)
    {
        // fill out defaults.
        // NB: cannot use
        //sg_image_desc desc = _sg_image_desc_defaults(desc_);

        sg_image_desc desc = *desc_;
        assert(desc.type && desc.num_slices == 1 && desc.num_mipmaps == 1);
        assert(!desc.render_target && desc.sample_count == 1);
        
        assert(desc.pixel_format == SG_PIXELFORMAT_RGBA8
               || desc.pixel_format == SG_PIXELFORMAT_BGRA8);


        // we are only one base image
        assert(desc.data.subimage[0][0].ptr && !desc.data.subimage[1][0].ptr);

        // from above
        const unsigned int pixel_size = 4; // _sg_pixelformat_bytesize(desc.pixel_format);
        
        unsigned int target_width = desc.width;
        unsigned int target_height = desc.height;

        // add in base size before mips
        unsigned int totalImageSize = desc.data.subimage[0][0].size;

        // combined size of mip buffers
        unsigned int tsize = totalImageSize/2;

        // allocate one area to contain all mip buffers
        assert(!target);
        target = new unsigned char[tsize];
        unsigned char* dp = target;
        
        for (int level = 1; level < SG_MAX_MIPMAPS; ++level)
        {
            unsigned char* source = (unsigned char*)desc.data.subimage[0][level - 1].ptr;
            if (!source) break;
            
            unsigned int source_width = target_width;
            //unsigned int source_height = target_height;

            target_width /= 2;
            target_height /= 2;
            if (target_width < 1 && target_height < 1) break;
            if (target_width < 1) target_width= 1;
            if (target_height < 1) target_height = 1;
            
            unsigned img_size = target_width * target_height * pixel_size;
            totalImageSize += img_size;
            
            desc.data.subimage[0][level].ptr = dp;
            desc.data.subimage[0][level].size = img_size;

            unsigned int sstride = source_width*pixel_size;
            //unsigned int dstride = target_width*pixel_size;

            unsigned int color;
            unsigned char* sp = source;
            unsigned int odd = (source_width & 1) ? pixel_size : 0;
            for (unsigned int y = 0; y < target_height; ++y)
            {
                assert(sp == source + y*sstride*2);
                //assert(dp == target + y*dstride);
                    
                unsigned int x = target_width;
                while (x)
                {
                    x--;

                    // a
                    color = sp[0];
                    color += sp[pixel_size];
                    color += sp[sstride];
                    color += sp[sstride + pixel_size];
                    *dp++ = color/4;
                    ++sp;

                    // b
                    color = sp[0];
                    color += sp[pixel_size];
                    color += sp[sstride];
                    color += sp[sstride + pixel_size];
                    *dp++ = color/4;
                    ++sp;

                    // c
                    color = sp[0];
                    color += sp[pixel_size];
                    color += sp[sstride];
                    color += sp[sstride + pixel_size];
                    *dp++ = color/4;
                    ++sp;

                    // d
                    color = sp[0];
                    color += sp[pixel_size];
                    color += sp[sstride];
                    color += sp[sstride + pixel_size];
                    *dp++ = color/4;

                    sp += pixel_size + 1;
                }

                sp += sstride + odd;
            }

            assert(dp - target <= tsize);
            if (desc.num_mipmaps <= level) desc.num_mipmaps = level + 1;
        }
        
        _newImageSize = totalImageSize;
        return desc;
    }

    sg_image make_image_with_mipmaps(const sg_image_desc* desc_)
    {
        sg_image_desc desc = prepare_mipmaps(desc_);
        return sg_make_image(&desc);
    }
};

struct ImTex
{
    typedef std::string string;
    typedef void* ImTexID;
    
    string      _name;
    ImTexID     _tid = 0;
    int         _w;
    int         _h;
    int         _chans;
    uint        _imageSize = 0; // bytes
    bool        _useMipmaps = false;

    ImTexID     _tidvid[2] = {0,0}; // other two planes for video

    bool operator<(const ImTex& t) const { return _name < t._name; }

    bool isVideo() const
    {
        // if we have other planes
        // also check loaded
        return _tid && _tidvid[0] && _tidvid[1];
    }

    uint getImageTID() const
    {
        int t = FROMTEXID(_tid);
        if (t && _chans > 1)
        {
            // the TID will be an simgui id. not an image id
            simgui_image_t si = simgui_image_from_imtextureid(_tid);
            simgui_image_desc_t idesc = simgui_query_image_desc(si);
            t = idesc.image.id;
        }
        return t;
    }

    void destroy()
    {
        if (_tid)
        {
            LOG3("destroy Image ", _name << " size (k):" << allImageSize()/1024);
            if (_chans > 1)  // not video
            {
                simgui_image_t si = simgui_image_from_imtextureid(_tid);
                simgui_image_desc_t idesc = simgui_query_image_desc(si);
                sg_destroy_image(idesc.image);
                simgui_destroy_image(si);
            }
            else
            {
                // we are a video plane not an simgui texture
                sg_image sgi{FROMTEXID(_tid)};
                sg_destroy_image(sgi);
            }

            _tid = 0;
        }

        for (int i = 0; i < 2; ++i)
        {
            if (_tidvid[i])
            {
                LOG2("destroy video plane for ", _name);
                sg_image sgi{FROMTEXID(_tidvid[i])};
                sg_destroy_image(sgi);
                _tidvid[i] = 0;
            }
        }

        _imageSize = 0;
    }

    void setDimensions(int w, int h, int c)
    {
        _w = w;
        _h = h;
        _chans = c;
        _imageSize = _w * _h * _chans;
        assert(_imageSize > 0);
    }

    bool valid() const { return _tid != 0; }
    operator bool() const { return valid(); }

    unsigned int allImageSize() const
    {
        // take into account video planes
        unsigned int sz = _imageSize;
        if (_tidvid[0]) sz += _imageSize/4;  // XX assume colours 1/4
        if (_tidvid[1]) sz += _imageSize/4;
        return sz;
    }

    sg_image makeImage(unsigned char* data)
    {
        assert(_w > 0 && _h > 0 && _chans > 0 && _imageSize > 0);

        sg_image_desc sgid = {};
        sgid.width = _w;
        sgid.height = _h;

        // fill out values needed for mipmap since it doesn't fill defaults
        sgid.type = SG_IMAGETYPE_2D;
        sgid.num_slices = 1;
        sgid.num_mipmaps = 1;
        sgid.sample_count = 1;
        
        if (_chans == 4)
        {
            sgid.pixel_format = SG_PIXELFORMAT_RGBA8;
            sgid.usage = SG_USAGE_IMMUTABLE;
        }
        else if (_chans == 3)
        {
            //sgid.pixel_format = SG_PIXELFORMAT_RGB;
            //sgid.usage = SG_USAGE_IMMUTABLE;
            assert(0);
        } 
        else if (_chans == 1)
        {
            sgid.pixel_format = SG_PIXELFORMAT_R8;

            // XX assume we're streaming image planes
            sgid.usage = SG_USAGE_STREAM;
        }
        else
        {
            assert(0);
        }

        if (data) // can supply data later
        {
            auto& si = sgid.data.subimage[0][0];
            si.ptr = data;
            si.size = _imageSize;
        }

        sg_image sgi;

        bool allowMipmaps = true;
        
#if defined(__ANDROID__) || defined(__EMSCRIPTEN__)
        // be sure these dont use mipmaps. ios??
        allowMipmaps = false;
#endif

        // note that `useMipmaps` is false if globally disabled in VNMan
        if (allowMipmaps && _useMipmaps)
        {
            MipMapMaker mm;
            sgi = mm.make_image_with_mipmaps(&sgid);
            
            // update our idea of the image size in bytes
            LOG3("made mipmap for ", _name << " size (k):" << _imageSize/1024 << " to:" << mm._newImageSize/1024);

            assert(mm._newImageSize >= _imageSize);

            _imageSize = mm._newImageSize;
        }
        else
        {
            sgi = sg_make_image(&sgid);
        }
        
        bool ok = sgi.id != SG_INVALID_ID;

        // validation
        if (ok)
        {
            sg_image_info ifo;
            ifo = sg_query_image_info(sgi);
            ok = ifo.slot.state == SG_RESOURCESTATE_VALID;
        }

 #ifdef LOGGING
        extern bool noTextureMemory;
        if (ok && noTextureMemory)
        {
            // debugging! pretend to run out of memory
            noTextureMemory = false;
            LOG2("DEBUG pretend to run out of texture memory ", _name);
            ok = false;
        }
#endif

        if (!ok)
        {
            //LOG1("WARNING poll decoder, failed to create image ", r._name);
            sg_destroy_image(sgi);
            sgi.id = SG_INVALID_ID;
        }
        
        return sgi;
    }
};

