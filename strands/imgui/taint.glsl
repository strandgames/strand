@ctype mat4 hmm_mat4

@vs vs
uniform vs_params {
   vec2 disp_size;
};
in vec2 pos;
in vec2 texcoord0;
in vec4 color0;
in vec4 tcol0;  // taint col

out vec2 uv;
out vec4 color;
out vec4 tcol;

void main() {
        gl_Position = vec4(((pos/disp_size)-0.5)*vec2(2.0,-2.0), 0.5, 1.0);
        uv = texcoord0;
        color = color0;
        tcol = tcol0;
    }
@end

@fs fs
uniform texture2D tex;
uniform sampler smp;

in vec2 uv;
in vec4 color;
in vec4 tcol;

out vec4 frag_color;

void main() {
     vec4 tx = texture(sampler2D(tex, smp), uv);
     frag_color = vec4(mix(tx.xyz, tcol.xyz, tcol.w), tx.w) * color;
}
@end

@program strand vs fs

