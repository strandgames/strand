//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//


#pragma once

struct base_vertex
{
    float x, y;
    float u, v;
    float col[4];
    float tcol[4];
};

typedef base_vertex taint_vertex;
typedef base_vertex mpg_vertex;

struct SOKState
{
#ifdef USESPINE    
    struct 
    {
         sg_pipeline pip;
         sg_bindings bind;
    } anim;
#endif

#ifdef USEMPG

#define MAX_MPG_VERTS 6
#define MAX_MPG_INDEX 12
    
    struct
    {
        sg_pipeline pip;
        sg_bindings bind;

        struct {
            int width;
            int height;
        } image_attrs[3];
        
    } mpg;
#endif

#ifdef USETAINT

#define MAX_S_IMG   16
#define MAX_S_VERTS (MAX_S_IMG*4)
#define MAX_S_INDEX (MAX_S_IMG*6)
    
    struct
    {
        sg_pipeline pip;
        sg_bindings bind;
    } taint;
#endif   
};



