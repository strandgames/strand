//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

// forward
struct AnimState;

struct SVertex
{
    float x, y;
    float r, g, b, a;
    float u, v;
    float rt, gt, bt;
};

#define MAX_SVERTICES   2048

struct SSpineCtx
{
    typedef std::list<AnimState*>    States;

    // all active states
    States              _states;
    int                 _renderCount = 0;

    // hold here various helpers
    ImTexLoader*        _loader = 0;
    void*               _s_ext = 0;

    static SSpineCtx* get()
    {
        static SSpineCtx _ctx;
        return &_ctx;
    }

    bool update(AnimState& a);
    int render(AnimState& a);
    bool animOp(AnimState& a);
    
};
