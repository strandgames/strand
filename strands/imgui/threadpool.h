//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include "logged.h"

struct ThreadPool
{
    bool _shutdown = false;           // Tells threads to stop looking for jobs
    std::mutex _lock;                  // Prevents data races to the job queue
    std::condition_variable _condition; // Allows threads to wait on new jobs or termination
    
    std::vector<std::thread> _threads;
    std::queue<std::function<void()>> _jobs;

    int  _threadCount = 0;

    ~ThreadPool()
    {
        if (_threadCount) stop();
    }

    void threadLoop()
    {
        while (true)
        {
            std::function<void()> job;
            
            {
                std::unique_lock<std::mutex> lock(_lock);
                _condition.wait(lock, [this] {
                    return !_jobs.empty() || _shutdown;
                });

                
                if (_shutdown) return;

                job = _jobs.front();
                _jobs.pop();
            }
            job();
        }
    }

    bool start(int n)
    {
        bool v = true;
        _threadCount = 0;
        for (int i = 0; i < n; i++)
        {
            _threads.emplace_back(std::thread(&ThreadPool::threadLoop, this));
            v = _threads.back().joinable();
            if (!v) break;
            ++_threadCount;
        }
        return v;
    }

    void enqueue(const std::function<void()>& job)
    {
        {
            std::unique_lock<std::mutex> lock(_lock);
            _jobs.push(job);
        }
        _condition.notify_one();
    }

    void stop()
    {
        // LOG1("stopping threadpool ", _threadCount);
        
        {
            std::unique_lock<std::mutex> lock(_lock);
            _shutdown = true;
            _threadCount = 0;
        }
        
        _condition.notify_all();
        
        for (std::thread& active_thread : _threads) active_thread.join();
        _threads.clear();
    }

    bool busy()
    {
        bool poolbusy = false;
        {
            std::unique_lock<std::mutex> lock(_lock);
            poolbusy = !_jobs.empty();
        }
        return poolbusy;
    }
    
};


