//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include "buttonx.h"
#include "imcol.h"

//#define BUTTON(_l, _s) ImGui::Button(_l,_s)

#define BUTTON(_l, _s)                                  \
    ImGui::ColoredButton(_l, _s, 0, butCol1, butCol2)


struct Keyboard
{
    static const int maxKeys = 8;
    unsigned int keys[maxKeys];
    
    int refocus=0;
    int kn = 0;
    bool shift = false;

    // base values
    float fontSize;
    float scale;
    float itemBaseSpacing;

    // scaled values
    float itemSpacing;
    float buttonSize;

    ImU32 butCol1 = 0;
    ImU32 butCol2 = 0;

    void reset()
    {
        // init base size factors
        
        ImGuiStyle& style = ImGui::GetStyle();

        // set the nominal base spacing based on the system
        itemBaseSpacing = style.ItemSpacing.x/2; // usually 4

        ImFont* font_current = ImGui::GetFont();
        fontSize = font_current->FontSize * 1.5f;

        scale = 0; // not set

        if (!butCol1) setButtonColors();
    }

    void styleChanged()
    {
        butCol1 = 0; // will recalc button colours
    }

    void setButtonColors()
    {
        Colour bg = StyleColour(ImGuiCol_WindowBg);
        Colour c1 = StyleColour(ImGuiCol_Button);
        //LOG1("style key button colour ", c1);
        
        c1 = c1.withoutAlpha(bg);
        butCol1 = ColourToU32(c1);
        Colour c2 = c1.brighter(0.7f);
        butCol2 = ColourToU32(c2);

        //butCol2 = IM_COL32(0,0,0,255);
        //LOG1("setButtonColours ", c1 << " " << c2);
    }

    void setScale(float s)
    {
        scale = s;
        buttonSize = fontSize * scale;
        itemSpacing = itemBaseSpacing * scale;
    }

    void press(unsigned int c)
    {
        refocus=0;
        if (c == '^')
        {
            // shift
            shift = !shift;
        }
        else
        {
            if (kn < maxKeys) keys[kn++] = c;
            shift = false;
        }
    }

    
    void line(const char* key)
    {
        while (*key)
        {
            char label[2];
            label[0] = *key;
            label[1] = 0;
            if (BUTTON(label,ImVec2(buttonSize,buttonSize))) press(*key);
            if (*++key) ImGui::SameLine();
        }
    }

    void lineShift(const char* key)
    {
        // key is alternate face and shift characters on the line
        while (*key)
        {
            char label[2];
            char c = shift ? key[1] : key[0];
            label[0] = c;
            label[1] = 0;
            if (BUTTON(label,ImVec2(buttonSize,buttonSize))) press(c);
            key += 2;
            if (*key) ImGui::SameLine();
        }
    }

    void addKey(ImGuiKey c)
    {
        auto& io = ImGui::GetIO();
        io.AddKeyEvent(c, true);
        io.AddKeyEvent(c, false);
    }

    void addChar(unsigned int c)
    {
        auto& io = ImGui::GetIO();
        io.AddInputCharacter(c);
    }
    
    void drain()
    {
        for (int i = 0; i < kn; ++i)
        {
            unsigned int k=keys[i];
            if (k == '\b') addKey(ImGuiKey_Backspace);
            else if (k == '\n') addKey(ImGuiKey_Enter);
            else if (k == '\t') addKey(ImGuiKey_Tab);
            else if (k == '^') addKey(ImGuiKey_UpArrow);
            else addChar(k);
        }
        kn = 0;
    }

    ImVec2 calculateSize(const ImVec2& cr, int limitpc = 0)
    {
        // limitpc is limit y space percent, eg 25
        
        int rows = 4;
        int line = 10;

        float w = line * fontSize + (line - 1)*itemBaseSpacing;
        float h = rows*fontSize + (rows - 1)*itemBaseSpacing;

        if (!scale)
        {
            // if not already scaled, scale to fit view width
            float vw = cr.x - itemBaseSpacing*2;
            float s = vw/w;

            float maxy = cr.y;

            if (limitpc)
                maxy *= (float)limitpc/100;

            if (h*s > maxy)
            {
                // limit y size
                s = maxy/h;
            }
            
            setScale(s);
        }

        //LOG1("keyboard view ", cr.x << " width " << w << " scaled " << w * scale);

        return ImVec2(w * scale, h * scale);
    }
    
    void show()
    {
        // assume `calculateHeight` called already
        
        if(refocus==0)
        {
            ImGui::SetKeyboardFocusHere(-1);
        }
        else if(refocus>=2)
        {
            drain();
        }
        
        ++refocus;

        ImGuiStyle& style = ImGui::GetStyle();

        // store current styles
        ImVec2 oldSpacing = style.ItemSpacing;
        float oldFrameRounding = style.FrameRounding;

        style.ItemSpacing = ImVec2(itemSpacing, itemSpacing);
        style.FrameRounding = buttonSize/4;

        float indentdelta = buttonSize/2;
        float bz3 = buttonSize*3 + itemSpacing*2;
        float bw = buttonSize + indentdelta;

        lineShift("q1w2e3r4t5y6u7i8o9p0");

        ImGui::Indent(indentdelta);
        line("asdfghjkl");
        ImGui::Unindent(indentdelta);


        {
            PushIconFont pf;
            if (BUTTON(ICON_MD_KEYBOARD_TAB,ImVec2(bw, buttonSize)))
                press('\t');
            

        }
        ImGui::SameLine();
        line("zxcvbnm");
        ImGui::SameLine();
        
        {
            PushIconFont pf;

            if (BUTTON(ICON_MD_KEYBOARD_BACKSPACE,
                              ImVec2(bw + itemSpacing, buttonSize)))
                press('\b');
            
            
            if (BUTTON(ICON_MD_KEYBOARD_ARROW_UP,ImVec2(bw,
                                                        buttonSize)))
                press('^');
        }

        
        ImGui::SameLine();
        line(",-");
        ImGui::SameLine();
        {
            PushIconFont pf;            
            if (BUTTON(ICON_MD_SPACE_BAR,ImVec2(bz3,buttonSize))) press(' ');
        }
        
        ImGui::SameLine();                
        line(".");
        ImGui::SameLine();

        {
            PushIconFont pf;
            // add two item spacing, one for gap above and one
            // for the overall 9 on the top row.
            int sz = bw + buttonSize + itemSpacing*2;
            if (BUTTON(ICON_MD_KEYBOARD_RETURN,ImVec2(sz,buttonSize)))
                press('\n');
        }

        // restore style
        style.ItemSpacing = oldSpacing;
        style.FrameRounding = oldFrameRounding;
    }
    
};

