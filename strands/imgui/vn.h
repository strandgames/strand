//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <iostream>
#include <string>
#include <list>
#include <functional>
#include "varset.h"
#include "av.h"
#include "imcol.h"
#include "rand.h"
#include "worldtime.h"
#include "pexpr.h"
#include "imrender.h"

extern bool enablePictureClickables;
extern void SetSuspendInput(bool);

struct VNMan: public ImRender
{
    struct World;
    struct Node;
    struct Event;
    typedef std::list<Node*>  Nodes;
    typedef AVF::Time Time;
    typedef std::function<void(Event*)> EventFn;
    using eNodeCtx = ST::eNodeCtx;
    using enode = ST::enode;

    struct Easer
    {
        static const int maxSamples = 64;

        float _x[maxSamples];
        float _y[maxSamples];
        
        enum Ease
        {
            ease_none = 0,
            ease_inquad,
            ease_incubic,
            ease_inquart,
            ease_outquad,
            ease_outcubic,
            ease_outquart,
            ease_inoutquad,
            ease_inoutcubic,
            ease_inoutquart,
            ease_outback,
            ease_inoutback,
            ease_outelastic,
            ease_inbounce,
            ease_outbounce,
        };

        Ease    _ease = ease_none;
        int     _samples;

        static float outbounce(float x)
        {
            const float n1 = 7.5625;
            float y;

            if (x < 36/99.0f)
            {
                y = n1 * x * x;
            }
            else if (x < 72/99.0f)
            {
                x -= 54/99.0f;
                y = n1 * x * x + 0.75f;
            }
            else if (x < 90/99.0f)
            {
                x -= 81/99.0f;
                y = n1 * x * x + 0.9375f;
            }
            else
            {
                x -= 21/22.0f;
                y = n1 * x * x + 0.984375f;
            }
            return y;
        }

        float eval(float x) const
        {
            //https://easings.net/
            float y;
            switch (_ease)
            {
            case ease_inquad:
                y = x * x;
                break;
            case ease_incubic:
                y = x*x*x;
                break;
            case ease_inquart:
                y = x * x;
                y *= y;
                break;
            case ease_outquad:
                y = 1 - (1 - x) * (1 - x);
                break;
            case ease_outcubic:
                {
                    float t = 1-x;
                    y = 1 - t*t*t;
                }
                break;
            case ease_outquart:
                {
                    float t = 1-x;
                    t *= t;
                    y = 1 - t*t;
                }
                break;
            case ease_inoutquad:
                {
                    if (x < 0.5) y = 2 * x * x;
                    else
                    {
                        float t = -2 * x + 2;
                        y = 1 - t*t / 2;
                    }
                }
                break;
            case ease_inoutcubic:
                {
                    if (x < 0.5)
                    {
                        y = 4 * x * x * x;
                    }
                    else
                    {
                        float t = -2*x + 2;
                        y = 1 - t*t*t / 2;
                    }
                }
                break;
            case ease_inoutquart:
                {
                    if (x < 0.5)
                    {
                        y = 8 * x * x * x * x;
                    }
                    else
                    {
                        float t = -2*x + 2;
                        t *= t;
                        y = 1 - t*t / 2;
                    }
                }
                break;
            case ease_outback:
                {
                    const float c1 = 1.70158;
                    const float c3 = c1 + 1;
                    
                    float t = x-1;
                    y = 1 + c3 * t*t*t + c1 * t*t;
                }
                break;
            case ease_inoutback:
                {
                    const float c2 = 2.5949095;
                    if (x < 0.5)
                    {
                        float t = 2*x;
                        y = (t*t * ((c2 + 1) * 2 * x - c2)) / 2;
                    }
                    else
                    {
                        float t = 2*x-2;
                        y = (t*t * ((c2 + 1) * (x * 2 - 2) + c2) + 2) / 2;
                    }
                }
                break;
            case ease_outelastic:
                {
                    //const c4 = (2 * Math.PI) / 3;
                    const float c4 = 2.094395102393195492308429;

                    if (x <= 0) y = 0;
                    if (x >= 1) y = 1;
                    else
                    {
                        y = powf(2, -10 * x) * sinf((x * 10 - 0.75f) * c4) + 1;
                    }
                }
                break;
#if 0                

            case Args::ease_inoutelastic:
                {
                    const c5 = (2 * Math.PI) / 4.5;

                    return x === 0
                        ? 0
                        : x === 1
                        ? 1
                        : x < 0.5
                        ? -(Math.pow(2, 20 * x - 10) * Math.sin((20 * x - 11.125) * c5)) / 2
                        : (Math.pow(2, -20 * x + 10) * Math.sin((20 * x - 11.125) * c5)) / 2 + 1;
                }
#endif
            case ease_inbounce:
                y = 1 - outbounce(1-x);
                break;
            case ease_outbounce:
                y = outbounce(x);
                break;
            default:
                y = x;
            }
            return y;
        }

        void start(Ease e, float dt)
        {
            // number of points with samples-1 gaps
            // arrays are valid from 0 to samples-1

            _ease = e;

            if (_ease != ease_none)
            {
                // create function array

                _samples = dt*30; // XX fps/2 ?
                if (_samples > maxSamples) _samples = maxSamples;

                // no point unless intermediate keys needed
                if (_samples < 3) _samples = 0;

                if (_samples)
                {
                    int n = _samples-1;

                    // ensure start and end are exact
                    _x[0] = 0;
                    _x[n] = dt;

                    _y[0] = 0;
                    _y[n] = 1;

                    float di = 1.0f/n; // gaps
                        
                    // fill in samples-2 intermediate points
                    float t = 0;
                    for (int i = 1; i < n; ++i)
                    {
                        t += di;
                        _x[i] = t*dt;
                        _y[i] = eval(t);
                    }
                }
            }
        }

        void apply(AVF& p,
                   Time t1, Time t2, float v2,
                   bool clear)
        {

            if (_ease == ease_none || !_samples)
            {
                p.set2l(t1,t2,v2, clear);
            }
            else
            {
                if (clear) p.clearToCurrent(t1);

                float v1 = p.at(t1);
                float dv = v2 - v1;

                for (int i = 0; i < _samples; ++i)
                {
                    float v = v1 + _y[i] * dv;
                    p._set(t1 + _x[i], v);
                }
            }
        }

    };

    typedef Easer::Ease Ease;

    struct Event
    {
        Node*           _node;  // eg hosting cycle node
        Time            _t;
        EventFn         _fn;
        AudioInfo       _pendingAudio;

        Event(Node* n) : _node(n) {}
    };

    typedef AV<Colour> AVCol;

    struct DrawList: public std::vector<Node*>
    {
        friend std::ostream& operator<<(std::ostream& os, const DrawList& p)
        {
#ifdef LOGGING            
            int cc = 0;
            for (auto ni : p)
            {
                if (cc++) os << ' ';
                os << ni->_name;
                if (ni->isLoaded()) os << '*';  // loaded indicator
            }
#endif            
            return os;
        }
    };
    
    static string getvalstr(const VarList& vs, const string& key)
    {
        string r;
        var v = vs.find(key);
        const char* s = v.rawString();
        if (s) r = s;
        return r;
    }

    enum Align
    {
        align_centre = 0, // default
        align_left,
        align_right,
    };

    struct Args
    {
        World*  _world;
        var     _v;
        int     _i = 0;

        // options
        float   _optAt;
        bool    _optAtSet = false;
        bool    _optClearFrom = false;
        Node*   _optNode = 0;
        bool    _optNodeFail = false;  // when a node is given but invalid
        Align   _optAlign = align_centre;
        bool    _optAlignSet = false;

        // audio
        int     _optChannel = 1;
        int     _optVol = 100; // 100% default

        // other properties
        bool    _self = false;

        // parent image resource = find resource child
        bool    _optOnResource = false;

        // parent image area = find area child
        bool    _optOnArea = false;

        bool    _optPush = false;
        bool    _optPop = false;
        bool    _optMipmap = false;

        Ease    _optEase = Easer::ease_none;
        
        float   _optRate = 1;

        Args(World* world, const var& v) : _world(world), _v(v.copy()) {} 

        int size() const { return  _v.size(); }

        bool fail() const { return _optNodeFail; }

        operator bool() const { return (bool)_v; }

        Node* onNode(Node* d) const { return _optNode ? _optNode : d; }
        float onAt(float d) const { return _optAtSet ? _optAt : d; }
        bool onClear(bool d) const { return _optClearFrom || d; }
        
        void _bump()
        {
            ++_i;
            if (_i >= _v.size()) _v.purge();
        }
        
        void operator++() { _bump(); }

        const var& get() const { return _v.isList() ? _v[_i] : _v; }
        const var& operator*() const { return get(); }

        bool more() const { return _i + 1 < size(); }

        bool bump()
        {
            bool r = more();
            _bump();
            return r;
        }

        float number() const
        {
            // get number without checking
            float v = 0;
            _world->eval(get(), v, "number");
            return v;
        }

        bool ifNumber(float& v, const char* msg = 0) const
        {
            // if we are a number get it, otherwise false
            bool r = _world->eval(get(), v, msg);
            if (!r && msg)
            {
                LOG2("VN expected a number for ", msg << " got " << get());
            }
            return r;
        }

        bool ifStr(string& s, const char* msg = 0)
        {
            const char* sp = get().rawString();
            if (sp) s = string(sp);
            else if (msg)
            {
                LOG2("VN expected a string for ", msg << " got " << get());
            }
            return sp != 0;
        }

        bool point(Point2f& pt, const char* msg = 0)
        {
            // consume two args to return a point
            Point2f ap;
            bool r = (_i + 1 < size());
            if (r)
            {
                assert(_v.isList());

                r = ifNumber(ap.x);
                if (r)
                {
                    ++_i;
                    r = ifNumber(ap.y);
                }
            }
            
            if (r) pt = ap;
            else if (msg)
            {
                LOG2("VN expected a point for ", msg << " got " << get());
            }
            return r;
        }

        bool box(Boxf& box, const char* msg = 0)
        {
            Boxf ab;
            bool r = (_i + 3 < size());
            if (r)
            {
                assert(_v.isList());
                
                r = ifNumber(ab[0]);
                for (int i = 1; r && i < 4; ++i)
                {
                    ++_i;
                    r = ifNumber(ab[i]);
                }
            }

            if (r) box = ab;
            else if (msg)
            {
                LOG2("VN expected a box for ", msg << " got " << get());
            }
            return r;
        }

        int boxPartial(Boxf& box, bool known[4])
        {
            // elements of the box can be missing
            // stop if args run out

            int n = 0;  // number of known ordinates supplied
            
            // reset knowns first
            for (int i = 0; i < 4; ++i) known[i] = false;

            // get up to 4 box ordinates
            if ((known[0] = ifNumber(box[0]))) ++n;
            for (int i = 1; more() && i < 4; ++i)
            {
                ++_i;
                if ((known[i] = ifNumber(box[i]))) ++n;
            }
            return n;
        }

        bool colour(Colour& col, const char* msg = 0)
        {
            const var& v = get();
            Colour c;

            bool r = v.isString();
            if (r)
            {
                const char* s = v.rawString();

                if (!u_stricmp(s, "background"))
                {
                    // a special colour code to mean the current background col
                    r = true;
                    c = _world->_host->backgroundColour();
                }
                else
                {
                    r = c.parse(s);
                }
            }
            
            if (r) col = c;
            else if (msg)
            {
                LOG2("VN expected color for ", msg << " got " << v);
            }
            return r;
        }

        Node* node(World* w, const char* msg = 0)
        {
            Node* n = 0;
            string name;
            if (ifStr(name, msg))
            {
                n = w->find(name);
                if (!n)
                {
                    _optNodeFail = true;
                    
                    if (msg)
                    {
                        LOG2("VN cannot find node for ", msg << " name " << name);
                    }
                }
            }
            return n;
        }

        bool parseProp(const string& prop, const char* msg = 0)
        {
            bool r = true;
            if (prop == IFI_SELF) _self = true;
            else if (prop == IFI_CENTRE)
            {
                _optAlign = align_centre;
                _optAlignSet = true;
            }
            else if (prop == IFI_LEFT)
            {
                _optAlign = align_left;
                _optAlignSet = true;
            }
            else if (prop == IFI_RIGHT)
            {
                _optAlign = align_right;
                _optAlignSet = true;
            }
            else if (prop == IFI_RESOURCE) _optOnResource = true;
            else if (prop == IFI_AREA) _optOnArea = true;
            else if (prop == IFI_PUSH) _optPush = true;
            else if (prop == IFI_POP) _optPop = true;
            else if (prop == IFI_MIPMAP) _optMipmap = true;
            else if (prop == IFI_EASEINQUAD) _optEase = Easer::ease_inquad;
            else if (prop == IFI_EASEINCUBIC) _optEase = Easer::ease_incubic;
            else if (prop == IFI_EASEINQUART) _optEase = Easer::ease_inquart;
            else if (prop == IFI_EASEOUTQUAD) _optEase = Easer::ease_outquad;
            else if (prop == IFI_EASEOUTCUBIC) _optEase = Easer::ease_outcubic;
            else if (prop == IFI_EASEOUTQUART) _optEase = Easer::ease_outquart;
            else if (prop == IFI_EASEINOUTQUAD)
                _optEase = Easer::ease_inoutquad;
            else if (prop == IFI_EASEINOUTCUBIC)
                _optEase = Easer::ease_inoutcubic;
            else if (prop == IFI_EASEINOUTQUART)
                _optEase = Easer::ease_inoutquart;
            else if (prop == IFI_EASEINOUTBACK)
                _optEase = Easer::ease_inoutback;
            else if (prop == IFI_EASEOUTBACK)
                _optEase = Easer::ease_outback;
            else if (prop == IFI_EASEOUTELASTIC)
                _optEase = Easer::ease_outelastic;
            else if (prop == IFI_EASEOUTBOUNCE)
                _optEase = Easer::ease_outbounce;
            else if (prop == IFI_EASEINBOUNCE)
                _optEase = Easer::ease_inbounce;
            else if (msg)
            {
                LOG2("VN unknown prop ", prop);
                r = false;
            }
            return r;
        }

        void parseOptions(World* w)
        {
            // collect any supplementary optional parameters
            const char* tag = "options";
            while (*this)
            {
                string k;
                if (ifStr(k, tag))
                {
                    //LOG1("VN parsing option ", k);
                    if (k == IFI_AT || k == IFI_BY)
                    {
                        if (!bump()) break;
                        _optAtSet = ifNumber(_optAt, tag);
                        if (k == IFI_AT) _optClearFrom = true;
                        
                        //if (_optAtSet) LOG1("VN parseOptions at ", _optAt << " clear " << _optClearFrom);

                    }
                    else if (k == IFI_ON)
                    {
                        if (!bump()) break;
                        _optNode = node(w, tag);
                    }
                    else if (k == IFI_RATE)
                    {
                        if (!bump()) break;
                        ifNumber(_optRate, tag);
                    }
                    else if (k == IFI_CHANNEL)
                    {
                        if (!bump()) break;
                        float ch;
                        if (ifNumber(ch, tag)) _optChannel = (int)ch;
                    }
                    else if (k == IFI_VOLUME)
                    {
                        if (!bump()) break;
                        float vf;
                        if (ifNumber(vf, tag))
                        {
                            _optVol = (int)vf;
                            if (_optVol > 100) _optVol = 100;
                            if (_optVol < 0) _optVol = 0;
                        }
                    }
                    else if (k == IFI_PROPS)
                    {
                        // can have a list of props including alignment
                        while (bump())
                        {
                            string prop;
                            if (!ifStr(prop, tag)) break;
                            if (!parseProp(prop, tag)) break;
                        }
                    }
                    else
                    {
                        LOG2("VN parseOptions, unknown option ", k);
                        break;
                    }
                }
                else break;

                bump();
            }
        }

        void parseOptionsAfter(World* w)
        {
            // collect any supplementary optional parameters
            if (bump()) parseOptions(w);
        }
    };
 
    struct Node
    {
        World* _host;
        string  _name;
        Node*   _parent = 0;
        Nodes   _children;
        string  _resource;
        Node*   _dead = 0; // mark a node for delete, can be this
        ImageMeta* _meta = 0; // for the resource (original meta)
        float    _maskScale = 1;  // for the original meta mask (if any)

        // local props
        AVF     _lpos[4];   // values 0-9
        bool    _known[4];  // are the local positions known?
        float   _aspect; //  = w/h, zero if unconstrained
        AVF     _lscale[2];
        Point2f _lscaleOrigin;
        bool    _lscaleOriginCentre = false; // override
        int     _lz;  // local z
        AVF     _lalpha;

        // rotation
        AVF     _lrot;
        Point2f _lrotOrigin;

        // given by properties to use in absence of resource aspect
        // or to override
        float   _laspect = 0;
        bool    _laspectSet = false;

        Colour  _bg[2]; // background
        int     _bgCount = 0; // none, gradient or single

        AVCol   _ltint;  // animated tint colour
        Colour  _tint;

        // world props
        Boxf    _pos;
        int     _z;   // world z
        float   _alpha;
        Boxf    _uv;

        AVF     _zoombox[4];
        Align   _zoomalign = align_centre;
        bool    _zoomself = false;

        // fidgets
        Point2f _fidwh;  // box
        Point2f _fidxy;  // target
        Point2f _fidmove;  // current fidget delta move
        bool    _fidtarget;
        float   _fidRate = 1;

        // cycles
        float   _cycleTransition = 0;
        float   _cycleHold = 0;
        int     _cycleCount = 0;

        // for videos
        int     _overlayMode = 0;
        float   _overlayFactor = 0.5;
        float   _overlayIntensity = 0; // default

        // destroy video during and after play
        bool    _videoOneshot = false;
        int     _videoPlays = 0;
        bool    _videoLoop = false;

        // if we issue an "at" time for video
        // this is it's due time.
        float   _resourceStartTime = 0;

        enum CycleState
        {
            cycle_fadein,
            cycle_hold,
            cycle_rotating,
        };

        CycleState    _cycleState;

        // push and pop, XX this should be a stack
        static const int pushPosSize = 2;
        Boxf    _pushPos[pushPosSize];
        int     _pushPosIx = 0;

        // render props
        Boxf    _rpos;
        float   _taperMin = 1.0f;  // full height, no taper
        Colour  _taperColour;
        Rotation _rot;
        bool    _mipmap = false;

        // list of clickables
        ImageMeta::Elts _eltClicks;

        // did this node render AND is user on a click?
        // only valid in render
        HitClick        _lastRenderHit;

        // properties that depend on variables that we have to re-eval
        // theoretically during world update.
        VarList   _evalProps;
        
        Node(World* host) : _host(host) { _init(); }
        ~Node()
        {
            // make sure there are no pending events for this node
            // we are about to delete
            _host->_timeTable.clearNode(this);

            if (!_resource.empty()) texLoader->dropVideoTex(_resource);

            clearFidget();
            clearLoops();
            ::purge(_eltClicks);
            purgeChildren();
        }

        void pushPos(const Boxf& b)
        {
            if (_pushPosIx < pushPosSize)
            {
                _pushPos[_pushPosIx++] = b;
            }
        }
                
        void popPos(Boxf& b)
        {
            // do nothing if no value on stack
            if (_pushPosIx > 0)
            {
                b = _pushPos[--_pushPosIx];
            }
        }

        void _init()
        {
            for (int i = 0; i < 4; ++i)
            {
                _lpos[i] = 0;
                _known[i] = false;
                _zoombox[i] = 0;
            }
            
            _lpos[PX2] = 1;  // max values if unknown
            _lpos[PY2] = 1;

            _zoombox[PX2] = 1;
            _zoombox[PY2] = 1;

            _uv[PX2] = 1;
            _uv[PY2] = 1;

            for (int i = 0; i < 2; ++i)
            {
                _lscale[i] = 1;
            }

            _lrot = 0;
            _aspect = 0;  // unconstrained
            _lz = 1;
            _z = 1;
            _lalpha = 1;
            _alpha = 1;
            _ltint = Colour(Colour::ctransparent); // no give tint
        }

        int size() const
        {
            // total nodes including children
            int n = 1;
            for (auto ci : _children) n += ci->size();
            return n;
        }

        bool hasBackground() const
        {
            for (int i = 0; i < _bgCount; ++i)
                if (!_bg[i].transparent()) return true;
            return false;
        }

        bool isLoaded() const
        {
            // are we a resource node and is that resource loaded?
            return !_resource.empty() && texLoader->isLoaded(_resource);
        }

        bool hasFidget() const
        {
            return (bool)_fidwh;
        }

        bool inMotion(Time now) const
        {
            // are we in motion?
            // moving (pos), zooming (not rotating)
            // ALSO fading in or out. as this is used for arrival and depart
            // NB: but not scaling!

            // if there are keys, they are on each box corner or not.
            if (_lpos[0].hasFutureKeys(now)) return true;
            if (_zoombox[0].hasFutureKeys(now)) return true;

            if (_lalpha.hasFutureKeys(now)) return true;
            return false;
        }

#define CCLOOP(_v) \
        if ((_v).inLoop()) { ++lc; if (clear) (_v).clearLoop(); }

        int countLoops(bool clear = false)
        {
            int lc = 0;
            for (int i = 0; i < 4; ++i) CCLOOP(_lpos[i]);
            for (int i = 0; i < 2; ++i) CCLOOP(_lscale[i]);
            CCLOOP(_lalpha);
            return lc;
        }

        void clearLoops()
        {
            int lc = countLoops(true);
            _host->_loopCount -= lc;
            assert(_host->_loopCount >= 0);
        }

        void clearFidget()
        {
            if (hasFidget())
            {
                _host->_fidgetCount--;
                assert(_host->_fidgetCount >= 0);
                _fidwh = Point2f(0,0);
            }
        }

        Node* find(const string& name)
        {
            Node* n = 0;
            if (!name.empty()) // blank name will not find
            {
                if (name == _name) n = this;
                else
                {
                    for (auto ni : _children)
                    {
                        n = ni->find(name);
                        if (n) break;
                    }
                }
            }
            return n;
        }

        void addChild(Node* child)
        {
            assert(child);
            assert(!::contains(_children, child));
            
            child->_parent = this;
            _children.push_back(child);
        }

        bool removeChild(Node* child)
        {
            // remove child from this node.
            // return true if ok
            // but do not delete child node

            bool r = false;
            Nodes::iterator it = _children.begin();
            Nodes::iterator ie = _children.end();
            while (it != ie)
            {
                Node* ni = *it;
                r = (ni == child);
                if (r)
                {
                    _children.erase(it);
                    child->_parent = 0; // parent invalid
                    break;
                }
                ++it;
            }
            return r;
        }

        bool solveBox(Boxf& pos)
        {
            bool r = true;

            float a = _aspect;

            if (_laspectSet)
            {
                // this can override natural aspect
                // even set it to zero to unconstrain it.
                a = _laspect;
            }
            
            if (a)
            {
                bool knoww = _known[PX2] && _known[PX1];
                bool knowh = _known[PY2] && _known[PY1];

                if (knoww && knowh)
                {
                    // if we know all then override any aspect constraints
                    // x1,y1 - x2,y2
                }
                else if (knoww || knowh)
                {
                    // know either the width, height not both
                    if (knoww)
                    {
                        // calculate the height from the known width
                        float h1 = pos.width()/a;
                        if (_known[PY1])
                        {
                            // x1,y1 - x2,0
                            // calculate y2
                            pos[PY2] = pos[PY1] + h1;
                        }
                        else if (_known[PY2])
                        {
                            // x1,0 - x2,y2
                            // calculate y1
                            pos[PY1] = pos[PY2] - h1;
                        }
                        else
                        {
                            // x1,0 - x2,0
                            // centre y but shift up when otherwise
                            // space above.
                            float y1 = pos.centrey() - h1/2;
                            float y2 = pos.centrey() + h1/2;

                            if (y1 > 0)
                            {
                                y2 -= y1;
                                y1 = 0;
                            }
                            
                            pos[PY1] = y1;
                            pos[PY2] = y2;
                            //r = false; // both y1 and y2 unknown
                        }
                    }
                    else
                    {
                        assert(knowh);
                        // calculate the width from known height
                        float w1 = pos.height()*a;
                        if (_known[PX1])
                        {
                            // x1,y1 - 0,y2
                            // calculate x2
                            pos[PX2] = pos[PX1] + w1;
                        }
                        else if (_known[PX2])
                        {
                            // 0,y1 - x2,y2
                            // calculate x1
                            pos[PX1] = pos[PX2] - w1;
                        }
                        else
                        {
                            // 0,y1 - 0,y2
                            // centre x
                            float x1 = pos.centrex() - w1/2;
                            float x2 = pos.centrex() + w1/2;
                            pos[PX1] = x1;
                            pos[PX2] = x2;
                        }
                    }
                }
                else
                {
                    // dont know the width nor height
                    // calculate from the aspect and max values
                    float w = u_min(pos.height()*a, pos.width());
                    float h = u_min(w/a, pos.height());
                    
                    if (_known[PX2] && _known[PY2])
                    {
                        // anchor bottom right, float top left
                        pos[PX1] = pos[PX2] - w;
                        pos[PY1] = pos[PY2] - h;
                    }
                    else
                    {
                        // treat as anchored top left and float bottom right
                        // regardless. This will be the interpretation
                        // of no pos at all
                        
                        //if (_known[PX1] && _known[PY1])
                        // anchor top left, but floating bottom,right
                        pos[PX2] = pos[PX1] + w;
                        pos[PY2] = pos[PY1] + h;
                    }
                }
            }
            return r;
        }
        
#define ARGOPTS                                 \
        args.parseOptionsAfter(_host);          \
        t = args.onAt(at) + now;                \
        if (t > te) te = t;

#define ARGOPTSHERE                             \
        args.parseOptions(_host);               \
        t = args.onAt(at) + now;                \
        if (t > te) te = t;

#define ARGNODE args.onNode(this)
#define ARGCLEAR args.onClear(clearFrom)

#define ARGFAIL args.fail()

        bool update(const VarList& vs, bool newnode)
        {
            // combine varset with local, overriding

            float at = 0;

            // do we clear all keys from the point we set?
            bool clearFrom = false;

            // properties set by this up in the future are to loop back
            // to current time
            float loop = 0;
            int loops = 0;
            
            Node* xfadeto = 0;

            // this can be used to apply to various updates
            Point2f origin;
            bool originSet = false;
            bool posSet = false;

            // check that each term is handled
            bool handled = false;
            
            // first pass look for directives, second pass do properties
            for (auto& vi : vs)
            {
                const string& k = vi.first;
                Args args(_host, vi.second);
                    
                if (k == IFI_AT)
                {
                    handled = true;
                    args.ifNumber(at, IFI_AT);

                    // "at" means clear what we're doing and set for "at"
                    clearFrom = true;
                }
                else if (k == IFI_BY)
                {
                    handled = true;
                    // BY means set a key but do not clear track
                    args.ifNumber(at, IFI_BY);
                }
                else if (k == IFI_LOOP)
                {
                    handled = true;
                    args.ifNumber(loop, IFI_LOOP);
                }
                else if (k == IFI_XFADE)
                {
                    handled = true;
                    string s;
                    args.ifStr(s, IFI_XFADE);
                    xfadeto = _host->find(s);

                    if (xfadeto)
                    {
                        // ensure it is a child node
                        bool ok = false;
                        for (auto ci : _children)
                        {
                            if (ci == xfadeto)
                            {
                                ok = true;
                                break;
                            }
                        }
                        if (!ok)
                        {
                            LOG2("VN update WARNING xfadeto ", s << " not a child of " << _name);
                            xfadeto = 0;
                        }
                    }
                    else
                    {
                        LOG2("VN update WARNING xfadeto node unknown ", s);
                    }
                }
                else if (k == IFI_ORIGIN)
                {
                    handled = true;
                    if (args.point(origin, IFI_ORIGIN)) originSet = true;
                
                }
                else if (k == IFI_ASPECT)
                {
                    handled = true;
                    _laspectSet = args.ifNumber(_laspect, IFI_ASPECT);
                }
                else if (k == IFI_MODE)
                {
                    handled = true;
                    float v = 0;
                    if (args.ifNumber(v, IFI_MODE))
                    {
                        _overlayMode = v;
                    }
                    else
                    {
                        LOG2("mode expects a number, ", _name);
                    }
                }
                else if (k == IFI_FACTOR)
                {
                    handled = true;
                    if (!args.ifNumber(_overlayFactor, IFI_FACTOR))
                    {
                        LOG2("factor expects a number, ", _name);
                    }
                }
                else if (k == IFI_INTENSITY)
                {
                    handled = true;
                    if (!args.ifNumber(_overlayIntensity, IFI_INTENSITY))
                    {
                        LOG2("intensity expects a number, ", _name);
                    }
                }
                else if (k == IFI_VIDEOLOOP)
                {
                    handled = true;
                    _videoLoop = true;
                }
                else if (k == IFI_ONESHOT)
                {
                    handled = true;
                    _videoOneshot = true;
                }
            }

            Time now = _host->_now;
            Time t = now + at;
            Time te = t;  // end time for properties that extend
            
            // second pass peform operations
            for (auto& vi : vs)
            {
                _host->_ectx.clearUsed();
                            
                const string& k = vi.first;
                Args args(_host, vi.second);

                bool isBreathe;
                
                if (k == IFI_PARENT)
                {
                    if (!newnode)
                    {
                        Node* p = 0;
                        _host->getTargetParent(vs, p);
                        
                        // apply parent change first. must not be new node
                        //string parent;
                        //args.ifStr(parent, IFI_PARENT);
                        //Node* p = _host->find(parent); // may be blank
                        if (p)
                        {
                            assert(_parent);
                    
                            if (p != _parent)
                            {
                                // change parent
                                // MB: caller will handle displacement of Z=1
                                _parent->removeChild(this);
                                p->addChild(this);
                            }
                        }
                        else
                        {
                            LOG2("VN update WARNING, bad parent ", vi.second << " for " << _name);
                        }
                    }
                }
                else if (k == IFI_ZOOM)
                {
                    // note zoom does not clear fidget
                    Boxf zbox; // the zoom origin box
                    if (args.box(zbox, IFI_ZOOM) && zbox)
                    {
                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            Time t1 = now;
                            bool clear = ARGCLEAR;

                            // if we are not clearing, then apply any
                            // easement from the last key (otherwise from now)
                            if (!clear)
                            {
                                Time t2 = ARGNODE->_zoombox[0].lastKeyTime();
                                if (t2 > t1) t1 = t2;
                            }

                            _host->_easer.start(args._optEase, t - t1);
                            
                            for (int i = 0; i < 4; ++i)
                            {
                                _host->_easer.apply(ARGNODE->_zoombox[i],
                                                    t1, t, zbox[i], clear);
                            }
                            
                            if (args._optAlignSet) _zoomalign = args._optAlign;

                            // do not cancel if set already
                            if (args._self) _zoomself = true;

                        }
                    }
                }
                else if (k == IFI_POS)
                {
                    posSet = true;
                    
                    Boxf pos;
                    bool known[4];
                    int nk = args.boxPartial(pos, known);
                    ARGOPTS;
                    if (!ARGFAIL)
                    {
                        Time t1 = now;
                        bool clear = ARGCLEAR;
                        Node* ni = ARGNODE;

                        // if we are not clearing, then apply any
                        // easement from the last key (otherwise from now)
                        if (!clear)
                        {
                            Time t2 = ni->_lpos[0].lastKeyTime();
                            if (t2 > t1) t1 = t2;
                        }
                        
                        if (args._optPush)
                        {
                            Boxf b1;
                            for (int i = 0; i < 4; ++i)
                                b1[i] = ni->_lpos[i].at(t1);
                            ni->pushPos(b1);
                        }
                        
                        if (args._optPop)
                        {
                            ni->popPos(pos);

                            // maintain old knowns
                            nk = 0;
                            for (int i = 0; i < 4; ++i)
                            {
                                known[i] = ni->_known[i];
                                if (known[i]) ++nk;
                            }

                            //LOG1("VN pos retrieved pos ", pos);
                        }

                        // XX if an unknown becomes known in the future
                        // it should be anchored now.

                        // as a special case if no values supplied to
                        // partial box, then we dont change the current
                        // position nor change knowns
                        if (nk > 0)
                        {
                            _host->_easer.start(args._optEase, t - t1);
                        
                            for (int i = 0; i < 4; ++i)
                            {
                                // can override known so that known becomes unknown
                                ni->_known[i] = known[i];
                                if (known[i])
                                {
                                    _host->_easer.apply(ni->_lpos[i],
                                                        t1, t, pos[i], clear);
                                }
                            }
                        }
                        else if (t > t1)
                        {
                            // place anchor at "at" position
                            for (int i = 0; i < 4; ++i)
                            {
                                if (ni->_known[i])
                                {
                                    // anchor value at t
                                    float a = ni->_lpos[i].at(t);
                                    //LOG1("placing anchor ", a << " at " << t << " clear:" << clear);
                                    ni->_lpos[i].set2l(t1, t, a, clear);
                                }
                            }
                            
                        }
                    }
                }
                else if (k == IFI_RESOURCE)
                {
                    // may subst a different resolution version
                    extern string selectPictureAlternative(const string& name);

                    string r;
                    args.ifStr(r, IFI_RESOURCE);
                    if (!r.empty())
                    {
                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            r = selectPictureAlternative(r);
                            assert(!r.empty());

                            if (_resource != r)
                            {
                                _resource = r;
                            
                                const ImagesMeta* ims = _host->_host->_imMeta;
                                if (ims)
                                {
                                    // NB: all versions will have same aspect ratio
                                    // save the meta in the node
                                    _meta = ims->findOriginal(_resource,
                                                              &_maskScale);
                                    if (_meta && _meta->_h)
                                    {
                                        // retrieve aspect ratio
                                        _aspect = (float)_meta->_w / _meta->_h;
                                    }
                                    else
                                    {
                                        LOG2("VN cannot locate aspect for ", _resource);
                                    }
                                }

                                if (t > now)
                                {
                                    // start in future.
                                    _resourceStartTime = t;
                                }

                                if (args._optMipmap)
                                {
                                    // only if globally enabled
                                    if (_host->_host->_enableMipmaps)
                                    {
                                        /// enable mipmaps for this tex
                                        ARGNODE->_mipmap = true;
                                    }
                                }

                                // do not kick off loading here.
                                // do this in world update,
                                // so that they can be requested in draw order
                            }

                            // if we are already loaded, then rewind video
                            auto rec = texLoader->getLoadedRec(r,
                                                               _host->_host->_renderCount);
                            if (rec && rec->_stream)
                            {
                                LOG2("VN resource, video rewind ", r);
                                rec->_stream->rewind();
                            }
                        }
                    }
                }
                else if (k == IFI_ROTATE)
                {
                    float ang;
                    if (args.ifNumber(ang, IFI_ROTATE))
                    {
                        // convert to radians
                        ang *= 0.0174532925199432957692369f;

                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            ARGNODE->_lrot.set2l(now, t,ang, ARGCLEAR);
                            
                            // collect any rotation origin
                            if (originSet) _lrotOrigin = origin;
                        }
                    }
                }
                else if (k == IFI_FIDGETBOX)
                {
                    int fid1 = hasFidget();
                    if (args.point(_fidwh, IFI_FIDGETBOX))
                    {
                        // set to (0,0) will just stop fidget and
                        // keep current XY offset.
                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            // find a new target
                            _fidtarget = true;
                            _fidRate = args._optRate;
                            _host->_fidgetCount += hasFidget() - fid1;
                        }
                    }
                }
                else if (k == IFI_Z)
                {
                    float lz;
                    if (args.ifNumber(lz, IFI_Z)) _lz = lz; // int
                }
                else if (k == IFI_CYCLE)
                {
                    // cycle hold time, transition time
                    // drop any existing cycle for this node
                    _host->_timeTable.clearNode(this);

                    Point2f xy;
                    if (args.point(xy, IFI_CYCLE))
                    {
                        _cycleHold = xy.x;
                        _cycleTransition = xy.y;
                        _cycleCount = -1; // loop

                        if (!_cycleHold) _cycleHold = 1;
                        if (!_cycleTransition) _cycleTransition = 1;
                        
                        if (_cycleTransition > _cycleHold)
                            _cycleTransition = _cycleHold;

                        _cycleState = cycle_fadein;

                        // start cycle
                        cycleNext(t);
                    }
                }
                else if (k == IFI_FADEIN || k == IFI_FADEOUT)
                {
                    // fadein time target ...
                    float ft;
                    float a = 0, b = 1;
                    
                    if (args.ifNumber(ft, k.c_str()))
                    {
                        if (args.bump())
                        {
                            // optional fadin/out target value
                            if (args.ifNumber(b)) args.bump();
                        }
                        
                        if (k == IFI_FADEOUT)
                        {
                            float t = a;
                            a = b;
                            b = t;
                        }
                        
                        // fadein time (from zero or alpha set manually below
                        // fadein applies at the given "at"
                        ARGOPTSHERE;
                        if (!ARGFAIL)
                        {
                            Time e = t + ft;
                            if (e > te) te = e;
                            ARGNODE->_lalpha.set4l(t, a, e,
                                                   b,
                                                   ARGCLEAR);
                        }
                    }
                }
                else if (k == IFI_ALPHA)
                {
                    float a;
                    if (args.ifNumber(a, IFI_ALPHA))
                    {
                        //LOG1("VN update node alpha ", a << " " << args.get());
                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            ARGNODE->_lalpha.set2l(now,
                                                   t,
                                                   a,
                                                   ARGCLEAR);
                        }
                    }
                }
                else if ((isBreathe = (k == IFI_BREATHE)) || k == IFI_SCALE)
                {
                    // breathe x,y at time
                    Point2f sxy;
                    if (args.point(sxy, IFI_SCALE))
                    {
                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            auto ni = ARGNODE;
                            for (int i = 0; i < 2; ++i)
                            {
                                AVF& si = ni->_lscale[i];

                                si.set2l(now, t,
                                         sxy[i],
                                         ARGCLEAR);

                                if (isBreathe) 
                                {
                                    // set mirror to scale back
                                    float t2 = t + t - now;
                                    if (t2 > te) te = t2;
                                    assert(!si.inLoop());
                                    
                                    si._set(t2, si.at(now));
                                    si.setloop(now, t2);
                                    ++loops;
                                }
                            }

                            if (isBreathe)
                            {
                                // default to centre for breathe
                                if (originSet) ni->_lscaleOrigin = origin;
                                else
                                {
                                    ni->_lscaleOriginCentre = true;
                                }
                            }
                            else
                            {
                                // any origin specified will apply to scale
                                // or zero otherwise
                                if (originSet) ni->_lscaleOrigin = origin;
                                else if (args._optAlignSet)
                                {
                                    if (args._optAlign == align_centre)
                                    {
                                        // centre on shape
                                        ni->_lscaleOriginCentre = true;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (k == IFI_BACKGROUND)
                {
                    if (args.colour(_bg[0], IFI_BACKGROUND))
                    {
                        _bgCount = 1;

                        // maybe gradient
                        if (args.bump())
                        {
                            if (args.colour(_bg[1], IFI_BACKGROUND))
                            {
                                // for now expect only 2 gradient points
                                ++_bgCount;
                            }
                        }
                    }
                }
                else if (k == IFI_TINT)
                {
                    Colour c;
                    if (args.colour(c, IFI_TINT))
                    {
                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            ARGNODE->_ltint.set2l(now, t, c, ARGCLEAR);
                        }
                    }
                }
                else if (k == IFI_SOUND)
                {
                    AudioInfo ai;
                    args.ifStr(ai._name, IFI_SOUND);
                    if (!ai._name.empty())
                    {
                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            ai._chan = args._optChannel;
                            ai._vol = args._optVol;
                            if (t > now)
                            {
                                // signal preload now and play layer
                                ai._preload = true;
                                play_audio(ai);

                                ai._preload = false;
                                ARGNODE->scheduleAudioAt(t, ai);
                            }
                            else
                            {
                                play_audio(ai);
                            }
                        }
                    }
                }
                else if (k == IFI_TAPER)
                {
                    float tv;
                    if (args.ifNumber(tv, IFI_TAPER))
                    {
                        ARGOPTS;
                        if (!ARGFAIL)
                        {
                            ARGNODE->_taperMin = tv;
                        }
                    }
                }
                else if (k == IFI_ERASE)
                {
                    ARGOPTSHERE;
                    if (!ARGFAIL)
                    {
                        if (t > now)
                        {
                            ARGNODE->scheduleEraseAt(t);
                        }
                        else
                        {
                            // mark a node for delete
                            _dead = args.onNode(ARGNODE);
                            assert(_dead);
                            //LOG1("VN delete ", _dead->_name);
                        }
                    }
                }
                else if (k == IFI_CONTINUE)
                {
                    ARGOPTSHERE;
                    if (!ARGFAIL)
                    {
                        ARGNODE->scheduleContinueAt(t);
                    }
                }
                else if (!handled)
                {
                    bool ignore = (k == IFI_NAME || k == IFI_CLICK);

                    if (!ignore)
                    {
                        // key was not handled in first or second pass
                        LOG2("VN Warning asset key not handled ", k << " in " << _name);
                    }
                }

                // were any variables accessed during update?
                // ignore if host needs eval as this is inside the
                // eval itself

                if (!_host->_needEval)
                {
                    if (_host->_ectx._usedTerm > 0)
                    {
                        // keep these properties so that we can re-eval later
                        //LOG1("VN Adding re-eval prop ", k << " " << _evalProps);
                        assert(!_evalProps.find(k));
                        _evalProps.add(k, vi.second); // consumes val
                    }
                    else
                    {
                        if (_evalProps.remove(k))
                        {
                            //LOG1(">>>> removed eval prop for ", k);
                        }
                    }
                }
            }

            if (newnode && !posSet && !_resource.empty())
            {
                // a new node with no position and a resource

                assert(_parent);
                
                if (_lz == 1 && _parent->_name == IFI_IMAGE)
                {
                    // first layer on "image"
                    // these have default position 0,null,1,null
                    _lpos[0].clearTo(0); _known[0] = true;
                    _lpos[2].clearTo(1); _known[2] = true;                    
                }
                else
                {
                    // by default we make this 0,0,1,?
                    // so that it will fill horizontal
                    _lpos[0].clearTo(0); _known[0] = true;
                    _lpos[1].clearTo(0); _known[1] = true;
                    _lpos[2].clearTo(1); _known[2] = true;
                }
            }

            if (xfadeto)
            {
                // ASSUME xfadeto is a child of this node
                
                // drop any existing cycle for this node
                _host->_timeTable.clearNode(this);

                _cycleTransition = 0.5f; // XX
                _cycleCount = 0; // only once
                _cycleState = cycle_fadein;

                bringToFront(xfadeto);

                // start cycle
                cycleNext(t);
            }

            // third pass, handle loops
            if (loop) for (auto& vi : vs)
                      {
                          const string& k = vi.first;
                          if (k == IFI_ZOOM)
                          {
                              for (int i = 0; i < 4; ++i)
                                  loops += _zoombox[i].resetloop(now, te, loop); 
                          }
                          else if (k == IFI_POS)
                          {
                              for (int i = 0; i < 4; ++i)
                                  loops += _lpos[i].resetloop(now, te, loop); 
                          }
                          else if (k == IFI_ROTATE)
                          {
                              loops += _lrot.resetloop(now, te, loop);
                          }
                          else if (k == IFI_FADEIN || k == IFI_FADEOUT || k == IFI_ALPHA)
                          {
                              loops += _lalpha.resetloop(now, te, loop); 
                          }
                          else if (k == IFI_SCALE)
                          {
                              // NB: breathe sets up its own loop!
                              for (int i = 0; i < 2; ++i)
                                  loops += _lscale[i].resetloop(now, te, loop);
                          }
                          else if (k == IFI_TINT)
                          {
                              loops += _ltint.resetloop(now, te, loop);
                          }
                      }

            if (te > _host->_endOfTime) _host->_endOfTime = te;

            _host->_loopCount += loops;
            assert(_host->_loopCount >= 0);

            LOG3("updated node at ", now << ": " << *this);
            return true;
        }

        void updateUV()
        {
            assert(_parent);

            // clip against parent
            float rw = _rpos.width();
            float rh = _rpos.height();

            _uv.setUnity();
                    
            float dx = _rpos[PX2] - _parent->_rpos[PX2];
            if (dx > 0)
            {
                _uv[PX2] = (rw - dx)/rw;
                _rpos[PX2] -= dx;
            }
            dx = _parent->_rpos[PX1] - _rpos[PX1];
            if (dx > 0)
            {
                _uv[PX1] = dx/rw;
                _rpos[PX1] += dx;
            }
                    
            float dy = _rpos[PY2] - _parent->_rpos[PY2];
            if (dy > 0)
            {
                _uv[PY2] = (rh - dy)/rh;
                _rpos[PY2] -= dy;
            }
            dy = _parent->_rpos[PY1] - _rpos[PY1];
            if (dy > 0)
            {
                _uv[PY1] = dy/rh;
                _rpos[PY1] += dy;
            }
        }

        void updateEltClicks()
        {
            // calculate clickable regions
            if (!_meta) return;

            // take the click polys in the meta which are 0-1,
            // scale and translate them into pixel space
            // and apply rotation, then store the result as our
            // elt clicks.
            // NB: polys will need any final pinch-zoom applied
            // which is done in render
            for (auto ei : _meta->_elts)
            {
                if (ei->_type == ImageMeta::m_elt_click)
                {
                    auto ec = (ImageMeta::EltClick*)ei;
                    if (ec->_active)
                    {
                        // make a copy
                        // and transform points 
                        Point2f sz = _pos.size();
                        Point2f top = _pos.topleft();
                                    
                        auto c2 = new ImageMeta::EltClick(*ec);
                        for (Poly& pi : c2->_polys._polys)
                        {
                            int n = pi.size();
                            for (int i = 0; i < n; ++i)
                            {
                                Point2f& p = pi._points[i];
                                p *= sz;
                                p += top;

                                if (_rot && _rot._baseSet)
                                    p = _rot.rotate(p);
                            }
                            pi.recalcBounds();
                        }
                                    
                        _eltClicks.push_back(c2);
                    }
                }
            }
        }

        bool updateWorld()
        {
            // update tree node here and below. return false if cannot
            // cannot update the root
            
            assert(_parent);

            // always throw away any previous clickable regions
            ::purge(_eltClicks);

            // parent positions are not scaled
            const Boxf& ppos = _parent->_pos;

            if (!ppos)
            {
                // early out. if parent box is empty, we are too
                //_rpos.clear();

                // ignore children
                return true; // done
            }

            if (_host->_needEval && !_evalProps.empty())
            {
                // we have any props that depend on variables these
                // need to be re-evaluated before update

                //LOG1("VN world update, re-eval ", _evalProps);
                update(_evalProps, false);
                assert(!_dead);  // re-eval should not delete anything!
            }

            float pw = ppos.width();
            float ph = ppos.height();
            Time t = _host->_now;

            _z = _lz + _parent->_z;
            _alpha = _parent->_alpha * _lalpha.at(t);

            Colour tint = _ltint.at(t);

            // if we have no tint, inherit our parent
            if (tint.alpha() == 0) _tint = _parent->_tint;
            else
            {
                _tint = _parent->_tint.overlaidWith(tint);
            }

            //if (_tint.alpha()) LOG1("tint ", _name << " = " << _tint);

            Boxf pos;
            Boxf zbox;
            for (int i = 0; i < 4; ++i)
            {
                pos[i] = _lpos[i].at(t);
                zbox[i] = _zoombox[i].at(t);
            }

            bool moving = inMotion(t);

#ifdef LOGGING
            if (!pos)
            {
                LOG2("VN bad pos box for ", _name << " = " << pos);
                assert(pos); // lpos should be valid
            }
#endif

            // fidget is applied at 0-1 pos scale not screen scale
            // NB: no need to fidget invisibles
            if (!moving && hasFidget() && _alpha > 0) 
            {
                Point2f pxy = pos.topleft();
                    
                if (_fidtarget)
                {
                    // select new target
                    _fidtarget = false;
                        
                    Point2f ran(_host->_host->_ran.genFloat(),
                                _host->_host->_ran.genFloat());

                    ran -= 0.5f;
                    _fidxy = pxy + (ran * _fidwh);
                }

                // vector to target
                Point2f v = _fidxy - pxy - _fidmove;

                if (v)
                {
                    Point2f av = v.abs();

                    // per-frame movement limit
                    // XX fps 20
                    float r = (0.0004f/20.0f) * _fidRate;
                    av = av.min(r);

                    if (v[0] < 0) av[0] = -av[0];
                    if (v[1] < 0) av[1] = -av[1];
                    
                    //LOG1("fidget ", _name << " by " << av*Point2f(pw, ph));

                    _fidmove += av;
                }
                else
                {
                    // find new target next time
                    _fidtarget = true;
                }
            }

            if (moving)
            {
                // XX systematically erode fidget drift while zooming
                _fidmove.x *= 0.95f;
                _fidmove.y *= 0.95f;
            }

            // apply fidget offset (even if not fidgeting)
            pos.move(_fidmove);

            pos.scale(pw, ph);
            
            // if no aspect is provided, then it doesnt matter whether
            // we know values or not. the unknown will be assumed to be
            // min or max and stretch to fill parent
            bool r = solveBox(pos);

            Point2f sxy(_lscale[0].at(t), _lscale[1].at(t));
            if (sxy.x != 1 || sxy.y != 1)
            {
                Point2f scaleOrigin;

                if (_lscaleOriginCentre)
                    scaleOrigin = pos.centre();
                else
                {
                    scaleOrigin = pos.topleft() + pos.size()*_lscaleOrigin;
                }
                
                pos.scaleOrigin(sxy, scaleOrigin);
                //LOG1("pos ", pos << " aspect " << pos.width()/pos.height());

            }
            
            bool zoom = r && !zbox.isUnity();
            if (zoom)
            {
                float sx = 1.0f/zbox.width();
                float sy = 1.0f/zbox.height();
                float s = u_min(sx, sy);

                pos.scale(s, s);
                
                if (_zoomself)
                {
                    pw = pos.width();
                    ph = pos.height();
                }
                else
                {
                    pw *= s;
                    ph *= s;
                }

                zbox.scale(pw, ph);

                //LOG1("VN zoom ", _name << " " << pos << " as:" <<  _aspect << " ol
                if (_zoomalign == align_right)
                {
                    //float cx = ppos.width() - zbox.width();
                    //pos.move(-zbox[PX1]+cx, -zbox[PY1]);

                    float dx = ppos.width() - zbox[PX2];
                    float dy = -zbox[PY1];
                    pos.move(dx, dy);
                }
                else if (_zoomalign == align_centre)
                {
                    // to centre, transform the zbox via the
                    // same calculation as above, then
                    // adjust pos box by transformed zbox centre
                    // adjustment

                    
                    //pos.move(-zbox[PX1], -zbox[PY1]);
                    //zbox.move(-zbox[PX1], -zbox[PY1]);
                    //float cx = ppos.centrex() - zbox.centrex();
                    //pos.move(cx, 0);

                    float cx = ppos.centrex() - zbox.centrex();
                    pos.move(cx, -zbox[PY1]);
                }
                else // left
                {
                    // already left, centre y
                    pos.move(-zbox[PX1], -zbox[PY1]);
                }
            }

            if (r)
            {
                // translate by parent origin
                pos.move(ppos[PX1], ppos[PY1]);

                bool drawPic = !_resource.empty();

                _pos = pos;
                bool ok = _pos;

                if (ok)
                {
                    // apply rotation.
                    // do not combine parent rotation, this is done in a
                    // second pass
                    _rot.setAngle(_lrot.at(t));

                    // if we are rotating set this up
                    // otherwise copy any rotate from parent
                    // however, the first image sets the origin of rotation
                    // set this and also propagate the centre
                    if (_rot) _rot.init();
                    else
                    {
                        // propagate
                        _rot = _parent->_rot;
                    }

                    if (drawPic && !_rot._baseSet) _rot.setBase(_pos);

                    // calculate clickable regions before uv is clipped
                    if (drawPic) updateEltClicks();
                }

                if (ok)
                {
                    // update rpos and UV, but this will be updated
                    // again in render if there is a zoom
                    _rpos = pos;

                    // clip uv and rpos
                    updateUV();

                    ok = _rpos;

                    if (ok)
                    {
                        // picture listed even if alpha zero
                        if (drawPic || hasBackground())
                        {
                            // add into the drawlist ordered by Z
                            DrawList::iterator it = _host->_drawList.begin();
                            DrawList::iterator ie = _host->_drawList.end();
                            while (it != ie)
                            {            
                                if (_z < (*it)->_z) break;
                                ++it;
                            }

                            // box and uv are valid
                            // might not be loaded
                            // might be tranparent
                            _host->_drawList.insert(it, this);
                        }

                        for (auto ci : _children) ci->updateWorld();
                    }
                }
            }
            else
            {
                LOG2("update world, cannot position ", _name);
            }
            
            return r;
        }

        void cycleNext(Time t)
        {
            if (t <= _host->_now)
            {
                // call immediately!
                updateCycle(0);
            }
            else
            {
                using namespace std::placeholders;  
                EventFn f = std::bind(&Node::updateCycle, this, _1);
                _host->_timeTable.add(f, t, this);
            }
        }

        void scheduleEraseAt(Time t)
        {
            // schedule node removal at t
            using namespace std::placeholders;  
            EventFn f = std::bind(&Node::eraseMe, this, _1);
            _host->_timeTable.add(f, t, this);
        }

        void scheduleAudioAt(Time t, const AudioInfo& ai)
        {
            // schedule audio play at t
            // ASSUME t> now
            using namespace std::placeholders;  
            Event ev(this);
            ev._t = t;
            ev._fn = std::bind(&Node::playAudio, this, _1);
            ev._pendingAudio = ai;
            _host->_timeTable.add(ev);
        }

        void scheduleContinueAt(Time t)
        {
            //LOG3("scheduling continue at ", t);

            if (t > _host->_now)
            {
                SetSuspendInput(true); // suspend until t

                using namespace std::placeholders;  
                Event ev(this);
                ev._t = t;
                ev._fn = std::bind(&Node::continueAt, this, _1);
                _host->_timeTable.add(ev);
            }
        }

        Node* minChildZ()
        {
            Node* cmin = 0;
            for (auto ci : _children)
                if (!cmin || ci->_lz < cmin->_lz) cmin = ci;

            return cmin;
        }

        Node* maxChildZ()
        {
            Node* c = 0;
            for (auto ci : _children)
                if (!c || ci->_lz > c->_lz) c = ci;
            return c;
        }

        Node* firstResourceChild(Time now)
        {
            Node* c = 0;
            if (!_resource.empty()) c = this;
            else
            {
                int z = _lz;
                for (auto ci : _children)
                {
                    // NB: do not choose a transparent base
                    if (!ci->_resource.empty() &&
                        (!c || ci->_lz + _lz > z) &&
                        ci->_lalpha.at(now) > 0)
                    {
                        z = ci->_lz + _lz;
                        c = ci;
                    }
                }

                if (!c)
                {
                    for (auto ci : _children)
                    {
                        c = ci->firstResourceChild(now);
                        if (c) break;
                    }
                }
            }
            return c;
        }

        Node* firstAreaChild()
        {
            // look for an area child. ie one without a resource
            // if so, return it, otherwise null.
            // NB: non-recursive. must be immediate
            Node* c = 0;
            for (auto ci : _children)
            {
                if (ci->_resource.empty())
                {
                    c = ci;
                    break;
                }
            }
            return c;
        }
#if 0        
        Node* _maxChildZRec(int z, int& z1)
        {
            Node* c = this;
            z1 = z + _lz;
            for (auto ci : _children)
            {
                int z2;
                Node* a = ci->_maxChildZRec(z + 1, z2);
                if (a && z2 > z1)
                {
                    c = a;
                    z1 = z2;
                }
            }
            return c;            
        }
        
        Node* maxChildZRec()
        {
            int z;
            Node* n = _maxChildZRec(_lz, z);
            return n;
        }
#endif        

        void bringToFront(Node* top)
        {
            Node* mc = maxChildZ();
            if (!mc) return;

            int maxz = mc->_lz;
            int gapz = top->_lz;

            for (auto ci: _children)
            {
                if (ci == top)
                {
                    // we become the max z
                    ci->_lz = maxz;
                }
                else if (ci->_lz > gapz)
                {
                    // those above us, down by 1
                    ci->_lz--;
                }
            }
        }

        void _fadeIn(Node* n, Time due)
        {
            // Arrange for node n to fade in by `due`.

            // if the alpha is currently zero, arrange for the
            // fade to start from a very small value which is
            // set immediately.

            // This will prioritise loading of this image over other
            // transparent resources - which will be loader afterwards
            // note that a very small alpha will convert to a zero integer
            // and thus the image wont actually get drawn just yet.
            // but it will get loaded!
            
            Time now = _host->_now;
            float a = n->_lalpha.at(now);
            if (a == 0) a = 0.001f;
            n->_lalpha.set4l(now,a, due,1.0f, true);            
        }

        void cycleClearBelow(Node* top)
        {
            Time now = _host->_now;

            float transt = _cycleTransition;
            //float holdt = _cycleHold;
            
            // make sure we're done before hold is over.
            transt /= 2;
            
            for (auto ci : _children)
            {
                if (ci->_lz < top->_lz)
                {
                    float la = ci->_lalpha.at(now);
                    if (la > 0)
                    {
                        //LOG1("VN cycle, fade out ", ci->_name);
                        Time due = now + transt;
                        ci->_lalpha.set2l(now,due,0.0f, true); 
                    }
                    else
                    {
                        //set this out
                        UNUSED int l = ci->_lalpha.clearTo(0);
                        assert(!l);
                    }
                }
            }
        }

        void eraseMe(Event* ev)
        {
            // callback to delete a node usually after fadeout
            _host->erase(this);
        }

        void playAudio(Event* ev)
        {
            LOG2("VN play audio ", ev->_pendingAudio._name);
            play_audio(ev->_pendingAudio);
        }
             
        void updateCycle(Event*)
        {
            Time now = _host->_now;

            float holdt = _cycleHold;
            float transt = _cycleTransition;

            if (_cycleState == cycle_rotating)
            {
                if (!_cycleCount) return; // finished

                _cycleCount--;
                    
                Node* low = minChildZ();
                bringToFront(low);

                _cycleState = cycle_fadein;
            }

            if (_cycleState == cycle_fadein)
            {
                Node* top = maxChildZ();
                if (!top) return;

                Time due = now + transt;
                
                float la = top->_lalpha.at(now);
                if (la < 1)
                {
                    // fade in over trans
                    //LOG1("VN cycle, fade in ", top->_name << " from " << la);
                    _fadeIn(top, due);
                    //top->_lalpha.set2l(now,due,1.0f, true);
                }

                _cycleState = cycle_hold;

                // time to fade in and hold
                cycleNext(due);
            }
            else if (_cycleState == cycle_hold)
            {
                //LOG1("VN cycle, holding for ", holdt);

                Node* top = maxChildZ();
                assert(top);
                
                // fade out everyone else
                cycleClearBelow(top);

                _cycleState = cycle_rotating;
                cycleNext(now + holdt);
            }
        }

        void continueAt(Event* ev)
        {
            //LOG2("VN continuing ", 1);
            SetSuspendInput(false); 
        }

        void purgeChildren()
        {
            ::purge(_children);
        }

        friend std::ostream& operator<<(std::ostream& os, const Node& n)
        {
#ifdef LOGGING            
            os << "Node:" << n._name;
            if (n._parent) os << '/' << n._parent->_name;
            os << " (";
            Time t = n._host->_now;
            for (int i = 0; i < 4; ++i)
            {
                if (i) os << ' ';
                if (n._known[i]) os << n._lpos[i].at(t);
                else os << '?';
            }
            os << " z=" << n._lz << ")/" << n._rpos << " z=" << n._z;
            os << ' ' << n._pos;
            if (n._aspect) os << " w/h:" << n._aspect;
            if (n._alpha != 1) os << " a:" << n._alpha;

            float s = n._lscale[0].at(t);
            if (s != 1) os << " lsx:" << s;
            s = n._lscale[1].at(t);
            if (s != 1) os << " lsy:" << s;

            //os << " lsx:" << n._lscale[0] << " lsy:" << n._lscale[1];
            //if (n._scale[0] != 1) os << " sx:" << n._scale[0];
            //if (n._scale[1] != 1) os << " sy:" << n._scale[1];
            if (!n._children.empty())
            {
                os << " {";
                int cc = 0;
                for (auto ci : n._children)
                {
                    if (cc++) os << ' ';
                    os << '{' << *ci << '}';
                }
                os << '}';
            }
#endif // LOGGING        
            return os;
        }
    };

    struct TimeTable
    {
        // ordered by time
        typedef std::list<Event>  Events;

        Events   _events;

        bool isActive() const { return !_events.empty(); }

        int trigger(Time due)
        {
            // if t >= any event, trigger it and remove it.
            // return number of events triggered

            int cc = 0;
            while (!_events.empty())
            {
                Event e = _events.front();
                if (e._t <= due)
                {
                    // NB: callback can change events list
                    _events.pop_front();
                    ++cc;
                    (e._fn)(&e);
                }
                else break; // events are in time order
            }
            return cc;
        }

        void add(const Event& ev)
        {
            // add to table in order
            Events::iterator it = _events.begin();
            Events::iterator ie = _events.end();

            while (it != ie)
            {
                const Event& e = *it;
                if (e._t > ev._t)
                {
                    // first entry later than us. insert here
                    break;
                }
                ++it;
            }

            //LOG1("VN adding cycle event ", n->_name << " at " << t);
            //_events.insert(it, Event{t, f, n});
            _events.insert(it, ev);
        }

        void add(const EventFn& f, Time t, Node* n)
        {
            Event ev(n);
            ev._t = t;
            ev._fn = f;
            add(ev);
        }
        
        void clearNode(Node* n)
        {
            // clear all events for this node
            //LOG1("VN clear cycles for ", n->_name);
            Events::iterator it = _events.begin();
            while (it != _events.end())
            {
                const Event& e = *it;
                if (e._node == n) it = _events.erase(it);
                else ++it;
            }
        }
       
    };
    
    struct World
    {
        VNMan*              _host;
        Node*               _root = 0;
        Node*               _view = 0;
        
        bool                _needUpdate = false;
        bool                _needEval = false; // re-eval dependent props
        Time                _now = 0; // current world time
        Time                _endOfTime = 0;
        bool                _endOfTimeBonus = false;

        // list of timed events
        TimeTable           _timeTable;

        // ongoing actions keep the world going
        int                 _loopCount = 0;
        int                 _fidgetCount = 0;

        // global list of resource nodes ordered by z
        DrawList            _drawList;

        // eval context
        eNodeCtx            _ectx;

        // last text area calculated by update world
        // only valid immediately after
        Node*               _textArea = 0;

        // text and picture overlap (set by update world)
        float               _textOverlap = 0;

        Easer               _easer;

        World(VNMan* h) : _host(h) { _init(); }

        ~World() { _purge(); }

        Node* root() const
        {
            // current root
            return _root;
        }

        Node* rootd()
        {
            // get root or make one
            Node* r = root();
            if (!r)
            {
                r = new Node(this);
                r->_name = "root";
                _root = r;
                
                // make the view
                // the view is given as the display area not including the
                // choices and input
                Node* v = new Node(this);
                v->_name = "view";
                _view = v;
                _root->addChild(v);
                
                for (int i = 0; i < 4; ++i)
                {
                    r->_known[i] = true;
                    v->_known[i] = true;
                }
            }
            return r;
        }

        void updateRoot(const Boxf& page, const Boxf& view)
        {
            // update the roots of the world
            
            assert(page);
            assert(view);

            Node* rn = rootd();

            if (rn->_pos != page)
            {
                _needUpdate = true;
                rn->_pos = page;
                rn->_rpos = page;
            }

            if (_view->_pos != view)
            {
                _needUpdate = true;
                _view->_pos = view;
                _view->_rpos = view;
            }
        }

        Node* find(const string& name)
        {
            Node* rn = root();
            return rn ? rn->find(name) : 0;
        }

        bool updateWorldTime(Time now)
        {
            _now = now;
            
            return _needUpdate
                || _loopCount > 0
                || _fidgetCount > 0 
                || _endOfTimeBonus 
                || now <= _endOfTime;
            //|| _timeTable.isActive(); 
        }

        bool updateWorld(Time now)
        {
            //LOG1("VN update world at ", now);
            bool v = updateWorldTime(now);

            // trigger events which might need update
            if (_timeTable.trigger(now)) v = true;
            
            if (v)
            {
                updateWorld();
                _textArea = findTextArea();
            }
            return v;
        }

        float updateTaper()
        {
            // everyone below the text is tapered according to where the
            // text top begins in Y.
            // return max overlap of text and picture
            Node* text = _textArea;

            float overlap = 0;
            
            if (text)
            {
                float ty = text->_rpos[PY1];

                Node* base = 0;
                float ycut;
                float basey2;
                for (Node* ni : _drawList)
                {
                    ni->_taperMin = 1;
                    //ni->_taperColour = ni->_tint;
                    
                    // specify negative text taper to cancel all taper
                    if (text->_taperMin < 0) continue;
                    
                    if (ni == text) break; // only those behind text

                    // we are in front of text
                    float y1 = ni->_rpos[PY1];
                    float y2 = ni->_rpos[PY2];
                    assert(y2 > y1); // clipped items should not be in drawlist

                    // look for first image 
                    if (!base)
                    {
                        // the base image sets the taper point
                        if (!ni->_resource.empty())
                        {
                            base = ni;
                            basey2 = y2;
                            ycut = (y2 - y1)*0.95f + y1;
                            if (ty < ycut) ycut = ty;

                            // amount picture overlaps text if any
                            overlap = u_max(y2 - ty, 0.0f);
                        }
                    }

                    if (base)
                    {
                        // NB: does not matter if > 1
                        
                        // the proportion from the top of this image
                        // where taper should begin
                        float t = (ycut - y1)/(y2 - y1);
                        if (t < 0) t = 0;

                        ni->_taperMin = t;  // can be > 1

                        if (t > 0 && t < 1)
                        {
                            assert(basey2 != ycut);

                            // the proportion of the background fade
                            // at the bottom of this image.
                            t = (y2 - ycut)/(basey2 - ycut);
                            if (t < 0) t = 0;
                            else if (t > 1) t = 1;

#ifdef USETAINT
                            Colour bg = _host->backgroundColour().withoutAlpha();
                            
                            // alpha defines how much we mix the taint
                            ni->_taperColour = bg.withAlpha(t);
#else
                            
                            t = 1.0f - t;
                            if (ni != base)
                            {
                                Colour bg = _host->backgroundColour().withoutAlpha();
                                bool dark = bg.toY() < 128;

                                if (dark)
                                {
                                    ni->_taperColour = Colour(t,t,t);
                                }
                                else
                                {
                                    ni->_taperColour = ni->_tint.fade(t);
                                }
                            }
                            else
                            {
                                ni->_taperColour = ni->_tint.fade(t);
                            }
#endif                                                    
                        }


                        //LOG1("setting taper ", ni->_taperMin << " to " << ni->_taperMaxAlpha << " for " << ni->_name);
                        
                        // max text overlap
                        float ov = y2 - ty;
                        if (ov > overlap) overlap = ov;
                    }
                }
            }
            else
            {
                LOG2("VN Warning update Taper no text box ", 0);
            }
            return overlap;
        }

        void initiateLoading(Node* ni)
        {
            // can have just a background and be in drawlist
            if (ni->_resource.empty()) return;
                        
            bool load = !ni->_videoOneshot || ni->_videoPlays == 0;

            if (load)
            {
                // will initiate loading or if loaded update LRU
                auto r = texLoader->getRec(ni->_resource,
                                           _host->_renderCount,
                                           ni->_mipmap);

                // but we want to set things before loading
                // so find from the requests
                if (!r)
                {
                    r = texLoader->_inRequest(ni->_resource);
                    if (r)
                    {
                        r->_vi._oneshot = ni->_videoOneshot;
                        r->_vi._loop = ni->_videoLoop;
                        r->_vi._startTime = ni->_resourceStartTime;
                    }
                }
            }
        }
        
        void updateLoading()
        {
            // trigger loading in draw order
            // load those visible first
            for (Node* ni : _drawList)
            {
                if (ni->_alpha > 0) initiateLoading(ni);
            }

            // then load invisible but prioritise local visibility
            for (Node* ni : _drawList)
            {
                if (ni->_alpha == 0 && ni->_lalpha.at(_now) > 0)
                    initiateLoading(ni);
            }

            // then load invisible ones
            for (Node* ni : _drawList)
            {
                if (ni->_alpha == 0 && ni->_lalpha.at(_now) == 0)
                    initiateLoading(ni);
            }
        }
        
        void updateWorld()
        {
            // return true if update performed
            
            if (_now <= _endOfTime)
            {
                // force one more update
                _endOfTimeBonus = true;
            }
            else
            {
                _endOfTimeBonus = false;
                _endOfTime = _now;
            }

            // trigger time table first
            //_timeTable.trigger(_now);

            Node* rn = root();
            if (rn)
            {
                int totalNodes = rn->size();
                _drawList.resize(0);
                _drawList.reserve(totalNodes);
                
                // start from the root children
                for (auto ci : rn->_children)
                {
                    if (ci != _view) ci->updateWorld();
                }

                assert(_view);
                for (auto ci : _view->_children) ci->updateWorld();

                updateLoading();
                
                _textOverlap = updateTaper();
                
                _needUpdate = false;  // updated!
                _needEval = false;
                
                LOG4("VN update world ", _now << " " << *this);
                LOG4("VN DrawList:", _drawList);
            }
        }

        void erase(Node* n)
        {
            // this should be the only place we delete a node
            // apart from the world destructor.
            Node* p = n->_parent;

            // should not use this to remove roots
            assert(p);
            p->removeChild(n);

            // this will adjust loopcount and fidget
            // also removes pending callbacks from time table
            delete n;
        }

        bool getTargetParent(const VarList& vs, Node*& np)
        {
            // return false if given a parent that doesn't exist
            bool r = true;
            var v = vs.find(IFI_PARENT);
            if (v)
            {
                r = false;
                Args args(this, v);
                string pname;
                if (args.ifStr(pname, "VN parent"))
                {
                    np = find(pname);
                    if (np)
                    {
                        r = true;

                        // handle parent options in props
                        args.parseOptionsAfter(this);

                        if (args._optOnResource)
                        {
                            // parent is to be on top of first resource
                            Node* tn = np->firstResourceChild(_now);
                            if (tn && tn != np)
                            {
                                //LOG1("VN getparent 'resource' relocating from ", np->_name << " to " << tn->_name);
                                np = tn;
                            }
                        }
                        else if (args._optOnArea)
                        {
                            // parent to be on child that is an area
                            // ie one without resource
                            // or if none then the given node
                            Node* tn = np->firstAreaChild();
                            if (tn && tn != np)
                            {
                                LOG2("VN getparent 'area' relocating from ", np->_name << " to " << tn->_name);
                                np = tn;
                            }
                        }
                    }
                }
            }
            return r;
        }

        bool update(const VarList& vs)
        {
            // updates a node or creates it
            bool res = true;
            
            bool hasResource(vs.find(IFI_RESOURCE));
            bool hasPos(vs.find(IFI_POS));

            Node* pn = 0;
            bool parent = getTargetParent(vs, pn);  // false if given not exist


            // names are allowed to be blank for operator assets
            string name = getvalstr(vs, IFI_NAME);

            bool newnode = false;
        
            Node* n = find(name);  // can be blank
            
#ifdef LOGGING

            bool hasZ(vs.find(IFI_Z));


            // do we look like an operator node?
            // ie a node that simply operates on an existing node
            // and not one to be created
            bool isOperator = !hasPos && !hasZ && !hasResource && parent;

            // a named node must either be new or operating on existing node
            // if new, it must be an area
            //
            if (!n && isOperator && !name.empty())
            {
                //&& name.find('/') == std::string::npos)
                LOG2("VN update WARNING, operator name not found ", name);
            }
#endif
            if (!n)
            {
                // only create named nodes, otherwise operator?
                if (name.empty())
                {
                    // is this an operational node?
                    // one that operates on other nodes but not itself?

                    //LOG3("VN update opertor node ", vs);
                    
                    // NB: has no parent!
                    Node nt(this);  // make a temp node
                    nt._name = "operator"; // XX temp name

                    // perform operation
                    res = nt.update(vs, false);

                    Node* dn = nt._dead;
                    if (dn)
                    {
                        LOG2("VN deleting node ", *dn);

                        // ensure we dont delete automatic!
                        if (dn != &nt) erase(dn);
                    }
                        
                    _needUpdate = true;
                }
                else 
                {
                    // create a new node
                    // NB: name is not empty
                
                    if (!pn)
                    {
                        if (hasResource)
                        {
                            // default to "image" container if not supplied
                            // and we have a resource
                            parent = true;
                            pn = find(IFI_IMAGE);
                        }
                    }
                    
                    if (pn || parent)
                    {
                        // fallback to root if parent empty
                        if (!pn) pn = rootd();

                        LOG3("VN adding new node ", name << " to " << pn->_name);
                        n = new Node(this);
                        n->_name = name; // not empty
                
                        // add as child
                        pn->addChild(n);

                        newnode = true;
                    }
                    else
                    {
                        // cannot operate on this node, ignore
                        LOG2("VN update WARNING parent not found ", parent << " for " << name);
                        res = false;
                    }
                }
            }
            
            if (n)
            {
                res = n->update(vs, newnode);

                // delete signalled?
                // can be this node or another
                if (n->_dead)
                {
                    Node* dn = n->_dead;
                    n->_dead = 0;
                    
                    //LOG1("VN deleting node ", *dn);

                    if (dn == n)
                    {
                        // erase ourself, no more to do
                        n = 0;
                    }

                    // can be node "n" or another
                    erase(dn);
                }

                _needUpdate = true;
            }

            if (n)
            {
                if (n->_lz == 1 && n->_parent != root() && n->_parent != _view)
                {
                    // there can only be one object at Z=1
                    // except at root and view
                    Node* p = n->_parent;
                    assert(p);

                    bool done = false;
                    
                    for (auto ci : p->_children)
                    {
                        if (ci != n && ci->_lz == 1)
                        {
                            erase(ci); // invalidates iterator
                            done = true;
                            break; // can stop
                        }
                    }

                    // if we are refreshing a z=1 node, we also
                    // want to remove the child stack
                    // this is when we re-issue the current background image
                    // or layer
                    //
                    // for this it has to be:
                    // existing node
                    // parent parent == root
                    // a resource OR a position
                    if (!done && !newnode && (hasResource || hasPos)
                        && (p->_parent == root()))
                    {
                        if (!n->_children.empty())
                        {
                            //LOG1("VN removing child nodes for ", n->_name);
                            n->purgeChildren();
                        }
                    }
                }
            }
            return res;
        }

        Node* findTextArea()
        {
            return find("text");
        }

        bool eval(const var& va, float& v, const char* msg = 0)
        {
            bool r = va.isNumber();
            if (r) v = va.toDouble();
            else if (va.isString())
            {
                ST::ParseExpr pe;
                pe._allowSimpleNames = true;
                pe._allowSimpleNamedTerms = true;
                
                const char* s = va.rawString();
                assert(s);

                // XX is this needed?
                if (!u_stricmp(s, "null"))
                {
                    // special case that "null" does not eval
                    // but only null on its own
                    return false;
                }
                
                ST::enode* en = pe.parse(s);
                if (en)
                {
                    var v2 = _ectx.eval(en);
                    r = v2.isNumber();
                    if (r)
                    {
                        v = v2.toDouble();
                        //LOG1("VN eval prop ", s << " = " << v << " from " << v2);
                    }
                    delete en;
                }
                
                if (!r && msg) LOG2("VN eval, WARNING does not eval ", va << ": " << msg);
            }
            return r;
        }

        friend std::ostream& operator<<(std::ostream& os, const World& w)
        {
#ifdef LOGGING            
            const Node* n = w.root();
            os << '{';
            if (n) os << *n;
            os << '}';
            //os << " DrawList:" << w._drawList;
#endif            
            return os;
        }
    
    private:

        var _evalEnodeVariable(eNodeCtx* ctx, const char* name)
        {
            assert(name && *name);

            // eval some known variables
            // return null if unable

            var v; // null

            string vname = name;
            if (vname == "rw")
            {
                v = _host->_appw;
                if (!_host->_appw) LOG2("VN WARNING app size not set ", 0);
            }
            else if (vname == "rh") v = _host->_apph;
            else LOG4("VN Warning evalEnodeVariable unknown ", vname);

            //LOG1("VN evalEnodeVariable ", vname << " = " << v);
            
            return v;
        }

        void _getEnodeArg(enode* e, var& a)
        {
            a = _ectx.eval(e);
            //LOG1("VN getEnodeArg ", a);
        }
        
        bool _getEnodeArgs2(enode* e0, var& a1, var& a2)
        {
            enode* e = e0->_next; // arg1
            if (e)
            {
                _getEnodeArg(e, a1);
                e = e->_next; // arg2
                if (e)
                {
                    _getEnodeArg(e, a2);
                    return true;
                }
            }
            LOG2("VN Warning, expected two args for ", e0->_v);
            return false;
        }

        bool _getEnodeArgsn(enode* e, var* args, int n)
        {
            bool r = true;
            enode* ei = e;
            for (int i = 0; i < n; ++i)
            {
                ei = ei->_next; // next arg
                if (ei)
                {
                    _getEnodeArg(ei, args[i]);
                }
                else
                {
                    LOG2("VN Warning, expected ", n << " args for " << e->_v);
                    r = false;
                    break;
                }
            }
            return r;
        }

        var _evalEnodeFunction(eNodeCtx*, enode* e)
        {
            var v(0);
            assert(e);
            string f = e->_v.rawString();

            //LOG1("VN eval function ", f);

            // eval some known functions
            bool ismax = (f == "max");
            bool ismin = (f == "min");
            if (ismax || ismin)
            {
                var a1, a2;
                if (_getEnodeArgs2(e, a1, a2))
                {
                    if (ismax)
                    {
                        if (a1 >= a2) v = a1.toDouble();
                        else v = a2.toDouble();
                    }
                    else
                    {
                        if (a1 <= a2) v = a1.toDouble();
                        else v = a2.toDouble();
                    }
                    //LOG1("eval node function ", f << ' ' << a1 << ' ' << a2 << " = " << v);
                }
            }
            else if (f == "ifg") // if >= 0
            {
                var args[3];

                if (_getEnodeArgsn(e, args, 3))
                {
                    if (args[0] >= 0) v = args[1].toDouble();
                    else v = args[2].toDouble();

                    //LOG1("VN eval ifg ", args[0] << " " << args[1] << " " << args[2] << " = " << v);
                                        
                }
            }
            else
            {
                LOG2("VN Warning, unknown function ", f);
            }

            return v;
        }

        void _init()
        {
            using namespace std::placeholders;
            
            // init eval context
            _ectx._termFn =
                std::bind(&World::_evalEnodeVariable, this, _1, _2);

            _ectx._fnFn =
                std::bind(&World::_evalEnodeFunction, this, _1, _2);

            _ectx._allowSimpleNameFunctions = true;
                            
        }

        void _purge()
        {
            // will delete all nodes including the view
            delete _root;
            _root = 0;
            _view = 0;
        }
    };

    // the VN manager
    
    bool                _enabled = false;
    World               _world;
    const ImagesMeta*   _imMeta = 0;
    uint64              _renderCount;
    Ranq1               _ran;
    PinchZoom           _pz;
    Time                _renderTime; // last render time

    // these are used to intialise area sizes *before* world update
    int                 _appw = 0;
    int                 _apph = 0;

    // if disabled, no mipmaps regardless
    bool                _enableMipmaps = true;

    // from the window style
    Colour              _backgroundColour; // transparent default

    const Colour&       backgroundColour() const
    {
        assert(_backgroundColour);
        return _backgroundColour;
    }

    void                backgroundColour(const Colour& c)
    { _backgroundColour = c; }

    VNMan() : _world(this)
    {
        _ran.seed(WorldTime::epoch());
    }

    void setImagesMeta(const ImagesMeta* m)
    {
        _imMeta = m;
    }

    void setAppSize(int w, int h)
    {
        if (_appw != w || _apph != h)
        {
            _appw = w;
            _apph = h;

            // next world update will need to re-eval properties that depend
            // on variables
            _world._needEval = true;
            //LOG1("setappsize world need eval!", 0);
        }
    }

    bool updateNode(const VarList& vs)
    {
        // take a bag of properties and locate the note for update
        // or create new node with those properties
        bool r = _world.update(vs);
        if (!r)
        {
            LOG2("VN failed to update node ", vs);
        }
        return r;
    }

    void updateRoot(const Boxf& page, const Boxf& view, uint64 renderCount)
    {
        //LOG1("VN update root ", page << " " << view);
        _renderCount = renderCount;
        _world.updateRoot(page, view);
        ImVec2 p = ImGui::GetCursorScreenPos();
        _viewBasePos.x = p.x;
        _viewBasePos.y = p.y;
    }

    bool updateWorld(double now)
    {
        return _world.updateWorld(now);
    }

    void forceWorldUpdate()
    {
        _world._needUpdate = true;
        _world.updateWorld(_world._now);
    }

    void renderBackground(Node* ni)
    {
        if (ni->hasBackground())
        {
            if (ni->_bgCount == 1)
            {
                // single colour
                Colour c = ni->_bg[0].mixAlpha(ni->_alpha);
                renderRect(ni->_rpos, c);
                        
            }
            else if (ni->_bgCount == 2)
            {
                Colour c1 = ni->_bg[0].mixAlpha(ni->_alpha);
                Colour c2 = ni->_bg[1].mixAlpha(ni->_alpha);
                renderGradient(ni->_rpos, c1, c2);
            }
        }
    }

    bool renderImage(Node* ni)
    {
        // return false if could not render (not loaded)
        // return true if transparent as no render is needed
        const string& name = ni->_resource;

        // we are not a picture node
        if (name.empty()) return true;

        // are we loaded? (initiate load?)
        auto r = texLoader->getRec(name, _renderCount, ni->_mipmap);
        if (r && r->_tex)
        {
#ifdef USEMPG            
            if (r->_stream)
            {
                Time st = r->_stream->_vi._startTime;
                if (st > 0)
                {
                    if (_renderTime < st)
                    {
                        //LOG1("render waiting for video time ", ni->_name << " " << st << " < " << _renderTime);

                        // when waiting treat this as ok, because it's not
                        // yet time to render the video
                        return true;
                    }
                }

                // pump decoder
                texLoader->pollStream(*r);
                
                if (r->_stream->_ended)
                {
                    ++ni->_videoPlays;
                    
                    // clean up buffers and decoder once finished
                    texLoader->destroyStream(r);
                    if (ni->_videoOneshot) texLoader->_purgeRec(r);
                }
                else if (r->_stream->_waitingForFrame)
                {
                    // cant render yet, even if texture present
                    LOG2("VN render image, waiting for frame ", ni->_name);
                    return false;
                }
            }
#endif            
            RenderInfo ri(this);
            ri._box = ni->_rpos;
            ri._tex = &r->_tex;
            ri._uv = ni->_uv;
            ri._overlayMode = ni->_overlayMode;
            ri._overlayFactor = ni->_overlayFactor;
            ri._overlayIntensity = ni->_overlayIntensity;

            // take the cumulative node tint.
            // the default is transparent, so if we still have that
            // change to white.
            Colour t = ni->_tint;
            if (t.alpha() == 0) t = Colour(Colour::white);
            
            // we can be transparent even if alpha > 0 (int rounding)
            // but add in actual alpha
            ri._tint = t.mixAlpha(ni->_alpha);
            
            ri._taperMin = ni->_taperMin;
            ri._taperColour = ni->_taperColour;
            ri._rot = &ni->_rot;

            // can we click?
            // NB: we could still have zero alpha when rounded to int.
            if (renderImageVN(ri) && enablePictureClickables)
            {
                // disable clicks when moving
                if (!ni->_eltClicks.empty() &&
                    !ni->inMotion(_world._now) &&
                    !PinchActive())
                {
                    // collect foremost clickable for this image
                    ni->_lastRenderHit = hitPathVN(ni->_eltClicks, ri, _pz);
                }
            }
        }

        if (!r)
        {
            // not loaded, but is it in request queue?
            auto rr = texLoader->_inRequest(name);

            if (rr && !rr->_firstRenderTime)
            {
                // keep a record of when we first wanted to render this
                // where alpha > 0
                rr->_firstRenderTime = _renderTime;
            }
        }

        //if (!v) LOG1("VN WARNING, image not loaded ", name);
        return r != 0;
    }

    bool mouseForPicture()
    {
        // does the mouse zoom apply to picture.
        // this is true unless it overlaps the text area

        Point2f mp = getAdjustedMousePos();
        bool inView = _world.rootd()->_rpos.contains(mp);
        bool r = inView;
        
        if (inView && _world._textArea)
        {
            bool inText = _world._textArea->_rpos.contains(mp);
            
            //LOG1("mouse ", mp);
            //LOG1("view ", _world.root()->_rpos << " " << inView);
            //LOG1("text ", _world._textArea->_rpos << " " << inText);
            
            if (inText) r = false; // cant zoom when in text
        }
        return r;
    }

    void resetZoomVN()
    {
        resetZoom();
        _pz.clear();
        _pz._lastBasename.clear();
    }

    bool _updateRenderUV(Node* n, Node* base, bool canChange)
    {
        // if canChange we allow zoom to change otherwise just
        // apply current factor
        if (!base)
        {
            if (!n->_resource.empty())
            {
                if (_pz._lastBasename.empty())
                {
                    _pz._lastBasename = n->_resource;
                }
                else
                {
                    if (n->_resource != _pz._lastBasename)
                    {
                        // trying to apply the old zoom to a new picture
                        // cancel the zoom
                        resetZoomVN();
                        return false; // bail
                    }
                }
                
                base = n;

                _pz._orig = n->_pos.centre();
                
                // adjust pz offset to clip the base
                calculatePinchZoom(n->_pos, _pz, canChange);
            }
        }
        
        if (base && _pz)
        {
            //LOG1("VN apply ", _pz);

            // apply PZ to uv's
            n->_rpos = n->_pos;
            _pz.apply(n->_rpos);
            n->updateUV();
        }
        
        for (auto ci : n->_children)
            if (!_updateRenderUV(ci, base, canChange)) return false;
        
        return true;
    }
    
    void updateRenderUV()
    {
        // check zoom applies to picture
        Node* rn = _world.root();
        if (rn)
        {
            bool canChange = mouseForPicture();
            for (auto ci : rn->_children)
                if (!_updateRenderUV(ci, 0, canChange)) break;
        }
    }

    void _resetRenderUV(Node* n)
    {
        n->_rpos = n->_pos;
        n->updateUV();
        for (auto ci : n->_children) _resetRenderUV(ci);
    }
    
    void resetRenderUV()
    {
        Node* n = _world.root();
        if (n)
        {
            for (auto ci : n->_children) _resetRenderUV(ci);
        }
    }

    void render(double now)
    {
        // find the clicks after the first render pass
        // so that only one front object is possible.
        for (Node* ni : _world._drawList) ni->_lastRenderHit.clear();

        // there should be no renderinfo's from last time
        assert(_usedRi.empty());

        _renderTime = now;

        // when we have a PZ zoom, re-calculate rpos and UV's
        updateRenderUV();

        // the drawlist is ordered in global Z order
        for (Node* ni : _world._drawList)
        {
            // nodes in the drawlist might have no alpha
            // they have eiher a background or a picture (or both)
            if (ni->_alpha > 0 && ni->_uv) // skip any clipped images
            {
                renderBackground(ni);

                if (!renderImage(ni))
                {
                    // if we have an image not loaded, do not continue
                    // drawing otherwise we draw the forground without the
                    // background
                    //LOG1("VN WARNING did not render ", ni->_name);
                    break;
                }
                
                //LOG1("render image ", ni->_name << " at " << ni->_rpos << " = " << v);
            }
        }

        bool m = false;

        if (!PinchActive())
        {
            // mouse click?

            //bool m = ImGui::IsMouseReleased(0) && !PinchActive();
            //m = _mouseMonitor._click && !PinchActive();

            // detect a mouse click and release within a time without drag
            auto& io = ImGui::GetIO();
            m = ImGui::IsMouseReleased(0) && io.MouseDownDurationPrev[0] < 0.3f && ImGui::IsMouseDragPastThreshold(0) == false;

            //m = ImGui::IsMouseReleased(0) && io.MouseDownDurationPrev[0] < 3.0f && io.MouseDragMaxDistanceSqr[0] < io.MouseDragThreshold * io.MouseDragThreshold;
        }

        // find any clicks
        Node* clickNode = 0;

        // find last node (most foreground)
        for (Node* ni : _world._drawList)
        {
            // did we render AND do we have clicks?
            // NB: lastrenderhit will not be set if we didn't render
            // eg alpha=0
            if (ni->_lastRenderHit) clickNode = ni;
            else
            {
                // if we are solid alpha, we can mask a previous click
                if (clickNode && ni->_alpha >= 1 && ni->_meta)
                {
                    // meta is original
                    assert(ni->_meta->isOriginal());

                    Point2f p = clickNode->_lastRenderHit._pos;
                    const Boxf& pos = ni->_pos;
                    if (pos.contains(p))
                    {
                        if (ni->_meta->_mask)
                        {
#if 1
                            // translate point into coordinate space
                            // of original image mask.

                            // use the pos box against the original w & h
                            Point2f p2 = p - pos.topleft();

                            float sx = ni->_meta->_w/pos.width();
                            float sy = ni->_meta->_h/pos.height();
                            p2 *= Point2f(sx, sy);

                            // now see if it is contained
                            // Note that the mask is actually quantized
                            // so edges might not be perfect.
                            bool hit = ni->_meta->_mask->contains(p2.x, p2.y);
                            
                            if (hit)
                            {
                                clickNode = 0;
                                //LOG1("non-solid layer ", ni->_name << " hides click");
                            }
#else                            
                            ST::Region r;

                            float sx = pos.width()/ni->_meta->_w;
                            float sy = pos.height()/ni->_meta->_h;

                            LOG3("maskscale ", sx << "," << sy);

                            // need to scale to screenspace
                            ni->_meta->_mask->scaled(r, sx, sy);

                            // move to layout box position
                            r.translate(pos[PX1], pos[PY1]);
                            
                            bool hit = r.contains(p.x, p.y);
                            
                            LOG2("non-solid layer ", ni->_name << " hides click" << " hit:" << hit);
                            //LOG1("pos ", ni->_pos);
                            //LOG1("box ", r.extent());
                            clickNode = 0;
#endif                            
                        }
                        else
                        {
                            // solid
                            //LOG1("solid layer ", ni->_name << " hides click");
                            clickNode = 0;
                        }
                    }
                }
            }
        }
        
        if (clickNode)
        {
            ImageMeta::EltClick* e = clickNode->_lastRenderHit._elt;
            assert(e);
            
            renderPathVN(e, _pz, clickNode->_rpos);
            
            // handle clickable
            if (m)
            {
                // handle the image clickable
                if (linkClickedLabel(e->_label))
                {
                    //LOG1("VN click ", _currentImageClick->_label);
                }
            }
        }
        else if (m)
        {
            resetZoomVN();

            // UB must be reset since these are not otherwise recalculated
            // until world update or zoom
            resetRenderUV();
        }
    }
};

template<>
inline Colour AV<Colour>::interpolate(Colour c1, Colour c2, float a)
{
    return c1.interpolate(c2, a);
}

