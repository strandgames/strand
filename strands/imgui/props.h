//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include "varset.h"
#include "growbuf.h"
#include "logged.h"

#define SPROP(_n, _v) Props::get().set(_n, _v)
#define SPROP_DEFAULT(_n, _v) Props::get().setDefault(_n, _v)

#define PROPI(_n, _d)  Props::get().propi(_n, _d)
#define PROPU(_n, _d)  ((unsigned int)Props::get().propi(_n, _d))
#define PROPB(_n, _d)  Props::get().propb(_n, _d)
#define PROPS(_n, _d)  Props::get().props(_n, _d)

#define PROPDEF(_n)  Props::get().propdefined(_n)

struct Props: public VarSet
{
    typedef std::string string;

    string      _filepath;
    bool        _changed = false;

    bool  propdefined(const string& name) const
    {
        var v = find(name);
        return !v.isVoid();
    }

    int64 propi(const string& name, int64 d) const
    {
        var v = find(name);
        return v ? v.toInt() : d;
    }

    bool propb(const string& name, bool d) const
    {
        var v = find(name);
        return v ? v.isTrue() : d;
    }

    string props(const string& name, const string& d) const
    {
        var v = find(name);
        return v ? v.toString() : d;
    }

    bool fromString(const char* s)
    {
        bool res = true;

        // clear existing settings
        clear();

        const char* e;
        for (;;)
        {
            while (u_isspace(*s)) ++s;
            if (!*s) break; // nothing
            
            // the value is a token up to a space
            e = s;
            while (*e && *e != ' ') ++e;
            if (e == s) break;
            string key(s, e - s);

            // skip spaces
            while (*e == ' ') ++e;
            if (!*e) break;

            // the value is the rest of the line
            s = e;
            while (*e && *e != '\n') ++e;
            if (s == e) break;

            string val(s, e - s);
            const char* sp = val.c_str();
            var v;

            if (v.parse(&sp))
            {
                //LOG1("UI property; ", key << "=" << val);
                add(key, v);
            }
            else
            {
                LOG1("bad UI property; ", key << "=" << val);
                res = false;
            }
            
            s = e;
        }
        
        return res;
    }

    bool fromString(const string& s) { return fromString(s.c_str()); }

    string toString() const
    {
        GrowString gs;
        var::Format fm;
        fm._quoteStrings = true;
        for (const_iterator it = cbegin(); it != cend(); ++it)
        {
            gs.append(it->first);
            gs.add(' ');
            gs.append(it->second.toString(&fm));
            gs.add('\n');
        }
        gs.add(0);
        return (const char*)gs;
    }

    bool set(const char* namec, const var& v)
    {
        string name = namec;
        var ov = find(name);
        bool r = !ov || ov != v;

        if (r)
        {
            //LOG1("Prop ", name << " = " << v);
            (*this)[name] = v;
            _changed = true;
        }
        return r;
    }

    bool setDefault(const char* namec, const var& v)
    {
        // only set value if not already set
        bool r = false;
        string name(namec);
        iterator it = parentT::find(name);
        if (it == end())
            r = add(name, v); // consume v

        return r;
    }

    static Props& get()
    {
        static Props p;
        return p;
    }
    
};

