//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#ifdef VN1 // no longer used

#include <array>
#include "earcut.h"
#include "region.h"
#include "rect.h"
#include "svgpath.h"

// The number type to use for tessellation
using Coord = float;

// The index type. Defaults to uint32_t, but you can also pass uint16_t if you know that your
// data won't have more than 65536 vertices.
using N = unsigned int;

// Create array
using Point = std::array<Coord, 2>;
typedef std::vector<Point> Points;
typedef std::vector<Points> Polygon;

// Fill polygon structure with actual data. Any winding order works.
// The first polyline defines the main polygon.
//polygon.push_back({{100, 0}, {100, 100}, {0, 100}, {0, 0}});
// Following polylines define holes.
//polygon.push_back({{75, 25}, {75, 75}, {25, 75}, {25, 25}});

// Run tessellation
// Returns array of indices that refer to the vertices of the input polygon.
// e.g: the index 6 would refer to {25, 75} in this example.
// Three subsequent indices form a triangle. Output triangles are clockwise.
//std::vector<N> indices = mapbox::earcut<N>(polygon);

static void makeRegionFromPoly(ST::Region& r,
                               const Poly& poly,
                               int w, int h)
{
    int sz = poly._points.size();
    if (sz < 3) return;

    // handle cases of a single triangle or quad
    if (sz <= 4)
    {
        StrandCommon::Rect rr(poly._points[0].x, poly._points[0].y);
        for (int i = 1; i < sz; ++i)
            rr.add(ROUND(poly._points[i].x*w),
                   ROUND(poly._points[i].y*h));

        r += MK_RRECT(rr);
    }
    else
    {
        // add polygon points, no holes
        Polygon pg;
        Points pts;
        for (const Point2f& pi : poly._points)
            pts.emplace_back(Point{pi.x, pi.y});
        pg.push_back(pts);

        // triangulate
        std::vector<N> idx = mapbox::earcut<N>(pg);

        // collect triangles
        unsigned int i = 0;
        while (i <= idx.size() - 3)
        {
            Point2f t[3];

            // exact each triangle from original points
            for (int j = 0; j < 3; ++j)
            {
                N k = idx[i];
                assert(k < pts.size());
                const Point& pi = pts[k];
                t[j].x = pi[0];
                t[j].y = pi[1];
                ++i;
            }

            //LOG1("triangle ", t[0] << ' ' << t[1] << ' ' << t[2]);

            // find bounding box of triangle and add to region
            StrandCommon::Rect rr(ROUND(t[0].x*w), ROUND(t[0].y*h));
            rr.add(ROUND(t[1].x*w), ROUND(t[1].y*h));
            rr.add(ROUND(t[2].x*w), ROUND(t[2].y*h));
            r += MK_RRECT(rr);
        }
    }
}

void makeRegionFromPolys(ST::Region& r,
                         const Polys& polys,
                         int w, int h)
{
    for (auto& pi : polys._polys)
    {
        // add to same region
        makeRegionFromPoly(r, pi, w, h);
    }
}

#endif
