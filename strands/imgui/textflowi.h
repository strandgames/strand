//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

//define implementation dependent types

#pragma once

#include <string>
#include "rect.h"
#include "region.h"
#include "colour.h"


// forward
struct TFont;

typedef Colour TColour;

struct TextProperties
{
    TFont*              _font = 0;
    
    TColour             _col;
    TColour             _hiCol;
    
    TColour             _disCol;
    TColour             _disHiCol;
    
    bool                _underline = false;
    bool                _canhover = false;

    bool                _hilite = false;
    bool                _disabled = false;
    bool                _scroll = false;

    friend std::ostream& operator<<(std::ostream& os, const TextProperties& p)
    {
        return os << "hilite:" << p._hilite << " disabled:" << p._disabled;
    }
    
};

enum BoxAlign
{
    boxalign_none = 0,
    boxalign_left,
    boxalign_middle,
    boxalign_right,
    boxalign_top,
    boxalign_bottom,
};

struct TextFlowInfo
{
    int         _objBorderSize;
    Colour      _objBorderCol{Colour::white};
    Colour      _objBorderColHi{Colour::white};

    //  x = left, y = top
    //  w = right, h = bot
    StrandCommon::Rect        _objPadBox;

    bool        _fillJustify = false;
    int         _squeezeWords = 0;
    int         _squeezeLines = 0;
    bool        _enableFullBlanks = false; // full height blanks
    int         _pictureSizePercent = P_PICTURE_SIZE_DEF;
};

struct TFont
{
    virtual ~TFont() {}
    
    virtual float getHeight() const = 0;  // lineheight
    virtual float getAscent() const = 0;
    virtual float getDescent() const = 0;
    virtual int compare(const TFont*) const = 0;

    virtual void setHeight(float f) = 0;


    bool operator==(const TFont& f) const
    {
        return !compare(&f);
    }
    
    virtual float getStringWidth(const std::string& s) const = 0;
    
    virtual void render(const std::string& s,
                        const StrandCommon::Rect& box,
                        const TextProperties& tp) const = 0;

    virtual bool clipped(const StrandCommon::Rect& box) const = 0;
    virtual void renderBackground(const ST::Region&) const = 0;

    float getSpaceWidth()
    {
        if (!_spaceWidth)
        {
            _spaceWidth = getStringWidth(" ");
            //LOG1("font space width ", _spaceWidth);
        }

        return _spaceWidth;
    }

    float               _spaceWidth = 0;
};

// keep track of people in conversation
struct Person
{
    std::string         _id;
    std::string         _name;
};

typedef std::vector<Person> Roster;




