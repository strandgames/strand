//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include "colour.h"

inline Colour ImVec4ToColour(const ImVec4& c)
{
    return Colour(c.x, c.y, c.z, c.w);
}

inline ImVec4 ColourToImVec4(Colour c)
{
    return ImVec4(c.redf(), c.greenf(), c.bluef(), c.alphaf());
}

inline Colour StyleColour(int ix)
{
    return ImVec4ToColour(ImGui::GetStyleColorVec4(ix));
}


inline ImU32 ColourToU32(Colour c)
{
    return c.toABGR();
    //ImVec4 v = ColourToImVec4(c);
    //return ImGui::ColorConvertFloat4ToU32(v);
}


