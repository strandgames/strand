//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#ifdef USEMPG

#include "cutils.h"
#include "utils.h"
#include "ti.h"

// forward
struct ImTex;

struct plm_t;
struct plm_buffer_t;
struct plm_frame_t;
struct plm_plane_t;


struct VideoInfo
{
    typedef std::string  string;
    string      _name;
    ImTex*      _tex = 0;
    bool        _loop = false;

    // will delete source data while playing
    bool        _oneshot = false;
    float       _startTime = 0; // if defined

    friend std::ostream& operator<<(std::ostream& os, const VideoInfo& vi)
    { return os << vi._name << " loop:" << vi._loop; }
};

struct VideoState
{
    typedef std::string  string;

    // decoder input buffer size
    static const int mpegBufSize = 1024*1024;
    
    struct Buf
    {
        unsigned char*  _data;
        int             _size;
        Buf*            _next = 0;

        Buf(unsigned char* d, int sz) : _data(d),_size(sz) {}
        ~Buf() { delete _data; }
    };

    struct Stream
    {
        VideoState* _host;
        VideoInfo _vi;
        Buf*    _data;
        Buf**   _dlast;
        uint    _size = 0; // total data size

        // input data position
        uint    _dpos;  // current input buffer pos
        Buf*    _dbuf = 0;  // current input buffer

        // decoder info
        plm_t* plm = 0;
        plm_buffer_t* plm_buffer = 0;

        bool    _started = false;  // kicked off loading
        bool   _loaded = false; // all data loaded
        bool   _ended = false; // playback ended
        bool   _stalled = false; // ran out of data while playing
        bool   _waitingForFrame = false; // decoding but no picture yet,

        TI         _timer;
        double     _playBeginTime = 0;

        Stream(VideoState& host, const VideoInfo& vi) : _host(&host), _vi(vi)
        {
            _data = 0;
            _dlast = &_data;

            // disable oneshot if loop requested
            if (_vi._loop) _vi._oneshot = false;
        }
        
        ~Stream() { _purge(); }

        void add(unsigned char* data, int sz)
        {
            // data is consumed
            if (sz)
            {
                Buf* b = new Buf(data, sz);
                *_dlast = b;
                _dlast = &b->_next;
                _size += sz;
            }
            else delete data; // rump end
        }

        void initDecoder();
        void plmpeg_load_callback(plm_buffer_t* self);
        void video_cb(plm_t* mpeg, plm_frame_t* frame);
        void updateTexture(int idx, plm_plane_t* plane);

        void pollDecode();

        void _poll()
        {
            /* this is called before output images created.
             * called initially from imtexloader while waiting for enough
             * data. Loader lock is ASSUMED for this call.
             *
             * also called from render via imtexloader pollStream
             * which has taken the load lock
             */

            // ASSUME LOADER LOCK
            if (!_dbuf && _data)
            {
                // data has arrived
                resetPos();
            }

            if (_dbuf) _stalled = false;
            
            if (!plm && _dbuf)
            {
                LOG3("stream initiating decode ", _vi << " " << _dbuf->_size);
                initDecoder();
            }

            pollDecode();
        }

        void resetPos()
        {
            _dbuf = _data;
            _dpos = 0;            
        }

        void rewind();

    protected:

        void _beginTimer()
        {
            _timer.stop();
            _playBeginTime = _timer.duration();
        }

        void _purgeData()
        {
            // delete all buffers
            Buf* b;
            while ((b = _data) != 0)
            {
                _data = b->_next;
                delete b;
            }

            _dlast = &_data;
            _dbuf = 0;
        }

        void _purgeDecoder();
        
        void _purge()
        {
            // destroy decoder and any buffers
            _purgeDecoder();
            _purgeData();
        }
        
        static void _plmpeg_load_callback(plm_buffer_t* self, void* user)
        {
            auto s = (Stream*)user;
            assert(s);
            s->plmpeg_load_callback(self);
        }

        static void _video_cb(plm_t* mpeg, plm_frame_t* frame, void* user)
        {
            auto s = (Stream*)user;
            assert(s);
            s->video_cb(mpeg, frame);
        }
       
    };

    uint64                         _renderCount = 0;

};

#endif


