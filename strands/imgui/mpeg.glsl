@ctype mat4 hmm_mat4

@vs vs
uniform vs_params {
   vec2 disp_size;
};
in vec2 pos;
in vec2 texcoord;
in vec4 color0;
in vec4 tcol0;  // taint col

out vec2 uv;
out vec4 color;
out vec4 tcol;

void main() {
    gl_Position = vec4(((pos/disp_size)-0.5)*vec2(2.0,-2.0), 0.5, 1.0);
    uv = texcoord;
    color = color0;
    tcol = tcol0;
}
@end

// movie render options
// mode 0 => normal
// mode 1 => blue screen
// mode 2 => black is transparent

@fs fs
uniform fs_params {
    int mode;
    float factor;
    float intensity;
};

uniform texture2D tex_y;
uniform texture2D tex_cb;
uniform texture2D tex_cr;
uniform sampler smp;

in vec2 uv;
in vec4 color;
in vec4 tcol;

out vec4 frag_color;

mat4 rec601 = mat4(
    1.16438f,  0.00000f,  1.59603f, -0.87079f,
    1.16438f, -0.39176f, -0.81297f,  0.52959f,
    1.16438f,  2.01723f,  0.00000f, -1.08139f,
    0.0f, 0.0f, 0.0f, 1.0f
);

void main() {
    float y = texture(sampler2D(tex_y, smp), uv).r;
    float cb = texture(sampler2D(tex_cb, smp), uv).r;
    float cr = texture(sampler2D(tex_cr, smp), uv).r;
    vec4 c = vec4(y, cb, cr, 1.0f) * rec601;

    /*
    if (mode == 1)
    {
        // blue screen
        if (frag_color.b > 0.6f && frag_color.r < 0.3f && frag_color.g < 0.3f)
        {
            frag_color.a = 0.0f;
        }
    }
    */
    
    if (mode == 2)
    {
       // make black transparent
       if (y < factor)
       {
           if (intensity > 1)
           {
                y = 1.0f - y;
                y = 1.0f - (y*y*y);
           }
           else if (intensity > 0)
           {
                y = 1.0f - y;
                y = 1.0f - (y*y);
           }
           c.a = y;
       }
   }

   frag_color = vec4(mix(c.xyz, tcol.xyz, tcol.w), c.a) * color;   
}
@end

@program mpeg vs fs

