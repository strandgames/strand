//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <set>
#include <list>
#include "logged.h"
#include "pbase.h"
#include "pexpr.h"
#include "porter.h"
#include "keywords.h"
#include "plural.h"

#define PRON_IT         "it"
#define PRON_HERE       "here"
#define PRON_HIM        "him"
#define PRON_HER        "her"
#define PRON_ALL        "all"
#define PRON_THAT       "that"

#define ART_A           "a"
#define ART_AN          "an"
#define ART_ANY         "any"
#define ART_SOME        "some"
#define ART_THIS        "this"
#define ART_THE         "the"
#define ART_MY          "my"
#define ART_YOUR        "your"
#define ART_THAT        "yon"

namespace ST
{

struct Word: public Traits
{
    enum POS
        {
            pos_void = 0,
            pos_verb = 1,
            pos_adj = 2,
            pos_prep = 4,
            pos_noun = 8,
            pos_adverb = 16,
            pos_conj = 32,
            pos_article = 64,
            pos_pronoun = 128,
            pos_ender = 256,
            pos_except = 512,
            pos_prepmod = pos_except*2, // "also" and "not".
            pos_property = pos_prepmod*2, // bastardised property grammar
            pos_value = pos_property*2, // holder for enode value expression
            pos_query = pos_value*2,
            pos_prep_rel = pos_query*2, // preposition relativiser
            pos_ID = pos_prep_rel*2, // is a term ID of an object
            pos_FLOW = pos_ID*2, // is a term ID of a flow
            pos_doverb = pos_FLOW*2, // require direct object
            pos_ioverb = pos_doverb*2, // verb requiring indirect object
            pos_canio = pos_ioverb*2, // IO lifting but not req IO
            pos_negate = pos_canio*2, // not

            // bi-transitive dative alternation allowed
            pos_dativealternation = pos_negate*2, // give mary the key
        };

    static constexpr const char* posNames[] =
        {
            "void",
            "verb",
            "adjective",
            "preposition",
            "noun",
            "adverb",
            "conjunction",
            "article",
            "pronoun",
            "ender",
            "except",
            "prepmod", // also/not
            "property",
            "value",
            "query",
            "preprel",
            "ID",
            "flow",
            "doverb",
            "ioverb",
            "canio",
            "negate",
            "alternation",
        };

    // these are additional properties
    static const uint pos_mask = ~(pos_doverb | pos_ioverb | pos_ID | pos_FLOW | pos_prep_rel | pos_dativealternation);

    static const char* posName(uint pos)
    {
        // these aren't part of the name
        pos &= pos_mask;
        
        uint i = 0;
        while (pos) { ++i; pos >>= 1; } // assume values are powers of 2
        assert(i < ASIZE(posNames));
        return posNames[i];
    }

    static unsigned int posOfName(const char* name)
    {
        unsigned int r = 0; // not found
        unsigned int pos = 1; // pos_verb
        for (int i = 1; i < ASIZE(posNames); ++i) // skip "void"
        {
            if (!compareIgnoreCase(name, posNames[i]))
            {
                r = pos;
                break;
            }
            pos <<= 1; // assume values are powers of 2
        }
        return r;
    }

    static int posOfName(const string& s) { return posOfName(s.c_str()); }
    
    enum scopeType
    {
        scope_void = 0, // none
        scope_any_one,
        scope_the_one,
        scope_here,  // 3
        scope_have,  // my
        scope_here_nothave,
    };

    string          _text;
    uint            _pos = 0;

    bool isVerb() const { return (_pos & pos_verb) != 0; }
    bool isNoun() const { return (_pos & pos_noun) != 0; }
    bool isArticle() const { return (_pos & pos_article) != 0; }
    bool isIOVerb() const { return (_pos & pos_ioverb) != 0; }
    bool isIOLift() const { return (_pos & (pos_canio | pos_ioverb)) != 0; }
    bool isDOVerb() const { return (_pos & pos_doverb) != 0; }
    bool isAlternationVerb() const
    { return (_pos & pos_dativealternation) != 0; }
    bool isID() const { return (_pos & pos_ID) != 0; }
    bool isFlow() const { return (_pos & pos_FLOW) != 0; }
    bool isQuery() const { return (_pos & pos_query) != 0; }
    bool isPrep() const { return (_pos & pos_prep) != 0; }
    bool isCanio() const { return (_pos & pos_canio) != 0; }

    // is a relativising preposition
    bool isPrepRel() const { return (_pos & pos_prep_rel) != 0; }
    bool isNegative() const { return (_pos & pos_negate) != 0; }

    bool isISARE() const
    {
        // "is" or "are"
        // NB: for now "is" and "are" are the only canio queries
        return isCanio() && isQuery();
    }

    void setIOVerb() { _pos |= pos_ioverb; }  // from templates
    void setCanIOVerb() { _pos |= pos_canio; }  // from templates
        
    Word(const string& t) : _text(t) {}

    scopeType scopeOf() const
    {
        scopeType st = scope_void;
        
        // words can indicate scope
        if (isArticle())
        {
            if (_text == ART_A || _text == ART_AN || _text == ART_ANY || _text == ART_SOME) st = scope_any_one;
            else if (_text == ART_THE) st = scope_the_one;
            else if (_text == ART_THIS) st = scope_here;
            else if (_text == ART_MY || _text == ART_YOUR) st = scope_have;
            else if (_text == ART_THAT) st = scope_here_nothave; 
        }
        return st;
    }

    int compare(const Word& w) const
    {
        // words compare by their string name
        // some words are TERMS
        
        bool t1 = !hasLower(_text);
        bool t2 = !hasLower(w._text);

        if (t1)
        {
            if (t2)
            {
                // straight compare
                return _text.compare(w._text);
            }
            return -1; // term < non-term
        }
        else
        {
            if (t2) return 1; // non term > term
            return compareIgnoreCase(_text, w._text);
        }
    }
    
    bool operator==(const Word& w) const { return compare(w) == 0; }
    bool operator==(const char* w) const
    {
        // compare to literal must be exact
        return _text == w;
    }
    
    bool operator<(const Word& w) const { return compare(w) < 0; }

    friend std::ostream& operator<<(std::ostream& os, const Word& w)
    { return os << w._text; }
};

struct Binding
{
    typedef std::list<Term*> BindList;
    typedef Word::scopeType scopeType;

    Binding() {}
    Binding(BindList& tl)
    {
        // consume terms of tl
        _terms.splice(_terms.begin(), tl);
    }

    void merge(Binding& b)
    {
        for (auto t : b._terms)
            if (!contains(_terms, t)) _terms.push_back(t);

        // copy over any scope flags if not already
        if (!_scope) _scope = b._scope;
    }

    Term* first()
    {
        assert(!_terms.empty());
        return _terms.front();
    }

    void add(Term* t)
    {
        _terms.push_back(t);
    }

    bool empty() const { return _terms.empty(); }
    int size() const { return _terms.size(); }
    void clear() { _terms.clear(); }

    BindList    _terms;
    scopeType   _scope = Word::scope_void;
};

struct pnode: public node<pnode>
{
    enum nodeType
    {
        p_void = 0,
        p_adjs,
        p_article,
        p_noun, //3
        p_anoun,
        p_unoun,
        p_nounlist,
        p_tnoun,
        p_dnoun,
        p_rnoun,
        p_rnounx,
        p_nouns,
        p_pronoun, // 12
        p_prep,
        p_prepmod,
        p_adverbs,
        p_verb,  //16
        p_averb,
        p_prepverb,
        p_cs, //19 command sentence
        p_property,
        p_value,
        p_query, //22
        p_qs, // 23 query sentence
        p_count,
    };

    
    const Word*     _word = 0;
    Binding*        _binding = 0;

    pnode(const Word* wt, int t = 0) : node(t), _word(wt) {}
    pnode(pnode* l, int t) : node(l, t) {}
    
    ~pnode() override { _purge(); }

    bool isNounPhrase() const { return _type >= p_noun && _type <= p_pronoun; }
    bool valid() const { return _type < p_count; }
    bool isSentence() const { return _type == p_cs; }
    bool isQuery() const { return _type == p_qs; }

    // converting to string uses a helper which optionally can have
    // a hook function to resolve nodes externally.
    struct SHelper
    {
        typedef std::function<bool(SHelper&)> SHook;
        
        string           s;
        SHook           _hook;
        const pnode*    _current;

        // used when pnode is without binding to provide one
        Term*           _binding = 0;
        bool            _expandTerms = false;
    };

    void _toString(SHelper& h) const { _toString(h, _next); }
    
    void _toString(SHelper& h, pnode* next) const
    {
        //printf("tostring [%d]\n", _type);

        bool done = false;
        h._current = this;

        if (h._hook)
        {
            // if a hook is provided, try that first
            done = (h._hook)(h);
        }

        if (!done)
        {
            if (_word)
            {
                assert(!_head);
                if (h.s.size()) h.s += ' ';
                h.s += _word->_text;
            }
            else
            {
                assert(_head);
                switch (_type)
                {
                case p_anoun:
                    assert(_head->_next);
                    _head->_next->_toString(h, 0); // only one adjective
                    _head->_toString(h, 0);
                    return;
                case p_tnoun:
                    assert(_head->_next);  // article
                    _head->_next->_toString(h); // article
                    _head->_toString(h, 0);
                    //return;
                    break;
                case p_value:
                    // head is not a pnode!
                    {
                        GETENODE(this);
                        h.s += en->toString();
                    }
                    break;
                default:
                    _head->_toString(h);
                }
            }
        }

        if (next) next->_toString(h);
    }

    string toString(SHelper* h = 0) const
    {
        SHelper h1;
        if (!h) h = &h1;
        _toString(*h);
        return h->s;
    }

    friend std::ostream& operator<<(std::ostream& os, const pnode& pn)
    { return os << pn.toString(); }

    string toStringStruct() const
    {
        // version that shows structure.

        if (_type == p_value)
        {
            GETENODE(this);
            return en->toString();
        }

        string s;

        if (_type == p_cs) s+="sentence:";
        else if (_type == p_qs) s += "query:";

        if (_word)
        {
            assert(!_head);
            s = _word->_text;
        }
        else
        {
            assert(_head);
            if (_head->_next) s += '(';
            s += _head->toStringStruct();
            if (_head->_next) s += ')';
        }

        if (_next)
        {
            s += ' ';
            s += _next->toStringStruct();
        }
        
        return s;
    }

    static pnode* makeValueNode(enode* e)
    {
        pnode* p = 0;

        if (e)
        {
            // LIES!
            // this is not a pnode at all. but they share a
            // common `node` baseclass whose types do not overlap
            p = new pnode((pnode*)e, nodeType::p_value);
        }
        return p;
    }

    pnode* copy() const
    {
        // deep copy!
        // but not the bindings
        assert(valid());

        pnode* pn;

        if (_type == p_value)
        {
            // copying a value?
            //LOG1("Warning, copying a value in ", toString());

            GETENODE(this);
            assert(en);
            pn = makeValueNode(en->copy());
            //LOG1("Value copy got ", pn->toString());
        }
        else
        {
            // either word or head, but not both
            assert((_word != 0) ^ (_head != 0));
            
            if (_word)
            {
                pn = new pnode(_word, _type);
            }
            else
            {
                pn = new pnode(_head->copy(), _type);
            }
        }
        
        if (_next)
        {
            pn->_next = _next->copy();
        }
        
        return pn;
    }

    pnode* getBindingNode()
    {
        // find binding
        pnode* pn = this;

        while (pn)
        {
            assert(pn->valid());
            if (pn->_binding) return pn;
            pn = pn->_head;
        }
        return 0;
    }

    Binding* getBinding()
    {
        // find binding
        pnode* pn = getBindingNode();
        return pn ? pn->_binding : 0;
    }
    
protected:
    
    void _purge()
    {
        delete _binding; _binding = 0;

        if (_type == p_value)
        {
            GETENODE(this);
            delete en;
        }
        else
        {
            delete _head;
        }
        _head = 0;
        delete _next; _next = 0;
    }

};

struct ParseCommand: public ParseBase
{
    typedef std::set<Word> Dictionary;
    typedef pnode::nodeType nodeType;
    
    Dictionary          _dictionary;
    const Word*         _it;
    const Word*         _that;

    // definition mode, will process ambiguous structures simpler.
    bool                _defMode = false;

    // parse unoun left recursive
    // eg give rowena the witch the map
    // to have; give (rowena the witch) the map
    // not give rowena the (witch the map)
    // this is set when we start a unoun and want to r-recurse the rest
    bool                _leftUNoun = false;

    struct ParseCommandCtx
    {
        ParseCommandCtx(const string& v) : _autoVerb(v) {}
        string  _autoVerb;
    };

    ParseCommandCtx*    _pctx = 0;

    void setParseCommandCtx(ParseCommandCtx* c)
    {
        // NB: we don't own the context
        _pctx = c;
    }

    const Word* findWord(const string& word) const
    {
        // dictionary words are case sensitive.
        // in the sense that uppercase words are TERM names
        // however, unless they are all uppercase, they should match
        // without case. this is done in Word.
        
        assert(word.size());
        auto it = _dictionary.find(Word(word));
        return it == _dictionary.cend() ? 0 : &(*it);
    }

    Word& internWord(const string& word)
    {
        assert(word.size());
        return const_cast<Word&>(*_dictionary.insert(Word(word)).first);
    }

    Word& internWordType(const string& word, uint pos)
    {
        Word& w = internWord(word);
        w._pos |= pos;
        return w;
    }

    const Word* wordType(const string& word, Word::POS pos) const
    {
        const Word* w = 0;
        if (!word.empty())
        {
            w = findWord(word);

            if (!w && pos == Word::pos_noun)
            {
                // if no match, try turning plural
                string pl = pluralOf(word);
                if (pl != word)
                {
                    //LOG1("trying noun plural ", pl);
                    w = findWord(pl);
                }
            }
            
            // if a POS given, make sure it suits
            if (w && (w->_pos & pos) == 0) w = 0;
        }
        return w;
    }

    struct StdWord
    {
        const char*  _word;
        uint         _pos;
    };

    void internStandardWords()
    {
        static const StdWord stdWords[] =
        {
            { "and", Word::pos_conj },
            { ",", Word::pos_conj },
            { "&", Word::pos_conj },
            
            { ".", Word::pos_ender },
            { "except", Word::pos_except },
            { "but", Word::pos_except },

            { ART_THE, Word::pos_article },
            { ART_A, Word::pos_article },
            { ART_AN, Word::pos_article },
            { ART_SOME, Word::pos_article },
            { ART_ANY, Word::pos_article },

            // not really articles, but works as such
            { ART_THIS, Word::pos_article },
            { ART_THAT, Word::pos_article },
            { ART_MY, Word::pos_article },
            { ART_YOUR, Word::pos_article },

            { PROP_IN, Word::pos_prep | Word::pos_prep_rel },
            { PROP_INTO, Word::pos_prep },
            { PROP_TO, Word::pos_prep },
            { PROP_ON, Word::pos_prep | Word::pos_prep_rel },
            { PROP_ONTO, Word::pos_prep },
            { "off", Word::pos_prep },  // XX?
            //{ "under", Word::pos_prep | Word::pos_prep_rel },
            //{ "behind", Word::pos_prep | Word::pos_prep_rel },
            { "about", Word::pos_prep },
            { "at", Word::pos_prep },
            { "for", Word::pos_prep },
            { PROP_WITH, Word::pos_prep },
            { "through", Word::pos_prep },
            { "out", Word::pos_prep },
            { "from", Word::pos_prep },
            { "inside", Word::pos_prep },

            { PRON_IT, Word::pos_pronoun },
            { PRON_THAT, Word::pos_pronoun },
            { PRON_HIM, Word::pos_pronoun },
            { PRON_HER, Word::pos_pronoun },
            { PRON_HERE, Word::pos_pronoun },
            { "there", Word::pos_pronoun },
            { "them", Word::pos_pronoun },
            { PRON_ALL, Word::pos_pronoun },

            { "put", Word::pos_verb | Word::pos_doverb | Word::pos_ioverb },
            { "set", Word::pos_verb | Word::pos_doverb },

            // not marked as verbs as these are treated as queries
            { "is", Word::pos_query | Word::pos_canio },
            { "are", Word::pos_query | Word::pos_canio },

            { "what", Word::pos_query },
            { "where", Word::pos_query },
            
            { "not", Word::pos_prepmod | Word::pos_negate },
            { "also", Word::pos_prepmod },

            { PREPMOD_REALLY, Word::pos_prepmod },
        };

        for (int i = 0; i < ASIZE(stdWords); ++i)
        {
            const StdWord& w = stdWords[i];
            internWordType(w._word, w._pos);
        }

        // locate various pronouns so we can test directly
        _it = findWord("it");
        _that = findWord("that");
    }

    string word()
    {
        // get next word
        string s;
        
        skipws();
        PUSHSPAN;  // does not need corresponding pop

        // words can be a number as well.
        // allow words to start with underscore for debug commands
        uint c = AT;
        bool ok = Utf8::_isalnum(c) || c == '_';

        if (ok)
        {
            BUMP; c = AT;
            // allow hypens and underscore (for embedded terms)
            while (Utf8::_isalnum(c) || c == '-' || c == '_')
            {
                BUMP; c = AT;
            }
            s = POPSPAN;
        }
        else
        {
            // punctuation characters appear as individual words
            GETCS(s);
        }
        return s;
    }

    string wordadj()
    {
        // get next word, expected to be an adjective
        // here we allow apostrophe in the words
        
        string s;
        
        skipws();
        PUSHSPAN;  // does not need corresponding pop

        // expect start with alpha
        // allow words to start with underscore for debug commands
        uint c = AT;
        bool ok = Utf8::_isalpha(c) || c == '_';

        if (ok)
        {
            BUMP; c = AT;
            // allow hypens and apostrophe and underscore

            while (Utf8::_isalnum(c) || c == '-' || c == '_'
                   || Utf8::isApostrophe(c))
            {
                BUMP; c = AT;
            }
            
            s = POPSPAN;
        }
        else
        {
            // punctuation characters appear as individual words
            GETCS(s);
        }

        return s;
    }

    pnode* parseType(const string& w, Word::POS pos, uint t = 0) const
    {
        pnode* pn = 0;
        if (w.size())
        {
            const Word* wt = wordType(w, pos);

            if (wt)
            {
                LOG5("parsing '", w << "' as " << Word::posName(pos));
            }
            
            if (wt) pn = new pnode(wt, t);
        }
        return pn;
    }

    pnode* _parseAndList(Word::POS ps, uint type)
    {
        // a and a and a ...
        // (a  a  a ...)
        pnode* pl;
        
        _push();
        pl = parseType(wordadj(), ps, type);
        if (pl)
        {
            pnode* plast = pl;
            _drop();

            for (;;)
            {
                _push();
                string w = wordadj();
                if (wordType(w, Word::pos_conj)) w = wordadj(); // skip "and"
                pnode* p2 = parseType(w, ps, type);
                if (p2)
                {
                    plast->_next = p2;
                    plast = p2;
                    _drop();
                }
                else
                {
                    _pop(); 
                    break;
                }
            }
        }
        else _pop();
        return pl;
    }

    pnode* parseAdjlist()
    {
        // (#adjs a  a  a ...)
        // eg (big old fat)
        return _parseAndList(Word::pos_adj, nodeType::p_adjs);
    }

    pnode* parseANoun()
    {
        // parse a single noun or noun with adjectives
        
        // #pnoun
        // eg cat
        
        // #anoun = (n a ...)
        // eg (cat big old fat)
        
        _push();
        pnode* pl = parseAdjlist();

        _push();
        pnode* pn = parseType(word(), Word::pos_noun, nodeType::p_noun);

        if (pn)
        {
            _drop(); // accept last word as noun
        }
        else
        {
            // did not get a noun
            
            _pop();  // word that failed to be a noun
            
            // can last adjective also be a noun?
            if (pl)
            {
                UNUSED int ac = 1;

                // walk to the last adj
                pnode** ap = &pl;
                while ((*ap)->_next)
                {
                    ap = &(*ap)->_next;
                    ++ac;
                }
                
                assert((*ap)->_word);

                // is it also a noun?
                if ((*ap)->_word->isNoun())
                {
                    // yes. promote it

                    pn = *ap; // noun!
                    pn->_type = nodeType::p_noun; // promote type
                    
                    *ap = 0; // remove from adj list

                    // if we only had one adj, pl should now be 0
                    assert(ac != 1 || !pl);
                }
            }
        }

        if (pn)
        {
            if (pl)
            {
                pn->_next = pl;
                pn = new pnode(pn, nodeType::p_anoun);
            }
            _drop();
        }
        else
        {
            delete pl;
            _pop();
        }
        return pn;
    }

    pnode* parseUNoun()
    {
        // parse attributive nouns or possessive syntax
        // eg "cat fur" where two (or more) nouns are put together
        // this means "fur of cat"
        // also "cat's fur", which is also "fur of cat"
        //
        // the last noun is the main noun (eg fur), so the ordering
        // has to be reversed.
        //
        // except if "of" actually used, keep same order
        
        // #pnoun
        // #anoun
        //
        // #unoun = (#anoun #pnoun #anoun ...)
        // ordered so first is the significant 

        pnode* pl = parseANoun();
        if (pl)
        {
            pnode* plast = pl;
            for (;;)
            {
                bool of = false;
                bool possesive = false;  // use of apostrophe
                bool ok = true;
                
                _push();

                // allow ' or 's
                if (Utf8::isApostrophe(AT))
                {
                    possesive = true;
                    BUMP;
                    if (AT == 's') BUMP;
                    else if (AT != ' ') ok = false; // must be 's or nothing
                }
                else
                {
                    // skip "of" word
                    of = parseLiteral("of");
                }

                if (_leftUNoun)
                {
                    // only allow "of" or possessives to be right recursive
                    // otherwise we do not parse a unoun here.
                    if (!of && !possesive) ok = false;
                }

                pnode* p2 = 0;
                
                if (ok)
                {
                    bool old = _leftUNoun;
                    _leftUNoun = true;
                    p2 = parseTNoun();
                    _leftUNoun = old;
                }
                
                if (p2)
                {
                    if (of)
                    {
                        // if "of" keep same order, append 
                        plast->_next = p2;
                        plast = p2;
                    }
                    else  // swap
                    {
                        // otherwise new node becomes head
                        // and last remains the same
                        p2->_next = pl;
                        pl = p2;
                    }
                    
                    _drop();
                }
                else
                {
                    _pop();
                    break;
                }
            }

            // if we are a list, make a single term
            if (pl->_next)
            {
                pl = new pnode(pl, nodeType::p_unoun);
                //LOG1("making p_unoun ", pl->toStringStruct());
            }
        }

        return pl;
    }

    pnode* parseNounList()
    {
        // list of nouns
        
        // #unoun
        // #nounlist = (unoun ...)
        
        pnode* pl = parseUNoun();
        if (pl)
        {
            pnode* plast = pl;
            for (;;)
            {
                pnode* p2 = 0;
                _push();
                if (wordType(word(), Word::pos_conj)) // and
                {
                    p2 = parseUNoun();
                    if (p2)
                    {
                        assert(!p2->_next);
                    
                        // same order
                        plast->_next = p2;
                        plast = p2;

                        _drop();
                    }
                }

                if (!p2)
                {
                    _pop();
                    break;
                }
            }

            if (pl->_next) pl = new pnode(pl, nodeType::p_nounlist);
        }
        return pl;
    }

    pnode* parseTNoun()
    {
        // #pronoun
        // #unoun
        // #tnoun = (#unoun the)
        // #tnoun = (#pronoun the)

        // Note: allow "the it" as this is convenient for scoping articles
        // such as "my it" etc.

        _push();
        string w = word();

        // [the] thing
        pnode* pa = parseType(w, Word::pos_article, nodeType::p_article);
        if (pa)
        {
            _push();
            w = word();
        }

        // it
        pnode* pn = parseType(w, Word::pos_pronoun, nodeType::p_pronoun);
        if (pn)
        {
            if (pa)
            {
                // make node (it the)
                assert(!pn->_next);
                pn->_next = pa;
                pn = new pnode(pn, nodeType::p_tnoun);
                _drop();
            }
            _drop();
        }
        else
        {
            if (pa) _pop();
            if (!pa) _poppush();

            // the (cup and saucer)
            pn = parseNounList();
            if (pn)
            {
                if (pa)
                {
                    // make node (noun the) or ((nouns) the)
                    assert(!pn->_next);
                    pn->_next = pa;
                    pn = new pnode(pn, nodeType::p_tnoun);
                }
                _drop();
                     
            }
            else
            {
                delete pa;
                _pop();
            }

        }
        return pn;
    }

    pnode* parseDNoun()
    {
        // placeholder for dnoun
        return parseTNoun();
    }

    pnode* makePrepMod(const Word* prepmod, pnode* prepn)
    {
        // either prep or (#prepmod (prepmod prep))
        pnode* pn = prepn;
        if (prepmod)
        {
            pnode* pm = new pnode(prepmod, 0);  // XX type?
            pm->_next = pn;
            pn = new pnode(pm, nodeType::p_prepmod);
        }
        return pn;
    }

    pnode* parseRNoun()
    {
        // parse noun relative
        // eg the box [not] in the hall

        // #tn
        // #rnoun = (#tnoun prep rnoun)
        // #rnoun = (#tnoun (#prepmod prepmod prep) rnoun)
        
        pnode* pn = parseDNoun();
        if (pn)
        {
            _push();

            // optional not or also
            // eg X not in Y, X also in Y.
            pnode* prep = parsePrep(false);
            pnode* rn = 0;

            // if prep is relativiser, we go ahead and parse as rel.
            // Later we might be missing an iobj, in which case we have to
            // lift out.
            //
            // NOTE: in definition mode, we never interpret prepositions
            // as relatisers, because we dont use relphrases for template
            // resolution, instead use IDs
            if (prep && !_defMode && prep->_word->isPrepRel())
            {
                // right recursive
                // eg key in the box in the hall
                rn = parseRNoun();
                if (rn)
                {
                    prep->_next = rn; // (prep rnoun)

                    assert(!pn->_next);
                    pn->_next = prep;

                    pn = new pnode(pn, nodeType::p_rnoun);
                    
                    _drop();
                }
            }

            if (!rn)
            {
                delete prep;
                _pop();
            }
        }
        return pn;
    }

    pnode* parseRNounx()
    {
        // parse except
        // eg all except the box
        // eg all but not the box

        // #rnounx = (#rnoun (except #rnounx))
        
        pnode* pn = parseRNoun();
        if (pn)
        {
            _push();
            pnode* p2 = 0;
            pnode* ex = parseType(word(), Word::pos_except);
            if (ex)
            {
                // optional "not"
                parseLiteral("not"); // XXX ignore it
                
                p2 = parseRNounx(); // right recursive

                // (but nouns) 
                ex->_next = p2;                

                assert(!pn->_next);
                pn->_next = ex;

                pn = new pnode(pn, nodeType::p_rnounx);

                _drop();
            }

            if (!p2)
            {
                delete ex;
                _pop();
            }
        }
        return pn;
    }

    pnode* parseNouns()
    {
        // #rnounx
        // #nounlist = (#rnounx ...)
        
        pnode* pl = parseRNounx();
        if (pl)
        {
            pnode* plast = pl;
            for(;;)
            {
                pnode* p2 = 0;
                _push();
                if (wordType(word(), Word::pos_conj))
                {
                    p2 = parseRNounx();
                    if (p2)
                    {
                        assert(!p2->_next);
                        
                        // same order
                        plast->_next = p2;
                        plast = p2;
                        _drop();                    
                    }
                }

                if (!p2)
                {
                    _pop();
                    break;
                }
            }

            if (pl->_next) pl = new pnode(pl, nodeType::p_nounlist);
        }
        return pl;
    }

    pnode* parseAdvlist()
    {
        // parse a list of adverbs
        // (a + a + a ...)
        return _parseAndList(Word::pos_adverb, nodeType::p_adverbs);
    }

    pnode* parseAVerb()
    {
        // parse a verb with an optional adverb
        // (verb)
        // (verb prep)
        // ((verb prep) adverb)
        
        _push();
        pnode* pl = parseAdvlist();
        pnode* pn = parseType(word(), Word::pos_verb, nodeType::p_verb);
        if (pn)
        {
            _push();
            pnode* prep = parseType(word(), Word::pos_prep, nodeType::p_prep);
            if (prep)
            {
                // talk to, look at, get in, etc
                pn->_next = prep;
                pn = new pnode(pn, nodeType::p_prepverb);
                _drop();
            }
            else _pop();
            
            if (pl)
            {
                pn->_next = pl;
                pn = new pnode(pn, nodeType::p_averb);
            }
            _drop();
        }
        else
        {
            delete pl;
            _pop();
        }
        return pn;
    }

    static pnode* findSubnode(pnode* ps, uint type)
    {
        // finds first subnode of type
        pnode* pn = 0;
        while (ps)
        {
            assert(ps->valid());
            if (ps->_type == type) return ps;
                            
            if (ps->_type != nodeType::p_value)  // avoid enodes
            {
                pn = findSubnode(ps->_head, type);
                if (pn) break;
            }
            ps = ps->_next;
        }
        return pn;
    }

    typedef std::list<pnode**> NodeRefs;

    static void findSubnodes(NodeRefs& nodes, pnode** pn, uint type)
    {
        while (*pn)
        {
            if ((*pn)->_type == type) nodes.push_back(pn);
            findSubnodes(nodes, &(*pn)->_head, type);
            pn = &(*pn)->_next;
        }
    }

    static pnode* getVerbNode(pnode* pn)
    {
        pnode* p0 = pn;
        
        // walk breadth first
        while (pn)
        {
            if (pn->_type == nodeType::p_verb ||
                pn->_type == nodeType::p_prepverb ||
                pn->_type == nodeType::p_averb)
                return pn;
            pn = pn->_next;
        }

        // walk depth
        while (p0)
        {
            if (p0->_head)
            {
                pn = getVerbNode(p0->_head);
                if (pn) break;
            }
            p0 = p0->_next;
        }
        return pn;
    }
    
    static const Word* getVerb(pnode* pn)
    {
        // gets the actual word
        pnode* vn = findSubnode(pn, nodeType::p_verb);
        return vn ? vn->_word : 0;
    }

    static pnode* getNodeByType(pnode* pn, uint t)
    {
        // either this node or directly below
        return pn->_type == t ? pn : findSubnode(pn->_head, t);
    }
    
    static const Word* getNoun(pnode* pn)
    {
        // gets the actual word
        pnode* nn = getNodeByType(pn, nodeType::p_noun);
        return nn ? nn->_word : 0;
    }

    static const Word* getPrep(pnode* pn)
    {
        pnode* nn = getNodeByType(pn, nodeType::p_prep);
        return nn ? nn->_word : 0;
    }

    static pnode* getIONode(pnode* ps)
    {
        pnode* io = 0;
        if (ps->isSentence())
        {
            // rummage through the nodes to see if
            // we have indirect object.
            pnode* pn = ps->_head;
            if (pn)
            {
                pnode* dobj = pn->_next;
                if (dobj)
                {
                    // io could be a prep, property or prepmod
                    // we only want IOs that are prep, ie object based iobj
                    pnode* ioprep = dobj->_next;
                    if (ioprep && ioprep->_type == nodeType::p_prep)
                    {
                        io = ioprep;  // accept
                    }
                }

            }
        }
        return io;
    }

    enode* parseValueExpr()
    {
        ParseExpr pe;

        // allow simple non-terms words as property values
        // these are wrapped as string.
        pe._allowSimpleNames = true;
        
        enode* en = pe.parse(POS, lineno);
        if (en)
        {
            SETPOS(pe.pos);
            LOG5("parseValueExpr, ", *en);
        }
        return en;
    }


    bool verbIsSet(pnode* ps)
    {
        // is this sentence "SET ..."
        assert(ps);
        assert(ps->_head);
        
        const Word* verb = getVerb(ps->_head);  // search verb branch
        return verb && verb->_text == SYM_SET;
    }

    pnode* parsePrep(bool allowprop, bool defprop = false)
    {
        // allowprop, also parse props as preps
        // defprop, define words as props that aren't preps
        pnode* pn = 0;
        
        _push();
        string w = word();

        // optional not/also
        const Word* prepmod = wordType(w, Word::pos_prepmod);
        if (prepmod) w = word();

        pnode* prep = parseType(w, Word::pos_prep, nodeType::p_prep);
        if (prep)
        {
            pn = makePrepMod(prepmod, prep); // (prepmod prep)
        }
        else if (allowprop)
        {
            string prop;
            
            // properties are not case sensitive
            w = toLower(w);

            if (atSimpleName(w.c_str())) prop = wordStem(w);
            else
            {
                //PERR1("expected a property word instead of ", w);
            }

            if (!prop.empty())
            {
                // look for existing prop word
                pn = parseType(prop, Word::pos_property, nodeType::p_property);
                
                if (!pn && defprop)
                {
                    /// intern prop as new prop
                    
                    LOG4("adding prop ", prop);
                    
                    // add properties to dictionary
                    // (prop)
                    pn = new pnode(&internWordType(prop, Word::pos_property),
                                   nodeType::p_property);
                }
                
                if (pn) pn = makePrepMod(prepmod, pn);  // prepmod can be null
            }
        }

        if (!pn)
        {
            _pop();
            delete prep;
        }
        else _drop();

        return pn;

    }
    
    bool parseSetVal(pnode* ps)
    {
        // parse (set noun prep value)
        // only for values!

        // NB: consider expanding to handle (set nouns prep iobj)
        // although this is already parsed before we get here
        // there is the problem of dealing with
        // (set nouns (prepmod prep) iobj) which currently doesn't work)
        
        // ASSUME verb is "set"
        // return false if error
        
        // ps = (verb (nouns))
        // forms are:
        // set N [not/also] prop value

        // (set nouns prop value)
        // (set nouns (#prepmod prepmod prop) value)

        assert(ps);
        assert(ps->_head);
        
        pnode* pn = ps->_head->_next; // nouns
        if (!pn) return true; // no error
        
        assert(!pn->_next); // no prep lifted yet.

        _push();
        
        bool ok = false;
        pnode* prop = parsePrep(true, true);

        if (prop)
        {
            enode* en = parseValueExpr();

            if (en)
            {
                LOG4("adding property ", *pn << " " << prop->toString() << " " << *en);
                // wrap expression as a `pnode`.
                prop->_next = pnode::makeValueNode(en);
                    
                // hook into sentence
                // (set nouns (prop val))
                // (set nouns ((prop prepmod) val))
                pn->_next = prop;
                ok = true;
            }
        }

        if (ok) _drop();
        else _pop();

        return ok;
    }


    bool liftParseIO(pnode* ps)
    {
        // preposition lifting on last rnoun phrase.
        // at this state we do not know the semantics, but an IO
        // verb must have an IO object, so we must lift something.

        bool changed = false;
        
        assert(ps);
        if (ps->isSentence()) // command sentence only
        {
            // head is verb
            // head next is dobj

            pnode* vn = ps->_head;
            assert(vn);
            pnode* dn = vn->_next;
            if (dn && !dn->_next)  // no current prep, therefore no IO
            {
                const Word* verb = getVerb(vn);
                if (verb && verb->isIOLift())  // ioverb or canio
                {
                    NodeRefs nodes;
                    findSubnodes(nodes, &vn->_next, nodeType::p_rnoun);

                    if (!nodes.empty())
                    {
                        pnode** ppr = nodes.back(); // last rnoun
                        pnode* pr = *ppr;

                        // head is some N
                        // head next is (prep rn)
                        assert(pr->_type == nodeType::p_rnoun);
                        assert(pr->_head); // TN or lower

                        // prep or prepmod
                        pnode* prep = pr->_head->_next;
                        assert(prep);

                        assert(prep->_type == nodeType::p_prep ||
                               prep->_type == nodeType::p_prepmod);

                        if (dn == pr)
                        {
                            vn->_next = pr->_head;
                        }
                        else
                        {
                            // steal (prep n)
                            dn->_next = prep;
                            pr->_head->_next = 0;  // &prep

                            // move subnode up to replace position of rnoun
                            *ppr = pr->_head;
                        }
                        
                        pr->_head = 0;
                        pr->_next = 0;
                        delete pr; // remove rnoun node
                        
                        changed = true;
                    }
                }
            }
        }
        return changed;
    }

    bool liftAlternation(pnode* ps)
    {
        bool changed = false;
        
        assert(ps);
        if (ps->isSentence()) // command sentence only
        {
            // head is verb
            // head next is dobj

            pnode* vn = ps->_head;
            assert(vn);
            pnode* dn = vn->_next;
            if (dn && !dn->_next)  // no current prep, therefore no IO
            {
                const Word* verb = getVerb(vn);

                if (verb && verb->isAlternationVerb())
                {
                    pnode* unoun = findSubnode(dn, nodeType::p_unoun);

                    if (unoun)
                    {
                        //LOG1("liftAlternation, considering ", unoun->toStringStruct());
                        dn = unoun->_head;  // become dn
                        
                        if (dn && !unoun->_next)
                        {
                            pnode* io = dn->_next;
                            if (io)
                            {
                                pnode* prep = parseType(PROP_TO, Word::pos_prep, nodeType::p_prep);
                                assert(prep);

                                // disconnect from original unoun
                                unoun->_head = 0;
                                
                                vn->_next = dn;
                                dn->_next = prep;
                                prep->_next = io;
                                //LOG1("liftAlternation, changed to ", ps->toStringStruct());
                                delete unoun;
                             
                                changed = true;
                            }
                        }
                    }
                }
            }
        }
        return changed;
    }

#if 0
    pnode* parseQueryX()
    {
        // what prep N, eg what [is] in hall => all X where, (X in hall).
        // what prop VAL, eg what [is] feels wet => all X where, (X feels wet).

        _push();
        string w = word();
        pnode* rn = 0;
        pnode* prep = parseType(w, Word::pos_prep, nodeType::p_prep);
        if (prep)
        {
            rn = parseRNoun();
        }
        else
        {
            string prop = wordStem(w);
            prep = parseType(prop, Word::pos_property, nodeType::p_property);
            if (prep)
            {
                // possibly null
                rn = pnode::makeValueNode(parseValueExpr());
            }
        }
        
        if (rn)
        {
            // make node (prep rnoun)
            // make node (prop value)
            prep->_next = rn; 
            _drop();
        }
        else
        {
            delete prep; prep = 0;
            _pop();
        }
        return prep;
    }
#else

    pnode* parseQueryX()
    {
        // what prep N, eg what [is] in hall => all X where, (X in hall).
        // what prop VAL, eg what [is] feels wet => all X where, (X feels wet).

        _push();
        
        pnode* prep = parsePrep(true, false);        
        if (prep)
        {
            pnode* rn = 0;

            rn = parseRNoun();

            if (!rn)
            {
                // possibly null
                rn = pnode::makeValueNode(parseValueExpr());
            }
        
            if (rn)
            {
                // make node (prep rnoun)
                // make node (prop value)
                prep->_next = rn; 
                _drop();
            }
            else
            {
                delete prep; prep = 0;
            }
        }

        if (!prep) _pop();
        return prep;
    }
#endif    

    pnode* parseQueryY()
    {
        // what N prep, eg what [is] hall in => all Y where (hall in Y).
        // what N prop, eg what [is] hall feel => all Y where (hall feel Y).

        // what N (prepmod prep) eg what player also in
        // what N (prepmod prop) eg what player also feels

        pnode* pn = parseRNoun();
        if (pn)
        {
            _push();

            pnode* prep = parsePrep(true, false);
            if (prep)
            {
                // make node (N prep)
                // make node (N prop)
                assert(!pn->_next);
                pn->_next = prep;
                _drop();
            }
            else
            {
                delete pn; pn = 0;
                _pop();
            }
        }
        return pn;
    }

    pnode* parseQuery()
    {
        // typeX
        // what prep N, eg what in hall => all X where, (X in hall).
        // what prop VAL, eg what feels wet => all X where, (X feels wet).

        // typeY
        // what N prep, eg what hall in => all Y where (hall in Y).
        // what N prop, eg what hall feel => all Y where (hall feel Y).

        // what is val

        // is/are
        // is N prep Y
        // is N prop V
        // is N -> (is N)
        // is VAL -> (is val)

        bool ok = false;

        _push();
        pnode* pq = parseType(word(), Word::pos_query, nodeType::p_query);
        if (pq)
        {
            //LOG1("Parsing query: ", pq->toStringStruct());        
            bool isare = pq->_word->isISARE();

            if (isare)
            {
                //LOG1("Parsing isare query: ", pq->toStringStruct());        
                                    
                // Is N ...
                pnode* pn = parseRNoun();
                if (pn)
                {
                    // (is N)
                    pq->_next = pn;

                    // (is N) is valid on its own
                    ok = true;
                    
                    // is N prep Y
                    // is N prop V
                    pnode* prep = parseQueryX();
                    if (prep)
                    {
                        // (is N (prep Y))
                        // (is N (prop V))
                        pn->_next = prep;
                    }
                }
                else
                {
                    // (is val)
                    // possibly null
                    pn = pnode::makeValueNode(parseValueExpr());
                    if (pn)
                    {
                        // (is VAL)
                        pq->_next = pn;
                        ok = true;
                    }
                }
            }
            else
            {
                // allow optional "is", but ignore it, to allow
                // what IS ....
                _push();
                const Word* wis = wordType(word(), Word::pos_query);
                if (wis && wis->isISARE()) _drop();
                else
                {
                    _pop();
                    wis = 0;
                }

                // (what (prep rn))  eg (what in box)
                // (what (prop value)) eg (what feel wet)
                pnode* prep = parseQueryX();

                // (what (N prep)) eg (what box in)
                // (what (N prop)) eg (what box feel)
                if (!prep) prep = parseQueryY();

                if (!prep && wis)
                {
                    // what is val
                    // possibly null
                    prep = pnode::makeValueNode(parseValueExpr());
                }

                if (prep)
                {
                    pq->_next = prep;
                    ok = true;
                }
            }
            
            if (ok)
            {
                pq = new pnode(pq, nodeType::p_qs);
                _drop();
            }
        }

        if (!ok)
        {
            _pop();
            delete pq; pq = 0;
        }
        return pq;
    }

    pnode* parseIO(pnode* ps)
    {
        // (prep nouns)
        // ((prepmod prep) nouns)
        // (prep)  is degenerate case for; pick x up.
        
        _push();

        pnode* pn = 0;
        pnode* prep = parsePrep(false);
        if (prep)
        {
            pn = parseNouns();
            if (pn)
            {
                prep->_next = pn;
                pn = prep;
                _drop();
            }
            else
            {
                bool accept = true;
                
                assert(ps);
                pnode* v = ps;

                if (v->_type == nodeType::p_averb)
                {
                    v = v->_head;
                    assert(v);
                }

                if (v->_type == nodeType::p_prepverb) accept = false;

                //const Word* verb = getVerb(v);
                //assert(verb);
                // if we require an IO, then this degenerate prep is wrong
                // what about "put headphones on"
                //if (verb->isIOVerb()) accept = false;

                if (accept)
                {
                    pn = prep; 
                    _drop(); // accept degenerate
                }
            }
        }

        if (!pn) _pop();

        return pn;
    }

    pnode* parseSentenceBody(pnode* ps)
    {
        pnode* pn = parseNouns(); 
        if (pn)
        {
            assert(!ps->_next);
            ps->_next = pn;  // (verb nouns)

            // indirect object?
            pnode* io = parseIO(ps); // (prep nouns) or (prep)
            if (io)
            {
                if (!io->_next)
                {
                    // there are no nouns. we have a degenerate
                    // eg pick X up.
                    pnode** v = &ps;

                    if ((*v)->_type == nodeType::p_averb)
                    {
                        v = &(*v)->_head;
                        assert(*v);
                    }

                    assert(io->_word && io->_word->isPrep());
                    assert((*v)->_word && (*v)->_word->isVerb());

                    pnode* vn = (*v)->_next;
                    (*v)->_next = io; io = 0; // not real io
                    
                    *v = new pnode(*v, nodeType::p_prepverb);
                    (*v)->_next = vn;
                }
                else
                {
                    pn->_next = io; // (verb nouns prep nouns)
                }
            }

            // (#cs (verb nouns prep nouns))
            ps = new pnode(ps, nodeType::p_cs);

            if (!io)
            {
                skipws();
                if (AT)
                {
                    if (verbIsSet(ps))
                    {
                        // more to parse!
                        if (parseSetVal(ps))
                        {
                            LOG4("Parse set: ", ps->toStringStruct());
                        }
                        else
                        {
                            LOG1("parse set problem ", POS);
                        }
                    }
                }

                // lift (verb nouns) -> (verb nouns prep nouns)
                
                bool v = liftAlternation(ps);
                if (v)
                {
                    LOG4("lifted alternation", ps->toStringStruct());
                }
                
                v = liftParseIO(ps);
                if (v)
                {
                    LOG4("lifted parse", ps->toStringStruct());
                }
            }
        }
        else
        {
            // elevate single verb to sentence
            assert(!ps->_next);
            ps = new pnode(ps, nodeType::p_cs);
        }
        return ps;
    }

    pnode* parseSentence()
    {
        // (#cs verb)
        // (#PS verb nouns)
        // (#qs ...)

        pnode* ps = parseAVerb();
        if (ps)
        {
            ps = parseSentenceBody(ps);
        }
        else
        {
            ps = parseQuery();
        }

        return ps;
    }

    typedef pnode* (ParseCommand::*pFn)();
    pnode* parseAux(const char* s, int line, pFn pfn, bool all = true)
    {
        // if `all`, expect to parse all of `s` rather than just the
        // first part

        setup(s, line);
        pnode* pn = (this->*pfn)();
        if (pn && all)
        {
            // if all the input text not parsed, then we fail.
            skipws();
            if (AT && AT != '\n')
            {
                // more not expected
                if (line)
                {
                    PERR1("extra text not expected", POS);
                }

                delete pn;
                pn = 0;
            }
        }
        
        if (!pn && line)
        {
            PERR0("command syntax error");
        }
        return pn;
    }

    pnode* parse(const char* s, int line = 0)
    {
        pnode* ps = parseAux(s, line, &ParseCommand::parseSentence);

        // recover sentence from just a nounphrase, when no line number
        // ie. an input command not from file.
        if (!ps && !line && _pctx)
        {
            // try for auto verb.
            if (!_pctx->_autoVerb.empty())
            {
                ps = parseVerb(_pctx->_autoVerb.c_str());
                if (ps)
                {
                    setup(s, line);

                    // this will always succeed, falling back to just the verb
                    // if necessary. Need to check all consumed.
                    ps = parseSentenceBody(ps);
                    assert(ps);

                    // if all the input text not parsed, then we fail.
                    skipws();
                    if (AT && AT != '\n')
                    {
                        delete ps;
                        ps = 0;
                    }
                }
                else
                {
                    LOG1("WARNING: auto verb not parse ", _pctx->_autoVerb);
                }
            }
        }
        
        return ps;
    }

    pnode* parseNoun(const char* s, int line = 0)
    {
        // not a complete noun phrase. Only the "the noun" part.
        // full NP is parseNouns.
        return parseAux(s, line, &ParseCommand::parseDNoun);
    }

    pnode* parseVerb(const char* s, int line = 0)
    {
        // if we start with a verb, return it.
        return parseAux(s, line, &ParseCommand::parseAVerb, false);
    }

    pnode* parse(const string& s, int line = 0)
    { return parse(s.c_str(), line); }

    void clear()
    {
        _dictionary.clear();
    }

};

}; // ST
