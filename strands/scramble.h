//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>

// forward
struct ScrambleState;

#define SCRAMBLE_FAIL_NONE 0

 // not scrambled
#define SCRAMBLE_FAIL_ID 1

// wrong version
#define SCRAMBLE_FAIL_VERSION 2

// wrong label name
#define SCRAMBLE_FAIL_LABEL 3

// bad data
#define SCRAMBLE_FAIL_DATA 4

ScrambleState* DescrambleStart(unsigned char* data,
                               unsigned int& size,
                               const char* label = 0);
bool DescrambleOK(const ScrambleState*);
void DescrambleNext(ScrambleState* ss, unsigned char* data, unsigned int size);
void DescrambleEnd(ScrambleState* ss);

// return scramble fail code, 0 if ok
int DescrambleAll(unsigned char* data, unsigned int& size,
                  const char* label = 0);


unsigned char* Scramble(const unsigned char* data,
                        unsigned int& size,
                        const std::string& label);
void ScrambleSetKey(const char* pw);
