//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include "kllib.h"

#ifdef IFI_BUILD
#include "ificlient.h"
#include "ifihandler.h"

// needed for strandi.h
extern IFIClient* ifi;
extern IFIHandler* ifih;
#endif // IFI_BUILD

// fix for macos/ios build - troubles with objective c build 
extern "C" {
    int stem(char * p, int i, int j);
};

#include "strandi.h"

namespace KLS
{
#include "tformat.h"
};


struct KLStrandi: public KLLib
{
    typedef KLStrandi Self;
    typedef Term (Self::*PrimFn)(List::iterator&, Env&);
    typedef PCallFn<PrimFn, Self> PC;
    typedef ST::Strandi Strandi;

    Strandi*        _strandi;
    
    KLStrandi(KL* host, Strandi* s) : KLLib(host), _strandi(s) { _init(); }
    const char* libname() const override { return "klstrandi"; }
    void destroy(Blob* b) override { delete b; }
    string asString(const Blob* b) const override { return "blob"; }

    string _textify(const Term& t)
    {
        string s;
        if (t)
        {
            if (t.isList())
            {
                for (List::iterator it(t.asList()); it; ++it)
                {
                    string si = _textify(*it);
                    if (si.size())
                    {
                        if (!s.empty()) s += ' ';
                        s += si;
                    }
                }
            }
            else if (t.isSymbol())
            {
                // copes with null
                s = _strandi->textify(ST::Term::find(t.asSymbol().label()));
            }
            else
            {
                s = t.toStringRaw();
            }
        }
        return s;
    }

    Term _primTextifyFn(List::iterator& ai, Env& env)
    {
        // (textify term)
        Term r;
        if (ai)
        {
            Term t = EVALAI;
            string s = _textify(t);
            if (!s.empty()) r = Stringt(s);
        }
        return r;
    }

    Term _primTermIDFn(List::iterator& ai, Env& env)
    {
        // (termid TERMNAME)  -> number
        Term r;
        if (ai)
        {
            Term t = EVALAI;
            if (t.isSymbol())
            {
                ST::Term* st = ST::Term::find(t.asSymbol().label());
                if (st)
                {
                    r = Int(st->_uiID);
                }
            }
        }
        return r;
    }

    Term _primCapsWordsFn(List::iterator& ai, Env& env)
    {
        // (capswords t)
        Term r;
        if (ai)
        {
            Term t = EVALAI;
            string s = CapitaliseStartWords(t.toStringRaw());
            r = Stringt(s);
        }
        return r;
        
    }

    Term _primPlainToHTMLFn(List::iterator& ai, Env& env)
    {
        Term r;
        
        return r;
    }
    
    Term _primHTMLToPlainFn(List::iterator& ai, Env& env)
    {
        Term r;
        if (ai)
        {
            KLS::TextFormat tf;

            Term t = EVALAI;
            tf.setHTML(t.toStringRaw());
            r = Stringt(tf._text);
            //LOG1("HTMLTOPLain = ", tf._text);
        }
        return r;
    }

    Term _primRunTermFn(List::iterator& ai, Env& env)
    {
        // (runtime name)
        Term r;
        if (ai)
        {
            Term t = EVALAI;
            string tname = t.toStringRaw();
            string s;
            if (!tname.empty())
            {
                s = _strandi->runSingleTermCap(tname);
                //LOG3("KLStrandi, eval sym ", tname << " = " << s);
            }
            r = Stringt(s);
        }
        return r;
    }

    Term _primUndoTermFn(List::iterator& ai, Env& env)
    {
        int r = 0;
        if (ai)
        {
            Term t = EVALAI;
            string tname = t.toStringRaw();
            
            if (!tname.empty())
            {
                if (_strandi->undoToTerm(tname)) r = 1;
            }
        }
        return Int(r);
    }

    Term _primElevateChoicesFn(List::iterator& ai, Env& env)
    {
        // (elevatechoice bool)
        Term r;
        if (ai)
        {
            r = EVALAI;
            bool v = KL::isTrue(r);

            assert(_strandi);
            _strandi->_elevateChoices = v;
        }
        return r;
    }

#if 0    
    Term _primAutolinkFn(List::iterator& ai, Env& env)
    {
        // (autolink "[name]")
        Term t = EVALAI;

        string s = t.toStringRaw();
        _strandi->autoLink(s, false);
        Term r = Stringt(s);
        return r;
    }

    Term _primPushAutoVerbFn(List::iterator& ai, Env& env)
    {
        // (push-autoverb "foo")
        Term t = EVALAI;

        string s = t.toStringRaw();
        _strandi->_ctx->pushAutoVerb(s);
        return Stringt(s);
    }

    Term _primPopAutoVerbFn(List::iterator& ai, Env& env)
    {
        // (pop-autoverb)
        _strandi->_ctx->popAutoVerb();
        Term r = Int(1);
        return r;
    }
#endif

    Term _primInternWordFn(List::iterator& ai, Env& env)
    {
        // (intern-word "foo" "noun" "preposition" ...)
        int r = 0;
        if (ai)
        {
            assert(_strandi);
            
            Term t = EVALAI;
            string word = t.toStringRaw();

            while (ai)
            {
                Term pos = EVALAI;
                string posname = pos.toStringRaw();
                unsigned int posv = ST::Word::posOfName(posname);
                if (posv)
                {
                    LOG4("primInternWord ", word << " as " << posname << ", " << posv);
                    _strandi->_pcom.internWordType(word, posv);
                    r |= posv;
                }
                else
                {
                    LOG1("WARNING: intern-word, not found \"", posname << "\"");
                }
            }
        }
        return Int(r);
    }

    Term _primListLocationsFn(List::iterator& ai, Env& env)
    {
        // (listlocations true) // true if visited
        assert(_strandi);
        
        List l;
        if (ai)
        {
            Term r = EVALAI;
            bool onlyVisited = KL::isTrue(r);

            List::iterator it(l);
            LValue lv(it);
            
            for (auto li : _strandi->_locations)
            {
                if (!onlyVisited || _strandi->getVisited(li))
                {
                    Term ti = Stringt(li->_name.c_str());
                    lv = *ti;
                }
            }
        }
        return l;
    }

    Term _primTickTimeFn(List::iterator& ai, Env& env)
    {
        // (ticktime) // return game tick
        Term r = Int(_strandi->_time);
        return r;        
    }

    Term _primUndoFn(List::iterator& ai, Env& env)
    {
        // (undo check)
        Term r = EVALAI;
        bool apply = KL::isTrue(r);
        bool v = _strandi->undoMove(apply);
        r = Int(v ? 1 : 0); 
        return r;
    }

    Term _primIsMobileFn(List::iterator& ai, Env& env)
    {
        // (is-mobile)
        // used to suggest game mobile font.
        
        int v;

        // XXX this is a hack because we dont go over IFI 
        extern bool isMobile;
        v = isMobile;

        return Int(v);
    }

    Term _primIsWebFn(List::iterator& ai, Env& env)
    {
        // (is-web)
        // eg used (with is-mobile) to determine hi/lo video res.
        
        int v;

        // XXX this is a hack because we dont go over IFI 
        extern bool isWeb;
        v = isWeb;

        return Int(v);
    }

#if 0    
    Term _primAppWidthFn(List::iterator& ai, Env& env)
    {
        int w = 0;

#ifdef IFI_BUILD
        extern int GetAppWidth();
        w = GetAppWidth();
#endif

        if (!w) w = 1; // XX non-zero
        return Int(w);
    }

    Term _primAppHeightFn(List::iterator& ai, Env& env)
    {
        int h = 0;

#ifdef IFI_BUILD
        extern int GetAppHeight();
        h = GetAppHeight();
#endif

        if (!h) h = 1; // XX non-zero
        return Int(h);
    }
#endif

    Term _primOpenUrlFn(List::iterator& ai, Env& env)
    {
        // (open-url t)
        Term r;
        if (ai)
        {
            Term r = EVALAI;
            string s = r.toStringRaw();

#ifdef IFI_BUILD
        extern bool urlClicked(const std::string& url);
        urlClicked(s);
#endif            
        }
        return r;
    }

    void _init()
    {
        Tree g;

        // data
        DEF_PRIM(Textify, "textify");
        DEF_PRIM(TermID, "termid");
        DEF_PRIM(CapsWords, "capswords");
        DEF_PRIM(HTMLToPlain, "htmltoplain");
        DEF_PRIM(PlainToHTML, "plaintohtml");
        DEF_PRIM(RunTerm, "runterm");
        DEF_PRIM(UndoTerm, "undoterm");
        DEF_PRIM(ElevateChoices, "elevatechoices");
        DEF_PRIM(ListLocations, "listlocations");
        DEF_PRIM(TickTime, "ticktime");
        //DEF_PRIM(Autolink, "autolink");
        //DEF_PRIM(PushAutoVerb, "push-autoverb");
        //DEF_PRIM(PopAutoVerb, "pop-autoverb");

        DEF_PRIM(InternWord, "intern-word");
        DEF_PRIM(Undo, "undo");

        DEF_PRIM(IsMobile, "is-mobile");
        DEF_PRIM(IsWeb, "is-web");
        //DEF_PRIM(AppWidth, "app-width");
        //DEF_PRIM(AppHeight, "app-height");

        DEF_PRIM(OpenUrl, "open-url");

        _host->_env._env = List(g, *_host->_env._env);

    }

};

void initKLStrandi(KL* host, ST::Strandi* s)
{
    LOG3("KLStrandi ", "init");
    static KLStrandi klstrandi(host, s);
}


// XX this should be in pcom.cpp, but we don't have one
constexpr const char* ST::Word::posNames[];


