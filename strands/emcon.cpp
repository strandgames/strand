//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#ifdef __EMSCRIPTEN__

#include <stdio.h>
#include <string>
#include <emscripten/fetch.h>
#include <emscripten.h>

struct ConReader
{
    bool _ready;
    std::string _data;
    
    static void _read_success(emscripten_fetch_t *fetch)
    {
        ConReader* cr = (ConReader*)fetch->userData;
        if (!cr)
        {
            printf("ConReader: Bad host!\n");
            emscripten_fetch_close(fetch);
        }
        else
        {
            cr->read_success(fetch);
        }
    }

    void read_success(emscripten_fetch_t *fetch)
    {
        // can be zero bytes
        _data = std::string(fetch->data, fetch->numBytes);
        emscripten_fetch_close(fetch);
        _ready = true;
    }

    static void _read_fail(emscripten_fetch_t *fetch)
    {
        printf("ConReader: fetch fail!\n");
        emscripten_fetch_close(fetch);
    }
    
    static void read_async(void* host)
    {
        emscripten_fetch_attr_t attr;
        emscripten_fetch_attr_init(&attr);
        strcpy(attr.requestMethod, "GET");
        attr.attributes = EMSCRIPTEN_FETCH_LOAD_TO_MEMORY;
        attr.onsuccess = _read_success;
        attr.onerror = _read_fail;
        emscripten_fetch_t* f = emscripten_fetch(&attr, "___terminal::read");
        f->userData = host;
    }

    bool read_sync()
    {
        _ready = false;
        read_async(this);        
        while (!_ready) emscripten_sleep(100);
        return true;
    }
};


std::string em_getline()
{
    ConReader cr;
    cr.read_sync();
    //printf("em_getline (%d) '%s'\n", (int)cr._data.size(), cr._data.c_str());
    return cr._data;
}

#endif // __EMSCRIPTEN__
