//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include <list>
#include "fda.h"
#include "varsetjson.h"
#include "pexpr.h"
#include "growbuf.h"
#include "svgp.h"

#define SYM_STICKY      '!'

// generator flags
#define SYM_RANDOM '~'
#define SYM_SHUFFLE '&'
#define SYM_NONRAMDOM '#'
#define SYM_SEQUENCE '<'
#define SYM_FIRST '='
#define SYM_ALL '$'
#define SYM_RUNNABLE_TERM '%'
#define SYM_LINKED '.'

// choice flags
#define SYM_CMD_CHOICES '>'

// object types
#define SYM_CHOICE '?'
#define SYM_OBJECT '@'
#define SYM_ASSET '*'
#define SYM_TERMDEF_EXTENSION '+'

// choice selector flags
#define SYM_ALWAYS  '+'
#define SYM_FILLER '-'
#define SYM_TERMINAL '!'

#define SYM_COMMAND '>'

// object flags
#define SYM_ASCHOICE '='

// input selector flag
#define SYM_INPUT '<'

// selector flags
#define SYM_CONDITION '?'
#define SYM_NOT '!'

// subflow
#define SYM_SUBFLOW_START '('
#define SYM_SUBFLOW_END ')'

// flow term properties
// these are placed in flow when a term is referenced, not in definition.
#define SYM_BACKGROUND '^'
#define SYM_BACKGROUND_STOP '#'

#define SYM_ATTRIBUTE ':'
//#define SYM_ATTR_SEP ','

#define SYM_FLOWBREAK '#'

#ifdef IFI_BUILD
typedef IFI::Ctx IFICtx;
#else
typedef void* IFICtx;
#endif

#define ATSELECTOR  (AT == '*' && AT1 != '*')
#define SELECTOR(_p) ((_p)[0] == '*' && (_p)[1] != '*')

namespace ST
{

struct ParseStrands: public ParseBase
{
    Term*       _startTerm = 0;
    string      _loadFilePrefix;  // files base dir

    GrowString  _sourceCollector;
    bool        _collectSource = false;

    // set when we parse a term that indicates it might be an extension
    // to an existing one
    bool        _termDefExtension = false;

    struct VoiceInfo
    {
        string          _speech;
        string          _actor;
        string          _filename;
    };

    typedef std::list<VoiceInfo>   VoiceSet;

    VoiceSet           _voiceSet;
    bool               _inQuote = false;
    bool               _collectVoices = false;

    /// collect the companion svg files and emit imagemeta json
    bool               _genImageMeta = false;

    // if we want to copy images
    string              _imageOutputDir;
    string              _scrambleKey;
    strings             _imagePaths; // additional paths for images

    string              _audioOutputDir;
    strings             _audioPaths; // additional paths for audio

    struct Resolution
    {
        int             _value = 0;  // length of longest side
        int             _quality = 0; // use default
        string          _pattern; // optional name to match

        string tailName() const
        {
            // the string added to original filename to mark this res
            char buf[128];
            sprintf(buf, "_res%d", _value);
            if (_quality)
            {
                sprintf(buf + strlen(buf), "q%d", _quality);
            }
            return buf;
        }

        bool parse(const char*& sp)
        {
            // eg 1024, 1k
            // eg foo=1000,2000,0
            // eg 1024q100
            const char* s = sp;
            bool r = true;

            if (!u_isdigit(*s))
            {
                // expect pattern=...
                const char* se = s;
                while (*se && *se != '=') ++se;
                if (!*se) r = false; // failed to find =
                else
                {
                    _pattern = string(s, se - s); // collect pattern name
                    s = se+1; // skip =
                }
            }

            if (r)
            {
                // expect a number
                r = parseInt(&s, _value);
                if (r)
                {
                    if (*s == 'k')
                    {
                        // optional kilo
                        ++s;
                        _value *= 1024;
                    }

                    if (*s == 'q')
                    {
                        // optional quality
                        ++s;
                        r = parseInt(&s, _quality);
                    }
                }
            }

            if (r) sp = s;
            return r;
        }

        friend std::ostream& operator<<(std::ostream& os, const Resolution& r)
        {
            return os << r.tailName();
        }
    };

    std::vector<Resolution>  _resolutions;

    struct ForeignTerm
    {
        Term*   _src;
        string  _dstName;
    };

    // collect these up while parsing and fix on end
    typedef std::list<ForeignTerm> ForeignTerms;
    ForeignTerms        _foreignTerms;
    
    string parseName()
    {
        // `[A-Z][A-Z0-9-_]+`

        int l = atName(POS);
        string s;
        if (l)
        {
            s = string(POS, l);
            SETPOS(POS + l); 
        }
        return s;
    }

    Term::RType parseRType()
    {
        char c = AT;

        Term::RType rt = Term::t_void;

        switch (c)
        {
        case SYM_RANDOM:
            rt = Term::t_random;
            break;
        case SYM_SHUFFLE:
            rt = Term::t_shuffle;
            break;
        case SYM_NONRAMDOM:
            rt = Term::t_nonrandom;
            break;
        case SYM_SEQUENCE:
            rt = Term::t_sequence;
            break;
        case SYM_FIRST:
            rt = Term::t_first;
            break;
        case SYM_ALL:
            rt = Term::t_all;
            break;
        case SYM_LINKED:
            rt = Term::t_linked;
            break;
        default:
            c = 0;
        }

        if (c)
        {
            BUMP;
            skipws();
        }

        return rt;
    }

    static string _locateResource(const strings& paths,
                                  const string& file)
    {
        // file is the path of the default location.
        // see if we have an alternative

        if (!file.empty())
        {
            string name = filenameOf(file);
    
            for (auto& pi : paths)
            {
                string p = makePath(pi, name);
                if (FD::existsFile(p)) return p; // found alternative
            }

            // otherwise original file might exist
            if (FD::existsFile(file)) return file;
        }
        return string();
    }

    string locateImageResource(const string& file) const
    {
        return _locateResource(_imagePaths, file);
    }

    string locateAudioResource(const string& file) const
    {
        return _locateResource(_audioPaths, file);
    }

    void collectVoice(const Flow::EltMedia& m)
    {
#ifdef TOOLS_BUILD
        if (m._attr)
        {
            var actor = m.findAttr(MEDIA_ATTR_VOICE);
            var speech = m.findAttr(MEDIA_ATTR_SPEECH);

            // if no actor, then this isn't a voice tag
            if (actor && speech)
            {
                VoiceInfo vi;
                vi._speech = speech.toString();
                vi._actor = actor.toString();
                vi._filename = m._filename;

                // check for duplicate filenames
                for (auto& v : _voiceSet)
                {
                    if (v._filename == vi._filename)
                    {
                        LOG1("WARNING: collect Voice, duplicte filename ", v._filename);
                        break;
                    }
                }
                
                _voiceSet.push_back(vi);
            }
        }
#endif // tools_build        
    }

    void _addText(Flow& f,
                  const char* st,
                  const char* p,
                  uint oktypes)
    {
        // backup over any space on the end
        // unless it's been escaped in!
        const char* q = p;
        while (q != st)
        {
            --q;
            if (!u_isspace(*q)) break;
            if (q != st && q[-1] == '\\') break; // space was escaped!
            p = q;
        }

        if (p != st)
        {
            //LOG1("add text '", string(st, p - st) << "'");
            string s = ProcessEscapes(st, p, _inQuote);
            if (s.size())
            {
                if (Flow::t_text & oktypes)
                    f._elts.push_back(new Flow::EltText(s));
                else
                {
                    string m = "\"";
                    m += s;
                    m += '"';
                    PERR1("text not allowed", m);
                }
            }
        }
    }

    void _addTerm(Flow& f, const string& s, uint flags, enode* refcond)
    {
        assert(s.size());
        auto t = new Flow::EltTerm(s);
        t->_flags = flags;
        t->_cond = refcond;

        if (refcond)
        {
            //LOG1("conditional reference ", t->toString());
        }
        
        f._elts.push_back(t);
    }

    Flow::EltMedia* _addMedia(Flow& f, const string& fname, MediaType mt)
    {
        assert(fname.size());
        auto m = new Flow::EltMedia(fname, mt);
        f._elts.push_back(m);
        return m;
    }

    Flow::EltCond* _addCond(Flow& f, enode* e)
    {
        assert(e);
        auto m = new Flow::EltCond(e);
        f._elts.push_back(m);
        return m;
    }

    static uint parseFlowTermFlags(const char* p)
    {
        uint flags = 0;

        for (;;)
        {
            if (*p == SYM_BACKGROUND) flags |= Flow::ft_background;
            else if (*p == SYM_BACKGROUND_STOP) flags |= Flow::ft_reset;
            else break;
            ++p;
        }
        return flags;
    }

    void addText(Flow& f, const string& t, uint oktypes)
    {
        // break authored text into its component flows;
        // stretches of plain text, term references and media refs.

        // assume we are not in quotes at the start of any text span
        _inQuote = false;

        // XX bit hacky
        // assume that term-only flows can have conditionals
        // eg conditional parents.
        bool allowcond = (oktypes == Flow::t_term);

        const char* q = t.c_str();
        for (;;)
        {
            while (u_isspace(*q)) ++q;  // skip spaces and newlines
            if (!*q) break;
            
            const char* p = q;
            char lastc = 0;
            uint ftFlags = 0;

            // optional term ref conditional
            enode* refcond = 0;

            const char* refstart = 0;
            const char* refend = 0;
            string refname;
            
            while (*p)
            {
                // support conditionals before term references.
                // must have the syntax:
                // ?FOO BAR or ?(FOO) BAR
                // no space after the "?"
                
                if (allowcond)
                {
                    // conditionals all start with ?
                    bool havec = (*p == '?');

                    // determine if we have a conditional
                    if (havec)
                    {
                        // then they can be an expression (...)
                        // or a term ?FOO
                        // or a neg term ?!FOO
                        // both types must follow immediately without ws
                        const char* p1 = p + 1;
                        if (*p1 != '(')
                        {
                            if (*p1 == '!') ++p1; // neg term?
                            if (!atName(p1))
                                havec = false; // not a condition
                        }
                    }

                    if (havec)
                    {
                        // in case somehow we already had one?
                        // escapes?
                        delete refcond; refcond = 0;
                        
                        PUSHP;
                        SETPOS(p + 1);

                        refcond = parseCond();
                        if (refcond)
                        {
                            refstart = p; // at '?'

                            // park after conditional, before name
                            p = POS;
                            _skipws(&p);
                        }
                        else
                        {
                            LOG1("malformed term pre-condition, ", p);
                        }
                        POPP;
                    }
                }
                
                int l = atName(p);
                if (l)
                {
                    if (lastc == '\\')
                    {
                        // escaped term, skip this name and treat as text
                        p += l;
                        refstart = 0;
                        refend = 0;
                        continue;
                    }

                    refname = string(p, l);
                    refend = p + l;
                    ftFlags = parseFlowTermFlags(refend);
                    refend += countBits(ftFlags);  // account for flag symbols

                    // now add text range q->p and the name.
                    break;
                }
                else if (*p == '.' && p != q)
                {
                    // collect suffix and see if it's a media filename
                    // media elements can have attributes,
                    // foo.jpg:wibble:1,wobble:2,whatever:something ...
                    // these are collected and stored in a varset
                    
                    const char* e = p;
                    e++;
                    while (u_isalnum(*e)) ++e;
                    MediaType mt = mediaSuffix(string(p, e - p));
                    if (mt)
                    {
                        // ok, look back from .suf and find the filename
                        const char* s = p;

                        // filenames do not have spaces (do they).
                        do
                        {
                            if (u_isspace(s[-1])) break;
                            --s;
                        } while (s != q);

                        // ignore ![](foo) and [](foo) markdowns
                        if (s != p && *s != '!' && *s != '[')
                        {
                            string fname = string(s, e - s);

                            // add any text up to the start of the media ref.
                            _addText(f, q, s, oktypes);
                            
                            Flow::EltMedia* m = _addMedia(f, fname, mt);
                            if (*e == SYM_ATTRIBUTE)
                            {
                                ++e; // skip ':'
                                VarSet* vs = m->_attr;
                                if (!vs) vs = new VarSet;
                                SVGParser::parseAttributes(*vs, &e);
                                if (!vs->empty()) m->_attr = vs;
                                else
                                {
                                    assert(!m->_attr);
                                    delete vs;
                                }
                            }

                            if (_collectVoices) collectVoice(*m);
                            
                            q = p = e;
                            assert(!refend);
                            break;

                        }
                    }
                }
                
                lastc = *p++;
            }

            // add section of text before name
            // will be up to refstart or p
            if (refstart) p = refstart;
            _addText(f, q, p, oktypes);

            q = p;
            
            // now add any name
            if (refend)
            {
                // add term ref
                // consumes refcond
                assert(!refname.empty());
                _addTerm(f,  refname, ftFlags, refcond);
                refcond = 0;
                q = refend;
            }
        }
    }

    void addCode(Flow& f, const string& c, uint oktypes)
    {
        if (c.size())
        {
            if (Flow::t_code & oktypes)
                f._elts.push_back(new Flow::EltCode(c));
            else
            {
                PERR1("code not allowed", c);
            }
        }
    }

    void addCommand(Flow& f, const string& c, uint oktypes)
    {
        if (c.size())
        {
            if (Flow::t_command & oktypes)
            {
                auto ec = new Flow::EltCommand(c);
                ec->_lineno = lineno; // keep track of where we were
                f._elts.push_back(ec);
            }
            else
            {
                string m = "'";
                m += c;
                m += '\'';
                PERR1("command not allowed", m);
            }
        }
    }

    void _parseFlow(Flow& f, Flow::Type startType,
                    int allowednl, uint oktypes)
    {
        // allowednl controls whether newline breaks the flow
        // == 0 => no newlines allowed, newline will break
        // == 1 => blank line will end
        // > 1 => more than blank line with next start a NAME will end
        //
        // (hacky) allowednl can be a termination character for text only!
        //
        // oktypes controls what element types allowed in flow
        // -1 => any
        // otherwise a mask of Flow::Type
        skipws();

        Flow::Type t = startType;
        Flow::Type tn = Flow::t_void;

        const char* startpos = POS;
    
        while (t)
        {
            switch (t)
            {
            case Flow::t_text:
                {
                    uint last = '\n';
                    bool skipender = false;

                    bool esc = false;

                    PUSHSPAN;
                    
                    tn = Flow::t_void;
                    while (AT)
                    {
                        if (esc)
                        {
                            last = GETC;
                            esc = false;
                            continue;
                        }

                        if (AT == '\\')
                        {
                            esc = true;
                            BUMP;
                            continue;
                        }
                    
                        // special break condition
                        if (allowednl >= ' ' && AT == (unsigned)allowednl)
                        {
                            skipender = true;
                            break;
                        }
                        
                        // newline can end
                        if (AT == '\n' && allowednl == 0)
                        {
                            skipender = true; // consume nl
                            break;
                        }

                        if (last == '\n')
                        {
                            if (ATSELECTOR)
                            {
                                // if we hit a selector, we stop
                                if (POS != startpos)
                                {
                                    // but only if not at very start
                                    // the text can be bold markup
                                    // eg
                                    // FOO
                                    // * bar
                                    // * *do it!*
                                    break;
                                }
                            }
                            
                            if (AT == SYM_COMMAND)
                            {
                                // start of a command
                                tn = Flow::t_command;
                                break;
                            }
                            else if (AT == '\n')
                            {
                                if (allowednl == 1)
                                {
                                    // blank line ends
                                    skipender = true;
                                    break;
                                }

                                // peek
                                {
                                    PUSHP;
                                    BUMP;

                                    assert(!skipender);

                                    // if next line starts with a NAME
                                    if (Traits::atName(POS))
                                    {
                                        //int l = Traits::atName(POS);
                                        //printf("############## break at '%s'\n", string(POS, l).c_str());
                                        skipender = true;
                                    }

                                    POPP;
                                    if (skipender) break;
                                }
                            }
                        }
                        if (AT == '{')
                        {
                            // start of code
                            tn = Flow::t_code;
                            break;
                        }
                        last = GETC;
                    }
                    string s = POPSPAN;
                    addText(f, s, oktypes);

                    // end character to be consumed (eg # or nl)
                    if (skipender) BUMP;
                }
                break;
            case Flow::t_code:
                {
                    assert(AT == '{');
                    BUMP;
                    skipws();

                    PUSHSPAN;
                    
                    int level = 1;
                    int quote = 0;
                    int esc = 0;
                    while (AT)
                    {
                        if (esc)
                        {
                            esc = 0;
                        }
                        else
                        {
                            if (AT == '\\') esc = 1;
                            else if (AT == '"') quote = !quote;
                            else if (!quote)
                            {
                                if (AT == '{') ++level;
                                else if (AT == '}')
                                {
                                    if (!--level)
                                    {
                                        string c = trim(POPSPAN);
                                        addCode(f, c, oktypes);
                                    
                                        BUMP; // '}'

                                        // eat space and single newline after code
                                        skipws();
                                        if (AT == '\n') BUMP;

                                        // assume text follows code
                                        tn = Flow::t_text;

                                        // don't allow any more flow
                                        if (!allowednl) tn = Flow::t_void;
                                
                                        break;
                                    }
                                }
                            }
                        }
                        BUMP;
                    }
                }
                break;
            case Flow::t_command:
                {
                    // will always be at > unless we're calling
                    // parsecommandflow, in which case it is optional.
                    if (AT == SYM_COMMAND)
                    {
                        BUMP;
                        skipws();
                    }
                    
                    PUSHSPAN;

                    // commands must be on a single line
                    // but can be ended by a flow break '#'
                    char c;
                    for (;;)
                    {
                        c = AT;
                        if (!c || c == '\n' || c == SYM_FLOWBREAK) break;
                        BUMP;
                    }
                    
                    addCommand(f, trim(POPSPAN), oktypes);

                    // eat char after command
                    if (c) BUMP;
                    
                    // assume text follows code
                    tn = Flow::t_text;

                    // don't allow any more flow
                    // except for flow-breaks which can add further text
                    if (!allowednl && c != SYM_FLOWBREAK) tn = Flow::t_void;
                }
                break;
            }

            t = tn;
        }
    }

    void parseFlow(Flow& f, int allowednl, uint oktypes)
    {
        // parse flow assuming text starts
        _parseFlow(f, Flow::t_text, allowednl, oktypes);
    }

    void parseCommandFlow(Flow& f, int allowednl, uint oktypes)
    {
        // parse flow assuming command starts
        _parseFlow(f, Flow::t_command, allowednl, oktypes);
    }

    bool parseProps(Term* t)
    {
        // [?@\*]\+?[~&>=]{0-2}!?[ ]*\n

        // get the type indicator
        char c = AT;

        // type is generator by default
        switch (c)
        {
        case SYM_CHOICE:
            t->_type = Term::t_choice;
            break;
        case SYM_OBJECT:
            t->_type = Term::t_object;
            break;
        case SYM_ASSET:
            t->_type = Term::t_asset;
            t->_assetProps = new VarList;
            break;
        default:
            c = 0;
        }

        if (c)
        {
            BUMP;
            skipws();
        }

        switch (t->_type)
        {
        case Term::t_generator:
            {
                Term::RType ty = parseRType();
                if (ty) t->_rtype = ty;  // default is random
                ty = parseRType();
                if (ty) t->_rtypenext = ty; // default void
                
                if (t->_rtype == Term::t_linked)
                {
                    string name = parseName();
                    if (!name.empty())
                    {
                        _foreignTerms.emplace_back(ForeignTerm{ t, name });
                    }
                    else
                    {
                        ERR0("expected name of foreign term in " << t->_name);
                    }
                }

                for (;;)
                {
                    // [!>]

                    bool used = false;
                    
                    if (AT == SYM_STICKY)
                    {
                        used = true;
                        if (t->_rtype == Term::t_shuffle
                            || t->_rtype == Term::t_nonrandom)
                        {
                            // no point having state for term used once
                            LOG1("sticky term changed to random ", t->_name);
                            t->_rtype = Term::t_random;
                        }
                        if (t->_rtype == Term::t_sequence ||
                            t->_rtype == Term::t_all)
                        {
                            // no need for sequence, same as first match
                            LOG1("sticky term changed to first ", t->_name);
                            t->_rtype = Term::t_first;
                        }
            
                        t->sticky(true);
                    }
                    else if (AT == SYM_RUNNABLE_TERM)
                    {
                        used = true;

                        // when term is referenced in a conditional, we run the
                        // term, rather than testing for visit.
                        // Such terms are expected to return EXEC_TRUE or EXEC_FALSE
                        t->runnableTerm(true);  // set flag
                    }
                    else if (AT == SYM_TERMDEF_EXTENSION)
                    {
                        used = true;
                        
                        // extending a generator causes it to be replaced
                        // not extended.
                        _termDefExtension = true;
                    }

                    if (!used) break;
                    
                    BUMP;
                    skipws();
                }

                if (AT == SYM_COMMAND || AT == '{' || u_isalnum(AT)) 
                {
                    // optional input top flow
                    // can be text, code or command
                    parseFlow(t->_topflow, 0, -1); // consume nl

                    if (t->_rtype != Term::t_random)
                    {
                        // for now we don't have meaning for filters
                        // with state indicators
                        // insist on them being random
                        
                        LOG1("term with topflow changed to random ", t->_name);
                        t->_rtype = Term::t_random;
                        
                    }
                }
                else if (AT == '\n')
                {
                    BUMP;
                }
                else
                {
                    PERR1("unexpected property", AT);
                    return false;
                }
            }
            break;
        case Term::t_choice:

            // ? !>
            for (;;)
            {
                bool used = false;
                
                if (AT == SYM_STICKY)
                {
                    t->sticky(true);
                    used = true;
                }
                else if (AT == SYM_CMD_CHOICES)
                {
                    // signify reactors marked as choices will be elevated
                    // to choices.  eg FOO?>
                    t->cmdChoices(true);
                    used = true;
                }

                if (!used) break;
                
                BUMP;
                skipws();
            }

            if (u_isalnum(AT) || AT == '?')
            {
                // topflow on choice are inherited choices
                // including ?conditional
                parseFlow(t->_topflow, 0, Flow::t_term);
            }
            else if (AT == '\n')
            {
                BUMP;
            }
            else
            {
                PERR1("unexpected property", AT);
                return false;
            }
            
            break;
        case Term::t_object:
            {
                // must immediately follow
                if (AT == SYM_TERMDEF_EXTENSION)
                {
                    _termDefExtension = true;
                    BUMP;
                }
                
                // object followed by optional list of parent names
                // these all have to be term references
                parseFlow(t->_topflow, 0, Flow::t_term);
            }
            break;
        case Term::t_asset:
            // currently do not have any options or parents
            if (AT == '\n')
            {
                BUMP;
            }
            break;
        default:
            // type not handled
            assert(0);
        }

        return true;
    }

    Term* parseTermHead()
    {
        // NAME PROPS
        Term* t = 0;
        string name = parseName();
        if (!name.empty())
        {
            t = new Term(name);
            skipws();

            bool v = parseProps(t);
            if (!v)  { delete t; t = 0; }
        }
        return t;
    }

    uint parseChoiceFlags(Selector* s)
    {
        uint f = 0;

        for (;;)
        {
            char c = AT;
            if (c == SYM_FILLER) f |= Selector::c_filler;
            else if (c == SYM_ALWAYS) f |= Selector::c_always;
            else if (c == SYM_TERMINAL) f |= Selector::c_terminal;
            else break;

            BUMP;
            skipws();
        }

        if ((f & Selector::c_always) == 0)
        {
            // not always, default to once
            f |= Selector::c_once; // make once by default
        }

        if ((f & Selector::c_terminal) && (f & Selector::c_filler))
        {
            PERR1("selector cannot be both terminal and filler", s->id());
            f &= ~Selector::c_filler;
        }
    
        return f;
    }

    uint parseObjectFlags(Selector* s)
    {
        uint f = 0;
        for (;;)
        {
            char c = AT;
            if (c == SYM_ASCHOICE) f |= Selector::o_aschoice;
            else break;

            BUMP;
            skipws();
        }
        return f;
    }

    uint parseInputFlags(Selector* s)
    {
        uint f = 0;
        uint fi = Selector::i_input;
        for (;;)
        {
            char c = AT;
            if (c == SYM_INPUT)
            {
                f |= fi;
                fi <<= 1;  // NB: assume input flags in power2 sequence.
                if (fi > Selector::i_exec) break;
            }
            else break;

            BUMP;
            skipws();
        }
        return f;
    }

    enode* parseCond()
    {
        ParseExpr pe;
        enode* en = pe.parse(POS, lineno);
        SETPOS(pe.pos);
        return en;
    }

    void parseCondFlow(Flow& f)
    {
        enode* en = parseCond();
        if (en)
        {
            //LOG1("parseCondFlow ", en->toString());
            _addCond(f, en);
        }
    }

    bool parseAssetProp(Selector* s,
                        const string& name, const string& val)
    {
        // parse an asset property value string
        // `name` is the prop name and `val` the value string
        // syntax is:
        // value
        // v1,v2,... or "v1 v2 ... " with spaces
        // a single value is assigned directly and multiple values
        // are turned into a list.
        // val can be blank in which case "1" is assumed (ie true)
        //
        // properties can be expressions
        
        auto vs = s->_host->_assetProps;
        assert(vs);

        bool r = true;
        var v;

        if (!val.empty())
        {
            const char* vp = val.c_str();
            var::Format f;
            f._mapBool = true;
            f._parseStringNonComma = true;
            f._parseStringRejects = " ";

            while (r)
            {
                while (u_isspace(*vp)) ++vp;
                if (!*vp) break;
                
                var v1;
                r = v1.parse(&vp, &f);

                if (r)
                {
                    while (u_isspace(*vp)) ++vp;

                    // optional comma separator
                    if (*vp == ',')
                    {
                        ++vp;
                        while (u_isspace(*vp)) ++vp;
                    }

                    bool done = !*vp;
                    
                    if (v.isList() || !done)
                    {
                        // append or make a list if we are not done
                        v.append(v1);  // v1 donated
                    }
                    else
                    {
                        v = v1; // donate
                        break;
                    }
                    
                    //LOG1("parsing asset prop list ", v);

                    if (done) break;
                }
                else
                {
                    LOG1("failed to parse asset ", s->_host->_name << " prop:" << name << " val:" << val);
                }
            }
        }
        else
        {
            // default map to "true"
            v = 1;
        }
        
        if (r)
        {
            vs->add(name, v); // v can be null
        }
        return r;
    }
    
    void parseAssetSelector(Selector* s)
    {
        assert(s->_host && s->_host->isAsset());

        string propname;
        int c;

        // the first token is the property
        PUSHSPAN;
        for (;;)
        {
            c = AT;
            if (!c || c == '\n' || c == ' ') break;
            BUMP;
        }
        
        propname = POPSPAN;
        string propargs;

        if (c == ' ')
        {
            // expect arg

            //Flow f;
            //parseFlow(t->_topflow, 0, Flow::t_term | Flow::t_text | Flow::t_media); 
            skipws();
        
            // then optional property values
            // collect these into a normal string and then process
            for (;;)
            {
                c = AT;
                if (!c || c == '\n') break;
                propargs.push_back((char)c); // XX assume values are ascii
                BUMP;
            }
        }

        if (c == '\n') BUMP; // eat final newline

        //LOG1("asset ", s->_host->_name << " prop:" << propname << " args:" << propargs);
        parseAssetProp(s, propname, propargs);
    }
    
    Selector* parseSelector(Term* host)
    {
        // POS just past "*"
        // *=[+-!][<][?]
        Selector* s = new Selector(host);
        s->_lineno = lineno;

        uint f = 0;

        bool choice = host->isChoice();
        bool obj = host->isObject();
        bool gen = host->isGenerator();
        
        if (obj) f |= parseObjectFlags(s);

        // objects can have choice flags
        if (obj || choice) f |= parseChoiceFlags(s);

        // so that flags can be in any order
        if (obj) f |= parseObjectFlags(s);

        if (gen)
            f |= parseInputFlags(s);

        s->_flags = f;

        skipws();

        // NB: object reactions can have conditionals just like 
        // everything else.
        // NB: can assets have conditionals?
        if (AT == SYM_CONDITION)
        {
            // selector conditional prefix
            BUMP;
            
            parseCondFlow(s->_cond);
            skipws();
        }

        if (AT != '\n')
        {
            // the main selector flow
            // if we are an object, treat the selector as a command
            // otherwise it's just a text flow.
            if (obj)
                parseCommandFlow(s->_text, 0, -1);
            else if (host->isAsset())
            {
                // assets parse as "property" "value"
                parseAssetSelector(s);
            }
            else
                parseFlow(s->_text, 0, -1); // no newline, all types
        }
        else
        {
            // flow can be empty
            BUMP; // skip \n
            
            if (obj) LOG1("Object ", host->_name << " cannot have empty selector");
        }

        // followed by another selector or blank => no action
        if (!ATSELECTOR && AT != '\n')
        {
            parseFlow(s->_action, 1, -1);
        }
        else if (AT == '\n')
        {
            //if no action eat newline
            BUMP;
        }
        
        return s;
    }

    void parseSelectors(Term* t)
    {
        assert(ATSELECTOR);

        int sid = 0;
        while (AT == '*')
        {
            BUMP;
            Selector* s = parseSelector(t);
            s->_id = sid++;  // selector id is nth index
            t->_selectors.add(s);

            const char* p = nextNonBlank();
            if (*p == '*') _advance(p);
        }
    }

    bool parseBody(Term* t)
    {
        uint oktypes = -1;
        
        // can be empty flow
        if (!ATSELECTOR)
        {
            // headflow: only commands allowed for objects
            if (t->_type == Term::t_object) oktypes = Flow::t_command;

            //NB: currently assets have no head flow, but we'll parse it anyway
            parseFlow(t->_flow, 2, oktypes);
        }

        const char* p = nextNonBlank();
        // a selector

        if (SELECTOR(p))
        {
            _advance(p);
            parseSelectors(t);
        }

        bool canhavetail = !t->isObject() && !t->isAsset();

        // NB: can't have tail without selectors
        // NB: objects never have tails. assets do not have tails
        
        if (canhavetail &&
            AT != '\n' && t->_selectors.size() && !Traits::atName(POS))
        {
            parseFlow(t->_postflow, 2, -1);
        }

        return true;
    }

    Term* parseTermDef()
    {
        lastdef = POS;
        _termDefExtension = false;
        Term* t = parseTermHead(); // sets termDefExtension if apply.
        if (t)
        {
            bool nobody = (AT == '\n');
            if (nobody)
            {
                // blank line follows head, no body.
                // unless a selector
                
                BUMP;

                const char* p = nextNonBlank();
                if (SELECTOR(p))
                {
                    // allow header blanks before first selector
                    nobody = false;
                }
            }

            if (!nobody)
            {
                if (!parseBody(t)) { delete t; t = 0; }
            }
        }
        return t;
    }

    bool linkTerm(Flow::EltTerm* et, FlowVisitor& fv)
    {
        bool v = et->_term != 0;
        if (!v)
        {
            //LOG1("Linking ", *et << " from " << *fv._hostTerm);
            et->_term = Term::find(et->_name);
            if (et->_term) v = true;
            else if (fv._err)
            {
                assert(fv._hostTerm);
                ERR0("missing term '" << et->_name << "'; in term " << fv._hostTerm->toString()); 
            }
        }
        return v;
    }

    bool linkEnode(enode* en, FlowVisitor& fv)
    {
        bool v = true;
        if (en->isTermName())
        {
            en->_binding = Term::find(en->name());
            if (!en->_binding)
            {
                v = false;
                if (fv._err)
                {
                    ERR0("missing cond term '" << *en << '\'');
                }
            }
        }
        return v;
    }

    bool linkCond(Flow::EltCond* et, FlowVisitor& fv)
    {
        assert(et->_cond);
        bool v = true;
        for (enode::It it(et->_cond); it; ++it)
        {
            enode* en = const_cast<enode*>(it._n);
            if (!linkEnode(en, fv)) v = false;
        }

        return v;
    }
    
    bool linkFlow(Flow& f, FlowVisitor& fv)
    {
        bool v = true;
        for (auto i = f._elts.begin(); i != f._elts.end(); ++i)
        {
            Flow::Elt* e = i;
            if (e->_type == Flow::t_term)
            {
                Flow::EltTerm* et = (Flow::EltTerm*)e;
                if (!linkTerm(et, fv)) v = false;
            }
            else if (e->_type == Flow::t_cond)
            {
                Flow::EltCond* et = (Flow::EltCond*)e;
                if (!linkCond(et, fv)) v = false;
            }
        }
        return v;
    }

    bool linkTerms(bool err = true)
    {
        // if err, emit error messages
        using namespace std::placeholders;  
        FlowVisitor ff(std::bind(&ParseStrands::linkFlow, this, _1, _2));
        ff._err = err; 
        bool v = true;

        // all flows in all terms need to be resolved.
        for (auto t : Term::_allTerms)
            if (!t->visit(ff)) v = false;

        return v;
    }

    bool linkForeignTerms()
    {
        bool v = true;
        for (auto t: Term::_allTerms)
        {
            if (t->_rtype == Term::t_linked)
            {
                assert(t->isGenerator());  // currently.
                bool found = false;
                for (auto& lt : _foreignTerms)
                {
                    if (lt._src == t)
                    {
                        found = true;
                        Term* d = Term::find(lt._dstName);
                        if (d)
                        {
                            t->_rtypeForeignTerm = d;
                        }
                        else
                        {
                            PERR0(t->_name << " foreign term " << lt._dstName << " not found");
                            v = false;
                        }
                        break;
                    }
                }

                if (!found)
                {
                    PERR0("foreign link from term " << t->_name << " not found");
                    v = false;
                }
            }
        }
        return v;
    }

    bool validateTerms()
    {
        bool v = true;
        for (auto t: Term::_allTerms)
        {
            if (t->_type == Term::t_object)
            {
                Term::TermList parents;
                t->getStaticParents(parents);
                for (auto p : parents)
                {
                    if (p->isObject())
                    {
                        // mark parent as class.
                        // leaves are instances
                        p->isClass(true);
                    }
                    else if (!p->isChoice())
                    {
                        PERR0(t->_name << " parent " << p->_name << " not object nor choice");
                        v = false;
                    }
                }
            }
            else
            {
                // check all flows contain non-objects
                ;
            }
        }
        return v;
    }

#ifdef TOOLS_BUILD
    void collect()
    {
        for (;;)
        {
            _skipc();
            uint c = AT;
            if (!c) break;
            _sourceCollector.addU(c);
            BUMP;
        }

        // ensure first term of next file is treated as a term
        // need to two blanks + 1 in case file not ending in newline
        _sourceCollector.append("\n\n\n");
    }
#endif /// tools_build

    void mergeObjects(Term* dst, Term* src)
    {
        // extend existing term
        LOG3("Term being extended: ", dst->_name);
                                
        // take all the selectors from src and give them to dst
        // however, if there is a conflict, the old selector is overridden
        auto i = src->_selectors._selectors.begin();
        while (i != src->_selectors._selectors.end())
        {
            Selector* si = *i;
            Selector* sj = 0;

            // does dst have a selector with the same primary flow?
            bool match = false;
            auto j = dst->_selectors._selectors.begin();
            while (j != dst->_selectors._selectors.end())
            {
                sj = *j;
                if (*si == *sj)
                {
                    // yes!
                    match = true;
                    break;
                }
                ++j;
            }
            
            if (match)
            {
                // sj is dest matching selector to si
                // j is parked on sj
                //LOG1("merge object, conflict ", dst->_name << " at " << *sj << " with " << *si); 

                // remove sj from dst list
                dst->_selectors._selectors.erase(j);

                // and delete old selector
                delete sj;
            }
            
            // add in new selector
            dst->_selectors._selectors.push_back(si);
            ++i;
        }

        // we have moved all pointers to dst, so just empty src list
        src->_selectors._selectors.clear();
    }

    bool processString(const char* data)
    {
        bool ok = true;
            
        SETPOSSTART(data);
        SETPOS(data);

#ifdef TOOLS_BUILD        
        if (_collectSource)
        {
            collect();
            SETPOS(data);
        }
#endif        
        
        for (;;)
        {
            skipblank();

            if (!AT) break;
        
            Term* t = parseTermDef();
            if (t)
            {
                Term* tbase = Term::find(t->_name);
                if (tbase)
                {
                    if (_termDefExtension)
                    {
                        if (t->_type == tbase->_type)
                        {
                            if (t->isObject())
                            {
                                mergeObjects(tbase, t);
                                delete t; t = 0;
                            }
                            else
                            {
                                // currently can only extend obj or gen
                                assert(t->isGenerator());

                                LOG3("Term being replaced: ", t->_name);

                                // remove old term.
                                Term* t1 = Term::remove(tbase);
                                assert(t1 == tbase);

                                // and delete it
                                delete t1;

                                // t will be added below
                                
                            }
                        }
                        else
                        {
                            LOG1("ERROR: cannot extend different term ", t->_name);
                            ok = false;
                            delete t; t = 0;
                        }
                    }
                    else
                    {
                        PERR1("ERROR: Duplicate term ", t->_name);

                        // make duplicates fatal - so we see them!
                        ok = false;
                        delete t; t = 0;
                    }
                }
                else if (_termDefExtension)
                {
                    LOG1("WARNING: extending non-existant term ", t->_name);
                }

                if (t)
                {
                    if (!_startTerm) _startTerm = t;
                    Term::add(t);
                    if (_dump) LOG1("", t->toString());
                }
            }
            else
            {
                PERR0("expected term");
                ok = false;
                break;
            }
        }
        return ok;
    }

    char* loadFile(const string& name,
                   IFICtx* ctx = 0,                   
                   bool* wasCompressed = 0)
    {
        unsigned char* data = 0;
        lineno = 1;
        
#ifndef IFI_BUILD

        //LOG1("loadFile ", name << " prefix:" << _loadFilePrefix);

        // name can be without a suffix or it can be a file name
        string filenameCompressed =
            makePath(_loadFilePrefix,
                     changeSuffix(name, STORY_COMPRESSED_SUFFIX));

        string filenameNonCompressed = 
            makePath(_loadFilePrefix,
                     changeSuffix(name, STORY_SUFFIX));

        if (suffixOf(name).empty())
        {
            if (FD::existsFile(filenameCompressed))
            {
                _filename = filenameCompressed;
                LOG1("LoadFile, located story ", _filename);
            }
            else
            {
                // try non-compressed suffix
                _filename = filenameNonCompressed;
            }
        }
        else
        {
            _filename = makePath(_loadFilePrefix, name);
        }
        
        LOG3("processfile ", _filename);

        // first try to load from local filesystem.
        // if not present and we are IFI, then try fetching it.

        FD fd;
        if (fd.open(_filename))
        {
            // try reading compressed first
            size_t usize;
            data = readz(fd, &usize);

            if (data)
            {
                FD::Pos sz = usize;
                FD::removeDOSLines(data, sz);
                if (wasCompressed) *wasCompressed = true;
            }
            else
            {            
                FD::Pos fsize;
                data = fd.readAll(&fsize, true); // remove dos newlines
            }
        }
#endif        

#ifdef IFI_BUILD

        _filename = name;

        //LOG2("loadFile ", name << " prefix:" << _loadFilePrefix);

        string filenameCompressed =
            changeSuffix(name, STORY_COMPRESSED_SUFFIX);

        string filenameNonCompressed =
            changeSuffix(name, STORY_SUFFIX);
            
        if (ctx && ctx->_loader)
        {
            int sz;

            if (suffixOf(name).empty())
            {
                // first try compressed version
                _filename = filenameCompressed;
                LOG2("Using fallback ifi loader for ", _filename);
                data = (unsigned char*)ctx->_loader(_filename.c_str(), sz);
                    
                if (!data)
                {
                    // otherwise must be non-compressed or bust
                    _filename = filenameNonCompressed;
                }
            }

            if (!data)
            {
                LOG2("Using fallback ifi loader for ", _filename);
                data = (unsigned char*)ctx->_loader(_filename.c_str(), sz);
            }
                
            if (data)
            {
                Compress cp;
                int v = decompz(cp, data, sz);
                if (v < 0)
                {
                    // decompressed but failed.
                    delete data; data = 0;
                }
                else if (v > 0)
                {
                    delete data;
                    data = cp.donate(0);
                    sz = v;
                    if (wasCompressed) *wasCompressed = true;
                }
            }
                
            if (data)
            {
                FD::Pos size = sz;
                FD::removeDOSLines(data, size);
            }
        }
        else
        {
            LOG1("WARNING: loadFile, no context ", name);
        }
        
#endif // IFI_BUILD

        if (!data)
        {
            ERR1("Can't open input file", _filename);
        }
        return (char*)data;
    }

    bool loadFiles(const strings& files,
                   IFICtx* ctx = 0,
                   bool loadGameFiles = true)
    {
        // false if any file fails
        bool v = true;
        bool wasCompressed = false;

        for (auto& f : files)
        {
            char* data = loadFile(f, ctx, &wasCompressed);
            if (!data) break;

            v = processString(data);
            delete data;
            
            // dont auto load game files if compressed
            // as we assume this is a complete story in one
            if (wasCompressed) loadGameFiles = false;
        }

        if (v)
        {
            Term* t = 0;

            if (loadGameFiles)
            {
                // look for well-known term to list extra files
                t = Term::find(TERM_GAME_FILES);
            }

            if (t)
            {
                // we're too early to run flows, so just go through
                // the headflow and look for file media elements.
                // load these.
                for (auto i = t->_flow._elts.begin(); i != t->_flow._elts.end(); ++i)
                {
                    Flow::Elt* e = i;
                    if (e->_type == Flow::t_media)
                    {
                        Flow::EltMedia* em = (Flow::EltMedia*)e;
                        if (em->_mType == m_file)
                        {
                            // a game source file
                            char* data = loadFile(em->_filename, ctx);
                            if (data)
                            {
                                processString(data);
                                delete data;
                            }
                            else v = false;
                        }
                    }
                }
            }
        }
    
        return v;
    }

#ifdef TOOLS_BUILD

    bool validateMediaCollect(Flow& f, FlowVisitor& fv)
    {
        // collect all media for validation
        assert(fv._allMedia); // where to collect
        
        for (auto i = f._elts.begin(); i != f._elts.end(); ++i)
        {
            Flow::Elt* e = i;
            if (e->_type == Flow::t_media)
            {
                Flow::EltMedia* em = (Flow::EltMedia*)e;
                assert(!em->_filename.empty());
                FlowVisitor::addEltMedia(*fv._allMedia, em); 
            }
        }
        return true;
    }

    bool validateMedia(FlowVisitor::MediaSet& ms)
    {
        // provide a mediaset if we want to also collect all media
        
        bool v = true;
        LOG4("validate media", "");
        
        using namespace std::placeholders;  
        FlowVisitor ff(std::bind(&ParseStrands::validateMediaCollect, this, _1, _2));
        // if err, emit error messages
        ff._err = true;
        ff._allMedia = &ms;

        // all flows in all terms need to be resolved.
        for (auto t : Term::_allTerms)
            if (!t->visit(ff)) v = false;

        FlowVisitor::MediaSet::iterator it = ms.begin();
        while (it != ms.end())
        {
            Flow::EltMedia* em = (Flow::EltMedia*)*it;
            assert(!em->_filename.empty());

            string filepath;

            if (em->isAudio())
            {
                filepath = locateAudioResource(em->_filename);
            }
            else if (em->isImage() || em->isAnim() || em->isVideo())
            {
                filepath = locateImageResource(em->_filename);
            }
            else
            {
                // fallback
                filepath = em->_filename;
            }
                
            LOG3("validating media '", em->_filename << "' at " << filepath);

            if (FD::existsFile(filepath))
            {
                ++it;
            }
            else
            {
                v = false;
                LOG1("WARNING, Media does not exist '", em->_filename << "'" << " type:" << em->_mType);

                // remove from set
                it = ms.erase(it);
            }
        }

        return v;
    }
#endif // tools_build

    bool validate(FlowVisitor::MediaSet* ms = 0)
    {
        // ensure special terms are present
        Term::intern(TERM_TICK);
        Term::intern(TERM_LAST);
        Term::intern(TERM_IT);
        Term::intern(TERM_THAT);
        Term::intern(TERM_SYSTEM_VERSION);
        
        bool v = linkTerms() && linkForeignTerms();
        if (v)
        {
            v = validateTerms();

            // NB: can continue even if media not valid
            if (v)
            {
#ifdef TOOLS_BUILD
                if (ms) validateMedia(*ms);
#endif                
            }
            else
            {
                ERR0("terms do not validate");
            }
        }
        else
        {
            ERR0("terms do not link");
        }

        return v;
    }
    
};

}; // ST


