//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

// reserved words

#define SYM_NAME    "name"
#define SYM_LABEL   "label"
#define SYM_SET     "set"
#define SYM_PUT     "put"
#define SYM_SCOPEIN "scopein"

// special terms
#define TERM_PLAYER  "PLAYER"
#define TERM_THING  "THING"
#define TERM_GAME_FILES "GAME_FILES"
#define TERM_LAST   "LAST"
#define TERM_IT     "IT"
#define TERM_THAT   "THAT"
#define TERM_TICK   "TICK"
#define TERM_NOCANDO "ERROR_NOCANDO"
#define TERM_NOSUCH "ERROR_NOSUCH"
#define TERM_SYNTAX "ERROR_SYNTAX"
#define TERM_SCOPE  "SCOPE_SEED"
#define TERM_SYSTEM_VERSION "SYSTEM_VERSION"
#define TERM_SYSTEM_INIT "SYSTEM_INIT"

// special terms for save & load
#define TERM_POSTSAVE_OK "POSTSAVE_OK"
#define TERM_POSTSAVE_FAIL "POSTSAVE_FAIL"
#define TERM_POSTLOAD_OK "POSTLOAD_OK"
#define TERM_POSTLOAD_FAIL "POSTLOAD_FAIL"
#define TERM_POSTRESTART_OK  "POSTRESTART_OK"

// special properties
#define PROP_IN     "in"
#define PROP_INTO   "into"
#define PROP_ON     "on"
#define PROP_ONTO   "onto"
#define PROP_WITH   "with"
#define PROP_TO     "to"
#define PREPMOD_REALLY "really"

// match empty result
#define MATCH_NULL  "null"

// match whole set
#define MATCH_THEM  "them"

#define EXEC_SYNTAX "syntax"
#define EXEC_ALREADY "already"
#define EXEC_UNABLE  "unable"
#define EXEC_OK     "ok"
#define EXEC_TRUE   "yes"
#define EXEC_FALSE  "no"

#define GAME_METATAG_PREFIX "GAME_"

// media
#define MEDIA_ATTR_VOICE        "voice"
#define MEDIA_ATTR_SPEECH       "speech"

// used for promoting masks to clickable poly
#define MEDIA_ATTR_CLICK        "click"



