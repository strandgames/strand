//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#if defined(_MSC_VER)

#define BUILD_VER "(MSVC) " __DATE__ "/" __TIME__

#elif defined(__EMSCRIPTEN__)

//#define BUILD_VER "(em) " __DATE__
#define BUILD_VER __DATE__ 

#elif defined(__GNUC__)

//#define BUILD_VER "(GCC) " __DATE__ "/" __TIME__
//#define BUILD_VER "(gcc) " __DATE__
#define BUILD_VER  __DATE__

#else
// dummy otherwise
#define BUILD_VER ""
#endif

//#define VERSION "1.1.10"

// 1.2 has sokol with separate samplers
//#define VERSION "1.2.1"

// autopager
//#define VERSION "1.2.2"

// autopager small "more"
//#define VERSION "1.2.3"

// image versions
//#define VERSION "1.2.4"

// new outlines. fix for text-more-click
//#define VERSION "1.2.5"

// updated tex memory management
//#define VERSION "1.2.6"

// IMGui 1.90.8 font rasterized to dpi
//#define VERSION "1.2.7"

// sokol updated 1 June 2024
//#define VERSION "1.2.8"

// zoom fixes arrow key choice select
//#define VERSION "1.2.9"

// encrypt story
//#define VERSION "1.2.11"

// video playback
//#define VERSION "1.2.12"

// fix throwing away font atlas
//#define VERSION "1.2.13"

// new VN image shader with taint
//#define VERSION "1.2.14"

// initial size on windows 7/10 also release version of Teapot
//#define VERSION "1.2.15"

// reinstate "-log" option
#define VERSION "1.2.16"





