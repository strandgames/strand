//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include "point.h"
#include "var.h"
#include "jsonwalker.h"

struct SpineInfo
{
    typedef std::string string;
    Point2f _dim;
    Point2f _offset;

    Point2f effectiveDim() const
    {
        return _dim;
    }

    friend std::ostream& operator<<(std::ostream& os, const SpineInfo& si)
    {
        return os << "dim: " << si._dim << " offset:" << si._offset;
    }

    bool readFrom(const string& anim)
    {
        bool res = false;
        
        for (JSONWalker jw(anim); jw.nextKey(); jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st) break; // bad json

            // keys used here are from the Spine file not from our schema
            if (jw._key == "skeleton")
            {
                if (isObject)
                {
                    string skel;
                    if (jw.atObj(skel))
                    {
                        for (JSONWalker jw(skel); jw.nextKey(); jw.next())
                        {
                            const char* st = jw.checkValue(isObject);
                            if (!st) break; // bad json
                            
                            if (!isObject)
                            {
                                var v = jw.collectValue(st);
                                if (jw._key == "width")
                                {
                                    _dim.x = v.toDouble();
                                    res = true;
                                }
                                else if (jw._key == "height")
                                {
                                    _dim.y = v.toDouble();
                                    res = true;
                                }
                                else if (jw._key == "x") _offset.x = v.toDouble();
                                else if (jw._key == "y") _offset.y = v.toDouble();
                            }
                            
                        }
                    }
                }
                break;
            }
        }
        return res;
    }
};
    
