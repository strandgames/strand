# How To Build


## Windows

### Prerequisites

Use gcc to build (see also):
http://www.msys2.org/ and https://www.msys2.org/#installation

* download
`msys2-x86_64_*.exe`

* install to (eg)
`d:\msys64`

* run shell
`d:\msys64\mingw64`

* update
`pacman -Syu`
close shell, run again
`pacman -Su`

* install tools
```
pacman -S --needed base-devel mingw-w64-x86_64-toolchain
pacman -S --needed ctags vim zip unzip git make
```

* change PATH
```
d:\msys64\usr\bin
d:\msys64\mingw64\bin
```

Changing the path means you can close the MSYS shell and work in a Windows shell. Alternatively you can work in the MSYS shell and not have to change the `PATH`.

### Building

Working from the top level `strands` directory;

```
make WINDOWS=1
```

This will build the `strands` command line program. This is still needed for building the GUI version.

Then build the `imgui` version.

```
cd imgui
make WINDOWS=1
```

This should build `imgui\game.exe` putting the objects in `imgui\obj`

## Mac

```
cd imgui
make MACOS=1
```


## Linux

### Prerequisites

These instructions are for Ubuntu.

Make sure you have a build environment:

sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install libx11-dev
sudo apt-get install libxi-dev
sudo apt-get install libxcursor-dev
sudo apt-get install libgl-dev
sudo apt-get install libasound2-dev

### Building

Working from the top level `strands` directory;

```
make LINUX=1
```

This will build the `strands` command line program. This is still needed for building the GUI version.

Then build the `imgui` version.

```
cd imgui
make LINUX=1
```

This should build `imgui/game` putting the objects in `imgui/obj`

### Running

See `README.md` for more details. Here's how you can checkout from cold, then run a game.

Windows:

```
mkdir if
cd if
git clone strandgames.com:/git/strand.git
cd strand\strands
make WINDOWS=1
cd imgui
make WINDOWS=1
cd ..\..\..
git clone strandgames.com:/git/ifideas.git
cd ifideas\picton\src
edit gobin.bat
change top line to:
set TOP=..\..\..\strand
gobin
```

Linux:

```
cd
mkdir if
cd if
git clone strandgames.com:/git/strand.git
cd strand/strands
make LINUX=1
cd imgui
make LINUX=1
cd ../../..
git clone strandgames.com:/git/ifideas.git
cd ifideas/picton/src
vi gobin.sh
change top line to:
TOP=../../../strand
bash gobin.sh
```

























