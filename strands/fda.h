//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include "fd.h"

#ifdef __ANDROID__

#include "androidutils.h"
#include "logged.h"

// android aware file loader
struct FDLoadAndroid
{
    FDLoadAndroid(const char* fn, bool removeDos = false)
    { _init(fn, removeDos); }
    FDLoadAndroid(const std::string& fn, bool removeDos = false)
    { _init(fn.c_str(), removeDos); }

    ~FDLoadAndroid() { delete _data; }

    FD                  _fd;
    FD::Pos             _size;
    unsigned char*      _data = 0;

    operator bool() const { return _data != 0; }

    unsigned char* donate()
    {
        unsigned char* d = _data;
        _data = 0;
        _size = 0;
        return d;
    }

    void _init(const char* fn, bool removeDos)
    {
        // try for regular file
        if (_fd.open(fn))
        {
            _data = _fd.readAll(&_size, removeDos);
        }
        else
        {
            // load from assets?
            unsigned int sz = 0;
            _data = android_loadfile(fn, sz);
            _size = sz;
            
            if (_data)
            {
                if (removeDos) FD::removeDOSLines(_data, _size);
            }
        }
    }

};

struct FDAndroid: public FDTraits
{
    ANativeActivity* _act = 0;
    AAsset* _fd = 0;
    string  _filename;

    FDAndroid()
    {
        _act = (ANativeActivity*)sapp_android_get_native_activity();
        assert(_act);
    }
    
    ~FDAndroid() { close(); }
    
    static bool existsFile(const char* path)
    {
        bool v = FD::existsFile(path);
        if (!v)
        {
            FDAndroid f;
            v = f.open(path);
            f.close();
        }
        return v;
    }

    static bool existsFile(const std::string& path)
    { return existsFile(path.c_str()); }

    static bool existsDir(const char* path)
    {
        return false;
    }

    static bool existsDir(const string& path)
    { return existsDir(path.c_str()); }

    operator bool() const { return isOpen(); }

    bool isOpen() const
    {
        return _fd != 0;
    }

    bool open(const char* path, int flags = fd_read)
    {
        _filename = path;
        
        AAssetManager* am = _act->assetManager;
        assert(am);

        // Open your file
        _fd = AAssetManager_open(am, path, AASSET_MODE_BUFFER);
        bool v = isOpen();
        //LOG1(">> FDA open ", path << " = " << v);
        return v;
    }

    void close()
    {
        if (isOpen())
        {
            AAsset_close(_fd);
            _fd = 0;
        }
    }

    bool read(unsigned char* buf, size_t amt, size_t& nread)
    {
        bool v = isOpen();
        if (v)
        {
            nread = AAsset_read(_fd, buf, amt);
            //LOG1("FDA read ", _filename << " = " << nread);
            v = nread >= 0;
        }
        return v;
    }

    bool read(unsigned char* buf, size_t amt)
    {
        // return if no error and read amt bytes
        size_t nr;
        return read(buf, amt, nr) && nr == amt;
    }

    bool write(const unsigned char* buf, size_t amt, size_t& nwrote)
    {
        LOG1("WARNING FDA write not supported ", _filename);
        return false;
    }

    bool write(const unsigned char* buf, size_t amt)
    {
        size_t nw;
        return write(buf, amt, nw);
    }

    static void removeDOSLines(unsigned char* data, Pos& sz)
    {
        return FD::removeDOSLines(data, sz);
    }

    bool seek(Pos pos)
    {
        return isOpen() && AAsset_seek(_fd, pos, SEEK_SET) >= 0;
    }
    
};

#define FDLoad FDLoadAndroid
#define FD FDAndroid

#else


#endif // __ANDROID__
