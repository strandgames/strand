//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once


#include <iostream>
#include <string.h> // memmove
#include <stdlib.h>
#include <assert.h>

// make a RRect from a `Rect`
#define MK_RRECT(_r) RRect((_r)._x, (_r)._y, (_r)._w, (_r)._h)

struct RRect
{
    RRect() {}
    RRect(int x, int y, int w, int h)
    {
        x1 = x;
        y1 = y;
        x2 = x + w;
        y2 = y + h;
    }

    int width() const { return x2 - x1; }
    int height() const { return y2 - y1; }
    
    int x1, y1, x2, y2;

    bool operator==(const RRect& r) const
    {
        return x1 == r.x1 && x2 == r.x2 && y1 == r.y1 && y2 == r.y2;
    }

    friend std::ostream& operator<<(std::ostream& os, const RRect& r)
    {
        return os << '(' << r.x1 << ',' << r.y1 << ")-(" << r.x2 << ',' << r.y2 << ')';
    }

    void expand(int dx, int dy)
    {
        // expand by dx or contract by -dx
        x1 -= dx;
        y1 -= dy;
        x2 += dx;
        y2 += dy;

        if (x2 < x1) x2 = x1;
        if (y2 < y1) y2 = y1;
    }
};

#define RECTS_INTERSECT(r1, r2) \
    ((r1)->x2 > (r2)->x1 && (r1)->x1 < (r2)->x2 && \
     (r1)->y2 > (r2)->y1 && (r1)->y1 < (r2)->y2)

/* This describes the internal representation of a Region.
   'rects' points at an array of 'size' rectangles of which
   'numRects' are used. These rectangles are sorted in a
   YXBanded manner.
   'extents' is the smallest rectangle which covers the region.

   As a special case to save memory, if 'numRects' is 0 or 1, then
   'size' can be 0 in which case rects points at 'extents'. */

typedef enum { RECT_OUT, RECT_IN, RECT_PART } RectInResult;

#define free_rects(_p) free(_p)

namespace ST
{

struct Region
{
    int         _size;   // space for rects not actual rects
    int         numRects;
    RRect*      rects;
    RRect       extents;
    
    // Constructors
    Region() { _init(0, 0); }
    Region(const RRect& r) { _init(&r, 1); }
    Region(const Region& r) { _size = 0; _copy(this, &r); }

    // Destructor
    ~Region() { _destroy(); }

    Region& operator=(const Region& r) 
    {
        if (this != &r)
            _copy(this, &r);
        return *this;
    }

    const RRect& extent() const { return extents; }
    unsigned int      size() const { return numRects; }
    const RRect& operator[](int i) const { return rects[i]; }
    operator bool() const { return numRects > 0; }
    
    void create(RRect *r, int size) 
    {
        _destroy();
        _init(r, size); 
    }

    void reset(RRect* rp);
    void reset() 
    {
        _destroy();
        _init(0,0);
    }

    void translate(int dx, int dy);
    void offsetAll(int dx, int dy) { translate(dx, dy); }
    void print();
    bool pointInside(int x, int y, RRect** bound) const;
    RectInResult rectInside(const RRect* rect) const;

    bool contains(const RRect& r) const { return rectInside(&r) == RECT_IN; }
    bool contains(int x, int y) const
    {
        RRect* rp;
        return pointInside(x, y, &rp);
    }
                           
    friend Region operator+(const Region& a, const RRect& r)
    {
        Region c;
        Region t(r);
        UnionRegion(&c, &a, &t);
        return c;        
    }

    friend Region operator+(const Region& a, const Region& b)
    {
        Region c;
        UnionRegion(&c, &a, &b);
        return c;
    }

    void operator+=(const RRect& r)
    {
        Region t(r);
        UnionRegion(this, this, &t);
    }

    void operator+=(const Region& b) { UnionRegion(this, this, &b); }


    friend Region operator-(const Region& a, const RRect& r) 
    {
        Region c;
        Region t(r);
        SubtractRegion(&c, &a, &t);
        return c;        
    }

    friend Region operator-(const Region& a, const Region& b)
    {
        Region c;
        SubtractRegion(&c, &a, &b);
        return c;
    }

    void operator-=(const RRect& r)
    {
        Region t(r);
        SubtractRegion(this, this, &t);
    }

    void operator-=(const Region& b) { SubtractRegion(this, this, &b); }
    bool isEmpty() const  { return numRects == 0; }

    // this only expands the extent boundary
    void expand(int left, int right, int top, int bot);

    // expand to minx/maxx 
    void expandMinMax(int minx, int maxx);
    void fillHoles();

    static void UnionRegion(Region* newr,
                            const Region* src1, const Region* src2);
    static void IntersectRegion(Region* newr,
                                const Region* src1, const Region* src2);
    static bool SubtractRegion(Region* newr,
                               const Region* src1, const Region* src2);
    static void InvertRegion(Region* newr,
                             const Region* src, const RRect* bounds);

    friend std::ostream& operator<<(std::ostream& os, const Region& r)
    {
        int nt = r.size();
        os << '{';
        for (int i = 0; i < nt; ++i)
        {
            if (i) os << ' ';
            os << r[i];
        }
        return os << '}';
    }

    void scaled(Region& r, float sx, float sy) const
    {
        // make a scaled version
        // this is not the same as scaling all the boxes
        // since they can merge together.
        for (int i = 0; i < numRects; ++i)
        {
            RRect ri = rects[i];
            ri.x1 = ri.x1 * sx + 0.5f;
            ri.x2 = ri.x2 * sx + 0.5f;
            ri.y1 = ri.y1 * sy + 0.5f;
            ri.y2 = ri.y2 * sy + 0.5f;
            r += ri;
        }        
    }

    void scaleUp(float sx, float sy)
    {
        // can just expand boxes
        for (int i = 0; i < numRects; ++i)
        {
            RRect& ri = rects[i];
            ri.x1 *= sx;
            ri.x2 *= sx;
            ri.y1 *= sy;
            ri.y2 *= sy;
        }

        extents.x1 *= sx;
        extents.y1 *= sy;
        extents.x2 *= sx;
        extents.y2 *= sy;
    }

    void quantize(float f)
    {
        Region rt;
        scaled(rt, 1/f, 1/f);  // scale down and contract 
        rt.scaleUp(f, f);  // scale back up
        *this = rt;
    }

    void expandBy(int dx, int dy)
    {
        // expand, but cannot contract dx and dy at both sides
        assert(dx >= 0 && dy >= 0);
        expand(0,dx,0,dy);
    }

#if 0
    void contractBy(Region& r, int dx, int dy)
    {
        // calculate extent - region = bits not covered
        Region b(extents);
        SubtractRegion(&b, &b, this);

        // expand the non covered region by delta
        Region c;
        b.expandBy(c, dx, dy);

        // shrink the original extent by contract delta
        RRect e = extents;
        e.expand(-dx, -dy);

        // subtract the expanded non-cover from the shrunk extent
        Region er(e);
        SubtractRegion(&r, &er, &c);
    }
#endif


    bool contigious() const
    {
        // no gaps in y
        // XX what about gaps in X ??
        if (numRects)
        {
            int y = rects[0].y2;
        
            // banded by Y, then by X
            for (int i = 1; i < numRects; ++i)
            {
                if (rects[i].y1 > y) return false; // gap
                y = rects[i].y2;
            }
        }
        return true;
    }

    void recalcExtent();
    
    
private:

    bool _validate();
    void _init(const RRect* r, int size);

    void _destroy()
    {
        if (_size > 0)
        {
            free_rects(rects);
            _size = 0;
        }
        numRects = 0;
        rects = &extents;
    }

    static void _copy(Region* dst, const Region* src);

};

} // ST

