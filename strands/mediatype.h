//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include "strutils.h"

struct MediaTraits
{
    typedef std::string string;

    enum MediaType
    {
        m_void = 0,
        m_image,
        m_audio,
        m_video,
        m_file,
        m_animation,
    };

    static MediaType mediaSuffix(const string& s)
    {
        MediaType m = m_void;
        
        static const char* imageSufTab[] =
            {
                ".jpg",
                ".jpeg",
                ".png",
                ".apng",
                ".webp",
                ".bmp",
            };

        static const char* audioSufTab[] =
            {
                ".wav",
                ".ogg",
                ".mp3",
            };

        static const char* videoSufTab[] =
            {
                ".mp4",
                ".mkv",
                ".mpg",
                ".mpeg",
            };

        static const char* fileSufTab[] =
            {
                ".str",
                ".txt",  // allow plain text to be used as game files
            };

        static const char* animSufTab[] =
            {
                ".json",
                ".atlas",
            };

        if (inTable(imageSufTab, ASIZE(imageSufTab), s)) m = m_image;
        else if (inTable(audioSufTab, ASIZE(audioSufTab), s)) m = m_audio;
        else if (inTable(videoSufTab, ASIZE(videoSufTab), s)) m = m_video;
        else if (inTable(fileSufTab, ASIZE(fileSufTab), s)) m = m_file;
        else if (inTable(animSufTab, ASIZE(animSufTab), s)) m = m_animation;
        return m;
    }

    static const char* mediaTypeName(MediaType m)
    {
        const char* tname;
        switch (m)
        {
        case m_image:
            tname = "image";
            break;
        case m_audio:
            tname = "audio";
            break;
        case m_video:
            tname = "video";
            break;
        case m_file:
            tname = "file";
            break;
        case m_animation:
            tname = "animation";
            break;
        default:
            tname = "void";
            break;
        }
        return tname;
    }

    static bool inTable(const char** table, uint sz, const char* s)
    {
        while (sz)
        {
            if (equalsIgnoreCase(*table, s)) return true;
            --sz;
            ++table;
        }
        return false;
    }

    static bool inTable(const char** table, uint sz, const string& s)
    {
        return inTable(table, sz, s.c_str());
    }

};
