//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#define IFI_COMMAND     "command"
#define IFI_CONFIGDIR   "configdir"
#define IFI_DATADIR     "datadir"
#define IFI_STORY       "story"
#define IFI_OBJECTS     "objects"
#define IFI_SUBCOMMAND  "subcommand"

#define IFI_LOADDATA    "loaddata"
#define IFI_SAVEDATA    "savedata"
#define IFI_RESUME      "resume"
#define IFI_MAP         "map"
#define IFI_PICTURE     "picture"
#define IFI_SOUND       "sound"
#define IFI_ITEMS       "items"
#define IFI_PEOPLE      "people"
#define IFI_META        "meta"
#define IFI_MOVES       "moves"
#define IFI_VALUES      "values"
#define IFI_RANDOMSEED  "randomseed"
#define IFI_DATA        "data"
#define IFI_BEGIN       "begin"
#define IFI_RESTART     "restart"
#define IFI_ANIMATE     "animate"
#define IFI_POSTSAVE    "postsave"
#define IFI_POSTLOAD    "postload"
#define IFI_REPORT      "report"

// reply only

#define IFI_TEXT        "text"
#define IFI_TITLE       "title"
#define IFI_EXITS       "exits"
#define IFI_PROMPT      "prompt"
#define IFI_NAME        "name"
#define IFI_ID          "id"
#define IFI_WORN        "worn"
#define IFI_ICON        "icon"
#define IFI_PLACES      "places"
#define IFI_BACKIMAGE   "backimage"
#define IFI_GX          "gx"
#define IFI_GY          "gy"
#define IFI_DARK        "dark"
#define IFI_SHOWUP      "up"
#define IFI_SHOWDOWN    "down"
#define IFI_LOCATION    "location"
#define IFI_UNUSE       "unuse"
#define IFI_USEWITH     "usewith"
#define IFI_USE         "use"
#define IFI_COMPASSGO   "compassgo"
#define IFI_REFRESHCMD  "refreshcmd"
#define IFI_CHOICE      "choice"
#define IFI_ASSET       "asset"

// choiceobj
#define IFI_CHOSEN      "chosen"
#define IFI_ENABLED     "enabled"

// metaobj
//#define IFI_TITLE       "title"
#define IFI_AUTHOR      "author"
#define IFI_ORGANISATION "organisation"
#define IFI_CREDITS     "credits"
#define IFI_COVERTEXT   "covertext"
#define IFI_VERSION     "version"
#define IFI_ANDROID_MARKET "android_market"
#define IFI_IOS_MARKET  "ios_market"
#define IFI_EFFECT      "effect"
#define IFI_UI_SIDEBAR  "ui_sidebar"
#define IFI_UI_TEXTINPUT "ui_textinput"
#define IFI_UI_TEXTINPUTFORCE "ui_textinput_force"
#define IFI_UI_COMPASS  "ui_compass"
#define IFI_UI_UCANVAS  "ui_ucanvas"
//#define IFI_BACKIMAGE   "backimage"
#define IFI_PRIME_COL   "primary_color"
#define IFI_CONTRAST_COL "contrast_color"
#define IFI_AUTOLINK    "autolink"
#define IFI_UI_RESTART  "ui_restart"
#define IFI_SAVELOAD    "saveload"
#define IFI_LAYOUT      "layout"
#define IFI_ICONNAME    "iconname"

// XX what is this?
//#define IFI_SAVELOADCHOICE "saveloadchoice"

//#define IFI_TITLE       "title"

// textobj
//#define IFI_TEXT      "text"
//#define IFI_ID        "id"
#define IFI_FONT       "font"
#define IFI_FONTWEIGHT  "fontweight"
#define IFI_COLOR       "color"

// soundobj
//#define IFI_NAME        "name"
#define IFI_DURATION    "duration"
#define IFI_CHANNEL     "channel"
#define IFI_VOLUME      "volume"
#define IFI_PRELOAD     "preload"

// pictureobj
//#define IFI_NAME        "name"
#define IFI_BRIGHTNESS  "brightness"
#define IFI_CONTRAST    "contrast"
#define IFI_SATURATION  "saturation"
#define IFI_LIGHTNESS   "lightness"
#define IFI_GAMMA       "gamma"
#define IFI_IMGTAG      "tag"
// fullscreen?
#define IFI_LARGE       "large"
#define IFI_TEXTWRAP    "textwrap"
//#define IFI_SIZE        "size"
#define IFI_INTEXT      "intext"

//#define IFI_PRELOAD     "preload"

// animobj
//#define IFI_NAME        "name"
#define IFI_ATLAS         "atlas"
#define IFI_PLAY          "play"
#define IFI_TRACK         "track"
#define IFI_LOOP          "loop"
#define IFI_DELAY         "delay"
#define IFI_HIDE          "hide"
#define IFI_SHOW          "show"
// atlas image
#define IFI_IMAGE         "image"
//#define IFI_BACKIMAGE   "backimage"

// person
//#define IFI_ID          "id"
//#define IFI_NAME        "name"
//#define IFI_ICON        "icon"
#define IFI_PERSONACTION  "action"
#define IFI_DIRECTION     "direction"

// masks for directions
#define IFI_DIR_N       1
#define IFI_DIR_NE      2
#define IFI_DIR_E       4
#define IFI_DIR_SE      8
#define IFI_DIR_S       16
#define IFI_DIR_SW      32
#define IFI_DIR_W       64
#define IFI_DIR_NW      128
#define IFI_DIR_UP      256
#define IFI_DIR_DOWN    512
#define IFI_DIR_IN      1024
#define IFI_DIR_OUT     2048

// imagemeta
#define IFI_CLICKABLES  "clickables"
//#define IFI_NAME        "name"
#define IFI_IMAGEMETA  "imagemeta"
#define IFI_WIDTH       "width"
#define IFI_HEIGHT      "height"
#define IFI_VERTS      "verts"
#define IFI_POLY       "poly"
#define IFI_POLYS      "polys"
#define IFI_ZORDER     "z"
#define IFI_MASK        "mask"
#define IFI_SIZE        "size"
#define IFI_RECTS        "rects"
//#define IFI_SHOW          "show"
//#define IFI_COLOR       "color
#define IFI_ORIGINAL    "original"
#define IFI_VERSIONS    "versions"
#define IFI_PRESENT     "present"
#define IFI_FILESIZE    "filesize"


// ASSETS
#define IFI_RESOURCE    "resource"
#define IFI_PARENT      "parent"
#define IFI_POS         "pos"
#define IFI_Z           "z"
#define IFI_ALPHA       "alpha"
#define IFI_FADEIN      "fadein"
#define IFI_FADEOUT     "fadeout"
#define IFI_AT          "at"
#define IFI_BY          "by"
#define IFI_ON          "on"
#define IFI_ERASE       "erase"
#define IFI_SCALE       "scale"
#define IFI_BACKGROUND  "background"
#define IFI_XFADE       "xfade"
#define IFI_FIDGETBOX   "fidgetbox"
#define IFI_ZOOM        "zoom"
#define IFI_CYCLE       "cycle"
#define IFI_ROTATE      "rotate"
#define IFI_ORIGIN      "origin"
#define IFI_ASPECT      "aspect"
#define IFI_CLICK       "click"
#define IFI_PROPS       "props"
#define IFI_BREATHE     "breathe"
#define IFI_TAPER       "taper"
#define IFI_TINT        "tint"
//#define IFI_SOUND       "sound"
#define IFI_MODE        "mode"
#define IFI_FACTOR      "factor"
#define IFI_INTENSITY   "intensity"
#define IFI_ONESHOT     "oneshot"
#define IFI_VIDEOLOOP   "videoloop"
#define IFI_CONTINUE    "continue"

// properties
#define IFI_LEFT        "left"
#define IFI_RIGHT       "right"
#define IFI_CENTRE      "centre"
#define IFI_SELF        "self"
#define IFI_PUSH        "push"
#define IFI_POP         "pop"
#define IFI_AREA        "area"
#define IFI_RATE        "rate"
#define IFI_MIPMAP      "mipmap"

// easing
#define IFI_EASEINQUAD  "easeinquad"
#define IFI_EASEOUTCUBIC  "easeoutcubic"
#define IFI_EASEOUTQUART  "easeoutquart"
#define IFI_EASEOUTQUAD  "easeoutquad"
#define IFI_EASEINCUBIC  "easeincubic"
#define IFI_EASEINQUART  "easeinquart"
#define IFI_EASEINOUTQUAD  "easeinoutquad"
#define IFI_EASEINOUTCUBIC  "easeinoutcubic"
#define IFI_EASEINOUTQUART  "easeinoutquart"
#define IFI_EASEOUTBACK "easeoutback"
#define IFI_EASEINOUTBACK "easeinoutback"
#define IFI_EASEOUTELASTIC "easeoutelastic"
#define IFI_EASEINBOUNCE "easeinbounce"
#define IFI_EASEOUTBOUNCE "easeoutbounce"

//#define IFI_IMAGE         "image"
//#define IFI_LOOP        "loop"
//#define IFI_NAME        "name"

///// Eval Special Commands

// drop from menu
#define IFI_UNUSE_DEFAULT "unuse {1:name}"

// for use X with Y
#define IFI_USEWITH_DEFAULT "use {1:name} with {2:name}"

// click on sidebar
#define IFI_USE_DEFAULT "use {1:name}"

#define IFI_COMPASSGO_DEFAULT "{1:id}"

#define IFI_REFRESHCMD_DEFAULT "look"



