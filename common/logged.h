//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//


#pragma once

#include <iostream>
#include <mutex>
#include <sstream>
#include "sfstream.h"

#ifdef __ANDROID__
#include "androidout.h"
#endif

template<class T> struct LogBase
{
    static int         _logLevel;
};

template<class T> int LogBase<T>::_logLevel = 1;

inline void padspace(std::basic_ostream<char,std::char_traits<char> >& os,
                     int val)
{
    while (val)
    {
        --val;
        os << ' ';
    }
}

template<class T> struct LogFiler: public LogBase<T>
{
#ifdef __ANDROID__
    static AndroidOut          _androidOut;
    static std::ostream        _stream;
#else    
    static StdFStream          _stream;
#endif    

#ifdef LOG_MT
    static std::mutex          _mutex;
#endif

#ifdef __ANDROID__
    LogFiler() {}
#else    
    LogFiler()
    {
#ifndef NDEBUG
        // default we copy to console, except for release build which
        // is not attached to console. instead use "-log" option to write 
        // to file in release mode
        setAlso(&std::cout);
#endif
    }
    
    static void setAlso(std::ostream* os)
    {
        // allow console output to be changed or suppressed (if 0)
        _stream.setAlso(os);
    }

    static bool openFile(const char* name)
    {
        // NB: non static method for that initialisation is performed
        // append to any existing log
        return _stream.open(name, "a");
    }
#endif // __ANDROID__
    
};

#ifdef __ANDROID__
template<class T> AndroidOut LogFiler<T>::_androidOut("Strand");
template<class T> std::ostream LogFiler<T>::_stream(&LogFiler<T>::_androidOut);
#else
template<class T> StdFStream LogFiler<T>::_stream;
#endif

#ifdef LOG_MT
template<class T> std::mutex LogFiler<T>::_mutex;
#endif

typedef LogFiler<int> Logged;

#define LOGBOOL(_b) (_b ? "true" : "false")

#ifdef LOG_MT
#define LOG(_msg, _x) \
{                                                               \
std::lock_guard<std::mutex> lock(Logged::_mutex);               \
Logged::_stream << _msg << _x << std::endl << std::flush;       \
}

#else
#define LOG(_msg, _x) \
    Logged::_stream << _msg << _x << std::endl << std::flush
#endif // LOG_MT

#define LOGN(_n, _msg, _x) if (Logged::_logLevel >= _n) LOG(_msg, _x)


#if defined(LOGGING)

#define LOG1(_msg, _x) LOGN(1, _msg, _x)
#define LOG2(_msg, _x) LOGN(2, _msg, _x)
#define LOG3(_msg, _x) LOGN(3, _msg, _x)
#define LOG4(_msg, _x) LOGN(4, _msg, _x)
#define LOG5(_msg, _x) LOGN(5, _msg, _x)

#else

// allow LOG1 to go into release builds
#define LOG1(_msg, _x) LOGN(1, _msg, _x)
#define LOG2(_msg, _x)
#define LOG3(_msg, _x)
#define LOG4(_msg, _x)
#define LOG5(_msg, _x)

#endif

