
#pragma once

static inline void wbyte16(unsigned int v, unsigned char* p)
{
    *p++ = (char)v;
    *p = (char)(v >> 8);
}

static inline void wbyte32(unsigned int v, unsigned char* p)
{
    wbyte16(v, p);
    wbyte16(v >> 16, p + 2);
}

static inline void wbyte64(uint64_t v, unsigned char* p)
{
    wbyte32(v, p);
    wbyte32(v >> 32, p + 4);
}

static inline unsigned int rbyte16(unsigned char* p)
{
    unsigned int l = *p;
    unsigned int h = p[1];
    return (h << 8) | l;
}

static inline unsigned int rbyte32(unsigned char* p)
{
    unsigned int l = rbyte16(p);
    unsigned int h = rbyte16(p + 2);
    return (h << 16) | l;
}

static inline uint64_t rbyte64(unsigned char* p)
{
    unsigned int l = rbyte32(p);
    unsigned int h = rbyte32(p + 4);
    return (((uint64_t)h) << 32) | l;
}
