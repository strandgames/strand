//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string.h>
#include "sstream.h"

#if defined(__EMSCRIPTEN__) && !defined(NDEBUG)
//#include <emscripten.h>
#endif

struct StdFStream: public StdStream
{
    typedef std::string string;

    // Constructors
    StdFStream()
    {
        _fp = 0;
        _also = 0;
    }
    
    ~StdFStream() { close(); }

    const char* filename() const override { return _filename.c_str(); }
    bool ok() const override { return _fp != 0; }
    
    bool open(const char* filename, const char* mode = "w")
    {
        close();
        _filename = filename;
        _fp = fopen(filename, mode);
        return _fp != 0;
    }

    void close()
    {
        if (_fp)
        {
            fclose(_fp);
            _fp = 0;
        }
    }

    void setAlso(std::ostream* os)
    {
        _also = os;
    }
    

    bool _emit(StdStreamBuf* buf) override
    {
        bool r = false;
        if (_fp)
        {
            size_t nw = buf->size();
            r = (fwrite(*buf, 1, buf->size(), _fp) == nw) && !fflush(_fp);
            
            if (!r)
            {
                // failed, signal error
                close();
            }
        }
        
        if (_also)
        {
            _also->write(*buf, buf->size());
            _also->flush();

#if defined(__EMSCRIPTEN__) && !defined(NDEBUG)
            //            const char* s = *buf;
            //            emscripten_log(EM_LOG_CONSOLE, s);
#endif            

        }
        
        return r;
    }

    void consume(StdFStream* s)
    {
        if (s)
        {
            // copy fp if not already
            // nomrmally this is a shared static, except for windows dll. yes i know.
            if (!_fp)
                _fp = s->_fp;
        }
    }

protected:


    string                                      _filename;
    FILE*                                       _fp;

    // optional addition output (eg cout)
    std::ostream*                               _also;

};

