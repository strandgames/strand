
#include "urlparse.h"
#include "logged.h"

struct Req
{
    typedef std::string string;
    
    URLParser   _up;
    bool        _ok;

    operator bool() const { return _ok; }

    Req(const string& url) : _up(url) {}

    virtual ~Req() {}
    virtual void connect() = 0;
    virtual void send() = 0;
    virtual void receive() = 0;
};

struct Network
{
    typedef std::string string;
    
    bool        _initialised = false;
    bool        _failed = false;

    virtual ~Network() {}
    virtual Req* makeReq(const string& url) = 0;
};


#ifdef _WIN32

#include <winsock2.h>
#include <WS2tcpip.h>
#include <windows.h>

//#pragma comment(lib,"ws2_32.lib")

using namespace std;

struct ReqWin: public Req
{
    SOCKET      _webSocket = INVALID_SOCKET;
    SOCKADDR_IN _sockAddr;

    operator bool() const { return _ok; }

    ReqWin(const string& url) : Req(url)
    {
        _init();
    }

    ~ReqWin() override
    {
        if (_webSocket != INVALID_SOCKET)
        {
            closesocket(_webSocket);
        }
    }

    void _init()
    {
        // We first prepare some "hints" for the "getaddrinfo" function
        // to tell it, that we are looking for a IPv4 TCP Connection.
        struct addrinfo hints;
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_INET;          // We are targeting IPv4
        hints.ai_protocol = IPPROTO_TCP;    // We are targeting TCP
        hints.ai_socktype = SOCK_STREAM;    // We are targeting TCP so its SOCK_STREAM

        // Aquiring of the IPv4 address of a host using the newer
        // "getaddrinfo" function which outdated "gethostbyname".
        // It will search for IPv4 addresses using the TCP-Protocol.
        struct addrinfo* targetAdressInfo = NULL;
        DWORD getAddrRes = getaddrinfo(_up._domain.c_str(), // "www.google.com",
                                       NULL,
                                       &hints,
                                       &targetAdressInfo);

        _ok = true;
        
        if (getAddrRes != 0 || targetAdressInfo == NULL)
        {
            LOG1("Could not resolve the Host Name",0);
            _ok = false;
        }

        if (_ok)
        {
            // Create the Socket Address Informations, using IPv4
            // We dont have to take care of sin_zero, it is only used to extend the length of SOCKADDR_IN to the size of SOCKADDR
            _sockAddr.sin_addr = ((struct sockaddr_in*) targetAdressInfo->ai_addr)->sin_addr;    // The IPv4 Address from the Address Resolution Result
            _sockAddr.sin_family = AF_INET;  // IPv4
            _sockAddr.sin_port = htons(_up._port);  // HTTP Port: 80

            // We have to free the Address-Information from getaddrinfo again
            freeaddrinfo(targetAdressInfo);
        }
    }

    void connect() override
    {
        if (_ok)
        {
            // Creation of a socket for the communication with the Web Server,
            // using IPv4 and the TCP-Protocol
            _webSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
            if (_webSocket == INVALID_SOCKET)
            {
                LOG1("Creation of the Socket Failed", 0);
                _ok = false;
            }
            else
            {
                // Establishing a connection to the web Socket
                LOG1("Connecting...", 0);
                if(::connect(_webSocket, (SOCKADDR*)&_sockAddr,
                           sizeof(_sockAddr)) != 0)
                {
                    LOG1("Could not connect", 0);
                    _ok = false;
                }
            }
        }
    }

    void send() override
    {
        if (_ok)
        {
            // Sending a HTTP-GET-Request to the Web Server
            char rbuf[1024];
            sprintf(rbuf, "GET %s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n", _up._file.c_str(), _up._domain.c_str());

            LOG1("sending '", rbuf << "'");
                
            //const char* httpRequest = "GET / HTTP/1.1\r\nHost: www.google.com\r\nConnection: close\r\n\r\n";
            int sentBytes = ::send(_webSocket,
                                 rbuf,
                                 strlen(rbuf),0);
                
            if (sentBytes < strlen(rbuf) || sentBytes == SOCKET_ERROR)
            {
                LOG1("Could not send the request to the Server",0);
                _ok = false;
            }
        }
    }

    void receive() override
    {
        if (_ok)
        {
            // Receiving and Displaying an answer from the Web Server
            char buffer[10000];
            memset(buffer, 0, sizeof(buffer));
            int dataLen;
            while ((dataLen = recv(_webSocket, buffer, sizeof(buffer), 0) > 0))
            {
                int i = 0;
                while (buffer[i] >= 32 || buffer[i] == '\n' || buffer[i] == '\r') {
                    cout << buffer[i];
                    i += 1;
                }
            }
        }
    }
};


struct NetworkWin: public Network
{
    WSADATA     _wsaData;

    ~NetworkWin() override
    {
        _purge();
    }
    
    void _init()
    {
        if (!_failed && !_initialised)
        {
            // Initialize Dependencies to the Windows Socket.
            if (WSAStartup(MAKEWORD(2,2), &_wsaData) == 0)
            {
                _initialised = true;
            }
            else
            {
                LOG1("WSAStartup failed", 0);
                _failed = true;
            }
        }
    }

    void _purge()
    {
        if (!_failed && _initialised)
        {
            WSACleanup();
            _initialised = false;
        }
    }

    Req* makeReq(const string& url) override
    {
        _init();
        return _failed ? 0 : new ReqWin(url);
    }
};

#endif // _WIN32
