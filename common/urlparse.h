#pragma once

#include <iostream>
#include <string>
#include "cutils.h"
#include "strutils.h"

struct URLParser
{
    typedef std::string string;

    string      _proto; // http, https
    string      _domain;
    int         _port;
    string      _file;

    URLParser() {}
    URLParser(const string& url) { set(url); }

    void set(const string& url)  { set(url.c_str()); }
    
    void set(const char* url)
    {
        // http://google.com...
        const char* a = findFirst(url, "://");
        if (a)
        {
            _proto = string(url, a - url);
            a += 3; 
        }
        else
        {
            // otherwise proto is missing. presume http
            _proto = "http";
            a = url;
        }

        _port = 80; // default

        // google.com:8080/foo...
        const char* c = findFirst(a, "/");
        const char* b = findFirst(a, ":");

        const char* e = b;
        if (!e) e = c;
        if (!e)
        {
            // no end means all domain and no file and no port
            _domain = a;
        }
        else
        {
            _domain = string(a, e - a);
            
            if (b)
            {
                // expect a port number
                int v = 0;
                ++b;
                while (u_isdigit(*b)) v = v*10 + (*b++ - '0');
                _port = v;
            }

            if (c)
            {
                _file = c;
            }
        }

        if (_file.empty())
        {
            _file = "/index.html";
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const URLParser& p)
    {
        os << p._proto << "://" << p._domain << ":" << p._port << p._file;
        return os;
    }
};
