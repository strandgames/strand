//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include "html.h"
#include "rect.h"
#include "unum.h"
#include "varset.h"
#include "logged.h"

struct SVGParser
{
    typedef std::string string;

    string			_filename;
    NodeParser			_parser;
    NodeTag*			_root = 0;
    NodeTag*			_svgroot = 0;
    StrandCommon::Rect		_viewBox;

    typedef HNode::Nodes::iterator nodeIterator;
    typedef NodeTag::NamedValueSet NamedValueSet;

    bool parseViewBox(const string& ss)
    {
        const char* s = ss.c_str();
        _viewBox._x = u_atoi(&s);
        _viewBox._y = u_atoi(&s);
        _viewBox._w = u_atoi(&s) - _viewBox._x;
        _viewBox._h = u_atoi(&s) - _viewBox._y;
        return _viewBox.valid();
    }

    bool parse(const char* data)
    {
        bool r;
        _parser.parse(data);
        _root = _parser.root();
        r = _root != 0;
        if (r)
        {
            _svgroot = _root->findTag("svg");
            r = _svgroot != 0;
            
            if (r)
            {
                LOG4("parsed: ", _svgroot->toString());

                string vbox = _svgroot->getAttr("viewBox").toString();
                r = parseViewBox(vbox);
                LOG3("viewbox ", _viewBox.toString());
            }
        }
        return r;
    }

#define ATTR_VAL_SEP(_p) (*(_p) == ':')
#define ATTR_SEP(_p) (*(_p) == ';' || *(_p) == ',')

    static bool parseAttributes(VarSet& vars, const string& s)
    {
        const char* sp = s.c_str();
        return parseAttributes(vars, &sp);
    }
    
    static bool parseAttributes(VarSet& vars, const char** qp)
    {
        // foo:bar;moo:1;

        // allow comma as well as semi-colon
        // foo:bar,moo:1  etc.
        // bool properties can omit "true", eg foo,bar:1 
        // same as foo:true,bar:1

        bool r = true;
        const char* p = *qp;
        for (;;)
        {
            const char* ts = p;

            // expect tag to be alphanum or underscore, or hypen
            if (u_isalpha(*p))
            {
                ++p;
                while (u_isalnum(*p) || *p == '_' || *p == '-') ++p;
            }
            
            if (ts == p) break; // none, we're done

            string tag(ts, p - ts);
            var vv;
            
            if (ATTR_SEP(p) || !*p || *p == '\n')
            {
                // end or comma (or semi) separator. 
                // Assume tag is bool and value true.
                vv = var(true);
            }
            else
            {
                // expect key:value
                if (!ATTR_VAL_SEP(p))
                {
                    // bad syntax
                    r = false;
                    break;
                }

                ++p;
                const char* vs = p;  // start of value

                // value ends with comma, semi, newline or null
                bool inQuote = false;
                while (*p)
                {
                    if (*p == '"') inQuote = !inQuote;
                    else
                    {
                        if (!inQuote && (*p == '\n' || ATTR_SEP(p))) break;
                    }
                    ++p;
                }
                
                if (vs == p) break; // no value given, drop out

                string val(vs, p - vs);

                var::Format vf;
                vf._mapBool = true;

                const char* vp = val.c_str();
                vv.parse(&vp, &vf);
            }
            
            if (vv) vars[tag] = vv; // consumes!
            
            // consume comma or semi. newline or null will stop
            if (ATTR_SEP(p)) ++p;
            *qp = p; // accept
        }
        return r;
    }

#ifdef LOGGING    
    void showTags(NodeTag* node= 0)
    {
        if (!node) node = _svgroot;
        for (nodeIterator it = node->_content.begin();
             it != node->_content.end(); ++it)
        {
            HNode* n = it;
            if (n->type() == HNode::nodeTypeTag)
            {
                NodeTag* nt = (NodeTag*)n;
                LOG1("tag ", nt->_tag);
            }
        }
    }

    void showAttr(NodeTag* t)
    {
        for (NamedValueSet::const_iterator it = t->_attr.cbegin();
             it != t->_attr.cend(); ++it)
        {
            LOG1("attr ", it->first);
        }        
    }
#endif // LOGGING    
};


