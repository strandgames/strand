
#ifdef __ANDROID__

#pragma once

#include <android/log.h>

struct AndroidOut: public std::stringbuf
{
    AndroidOut(const char* kLogTag) : logTag_(kLogTag){}

protected:
    
    virtual int sync() override
    {
        __android_log_print(ANDROID_LOG_DEBUG, logTag_, "%s", str().c_str());
        str("");
        return 0;
    }

    const char* logTag_;
};


#endif // __ANDROID__
