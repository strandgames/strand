//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//


#pragma once

#include <iostream>
#include <string>
#include <ctype.h>
#include <vector>
#include "var.h"
#include "growbuf.h"
#include "cutils.h"
#include "strutils.h"
#include "logged.h"

struct JSONWalker
{
    typedef std::string string;
    
    const char*         _json;
    const char*         _pos;

    // start of subobject, valid after `getValue`
    const char*         _obj;

    // current key
    string              _key;
    bool                _end;
    bool                _error;
    int                 _level;
    int                 _blevel;

    JSONWalker(const char* json) { _json = json; begin(); }
    JSONWalker(const string& j) { _json = j.c_str(); begin(); }

    bool ok() const { return _error == false; }

    friend std::ostream& operator<<(std::ostream& os, const JSONWalker& jw)
    {
        return os << string(jw._json, jw._pos - jw._json);
    }

    char _get()
    {
        char c = *_pos;
        if (c) ++_pos;
        return c;
    }

    void _unget(char c)
    {
        if (c) --_pos;
    }

    void _init()
    {
        _level = 0;
        _blevel = 0;
        _end = false;
        _error = false;
        _pos = _json;
        skipSpace();
    }

    void begin()
    {
        _init();
        char c = _get();
        if (c != '{')
        {
            _error = true;
        }
        else
        {
            ++_level;
            skipSpace();
            c = _get();
            if (c == '}')  
            {
                // empty json {}
                --_level;
                _end = true;
            }
            else
            {
                _unget(c);
            }
        }
    }

    void beginArray()
    {
        _init();
        char c = _get();
        if (c != '[')
        {
            _error = true;
        }
        else
        {
            ++_blevel;
            skipSpace();
            c = _get();
            if (c == ']')  
            {
                // empty array []
                --_blevel;
                _end = true;
            }
            else
            {
                _unget(c);
            }
        }
    }
    
    void skipSpace()
    {
        for (;;)
        {
            char c = _get();
            if (!u_isspace(c))
            {
                _unget(c);
                break;
            }
        }
    }

    void unskipSpace()
    {
        while (_json != _pos)
        {
            if (!u_isspace(_pos[-1])) break;
            --_pos;
        }
    }

    bool nextKey()
    {
        _key = _getKey();
        if (_error)
        {
            LOG2("JSON error at '", string(_pos).substr(0,40) << "'... in:\n" << _json << "\n\n");
        }
        return !_key.empty();
    }

    string _getKey()
    {
        // leaves position next char after ':'
        string k;
        if (!_end && !_error)
        {
            skipSpace();
            char c = _get();
            if (c)
            {
                if (c != '"') _error = true;
                else 
                {
                    const char* st = _pos;
                    for (;;)
                    {
                        c = _get();
                        if (!c)
                        {
                            _error = true;
                            break;
                        }
                        else if (c == '"')
                        {
                            // ASSUME we can't escape " inside keys
                            // end of key

                            k = string(st, _pos - st - 1);
                            
                            skipSpace();
                            c = _get();
                            if (c != ':') _error = true;
                            break;
                        }
                    }
                }
            }
        }
        return k;
    }

    void skipValue()
    {
        // skips over one json value, which can be a whole {object} or
        // a whole [array]
        // leaves pos next char after that which ended value
        
        int level0 = _level;
        int blevel0 = _blevel;
        bool inquote = false;
        bool esc = false;
        char c;
        
        while (!_error)
        {
            c = _get();
            if (!c) break;
            if (esc)
            {
                esc = false;
                continue;
            }

            if (c == '\\')
            {
                esc = true;
                continue;
            }
            
            if (c == '"')
            {
                inquote = !inquote;

                // pos at endquote + 1
                if (!inquote && _level == level0 && _blevel == blevel0) break;
            }                        
            else if (!inquote)
            {
                if (c == '{') ++_level;
                else if (c == '}')
                {
                    --_level;

                    if (_level < level0)
                    {
                        // must have ended
                        _end = true;

                        // final } is end of value+1 in this case
                        _unget(c);

                        // [] should be level
                        if (_blevel != blevel0) _error = true;
                        break;
                    }

                    // leave pos at close+1
                    if (_level == level0 && _blevel == blevel0) break;
                }
                else if (c == '[')
                {
                    ++_blevel;
                }
                else if (c == ']')
                {
                    --_blevel;
                    if (_blevel < blevel0)
                    {
                        // ended or error
                        _end = true;

                        // final ] is end of value+1 in this case
                        _unget(c);

                        if (_level != level0) _error = true;
                        break;
                    }
                    
                    if (_level == level0 && _blevel == blevel0) break;
                }
                else
                {
                    // can leave pos at comma
                    if (c == ',' &&
                        _level == level0 && _blevel == blevel0) 
                    {
                        _unget(c);
                        break;
                    }
                }
            }
        }
    }

    const char* checkValue(bool& isObject)
    {
        // return the start of the value object
        // leaves pos next char after that which ended value
        skipSpace();
        const char* st = _pos;
        skipValue();

        isObject = false;

        if (_error || st == _pos)
        {
            LOG1("Bad JSON at ", st);
            st = 0;
        }
        else
        {
            char c = *st;
            if (c == '{')
            {
                isObject = true;

                // remember the start of the sub-object
                _obj = st;
            }
            else if (c == '[')
            {
                isObject = true;
                _obj = st;
            }
        }
        
        return st;
    }

    bool atObj(string& subjs)
    {
        if (*_obj == '{')
        {
            subjs = string(_obj, _pos - _obj);
            if (_pos[-1] == '}') return true;
            LOG1("malformed json object; ", _key << ":" << subjs);
        }
        return false;
    }

    bool atList(string& subjs)
    {
        // the return substring includes brackets, eg `[foo, bar]`
        if (*_obj == '[')
        {
            subjs = string(_obj, _pos - _obj);
            if (_pos[-1] == ']') return true;
            LOG1("malformed json list; ", _key << ":" << subjs);
        }
        return false;
    }

    var getValue(bool& isObject)
    {
        var v;
        const char* st = checkValue(isObject);
        if (st && !isObject)
            v = collectValue(st);
        
        return v;
    }

    const char* collectStringBounds(const char* st, size_t& size)
    {
        assert(*st == '"');
        
        // pos is end quote + 1
        // [st+1, _pos-1) represents the string without quotes

        if (_pos[-1] != '"')
        {
            LOG3("JsonWalker, malformed string '", st << "'");
            size = 0;
        }
        else
        {
            ++st;
            size = (_pos - 1) - st;
        }
        return st;
    }

    string collectRawStringValue(const char* st)
    {
        // expect string value
        
        assert(!_error);
        assert(st != _pos);
        assert(*st != '{' && *st != '[');
        
        char c = *st;
        if (c == '"')
        {
            size_t size;
            st = collectStringBounds(st, size);
            return string(st, size);
        }
        return string();
    }

    var collectValue(const char* st)
    {
        // does not get objects
        var r;

        assert(!_error);
        assert(st != _pos);
        assert(*st != '{' && *st != '[');
        
        char c = *st;
        if (c == '"')
        {
            size_t size;
            st = collectStringBounds(st, size);
            
            GrowString gs;
            decodeString(gs, st, size);
            gs.add(0);
            r = var(gs.start());
        }
        else
        {
            string v(st, _pos - st);
            if (v == "true")
            {
                // convert true and false to int
                r = var(1);
            }
            else if (v == "false")
            {
                r = var(0);
            }
            else
            {
                r._parseNumber(&st);
            }
        }
        return r;
    }

    static bool decodeStringArray(std::vector<std::string>& sa,
                                  const string& js)
    {
        const char* s = js.c_str();
        bool r = *s++ == '[';
        while (r)
        {
            while (u_isspace(*s)) ++s;
            if (*s == ']') break; // done

            int z = encodedStringLength(s);
            if (z < 2) break; // not a string

            GrowString gs;
            decodeString(gs, s+1, z-2);
            gs.add(0);
            sa.push_back(gs.start());

            s += z;
            while (u_isspace(*s)) ++s;
            if (*s == ',') ++s;
        }
        return r;
    }

    void next()
    {
        // assume at end char of last value + 1

        if (_end || _error) return;

        assert(_json && _json != _pos);

        skipSpace();

        // char that ended term
        char c = *_pos;

        if (c == ',')
        {
            // OK
            ++_pos;
        }
        else if (c == '}')
        {
            // must be final }
            _end = true;
            --_level;
            if (_level || _blevel) _error = true;
        }
        else if (c == ']')
        {
            // end of array
            // this should only happen when we are directly parsing an array
            // via `beginArray`, and this is the final close
            _end = true;
            --_blevel;
            if (_blevel) _error = true;
        }
        else
        {
            LOG3("JSON next unexpected '", _pos << "'");
            _error = true;
        }
    }

    // helpers

    static void decodeString(GrowString& gs,
                             const char* st,
                             size_t size)
    {
        gs.append(RemoveEscapes(st, st + size));
    }

    static int encodedStringLength(const char* s0)
    {
        // expect a string in quotes "foo". also "foo\"bar"
        // length includes quotes, return 0 if not valid
        const char* s = s0;
        if (*s == '"')
        {
            bool esc = false;
            char c;
            while ((c = *++s) != 0)
            {
                if (!esc)
                {
                    if (c == '\\')
                    {
                        esc = true;
                        continue;
                    }
                    if (c == '"')
                    {
                        ++s;
                        break;
                    }
                }
                else
                {
                    esc = false;
                }
            }
        }
        return s - s0;
    }
    
    static void decodeString(GrowString& gs, const char* p)
    {
        decodeString(gs, p, strlen(p));
    }

    static void encodeString(GrowString& gs, const char* p)
    {
        gs.add('"');
        gs.append(AddEscapes(p));
        gs.add('"');
    }

    static void addKey(GrowString& gs, const char* key)
    {
        gs.add('"');
        gs.append(key);
        gs.add('"');
        gs.add(':');
    }

    static void addValue(GrowString& gs, const var& v)
    {
        if (v)
        {
            toAdd(gs);
            if (v.isString()) encodeString(gs, v.toString().c_str());
            else gs.append(v.toString());        
        }
    }

    static void addKeyValue(GrowString& gs, const char* key, const var& v)
    {
        if (v)
        {
            var::Format f;
            f._listSquareBrackets = true; // json lists in square brackets
            f._quoteStrings = true;
            
            toAdd(gs);
            addKey(gs,key);
            if (v.isString()) encodeString(gs, v.toString().c_str());
            else gs.append(v.toString(&f));
        }
    }
    
    static void addKeyValue(GrowString& gs, const string& key, const var& v)
    {
        addKeyValue(gs, key.c_str(), v);
    }

    static void addStringValue(GrowString& gs,
                               const char* key, const char* v)
    {
        toAdd(gs);
        addKey(gs,key);
        encodeString(gs, v);
    }

    static void addRawStringValue(GrowString& gs,
                                  const char* key, const char* v)
    {
        // do not encode string
        // only use if you _know_ the string is clean
        toAdd(gs);
        addKey(gs,key);
        gs.add('"');
        gs.append(v);
        gs.add('"');
    }

    static void addStringValue(GrowString& gs,
                               const char* key, const string& v)
    {
        addStringValue(gs, key, v.c_str());
    }

    static void addBoolValue(GrowString& gs, const char* key, bool v)
    {
        toAdd(gs);
        addKey(gs,key);
        gs.append(v ? "true" : "false");
    }

    static void addKeyObject(GrowString& gs, const char* key, const char* json)
    {
        // add a json object as value of `key`
        // ASSUME `json` is valid!

        toAdd(gs);
        addKey(gs, key);
        gs.append(json);
    }

    static void addKeyObject(GrowString& gs, const string& key, const char* json)
    {
        addKeyObject(gs, key.c_str(), json);
    }

    static char lastChar(const GrowString& gs)
    {
        // last char not a space or 0
        char c = 0;
        int sz = gs.size();
        const char* s = gs;
        const char* e = s + sz;
        while (s != e)
        {
            c = *--e;
            if (!u_isspace(c)) break;
        }
        return c;
    }

    static void toAdd(GrowString& gs)
    {
        char c = lastChar(gs);
        if (c && c != ',' && c != '{' && c != '[' && c != ':') gs.add(',');
    }

    static bool reopenJSON(GrowString& js, const string& oldjs)
    {
        const char* s = oldjs.c_str();

        while (u_isspace(*s)) ++s;
        if (*s != '{') return false; // malformed
        
        const char* p = s + strlen(s);

        // backup to find final closing "}"
        while (p != s) if (*--p == '}') break;

        if (p != s)
        {
            // add { ... but not closing }"
            js.append(s, p - s);
            return true;
        }

        return false; // malformed
    }

    


};
