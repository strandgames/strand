//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (©) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <vector>
#include <string>
#include <assert.h>
#include "fd.h"
#include "fdbuf.h"
#include "lz.h"
#include "utils.h"

// current versions
#define HUNK_VERSION_MAJOR   0
#define HUNK_VERSION_MINOR   0

// these are the ids on the hunk head
#define HUNK_INFO_ID 0x49525453         // STRI
#define HUNK_DATA_ID 0x44525453         // STRD

#define HUNK_COMPRESSION_NONE 0
#define HUNK_COMPRESSION_LZW 1

struct HunkHead
{
    int         _versionMajor = HUNK_VERSION_MAJOR;
    int         _versionMinor = HUNK_VERSION_MINOR;
    int         _size = 0;
    unsigned char* _data = 0;
    mutable bool   _owned = false;

    HunkHead() {}
    HunkHead(unsigned char* data, int size, bool owned = false)
    { setData(data, size, owned); }
    HunkHead(const std::string& d)
    {
        // NB: ensure string does not go out of scope
        setData((unsigned char*)d.c_str(), d.size());
    }
    HunkHead(const HunkHead& hh) { *this = hh; }

    HunkHead& operator=(const HunkHead& hh)
    {
        // NOTE: donate semantics
        setData(hh._data, hh._size, hh._owned);
        
        // ownership moved. donor as just a copy
        hh._owned = false;

        return *this;
    }

    void setData(unsigned char* data, int size, bool owned = false)
    {
        purge();
        _data = data;
        _size = size;
        _owned = owned;
    }

    ~HunkHead() { purge(); }

    void purge()
    {
        if (_owned && _data)
        {
            delete [] _data;
            _data = 0;
            _size = 0;
            _owned = false;
        }
    }

#ifdef LOGGING    
    friend std::ostream& operator<<(std::ostream& os, const HunkHead& hh)
    {
        return os << hh._versionMajor << '.' << hh._versionMinor << " size:" << hh._size;
    }
#endif    
};

struct HunkInfo
{
    typedef std::string string;
    
    HunkHead    _head;

    // info has a type and a name.
    // the type tells us it's purpose. eg "strand", "gui"
    // and the name is specific, eg the strand game name.
    string      _type;
    string      _name;

    bool        _dontCompress = false;
    int         _compression = 0;
    int         _size;  // decompressed size (might be different from head)

    HunkInfo() {}
    HunkInfo(const string& type, const string& name)
        : _type(type), _name(name) {}

#ifdef LOGGING    
    friend std::ostream& operator<<(std::ostream& os, const HunkInfo& hi)
    {
        return os << hi._type << ':' << hi._name << ' ' << hi._head;
    }
#endif    
        
};

struct HunkFile
{
    typedef std::string string;

    string                      _filename;
    std::vector<HunkInfo*>      _hunks;
    FDFile                      _file;

    // keep compress off the stack
    Compress*                   _cp = 0;

    HunkFile(const string& name) : _filename(name) {}

    ~HunkFile() { clear(); }

    int size() const { return _hunks.size(); }

    Compress& cp()
    {
        if (!_cp) _cp = new Compress;
        return *_cp;
    }
    
    HunkInfo* add(const string& type, const string& name, const HunkHead& hh)
    {
        assert(hh._data && hh._size);
        
        HunkInfo* hi = new HunkInfo(type, name);
        hi->_head = hh;
        _hunks.push_back(hi);
        return hi;
    }

    HunkInfo* find(const string& type, const string* name = 0) const
    {
        // if given just a type, return the first info for that type
        // otherwise find the exact type+name match
        
        for (auto hi : _hunks)
        {
            if (equalsIgnoreCase(hi->_type, type) &&
                (!name || equalsIgnoreCase(hi->_name, *name))) return hi;
        }
        return 0;
    }

    bool write()
    {
        // write all hunkinfos to file
        assert(!_hunks.empty());

        bool r = _file.open(_filename.c_str(), FDFile::fd_new);
        if (!r) return r;

        FDBufFile _fdb(_file);

        // compress hunks?
        for (HunkInfo* hi : _hunks)
        {
            // remember the original data size
            hi->_size = hi->_head._size;
            
            if (!hi->_compression && !hi->_dontCompress)
            {
                //LOG1("hunkfile, trying to compress ", *hi);
                
                // try to compress
                cp().compress(hi->_head._data, hi->_head._size);
                int cz = _cp->_outsz;
                if (cz < hi->_head._size)
                {
                    //LOG1("hunkfile, compressed to ", cz);
                    // compressed!
                    // NB: compressed data is owned by hunk
                    hi->_head.setData(_cp->donate(0), cz, true);
                    hi->_compression = HUNK_COMPRESSION_LZW;
                }
                else
                {
                    //LOG1("hunkfile, did not compress ", hi->_head._size);
                    // didn't compress
                    hi->_dontCompress = true;
                }
            }
        }

        for (HunkInfo* hi : _hunks)
        {
            // first write info
            FDBufFile::Elt e(HUNK_INFO_ID, HUNK_VERSION_MAJOR, HUNK_VERSION_MINOR, "hunkinfo");
            r = _fdb.writeEltBegin(e);
            
            r = r && _fdb.putstring(hi->_type.c_str());
            r = r && _fdb.putstring(hi->_name.c_str());
            r = r && _fdb.putc(hi->_compression); 
            r = r && _fdb.putc(hi->_dontCompress);
            r = r && _fdb.putint(hi->_size);
            r = r && _fdb.writeEltEnd(e);
            if (!r) break;

            // then write data
            FDBufFile::Elt ed(HUNK_DATA_ID, hi->_head._versionMajor, hi->_head._versionMinor, "hunkdata");
            r = _fdb.writeEltBegin(ed);
            r = r && _fdb.write(hi->_head._data, hi->_head._size);
            r = r && _fdb.writeEltEnd(ed);
            if (!r) break;
        }

        return r;
    }

    void decompress(HunkInfo* hi, unsigned char* d, int sz)
    {
        assert(hi->_size > (int)sz);

        // Note that an extra zero is always placed
        // after the decompressed data
        cp().decompress(d, sz, hi->_size);

        // collect the decompressed data
        d = _cp->donate(&sz);

        if (sz > hi->_size)
        {
            LOG1("hunkfile warning decompressed size not expected, ", sz << " instead of " << hi->_size);
            sz = hi->_size;
        }

        // will delete compressed data and
        // own new data
        hi->_head.setData(d, sz, true);
        hi->_compression = 0;
    }
         
    bool read()
    {
        clear();
        
        //LOG1("hunkfile opening ", _filename);

        bool r = _file.open(_filename.c_str());
        if (!r) return r;

        FDBufFile _fdb(_file);

        //LOG1("hunkfile reading ", _filename);        
        
        for (;;)
        {
            // first read eltinfo
            FDBufFile::Elt e;
            FDBufFile::Elt ei(HUNK_INFO_ID, HUNK_VERSION_MAJOR, HUNK_VERSION_MINOR, "hunkinfo");        
            
            if (!_fdb.readEltBegin(e))
            {
                // if we have already some hunks then this is all
                // and not an error
                if (_hunks.size()) break;
                
                // otherwise we are in error
                r = false;
                break;
            }
            
            r = _fdb.readEltCheck(e, ei);
            if (!r)
            {
                LOG1("hunkfile failed to read elt begin for ", _filename);
                break;
            }
               
            HunkInfo* hi = new HunkInfo;
            hi->_type = _fdb.getstring();
            hi->_name = _fdb.getstring();
            hi->_compression = _fdb.getc();
            hi->_dontCompress = _fdb.getc();
            hi->_size = _fdb.getint();  // uncompressed size
            r = !_fdb._eof;
            r = r && _fdb.readEltEnd(e);

            //LOG1("hunkfile got header ", *hi << " in " << _filename);
            if (r)
            {
                r = _fdb.readEltBegin(e);
                if (r)
                {
                    int sz = e._dsize;
                    unsigned char* d = new unsigned char[sz + 1];
                    
                    // regardless of data, ensure a zero follows when we read
                    // so that it can be treated as a string
                    d[sz] = 0;
                    
                    hi->_head.setData(d, sz, true); // owns data
                    r = _fdb.read(d, sz);
                    if (r)
                    {
                        // compression is the compression mode
                        // ASSUME LZW for now                        
                        if (hi->_compression)
                        {
                            //LOG3("hunkfile decompressing ", _filename << " size:" << hi->_size << " from:" << sz);
                            decompress(hi, d, sz);
                        }

                        //LOG1("hunkfile read data ", hi->_head._size);
                        _hunks.push_back(hi);
                        r = _fdb.readEltEnd(e);
                        assert(r);
                    }
                }
                else
                {
                    LOG1("hunkfile failed to read data begin ", _filename);
                }
            }

            if (!r)
            {
                delete hi;
                break;
            }
        }

        return r;
    }

    void clear()
    {
        ::purge(_hunks);
        delete _cp; _cp = 0;
    }
    
};
