#pragma once

#include <string>
#include <assert.h>

/*
* Base64 encoding/decoding (RFC1341)
* Copyright (c) 2005-2011, Jouni Malinen <j@w1.fi>
*
* This software may be distributed under the terms of the BSD license.
* See README for more details.
*/

// 2016-12-12 - Gaspard Petit : Slightly modified to return a std::string 
// instead of a buffer allocated with malloc.

#include <string>



#ifdef COMPAT
// returns traditional encoding
static const unsigned char base64_table[65] =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
#else

   // NB could use '/' -> '_' and '+' -> '-'
   // to avoid problems with filenames
static const unsigned char base64_table[65] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";
#endif

static inline std::string base64_encode(const unsigned char *src,
                                        unsigned int len)
{
    unsigned char *out, *pos;
    const unsigned char *end, *in;

    unsigned int olen;

#ifdef COMPAT    
    olen = len * 4 / 3 + 4; /* 3-byte blocks to 4-byte */
    //olen++; /* nul termination */
#else
    // each 3  bytes goes to 4 base64 chars
    unsigned int lq = len/3;
    unsigned int lr = len - lq*3;
    olen = lq*4;
    if (lr) olen += lr + 1;
#endif    

    std::string outStr;
    outStr.resize(olen);
    out = (unsigned char*)&outStr[0];

    end = src + len;
    in = src;
    pos = out;
    while (end - in >= 3)
    {
        *pos++ = base64_table[in[0] >> 2];
        *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
        *pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
        *pos++ = base64_table[in[2] & 0x3f];
        in += 3;
    }


#ifdef COMPAT    
    if (end - in) {
        *pos++ = base64_table[in[0] >> 2];
        if (end - in == 1) {
            *pos++ = base64_table[(in[0] & 0x03) << 4];
            *pos++ = '=';
        } else {
            *pos++ = base64_table[((in[0] & 0x03) << 4) |
                                  (in[1] >> 4)];
            *pos++ = base64_table[(in[1] & 0x0f) << 2];
        }
        *pos++ = '=';
    }
#else
    // if remainder is 1, emits 2 chars
    // if remainder is 2, emits 3 chars
    if (lr > 0) // 1 or 2
    {
        *pos++ = base64_table[in[0] >> 2];
        if (lr == 1)
        {
            *pos++ = base64_table[(in[0] & 0x03) << 4];
        }
        else // lr == 2
        {
            *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
            *pos++ = base64_table[(in[1] & 0x0f) << 2];
        }
    }
#endif    

    return outStr;
}

static const unsigned int B64index[256] =
#ifdef COMPAT
{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 62, 0, 0, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 0, 0, 0, 63, 0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
#else    
{ 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 62, 63, 62, 62, 63, 52, 53, 54, 55,
56, 57, 58, 59, 60, 61,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  3,  4,  5,  6,
7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,  0,
0,  0,  0, 63,  0, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51 };
#endif

static inline std::string b64decode(const char* data, unsigned int len)
{
    // converting runs of 4 chars into 3 bytes
    unsigned int pad = len % 4;  // reminder will be 0, 2 or 3 chars.
    assert(pad != 1);
    
    const unsigned int L = len - pad; // number of whole 4 into 3

    // if pad = 2, emit 1. if pad = 3, emit 2
    unsigned int olen = L/4 * 3;
    if (pad) olen += pad - 1;
    
    std::string str(olen, '\0');
    const unsigned char* p = (const unsigned char*)data;

    unsigned int j = 0;
    for (unsigned int i = 0; i < L; i += 4)
    {
        unsigned int n = B64index[p[i]] << 18 | B64index[p[i + 1]] << 12 | B64index[p[i + 2]] << 6 | B64index[p[i + 3]];
        str[j++] = n >> 16;
        str[j++] = n >> 8 & 0xFF;
        str[j++] = n & 0xFF;
    }

    if (pad)
    {
        unsigned int n = B64index[p[L]] << 18 | B64index[p[L + 1]] << 12;
        str[j++] = n >> 16;

        if (pad == 3)
        {
            n |= B64index[p[L + 2]] << 6;
            str[j++] = n >> 8 & 0xFF;
        }
    }
    return str;
}

#if 0
static void makeTable()
{
    unsigned char dtable[256];

    int i;
    memset(dtable, 0, 256);
    for (i = 0; i < sizeof(base64_table) - 1; i++)
        dtable[base64_table[i]] = (unsigned char) i;

    printf("{ ");
    for (i = 0; i < 255; ++i)
    {
        printf("%d, ", dtable[i]);
    }
    printf("%d\n};\n", dtable[i]);
}
#endif


