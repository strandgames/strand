//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include "varset.h"
#include "growbuf.h"
#include "jsonwalker.h"

struct VarSetJson
{
    typedef std::string string;

    // T is VarSet or VarList

    template<class T>
    static void addJson(GrowString& gs, const char* tag, const T& vs)
    {
        // add a varset as an object with a tag
        JSONWalker::toAdd(gs);
        JSONWalker::addKey(gs, tag);
        gs.add('{');

        for (auto& vi : vs)
            JSONWalker::addKeyValue(gs, vi.first, vi.second);
                
        gs.add('}');
    }

    static bool parseJson(VarList& vs, const string& js)
    {
        // { foo:bar, ... }
        bool r = true;
        for (JSONWalker jw(js); jw.nextKey(); jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st) { r = false;  break; }
            
            if (!isObject)
            {
                var v = jw.collectValue(st);
                if (v)
                {
                    vs.add(jw._key, v); // consume v
                }
                else
                {
                    LOG1("void value in varset, key ", jw._key << " in " << js);
                }
            }
            else
            {
                // a json list becomes a var list
                string subjs;
                r = jw.atList(subjs);
                if (r)
                {
                    const char* s = subjs.c_str();
                    var v;
                    var::Format f;

                    // only allow tokens in strings not all chars
                    f._parseStringExpr = true;
                    f._stringTokensExtra = "#"; // to parse colour strings
                    r = v.parseList(&s, &f);
                    if (r) vs.add(jw._key, v);
                }

                if (!r)
                {
                    LOG1("bad object in varset ", js);
                }
            }
        }
        return r;
    }
};
