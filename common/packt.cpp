
#include <stdio.h>
#include <iostream>

#include "pack7.h"
#include "base64.h"


using namespace std;

int main(int argc, char** argv)
{
    const char* s = 0;
    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
        }
        else
        {
            s = argv[i];
        }
    }

    if (s)
    {
        unsigned olen;
        unsigned char* d = pack7(s, olen);
        cout << "pack7 into " << olen << endl;

        char* s7 = unpack7(d, olen);
        printf("unpack7 '%s'\n", s7);
        delete s7;

        string s64 = base64_encode(d, olen);
        cout << "pack7 into base64 << '" << s64 << "' size:" << s64.size() << endl;
        
        string s64d = b64decode(s64.c_str(), s64.size());
        cout << "unpacked64 into " << s64d.size() << endl;
        assert(s64d.size() == olen);
        assert(!memcmp(d, s64d.c_str(), olen));
        delete d;

        cout << "all in one\n";
        {
            unsigned int olen;
            unsigned char* d = pack7(s, olen);
            string s64 = base64_encode(d, olen);
            cout << "encoded '" << s << "' into '" << s64 << "'\n";
            delete d;

            string s64d = b64decode(s64.c_str(), s64.size());
            char* s1 = unpack7((unsigned char*)s64d.c_str(), s64d.size());
            cout << "decoded '" << s64 << "' into '" << s1 << "'\n";

            assert(!strcmp(s, s1));
            delete s1;
        }
    }
    else
    {
        //makeTable();
        
        fprintf(stderr, "usage: %s string\n", argv[0]);
    }
    return 0;
}
