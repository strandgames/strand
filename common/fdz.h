/**
 *
 *    _    __        _      __                           
 *   | |  / /____   (_)____/ /_      __ ____ _ _____ ___ 
 *   | | / // __ \ / // __  /| | /| / // __ `// ___// _ \
 *   | |/ // /_/ // // /_/ / | |/ |/ // /_/ // /   /  __/
 *   |___/ \____//_/ \__,_/  |__/|__/ \__,_//_/    \___/ 
 *                                                       
 *  Copyright (�) Voidware 2017.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 * 
 *  contact@voidware.com
 */

#pragma once

#include "fdbuf.h"
#include "lz.h"

#define WRITEZ_VERSION 0

// version + usize + csize
#define HEADZ_SIZE (1 + 4 + 4)


#if 0
inline bool writez2(Compress& cp, FDBuf& fb, size_t* nwrote = 0)
{
    nwrote = 0;

    uint version = WRITEZ_VERSION;
    uint usize = cp._insz;
    uint csize = cp._outsz;

    uint startpos = fb._pos;

    // write the compress header
    // high bit set the first byte (with the version) so that it also
    // marks the data as binary. In cases where we can have either a
    // compressed version or plain text, this first byte will act as
    // as a marker for the compressed format.
    fb.putc(version + 0x80);
    fb.putint(usize);
    fb.putint(csize);

    // size of the above data
    uint headSize = fb._pos - startpos;
    
    // now write compressed data
    size_t nr;
    bool res = fb.write(cp._outbuf, csize, nr) && nr == csize;
    if (res) nr += headSize;
    if (nwrote) *nwrote = nr;

    return res;
}

inline bool writez(FDBuf& fb, const unsigned char* buf,
                   size_t amt, size_t* nwrote = 0)
{
    Compress cp;
    writez1(cp, buf, amt);
    return writez2(cp, fb, nwrote);
}

inline bool writez(FD& fd, const unsigned char* buf,
                   size_t amt, size_t* nwrote = 0)
{
    FDBuf fb(fd);
    return writez(fb, buf, amt, nwrote);
}
#endif

inline bool writezPrepare(Compress& cp, const unsigned char* buf, size_t amt)
{
    cp.compress(buf, amt);

    bool v = cp.compressSpaceSpare() >= HEADZ_SIZE;

    // if we have space for compress header, use it.
    // otherwise there is no point in compressing
    if (v)
    {
        memmove(cp._outbuf + HEADZ_SIZE, cp._outbuf, cp._outsz);

        GetPutBuf gb(cp._outbuf);

        uint version = WRITEZ_VERSION;
        uint usize = cp._insz;
        uint csize = cp._outsz;

        // write the compress header
        // high bit set the first byte (with the version) so that it also
        // marks the data as binary. In cases where we can have either a
        // compressed version or plain text, this first byte will act as
        // as a marker for the compressed format.
        gb.putc(version + 0x80);
        gb.putint(usize);
        gb.putint(csize);

        // adjust buffer to include header size
        cp._outsz += HEADZ_SIZE;
    }
    return v;
}


inline unsigned char* readz(FDBuf& fb, size_t* fsize = 0)
{
    // fsize is set to the uncompressed size
    // but the allocated buffer is +1 and zero terminated so that it
    // can be used a string.
    
    uint startpos = fb._pos;

    // read header
    uint version = fb.getc();

    if (version == (uint)-1 || version < 0x80)
    {
        // not binary. seek back to before the header and return fail
        // the caller could use this to fallback to reading a text version
        fb._fd->seek(startpos);
        return 0; // fail
    }

    version &= 0x7f; 

    if (version > WRITEZ_VERSION)
    {
        LOG1("readz newer version", version);
        return 0; // fail
    }
    
    uint usize = fb.getint();
    uint csize = fb.getint();
    //uint headSize = fb._pos - startpos;

    bool res = csize != (uint)-1;
    unsigned char* data = 0;

    if (res)
    {    
        //LOG3("ZHeader; compress size: ", csize << ", uncompress size:" << usize);
        uchar* buf = new unsigned char[csize];
        size_t nread;
        res = fb.read(buf, csize, nread) && csize == nread;
            
        if (res)
        {
            Compress cp;
            cp.decompress(buf, csize, usize);

            int sz;
            data = cp.donate(&sz);
            if (fsize) *fsize = sz;
        }
        delete [] buf;
    }
    return data;
}

inline unsigned char* readz(FD& fd, size_t* fsize = 0)
{
    FDBuf fb(fd);
    return readz(fb, fsize);
}

inline uint _shortat(unsigned char* d)
{
    uint a = d[0];
    uint b = d[1];
    return a | b << 8;
}

inline uint _uintat(unsigned char* d)
{
    uint a = _shortat(d);
    uint b = _shortat(d + 2);
    return a | b << 16;
}

inline int decompz(Compress& cp, unsigned char* d, uint sz)
{
    /* attempt to decompress a buffer with a header.
     * NOTE: this might not be compressed data.
     *
     * return;
     * 0 => not compressed. 
     * <0 => was compressed but did not decode
     * >0 => decompressed and return new data size
     */

    int v = 0;
    if (sz > HEADZ_SIZE)
    {
        uint version = d[0];
        if (version >= 0x80)
        {
            // compress header
            version &= 0x7f;

            // version is newer
            if (version > WRITEZ_VERSION) return -1; // fail

            ++d;
            uint usize = _uintat(d); d += 4;
            uint csize = _uintat(d); d += 4;

            // must have exact compressed size
            if (csize != sz - HEADZ_SIZE) return -1;
            
            cp.decompress(d, csize, usize);
            v = usize;
            assert(v > 0);
        }
    }
    return v;
}
