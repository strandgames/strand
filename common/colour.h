/**
 *
 *    _    __        _      __                           
 *   | |  / /____   (_)____/ /_      __ ____ _ _____ ___ 
 *   | | / // __ \ / // __  /| | /| / // __ `// ___// _ \
 *   | |/ // /_/ // // /_/ / | |/ |/ // /_/ // /   /  __/
 *   |___/ \____//_/ \__,_/  |__/|__/ \__,_//_/    \___/ 
 *                                                       
 *  Copyright (�) Voidware 2017.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 * 
 *  contact@voidware.com
 */

#pragma once

#include <iostream>
#include <string>
#include <math.h>
#include "types.h"
#include "cutils.h"
#include "unum.h"

#define RED_MASK 0x00ff0000
#define GREEN_MASK 0x0000ff00
#define BLUE_MASK 0x000000ff

class Colour
{
public:

    static float clampf(float v)
    {
        return v > 0 ? (v <= 255.0f ? v : 255.0f) : 0;
    }

    static uint clamp(int v)
    {
        return v > 0 ? (v <= 255 ? v : 255) : 0;
    }

    static uint _toARGB(uchar a, uchar r, uchar g, uchar b)
    {
        // ASSUME no clamp needed
        return ((uint)a << 24) | ((uint)r << 16) | ((uint)g << 8) | b;
    }

    uint toABGR() const
    {
        // swap blue and red
        uint r = _argb & RED_MASK;
        uint b = _argb & BLUE_MASK;
        return (_argb & ~(RED_MASK | BLUE_MASK)) | (b << 16) | (r >> 16);
    }

    static uchar floatToUChar(float v)
    {
        // rounds
        return (uchar)clampf(v*255.0f + 0.5f);
    }

    uint alpha() const { return _argb >> 24; }
    uint red() const { return (_argb >> 16) & 0xff; }
    uint green() const { return (_argb >> 8) & 0xff; }
    uint blue() const { return _argb & 0xff; }

    float redf() const { return ((float)red())/255.0f; }
    float greenf() const { return ((unsigned int)green())/255.0f; }
    float bluef() const { return ((unsigned int)blue())/255.0f; }
    float alphaf() const { return ((unsigned int)alpha())/255.0f; }

    void clearAlpha()
    {
        // not pre-multiply, just clear
        _argb &= (RED_MASK | GREEN_MASK | BLUE_MASK);
    }

    struct HSB
    {
        float hue, saturation, brightness;

        HSB(Colour col) 
        {
            int r = col.red();
            int g = col.green();
            int b = col.blue();

            int hi = u_max (r, g, b);
            int lo = u_min (r, g, b);

            if (hi != 0)
            {
                saturation = (hi - lo) / (float) hi;

                if (saturation > 0)
                {
                    float invDiff = 1.0f / (hi - lo);
                    float red   = (hi - r) * invDiff;
                    float green = (hi - g) * invDiff;
                    float blue  = (hi - b) * invDiff;

                    if (r == hi)
                        hue = blue - green;
                    else if (g == hi)
                        hue = 2.0f + red - blue;
                    else
                        hue = 4.0f + green - red;

                    hue *= 1.0f / 6.0f;

                    if (hue < 0)
                        ++hue;
                }
                else hue = 0;
            }
            else saturation = hue = 0;

            brightness = hi / 255.0f;
        }

        Colour toColour() const 
        {
            return Colour(toRGB(hue, saturation, brightness, 0xff));
        }

        static uint toRGB (float h, float s, float v, uchar alpha)
        {
            uchar intV = floatToUChar(v);
            
            if (s <= 0)
                return _toARGB(alpha, intV, intV, intV);
            
            s = u_min (1.0f, s);
            h = (h - floor (h)) * 6.0f + 0.00001f;
            float f = h - floor (h);
            uchar x = floatToUChar(v * (1.0f - s));

            if (h < 1.0f)   return _toARGB(alpha, intV, floatToUChar (v * (1.0f - (s * (1.0f - f)))), x);
            if (h < 2.0f)   return _toARGB (alpha, floatToUChar (v * (1.0f - s * f)), intV, x);
            if (h < 3.0f)   return _toARGB (alpha, x, intV, floatToUChar (v * (1.0f - (s * (1.0f - f)))));
            if (h < 4.0f)   return _toARGB (alpha, x, floatToUChar (v * (1.0f - s * f)), intV);
            if (h < 5.0f)   return _toARGB (alpha, floatToUChar (v * (1.0f - (s * (1.0f - f)))), x, intV);
            return _toARGB (alpha, intV, x, floatToUChar (v * (1.0f - s * f)));
        }
    };
    
    Colour() { _argb = 0; }
    Colour(uint argb) : _argb(argb) {}
    Colour(uint r, uint g, uint b) { _argb = _toARGB(0xff, r, g, b); }
    Colour(uint r, uint g, uint b, uint a) { _argb = _toARGB(a, r, g, b); }
    Colour(uint r, uint g, uint b, float a)
    { _argb = _toARGB(floatToUChar(a), r, g, b); }

    // for values 0-1
    Colour(float r, float g, float b)
    {
        _argb = _toARGB(0xff,
                        floatToUChar(r), floatToUChar(g), floatToUChar(b));
    }
    Colour(float r, float g, float b, float a)
    {
        _argb = _toARGB(floatToUChar(a),
                        floatToUChar(r), floatToUChar(g), floatToUChar(b));
    }

    Colour withAlpha(uint newAlpha) const 
    {
        return Colour((newAlpha << 24) | (_argb & 0xffffff));
    }

    Colour withAlpha(float newAlpha) const
    {
        return withAlpha((uint)floatToUChar(newAlpha));
    }

    Colour withoutAlpha() const
    {
        // pre-multiply alpha
        float a = alphaf();
        if (a == 1) return *this; // has no alpha

        float r = redf() * a;
        float g = greenf() * a;
        float b = bluef() * a;

        return Colour(r, g, b);
    }

    Colour withoutAlpha(Colour bg) const
    {
        // pre-multiply alpha
        float a = alphaf();
        if (a == 1) return *this; // has no alpha

        // otherwise mix with bg

        Colour bg1 = bg.withoutAlpha();

        float r = redf()*a + (1-a)*bg1.redf();
        float g = greenf()*a + (1-a)*bg1.greenf();
        float b = bluef()*a + (1-a)*bg1.bluef();

        return Colour(r, g, b);
    }

    Colour mixAlpha(float alpha) const { return withAlpha(alphaf() * alpha); }

    Colour overlaidWith(Colour src) const
    {
        uint destAlpha = alpha();

        if (destAlpha == 0) return src;

        int invA = 0xff - src.alpha();
        int resA = 0xff - (((0xff - destAlpha) * invA) >> 8);

        if (resA <= 0) return *this;

        int da = (invA * destAlpha) / resA;

        return Colour ((src.red() + ((((int) red() - src.red()) * da) >> 8)),
                       (src.green() + ((((int) green() - src.green()) * da) >> 8)),
                       (src.blue()  + ((((int) blue()  - src.blue())  * da) >> 8)), (uint)resA);
    }

    bool operator==(const Colour& c) const { return _argb == c._argb; }
    bool operator!=(const Colour& c) const { return _argb != c._argb; }

    explicit operator bool() const { return _argb != 0; }

    bool transparent() const { return alpha() == 0; }

    enum Colours
    {
        black = 0xff000000,
        white = 0xffffffff,
        cred = 0xffff0000,
        cgreen = 0xff00ff00,
        cblue = 0xff0000ff,
        cyellow = 0xffffff00,
        ctransparent = 0,
    };

    static float getPerceivedBrightness(Colour c)
    {
        float r = c.redf();
        float g = c.greenf();
        float b = c.bluef();
        return sqrt (r * r * 0.241f + g * g * 0.691f + b * b * 0.068f);
    }

    bool isGrey() const
    {
        return red() == green() && green() == blue();
    }

    bool isWhite() const
    {
        return _argb == white;
    }

    bool isBlack() const
    {
        return (_argb & 0xFFFFFF) == 0;
    }
    
    Colour contrasting(float amount) const
    {
        return overlaidWith ((getPerceivedBrightness(*this) >= 0.5f
                              ? Colour(black)
                              : Colour(white)).withAlpha(amount));
    }

    Colour brighter(float factor) const
    {
        // or dimmer
        if (factor < 0) factor = 0;
        uint a = alpha();

        if (a != 0xff)
        {
            float af = alphaf() * factor;
            //af = u_min(1.0f, af * factor);
            return Colour(red(), green(), blue(), af);
        }
        
        Colour::HSB cc(*this);
        //cc.brightness = u_min(1.0f, cc.brightness*factor);
        cc.brightness = cc.brightness*factor;
        return cc.toColour();
    }

    Colour fade(float f) const
    {
        // fade the alpha by f.
        uint a = (int)(alpha() * f);
        if (a < 0) a = 0;
        else if (a > 255) a = 255;
        return ((_argb & 0x00FFFFFF) | (a << 24));
    }

    std::string toString() const
    {
        char buf[16];
        sprintf(buf, "#%02X%02X%02X%02X", alpha(), red(), green(), blue());
        return buf;
    }

    bool parse(const std::string& s) { return parse(s.c_str()); }

    struct NamedColour
    {
        const char*  _name;
        unsigned int _col;
    };

    bool parseSpecial(const char* s)
    {
        bool v = false;

        static const NamedColour colours[] =
        {
            { "black", black },
            { "white", white },
            { "red", cred },
            { "green", cgreen },
            { "blue", cblue },
            { "yellow", cyellow },
            { "transparent", ctransparent },
        };

        for (int i = 0; i < ASIZE(colours); ++i)
        {
            const NamedColour& ci = colours[i];
            if (!u_stricmp(ci._name, s))
            {
                _argb = ci._col;
                v = true;
                break;
            }
        }
        return v;
    }
    
    bool parse(const char* s)
    {
        // parse hex colours, #RRGGBB or #AARRGGBB
        
        bool v = parseSpecial(s);
        if (!v && *s == '#')
        {
            ++s;
            const char* e = s;
            while (u_ishex(*e)) ++e;
            unsigned int c = u_atohex(s);

            if (e - s == 8)
            {
                // have all including alpha
                v = true;
            }
            else if (e - s == 6)
            {
                // we have 6 digits
                c |= 0xFF000000; // alpha
                v = true;
            }

            if (v) _argb = c;

        }
        return v;
    }

    static float interpolate(float v1, float v2, float a)
    {
        if (a <= 0) return v1;
        if (a >= 1) return v2;
        return v1*(1-a) + v2*a;
    }

    Colour interpolate(Colour c2, float a) const
    {
        float r = interpolate(redf(), c2.redf(), a);
        float g = interpolate(greenf(), c2.greenf(), a);
        float b = interpolate(bluef(), c2.bluef(), a);
        float al = interpolate(alphaf(), c2.alphaf(), a);
        return Colour(r, g, b, al);
    }

#define RGB2Y(R, G, B) (((66 * (R) + 129 * (G) +  25 * (B) + 128) >> 8) +  16)
#define RGB2U(R, G, B) ((( -38 * (R) -  74 * (G) + 112 * (B) + 128) >> 8) + 128)
#define RGB2V(R, G, B) (((112 * (R) -  94 * (G) -  18 * (B) + 128) >> 8) + 128)

    Colour toYUV() const
    {
        int r = red();
        int g = green();
        int b = blue();
        return Colour(clamp(RGB2Y(r,g,b)),clamp(RGB2U(r,g,b)),clamp(RGB2V(r,g,b)));
    }

    unsigned int toY() const
    {
        int r = red();
        int g = green();
        int b = blue();
        return clamp(RGB2Y(r,g,b));
    }

    float toYf() const
    {
        return toY()/255.0f;
    }

    friend std::ostream& operator<<(std::ostream& os, const Colour& c)
    { return os << c.toString(); }

    uint _argb;
};
