/**
 *
 *    _    __        _      __                           
 *   | |  / /____   (_)____/ /_      __ ____ _ _____ ___ 
 *   | | / // __ \ / // __  /| | /| / // __ `// ___// _ \
 *   | |/ // /_/ // // /_/ / | |/ |/ // /_/ // /   /  __/
 *   |___/ \____//_/ \__,_/  |__/|__/ \__,_//_/    \___/ 
 *                                                       
 *  Copyright (�) Voidware 2016-2017.
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to
 *  deal in the Software without restriction, including without limitation the
 *  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 *  sell copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 * 
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 *  IN THE SOFTWARE.
 * 
 *  contact@voidware.com
 */

#pragma once

#include <string.h>
#include <string>
#include <ctype.h>
#include <vector>
#include "cutils.h"
#include "utf8.h"

inline std::string replaceAll(const std::string& source,
                              const std::string& from,
                              const std::string& to)
{
    std::string newString;
    newString.reserve(source.length());  // avoids a few memory allocations

    std::string::size_type lastPos = 0;
    std::string::size_type findPos;

    while(std::string::npos != (findPos = source.find(from, lastPos)))
    {
        newString.append( source, lastPos, findPos - lastPos );
        newString += to;
        lastPos = findPos + from.length();
    }

    newString += source.substr( lastPos );
    return newString;
}

inline std::string replaceAll(const std::string& source,
                               uint from,
                               uint to)
{
    // Unicode subst
    // `to` can be 0 to remove.
    std::string s;
    
    Utf8 us(source);
    Utf8 ue = us;
    uint c;
    
    while ((c = ue.get()) != 0)
    {
        if (c == from)
        {
            Utf8 unext = ue;
            ue.backup(); // to from char
            s.append(us._s, ue._s - us._s); // add in ok chars
            if (to) Utf8::appendU(s, to); // add to char
            ue = us = unext;  // continue after from char
        }
    }

    if (s.empty()) return source;  // optimization
    
    s.append(us._s, ue._s - us._s); 
    return s;
}


inline void replaceCharsInplace(std::string& source, char sc, char dc)
{
    // XX apparently you're not supposed to modify the internals
    // util C++17 ?
    char* p = const_cast<char*>(source.data());
    while (*p)
    {
        if (*p == sc) *p = dc;
        ++p;
    }
}

inline bool startsWith(const char* s1, const char* pat)
{
    for(; *pat && *s1 == *pat; ++s1, ++pat) ;
    return !*pat;
}

inline bool startsWith(const std::string& s, const char* pat)
{ return startsWith(s.c_str(), pat); }

inline bool startsWithIgnoreCase(const char* s1, const char* pat)
{
    // XX consider utf8 lower case conversion
    for(; *pat && u_tolower(*s1) == u_tolower(*pat); ++s1, ++pat) ;
    return !*pat;
}

inline bool startsWithIgnoreCase(const std::string& s, const char* pat)
{ return startsWithIgnoreCase(s.c_str(), pat); }

inline bool endsWith(const char* s, char c)
{
    bool res = false;
    if (s)
    {
        size_t sz = strlen(s);
        res = sz > 0 && s[sz-1] == c;
    }
    return res;
}

inline bool endsWith(const std::string& s, char c)
{
    return endsWith(s.c_str(), c);
}

inline std::string suffixOf(const char* s)
{
    // eg ".dat"
    // find any end of path
    const char* sep = strrchr(s, '/');
    if (!sep) sep = strrchr(s, '\\');

    // any existing suffix must be after path
    const char* p = strrchr(s, '.');
    if (p && (!sep || p > sep)) return p;
    return std::string();
}

inline std::string suffixOf(const std::string& s)
{ return suffixOf(s.c_str()); }

inline std::string changeSuffix(const char* s, const char* suf)
{
    // add or change suffix of `s` to `suf'.
    // eg suf is ".moo"
    // suf 0 to remove

    std::string s2;

    // find any end of path
    const char* sep = strrchr(s, '/');
    if (!sep) sep = strrchr(s, '\\');

    // any existing suffix must be after path
    const char* p = strrchr(s, '.');
    if (p && (!sep || p > sep))
        s2 = std::string(s, p - s); // stem
    else
        s2 = s;

    if (suf) s2 += suf;
    
    return s2;
}

inline std::string changeSuffix(const std::string& s, const char* suf)
{
    return changeSuffix(s.c_str(), suf);
}

inline std::string trim(const std::string& s)
{
    const char* sp = s.c_str();
    const char* ep = sp + s.size();
    while (u_isspace(*sp)) ++sp;
    while (ep > sp && u_isspace(ep[-1])) --ep;
    return std::string(sp, ep - sp);
}

inline void trimInplaceLeft(std::string& s)
{
    const char* sp = s.c_str();
    const char* ep = sp;
    
    while (u_isspace(*ep)) ++ep;
    if (ep != sp) s.erase(0, ep - sp);
}

inline void trimInplaceRight(std::string& s)
{
    const char* sp = s.c_str();
    const char* ep = sp + s.size();
    
    while (ep > sp && u_isspace(ep[-1])) --ep;
    if (*ep) s.erase(ep - sp, std::string::npos);
}

inline void trimInplace(std::string& s)
{
    trimInplaceLeft(s);
    trimInplaceRight(s);
}

inline std::string toUpper(const char* s)
{
    Utf8 us(s);
    return us.toupper();
}

inline std::string toUpper(const std::string& s) { return toUpper(s.c_str()); }
inline std::string toLower(const char* s)
{
    Utf8 us(s);
    return us.tolower();
}

inline std::string toLower(const std::string& s) { return toLower(s.c_str()); }

// XX consider utf8
inline int compareIgnoreCase(const char* s1, const char* s2)
{ return u_stricmp(s1, s2) ; }

inline int compareIgnoreCase(const std::string& s1, const char* s2)
{ return compareIgnoreCase(s1.c_str(), s2); } 

inline int compareIgnoreCase(const char* s1, const std::string& s2)
{ return compareIgnoreCase(s1, s2.c_str()); }

inline int compareIgnoreCase(const std::string& s1, const std::string& s2)
{ return compareIgnoreCase(s1.c_str(), s2.c_str()); }

inline bool equalsIgnoreCase(const char* s1, const char* s2)
{ return compareIgnoreCase(s1, s2) == 0; }

inline bool equalsIgnoreCase(const std::string& s1, const char* s2)
{ return compareIgnoreCase(s1.c_str(), s2) == 0; } 

inline bool equalsIgnoreCase(const char* s1, const std::string& s2)
{ return compareIgnoreCase(s1, s2.c_str()) == 0; }

inline bool equalsIgnoreCase(const std::string& s1, const std::string& s2)
{ return compareIgnoreCase(s1.c_str(), s2.c_str()) == 0; }

inline bool lessIgnoreCase(const std::string& s1, const std::string& s2)
{ return compareIgnoreCase(s1.c_str(), s2.c_str()) < 0; }

inline bool sameWithUnderscore(const char* s1, const char* s2)
{
    // compare two strings ignoring case and allow space to match underscore
    for (;;) 
    {
        char c1 = *s1 == '_' ? ' ' : u_tolower(*s1);
        char c2 = *s2 == '_' ? ' ' : u_tolower(*s2);

        if (c1 != c2) return false;
        if (!c1) break;
        ++s1;
        ++s2;
    }
    return true;
}

inline void split(std::vector<std::string>& list, const char* p, char c = ' ')
{
    while (*p == c) ++p;
    while (*p)
    {
        const char* start = p;
        while (*p && *p != c) ++p;
        if (p != start)
            list.emplace_back(std::string(start, p));
        while (*p == c) ++p;
    }
}

inline void split(std::vector<std::string>& list, 
                  const std::string& s, char c = ' ')
{
    split(list, s.c_str(), c);
}

inline std::vector<std::string> split(const std::string& s, char c = ' ')
{
    std::vector<std::string> list;
    split(list, s, c);
    return list;
}

inline std::string unsplit(const std::vector<std::string>& ws,
                           const char* sep, size_t start, size_t n)
{
    std::string res;
    size_t sz = ws.size();
    if (n > sz) n = sz;
    
    if (n > 0)
    {
        size_t i;
        for (i = start; i < n-1; ++i) { res += ws[i]; res += sep; }
        res += ws[i];
    }
    return res;
}

inline std::string unsplit(const std::vector<std::string>& ws,
                           char c, size_t start, size_t n)
{
    char buf[2];
    buf[0] = c;
    buf[1] = 0;
    return unsplit(ws, buf, start, n);
}

inline std::string unsplit(const std::vector<std::string>& ws, char c = ' ')
{
    return unsplit(ws, c, 0, ws.size());
}

inline std::string unsplit(const std::vector<std::string>& ws, const char* sep)
{
    return unsplit(ws, sep, 0, ws.size());
}

inline const char* findFirst(const char* s, const char* pat)
{
    // find location of `pat' in `s', or 0 if none.
    // TODO: utf8
    return strstr(s, pat);
}

inline const char* findFirst(const std::string& s, const char* pat)
{
    return findFirst(s.c_str(), pat);
}

inline const char* findFirst(const std::string& s, const std::string& pat)
{
    return findFirst(s.c_str(), pat.c_str());    
}

inline const char* findFirstIgnoreCase(const char* s, const char* pat)
{
    // does `pat` occur in `s`
    // NB: not utf8
    return u_stristr(s, pat);
}

inline const char* findFirstIgnoreCase(const std::string& s, const char* pat)
{
    // does `pat` occur in `s`
    // NB: not utf8
    return u_stristr(s.c_str(), pat);
}

inline const char* findFirstIgnoreCase(const std::string& s,
                                       const std::string& pat)
{
    // does `pat` occur in `s`
    // NB: not utf8
    return u_stristr(s.c_str(), pat.c_str());
}

inline std::string simplifyChars(const char* s)
{
    // eliminate some unnecessary utf8
    static const uint from[] = 
        {
            0x2018, 0x2019,  // '
            0x2013, 0x2014,  // -
            0x201c, 0x201d,  // "
        };
    
    static const uint to[] = 
        {
            '\'', '\'',
            '-', '-',
            '"', '"',
        };

    Utf8 u(s);

    // map special spaces into normal ones 
    return u.map(from, to, ASIZE(from), Utf8::mflag_dropspaces);
}

inline std::string simplifyChars(const std::string& s)
{
    return simplifyChars(s.c_str());
}

inline std::string lastLineOf(const char* s, size_t sz,
                              size_t maxc = 0, // max chars to scan
                              size_t* offset = 0) // optional offset of line
{
    const char* ep = s + sz;

    // skip back any newlines or space on end
    while (ep != s && u_isspace(ep[-1])) --ep;
    
    const char* e = ep;
    while (e != s)
    {
        --e;
        if (ep - e == (int)maxc) break; // reached limit
        if (u_isnl(*e))
        {
            ++e;
            break;
        }
    }

    // either e == s, at start OR e is after a newline
    if (offset) *offset = e - s; // offset of last line in original
    return std::string(e, ep - e);
}

inline std::string lastLineOf(const std::string& s, 
                              size_t maxc = 0, size_t* offset = 0)
{
    return lastLineOf(s.c_str(), s.size(), maxc, offset);
}

inline bool isDigits(const char* p)
{
    while (*p) 
    {
        if (!u_isdigit(*p)) return false;
        ++p;
    }
    return true;
}

inline bool isDigits(const std::string& s) { return isDigits(s.c_str()); }

inline bool isNumeric(const std::string& s)
{
    const char* p = s.c_str();
    bool point = false;
    if (*p == '-') ++p; // allow initial minus
    while (*p) 
    {
        if (*p == '.')
        {
            // allow just one point
            if (point) return false;
            point = true;
        }
        else if (!u_isdigit(*p)) return false;
        ++p;
    }
    return true;
}

inline std::string firstWordOf(const char* s)
{
    while (u_isspace(*s)) ++s;
    const char* p = s;
    while (*p && !u_isspace(*p)) ++p;
    return std::string(s, p - s);
}

inline std::string firstWordOf(const std::string& s)
{
    return firstWordOf(s.c_str());
}

inline std::string alphaOf(const char* s)
{
    while (u_isspace(*s)) ++s;
    const char* p = s;
    while (u_isalpha(*p)) ++p;
    return std::string(s, p - s);
}

inline std::string alphaOf(const std::string& s)
{
    return alphaOf(s.c_str());
}

inline std::string lastWordOf(const char* s)
{
    const char* p = strrchr(s, ' ');
    if (p) s = p + 1;
    return s;
}

inline std::string lastWordOf(const std::string& s)
{
    return lastWordOf(s.c_str());
}

inline std::string CapitaliseStartWords(const std::string& s)
{
    // capitalise the start of each word
    std::string r;
    const char* p = s.c_str();

    while (*p)
    {
        while (isspace(*p)) r += *p++;
        if (!*p) break;
        r += u_toupper(*p);
        ++p;
        while (*p && !isspace(*p)) r += *p++;
    }
    return r;
}

inline std::string CapitaliseStart(const char* s)
{
    std::string s1;
    if (*s)
    {
        Utf8 us(s);
        uint c = Utf8::toupper(us.get());
        Utf8::appendU(s1, c);
        s1 += us._s;
    }
    return s1;
}

inline std::string CapitaliseStart(const std::string& s)
{ return CapitaliseStart(s.c_str()); }


inline bool isUpper(const char* s)
{
    // string is all uppercase letters or empty
    // strings with chars that do not have lower case (eg punctuation)
    // are not uppercase strings
    Utf8 us(s);
    uint c;
    while ((c = us.get()) != 0)
    {
        if (!Utf8::isUpper(c)) return false;
    }
    return true;
}

inline bool isUpper(const std::string& s)
{ return isUpper(s.c_str()); }

inline bool hasLower(const char* s)
{
    // true if does not contain lower case letter
    Utf8 us(s);
    uint c;
    while ((c = us.get()) != 0)
        if (Utf8::isLower(c)) return true;

    return false;
}

inline bool hasLower(const std::string& s)
{ return hasLower(s.c_str()); }

static inline std::string simplifyPath(const std::string& p)
{
    std::string p1 = replaceAll(p, "/./", "/");
    if (startsWith(p1, "./")) p1.erase(0,2);
    
    int n = p1.size();
    if (n > 1)
    {
        if (p1[n-1] == '.' && p1[n-2] == '/')
        {
            // ends in "/."
            p1.erase(n-1); // drop "."
        }
    }
    return p1;
}

static inline std::string makePath(const std::string& prefix,
                                   const std::string& name) 
{
    std::string path;
    if (!name.empty())
    {
        // windows style or linux style absolute path given
        // then do not apply prefix
        if (name.find(':') != std::string::npos || 
            name.at(0) == '/' ||
            name.at(0) == '~')
        {
            // if we have a prefix like C: or https:// or "/" or ~ then
            // assume `name` is an absolute path.
            path = name;
        }
        else
        {
            path = prefix;
            if (!path.empty())
            {
                char c = path.back();
                if (c != '/' && c != '\\') path += '/';
            }
            path += name;
        }

        // enough backslashes! windows files work forwards too!
        replaceCharsInplace(path, '\\', '/');
        path = simplifyPath(path);
    }

    return path;
}

static inline std::string filenameOf(const std::string& path)
{
    std::string name;

    // look for last file separator or, eg, c:
    size_t p = path.find_last_of("\\/:"); 
    if (p != std::string::npos)
    {
        name = path.substr(p + 1);
    }
    else
    {
        // path is just a filename
        name = path;
    }

    return name;
}

static inline std::string pathOf(const std::string& path)
{
    // if the path ends in a filename remove it, and the final slash
    
    std::string ps;

    // look for last file separator or, eg, c:
    size_t p = path.find_last_of("\\/"); 
    if (p != std::string::npos)
    {
        ps = path.substr(0, p);
    }
    else
    {
        ps = ".";  // was just a filename
    }

    return ps;
}

static inline const char* parseMarkdownLink(const char* s,
                                            std::string* head = 0,
                                            std::string* tail = 0)
{
    // return the point atfter the markdown
    // but only if valid, otherwise return original s.
    // will only skip fully defined markdown [head](tail)
    //

    Utf8 us(s);
    uint c = us.get();
    if (c == '[')
    {
        Utf8 ue(us);  // ue & us at start of head
        for (;;)
        {
            c = ue.get();
            if (!c) break;

            if (c == ']')
            {
                if (head)
                {
                    ue.backup(); // back to ']'
                    
                    // collect head string
                    *head = trim(std::string(us._s, ue._s - us._s));
                    ue.get();  // ']'
                }
                break;
            }
        }

        // now read the '(tail)' part
        if (c)
        {
            c = ue.get();
            if (c == '(')  // must follow immediately
            {
                us = ue; // ue & us at start of tail
                int level = 1;
                for (;;)
                {
                    c = ue.get();
                    if (!c) break;

                    if (c == '(') ++level;
                    else if (c == ')')
                    {
                        if (!--level)
                        {
                            // found end. ue is char after ')'
                            s = ue._s;

                            if (tail)
                            {
                                ue.backup(); // onto ')'
                                *tail = trim(std::string(us._s, ue._s - us._s));
                            }
                            
                            break;
                        }
                    }
                }
            }
        }
    }
    return s;
}

static inline std::string ProcessEscapes(const char* text,
                                         const char* te, bool& inQuote)
{
    // also newlines within a single text segment are changed to space
    // multiple spaces are combined.
    std::string s;
    bool esc = false;
    uint last = 0;

    Utf8 us(text);
        
    while (us._s != te)
    {
        uint c = us.get();
        if (!c) break;

        // peek the next 2 chars
        uint c1 = *us;
        uint c2 = us.at1();

        if (esc)
        {
            esc = false;
            if (c == 'n') c = '\n'; // \n into newline
            else if (c == 'r') c = '\r'; // \r into soft newline
        }
        else
        {
            if (c == '\\')
            {
                esc = true;
                continue;
            }
            
            if (Utf8::isQuote(c))
            {
                // convert to Unicode start and end quote.
                if (c == '"')
                    c = inQuote ? Utf8::closeQuote : Utf8::openQuote;
                
                inQuote = !inQuote;
            }
            else if (Utf8::isNL(c))
            {
                if (Utf8::isNL(c1))
                {
                    // line break is preserved.
                    Utf8::appendU(s, c); // nl
                    c = us.get();
                }
                else
                {
                    c = ' '; // single nl  changed to space
                }
            }
            else if (c == '.' && c1 == '.' && c2 == '.')
            {
                // ellipsis
                c = Utf8::ellipsis;

                // consume next 2 dots.
                us.get();
                us.get();
            }
            else if (c == '-' && c1 == '-' && c2 != '-')
            {
                c = Utf8::longHyphen;
                us.get(); // consume second dash
            }
            else if (c == '\'')
            {
                // change to smart apostrophe.
                
                // end or space (or MD) following must be a close
                bool close = !c1 || Utf8::_isspace(c1) || c1 == '*' || c1 == '_';

                // otherwise
                // if no last or space/punct before (or MD), then open
                if (!close &&
                    (!last
                     || Utf8::_isspace(last)
                     || Utf8::isPunct(last)
                     || last == '*' || last == '_'  // markdown
                     ))
                {
                    c = Utf8::openApostrophe;
                }
                else
                {
                    // otherwise close
                    c = Utf8::closeApostrophe;
                }
            }
        }

        if (c == '\t') c = ' ';  // tabs changed to space

        // remove duplicate space
        if (c == ' ' && last == ' ') c = 0;

        if (c)
        {
            Utf8::appendU(s, c);
            last = c;
        }
    }
        
    //LOG1("Escape before, '", text << "'");
    //LOG1("Escape after '", s << "'");

    return s;
}

static inline std::string RemoveEscapes(const char* s, const char* e = 0)
{
    std::string ss;
    bool esc = false;

    if (!e) e = s + strlen(s);
    while (s != e)
    {
        char c = *s++;

        if (!esc)
        {
            if (c == '\\')
            {
                esc = true;
                continue;
            }
        }
        else
        {
            esc = false;
            if (c == 'n') c = '\n';
            else if (c == 'r') c = '\r';
        }

        ss += c;
    }
    return ss;
}

static inline std::string RemoveEscapes(const std::string& t)
{ return RemoveEscapes(t.c_str()); }

static inline std::string AddEscapes(const char* t)
{
    // put escapes into string 
    std::string s;
    for (;;)
    {
        char c = *t++;
        if (!c) break;
        if (c == '\n')
        {
            s += '\\';
            c = 'n';
        }
        if (c == '\r')
        {
            s += '\\';
            c = 'r';
        }
        else if (c == '"' || c == '\\') s += '\\';
        
        s += c;
    }
    return s;
}

static inline std::string AddCodes(const char* t)
{
    std::string s;

    Utf8 us(t);
    uint c;

    while ((c = us.get()) != 0)
    {
        Utf8::appendU(s, c);
        if (c >= 0x80)
        {
            s += '(';
            char buf[10];
            sprintf(buf, "%0X", c);
            s += buf;
            s += ')';
        }
    }

    return s;
}

static inline std::string RemoveMarkdown(const std::string& t,
                                         bool keeptail = true)
{
    // remove markdown
    // if `keeptail`, we include link tails so "[foo](bar)" becomes "foo (bar)"
    // otherwise "[foo](bar)" becomes "foo".
    std::string s = replaceAll(t, '*', 0);
    s = replaceAll(s, '_', 0);

    std::string sr;
    const char* ss = s.c_str();
    const char* se = ss;
    for (;;)
    {
        const char* s1 = strchr(se, '[');
        if (s1)
        {
            se = s1;
            std::string head;
            std::string tail;
            s1 = parseMarkdownLink(se, &head, &tail);
            if (s1 != se)
            {
                sr.append(ss, se-ss);  // add existing until'['
                sr.append(head);

                if (keeptail)
                {
                    sr += " (";
                    sr.append(tail);
                    sr += ")";
                }
                
                se = ss = s1;
            }
            else
            {
                // parse failed, continue
                ++se; // skip failed '['
            }
        }
        else
        {
            // last piece
            sr.append(se);
            break;
        }
    }
    return sr;
}

static inline std::string AddEscapes(const std::string& t)
{ return AddEscapes(t.c_str()); }

static inline void concatStrings(std::string& s, const std::string& s1)
{
    // append s1 to s and add space if necessary

    // note, some cases cannot be fixed.
    // eg "FOO" FOO -> "foo"foo
    // because we dont know if it's a start or end quote
    // otherwise get, " foo" foo
    // where one is correct and other end wrong.
    // NB: this is fixed by using start and end quote codes.

    if (!s1.empty() && !s.empty())
    {
        Utf8 us(s);

        uint last2 = 0;
        uint last3 = 0;
            
        Utf8 uslast(us);
        int sz = uslast.toLast();

        if (sz > 1)
        {
            uslast.backup();
            uslast.backup();
            last3 = uslast.get();
            last2 = uslast.get();
        }
        else if (sz)
        {
            // if we have a last but one, get it
            uslast.backup();
            last2 = uslast.get();
        }
            
        uint last = uslast.get();
            
        Utf8 us1(s1);
        uint first = *us1; // first char of second string
        uint second = us1.at(1);
        uint third = us1.at(2);

        // whether we are to add a space
        bool spc = false;

        // normally wont end in a space already but it could
        // be escaped in.
        // first char wont be space unless escaped in
        if (!u_isspace(last) && !u_isspace(first))
        {
            // open quotes must be preceded by space
            if (first == Utf8::openQuote) spc = true;
            else
            {
                // or any other open bracket
                if (first < 0xff && strchr("([{",first)) spc = true;
            }

            bool alnumFirst = false;

            if (first == '_')
            {
                // _foo is start of word
                alnumFirst = Utf8::_isalnum(second);
            }
            else if (first == '*')
            {
                // **foo is start of word
                if (second == '*' && Utf8::_isalnum(third))
                    alnumFirst = true;
            }
            else
            {
                alnumFirst = Utf8::_isalnum(first);
            }

            if (!spc)
            {
                // end quote or close bracket followed by alphanum
                // (ie non-punc)
                if (alnumFirst)
                {
                    if (last == Utf8::closeQuote) spc = true;
                    else if (last < 0xff && strchr(")}]", last)) spc = true;
                }
            }
                    
            if (!spc)
            {
                uint c1 = last;
                if (last == '_')
                {
                    // end of last word with underscore is markdown
                    // take previous char
                    c1 = last2;
                }
                else if (last == '*' && last2 == '*')
                {
                    // end of last word with two asterisk is markdown
                    // take previous char
                    c1 = last3;
                }
                    
                bool albefore = Utf8::_isalnum(c1);
                bool puncbefore = Utf8::isPunct(c1);
                    
                // preceding punctuation that requires a space following
                // when we continue with a normal word.
                if (albefore || puncbefore)
                {
                    // add a space if next segment starts with non-punctuation
                    spc = alnumFirst;

                    // also certain chars count like letters here, eg $40
                    if (first < 0xff && strchr("£$", first) != 0) spc = true;

                    // plain quotes following punctuation must have
                    // space. eg he said, "hello"
                    if (puncbefore && first == '"') spc = true;
                }
            }
        }

        if (spc) s += ' ';

        bool done = false;
            
        if (Utf8::isPeriod(last))
        {
            // consider capitalisation following full-stop

            // if we start with lower then need it
            // NB: if we start with escaped space then ignore.
            first = us1.get();
            if (first == '[')
            {
                // handle capitalisation needed in markdown
                // wobble. [the parrot](..) doesn't want it
                uint first2 = us1.get();
                uint ufirst2 = Utf8::toupper(first2);

                if (ufirst2 != first2)
                {
                    s += first;
                    Utf8::appendU(s, ufirst2);  // capitalise
                    s += us1._s; // append remainder
                    done = true;                        
                }
            }
            else
            {
                // handle starting with lower case
                uint ufirst = Utf8::toupper(first);
                if (ufirst != first)
                {

                    Utf8::appendU(s, ufirst);
                    s += us1._s; // append remainder
                    done = true;
                }
            }
        }

        if (!done) s += s1;
    }
    else s += s1;
}

#if 0
inline void subst(char* s, size_t n, const char* replace)
{
    // replace `n` chars at `s` with `replace`
    char* q = s + n;
    size_t len = strlen(q);
    size_t rlen = strlen(replace);
    memmove(s + rlen, q, len + 1); // +terminator
    memcpy(s, replace, rlen);
}
#endif

