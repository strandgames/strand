#pragma once

#include <assert.h>
#include <string.h> // strlen

static inline unsigned char* pack7(const char* s, unsigned int& olen)
{
    unsigned int sz = strlen(s);
    unsigned int l = (sz*7 + 7)/8;
    unsigned char* d = new unsigned char[l];

    unsigned int v = 0;
    unsigned char* dp = d;
    int bc = 0;
    for (unsigned int i = 0; i < sz; ++i)
    {
        v <<= 7;
        v |= s[i];
        bc += 7;
        if (bc >= 8)
        {
            unsigned int a = v >> (bc - 8);
            *dp++ = a;
            bc -= 8;
        }
    }

    if (bc)
    {
        // bc < 8
        v <<= (8 - bc);
        *dp++ = v;
    }

    //printf("pack len %d, calc len %d\n", dp - d, l);
    assert(dp - d == l);
    
    olen = l;
    return d;
}

static inline char* unpack7(const unsigned char* d, unsigned int sz)
{
    unsigned int olen = sz*8/7 + 1;
    char* s = new char[olen];
    char* sp = s;
    int bc = 0;
    unsigned int v = 0;
    for (unsigned int i = 0; i < sz; ++i)
    {
        v <<= 8;
        v |= *d++;
        bc += 8;

        while (bc >= 7)
        {
            unsigned int a = v >> (bc - 7);
            *sp++ = a & 0x7f;
            bc -= 7;
        }
    }
    if (bc)
    {
        //printf("unpack spare bits %d\n", bc);
        
        // bc < 7
        v <<= (7 - bc);
        *sp++ = v & 0x7f;
    }
    *sp = 0; // terminate
    return s;
}
