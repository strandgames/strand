//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include "point.h"

// left, top, right, bottom
#define PX1  0
#define PY1  1
#define PX2  2
#define PY2  3

template<typename T> struct Box
{
    typedef Point2T<T> Point2;
    
    T       _v[4];

    T& operator[](int i) { return _v[i]; }
    const T& operator[](int i) const { return _v[i]; }

    T  width() const { return _v[PX2] - _v[PX1]; }
    T  height() const { return _v[PY2] - _v[PY1]; }

    Point2 topleft() const { return Point2(_v[PX1], _v[PY1]); }
    Point2 topright() const { return Point2(_v[PX2], _v[PY1]); }
    Point2 bottomleft() const { return Point2(_v[PX1], _v[PY2]); }
    Point2 bottomright() const { return Point2(_v[PX2], _v[PY2]); }
    
    Box() { clear(); }
    
    Box(T x1, T y1, T x2, T y2)
    {
        _v[PX1] = x1;
        _v[PY1] = y1;
        _v[PX2] = x2;
        _v[PY2] = y2;
    }

    bool valid() const { return _v[PX2] > _v[PX1] && _v[PY2] > _v[PY1]; }
    operator bool() const { return valid(); }

    bool operator==(const Box& b) const
    {
        for (int i = 0; i < 4; ++i) if (_v[i] != b._v[i]) return false;
        return true;
    }

    bool operator!=(const Box& b) const { return !(*this == b); }

    bool isUnity() const
    {
        return _v[PX1] == 0 && _v[PY1] == 0 && _v[PX2] == 1 && _v[PY2] == 1;
    }

    void setUnity()
    {
        _v[PX1] = 0;
        _v[PX2] = 1;
        _v[PY1] = 0;
        _v[PY2] = 1;
    }

    void clear() { _v[PX1] = _v[PX2] = _v[PY1] = _v[PY2] = 0; }
    
    void move(T dx, T dy)
    {
        _v[PX1] += dx;
        _v[PY1] += dy;
        _v[PX2] += dx;
        _v[PY2] += dy;        
    }

    void move(const Point2& d) { move(d.x, d.y); }
    void expand(T d) { expand(d, d); }

    void expand(T dx, T dy)
    {
        _v[PX1] -= dx;
        _v[PY1] -= dy;
        _v[PX2] += dx;
        _v[PY2] += dy;
        //if (!valid()) clear();
    }

    void expandRight(T dx)
    {
        _v[PX2] += dx;
        //if (!valid()) clear();
    }

    void scale(T sx, T sy)
    {
        _v[PX1] *= sx;
        _v[PY1] *= sy;
        _v[PX2] *= sx;
        _v[PY2] *= sy;
    }

    void scale(const Point2& p)
    {
        scale(p.x, p.y);
    }

    void scaleOrigin(const Point2& sc, const Point2& o)
    {
        move(-o.x, -o.y);
        scale(sc);
        move(o.x, o.y);              
    }

    void intersect(const Box& b)
    {
        if (b)
        {
            int x1 = u_max(_v[PX1], b._v[PX1]);
            int y1 = u_max(_v[PY1], b._v[PY1]);
            int x2 = u_min(_v[PX2], b._v[PX2]);
            int y2 = u_min(_v[PY2], b._v[PY2]);
            _v[PX1] = x1;
            _v[PY1] = y1;
            _v[PX2] = x2;
            _v[PY2] = y2;
        }
        else
        {
            clear();
        }
    }

    bool contains(const Point2& p) const
    {
        return p.x >= _v[PX1] && p.x < _v[PX2] &&
            p.y >= _v[PY1] && p.y < _v[PY2];
    }


    T centrex() const { return (_v[PX1] + _v[PX2])/2; }
    T centrey() const { return (_v[PY1] + _v[PY2])/2; }
    Point2 centre() const { return Point2(centrex(), centrey()); }
    Point2 size() const { return Point2(width(), height()); }

    /*
    void round(const Box<float>& b)
    {
        for (int i = 0; i < 4; ++i) _v[i] = floor(b._v[i] + 0.5f);
    }
    

    template<typename C>
    void from(const Box<C>& b)
    {
        _from(*this, b);
    }

    static void _from(Box<T>& a, const Box<T>& b)
    {
        for (int i = 0; i < 4; ++i) a._v[i] = b._v[i];
    }
    
    static void _from(Box<float>& a, const Box<int>& b)
    {
        for (int i = 0; i < 4; ++i) a._v[i] = b._v[i];
    }

    static void _from(Box<int>& a, const Box<float>& b)
    {
        for (int i = 0; i < 4; ++i) a._v[i] = floor(b._v[i] + 0.5f);
    }
    */


    friend std::ostream& operator<<(std::ostream& os, const Box& b)
    {
        os << '[';
        for (int i = 0; i < 4; ++i)
        {
            if (i) os << ' ';
            os << b._v[i];
        }
        return os << ']';
    }
};

typedef Box<float> Boxf;
typedef Box<int> Boxi;







