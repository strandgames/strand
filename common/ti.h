//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#define USE_PRINTFxx

#include <chrono>

#ifndef USE_PRINTF
#include "logged.h"
#endif

#ifdef LOGGING
#define TIMER(_n)  TILog ti(_n)
#define TIMERV(_v, _n)  TILog _v(_n)
#else
#define TIMER(_n)
#define TIMERV(_v, _n)
#endif
struct TI
{
    typedef std::chrono::steady_clock Clock;
    typedef std::chrono::time_point<Clock> Time;

    Time        _start;
    Time        _end;

    static Time now()
    {
        return Clock::now();
    }
    
    void start()
    {
        _start = now();
    }

    void stop()
    {
        _end = now();
    }

    double duration() const
    {
        std::chrono::duration<double> dt = _end - _start;
        double d = dt.count(); // seconds
        return d;
    }

};

#ifdef LOGGING
struct TILog: public TI
{
    const char* _name;
    
    TILog(const char* name) : _name(name) { start(); }
    
    ~TILog()
    {
        stop();
        report();
    }

    void report(const char* msg = 0, double dmin = 0)
    {
        double d = duration() * 1000.0; // to ms
        if (d >= dmin)
        {
            if (msg)
            {
#ifdef USE_PRINTF
                printf("%s %s, %g\n", _name, msg, d);
#else                
                LOG1(_name, " " << msg << ", " << d);
#endif                
            }
            else
            {
#ifdef USE_PRINTF
                printf("%s, %g\n", _name, d);
#else                
                LOG1(_name, " time " << d);
#endif                
            }
        }
    }
};
#endif // LOGGING

