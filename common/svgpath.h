//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <math.h>
#include <float.h>
#include "point.h"
#include "colour.h"
#include "unum.h"
#include "ifischema.h"
#include "jsonwalker.h"
#include "logged.h"

struct PathTraits
{
    typedef std::string                 string;
    typedef std::vector<Point2f>        Points;
};

struct Poly: public PathTraits
{
    // closed polygon without holes defined by a set of points
    Points      _points;
    Point2f     _minp;
    Point2f     _maxp;
    float       _strokeW = 1;
    Colour      _col = Colour(Colour::white);

    Poly() { clear(); }

    void add(const Point2f& p)
    {
        _points.push_back(p);

        // update bounds
        if (p.x < _minp.x) _minp.x = p.x;
        if (p.x > _maxp.x) _maxp.x = p.x;
        if (p.y < _minp.y) _minp.y = p.y;
        if (p.y > _maxp.y) _maxp.y = p.y;
    }

    void addNext(const Point2f& p)
    {
        // for adding one by one. ensure no repeats
        int n = _points.size();
        if (!n || _points[n-1] != p) add(p);
    }

    bool valid() const
    {
        bool r = !_points.empty();
        if (r)
        {
            assert(_maxp.x >= _minp.x);
            assert(_maxp.y >= _minp.y);
        }
        return r;
    }

    void recalcBounds()
    {
        _minp.x = _minp.y = FLT_MAX;
        _maxp.x = _maxp.y = -FLT_MAX;
        int n = size();
        for (int i = 0; i < n; ++i)
        {
            Point2f& p = _points[i];
            if (p.x < _minp.x) _minp.x = p.x;
            if (p.x > _maxp.x) _maxp.x = p.x;
            if (p.y < _minp.y) _minp.y = p.y;
            if (p.y > _maxp.y) _maxp.y = p.y;
        }
    }

    bool contains(Point2f p) const
    {
        bool r = valid();
        if (r)
        {
            // inside bounds?
            r = (p.x >= _minp.x && p.x < _maxp.x &&
                 p.y >= _minp.y && p.y < _maxp.y);

            if (r)
            {
                // yes, perform actual polygon test
                
                // https://wrfranklin.org/Research/Short_Notes/pnpoly.html
                // https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon

                int n = size();
                int i, j, c = 0;
                for (i = 0, j = n-1; i < n; j = i++)
                {
                    if ( ((_points[i].y > p.y) != (_points[j].y > p.y)) &&
                         (p.x < (_points[j].x - _points[i].x) *
                          (p.y-_points[i].y) / (_points[j].y-_points[i].y) +
                          _points[i].x) )
                        c = !c;
                }

                if (!c) r = false;
            }
        }
        return r;
    }

    double area() const
    {
        double a = 0;
        int n = _points.size();
        for (int i = 0; i < n; ++i)
        {
            const Point2f* p = &_points[i];
            int j = i + 1;
            if (j >= n) j = 0;
            const Point2f* p1 = &_points[j];
            a += (p->x * p1->y) - (p1->x * p->y);
        }
        if (a < 0) a = -a;
        return a/2;
    }
    
    void clear()
    {
        _points.clear();
        _minp.x = _minp.y = FLT_MAX;
        _maxp.x = _maxp.y = -FLT_MAX;
    }
            
    int size() const { return _points.size(); }

    operator bool() const { return size() != 0; }
    Point2f operator[](int i) const
    {
        // ASSUME i < size
        return _points[i];
    }

    friend std::ostream& operator<<(std::ostream& os, const Poly& p)
    {
        GrowString gs;
        p.buildJson(gs);
        gs.add(0);
        return os << gs;
    }

    void buildJson(GrowString& gs) const
    {
        // { "verts":[x.y,x,y...] }
        JSONWalker::toAdd(gs);
        gs.add('{');
        if (_strokeW > 1)
            JSONWalker::addKeyValue(gs, IFI_WIDTH, _strokeW);
        if (!_col.isWhite())
            JSONWalker::addKeyValue(gs, IFI_COLOR, _col.toString());

        JSONWalker::toAdd(gs);
        JSONWalker::addKey(gs, IFI_VERTS);
        gs.add('[');
        for (auto& pi : _points)
        {
            JSONWalker::addValue(gs, pi.x);
            JSONWalker::addValue(gs, pi.y);
        }
        gs.add(']');
        gs.add('}');
    }

    bool parseVertsArray(const string& js)
    {
        // [0.074622,0.777418...]
        bool r = false;
        
        const char* s = js.c_str();
        while (u_isspace(*s)) ++s;
        r = *s == '[';
        if (r)
        {
            ++s;
            while (*s)
            {
                Point2f pt;
                pt.x = u_atof(&s);
                while (u_isspace(*s)) ++s;
                if (*s != ',') { r = false; break; }
                ++s;
                pt.y = u_atof(&s);
                add(pt);

                while (u_isspace(*s)) ++s;
                if (!*s || *s == ']') break; // done
                if (*s == ',') ++s;
            }
        }
        
        return r;
    }

    bool parseJson(const string& js)
    {
        // { "width":2,"verts":[0.074622,0.777418...] }
        bool r = true;
        for (JSONWalker jw(js); r && jw.nextKey(); jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st) { r = false;  break; }

            if (!isObject)
            {
                var v = jw.collectValue(st);
                if (jw._key == IFI_WIDTH) _strokeW = v.toDouble();
                else if (jw._key == IFI_COLOR) _col.parse(v.toString());
            }
            else
            {
                if (jw._key == IFI_VERTS)
                {
                    string subjs;
                    r = jw.atList(subjs) && parseVertsArray(subjs);
                }
            }
        }
        return r;
    }

#if 0    
    static float perpendicularDistance(const Point2f& pt,
                                       const Point2f& lineStart,
                                       const Point2f& lineEnd)
    {
        float dx = lineEnd.x - lineStart.x;
        float dy = lineEnd.y - lineStart.y;

        // Normalize the line vector
        float mag = std::sqrt(dx * dx + dy * dy);
        if (mag > 0.0) {
            dx /= mag;
            dy /= mag;
        }

        // Calculate the projection of the point onto the line
        float pvx = pt.x - lineStart.x;
        float pvy = pt.y - lineStart.y;

        // Calculate the distance from the point to the line
        float proj = pvx * dx + pvy * dy;
        float ax = proj * dx;
        float ay = proj * dy;

        float dist = std::sqrt((pvx - ax) * (pvx - ax) + (pvy - ay) * (pvy - ay));
        return dist;
    }

    static void simplifyDP(float epsilon,
                           const Points& points,
                           Points& out,
                           size_t start, size_t end)
    {

        // Recursive function for the Douglas-Peucker algorithm

        if (end - start < 2) {
            return;
        }

        // Find the point with the maximum distance from the line formed by the first and last points
        float maxDist = 0.0;
        size_t index = start;

        for (size_t i = start + 1; i < end; ++i) {
            float dist = perpendicularDistance(points[i], points[start], points[end % points.size()]);
            if (dist > maxDist) {
                LOG1("dist ", i << " = " << dist);
                index = i;
                maxDist = dist;
            }
        }

        // If the maximum distance is greater than epsilon, recursively simplify
        if (maxDist > epsilon) {
            simplifyDP(epsilon, points, out, start, index);
            simplifyDP(epsilon, points, out, index, end);
        } else {
            // If max distance is less than epsilon, add the start point to the result
            out.push_back(points[start]);
        }
    }
#endif    

 };

struct Polys: public PathTraits
{
    // a set of polygons to define a region
    typedef std::list<Poly>  Polyst;
    
    Polyst       _polys;

    void add(const Poly& p) { if (p) _polys.push_back(p); }
    void clear() { _polys.clear(); }
    int size() const { return _polys.size(); }

    int vertexSize() const
    {
        int t = 0;
        for (auto& pi : _polys) t += pi.size();
        return t;
    }

    string vertexSizeString() const
    {
        // return a string list of vertex sizes for each poly.
        string s;
        for (auto& pi : _polys)
        {
            if (!s.empty()) s += ',';
            s += std::to_string(pi.size());
        }
        return s;
    }

    void buildJson(GrowString& gs) const
    {
        // add "polys:[{},{}]
        JSONWalker::toAdd(gs);
        JSONWalker::addKey(gs, IFI_POLYS);
        gs.add('[');
        for (auto& pi : _polys)
        {
            pi.buildJson(gs);
        }
        gs.add(']');
    }

    bool parseJson(const string& js)
    {
        // eg [{poly}...]

        bool r = true;
        JSONWalker jw(js);
        for (jw.beginArray();r && !jw._error && !jw._end;jw.next())
        {
            bool isObject;
            const char* st = jw.checkValue(isObject);
            if (!st)  { r = false;  break; }

            // expect a list of poly objects
            if (!isObject)
            {
                LOG1("Bad json polys ", st);
                continue;
            }

            Poly poly;
            string polyobj;
            r = jw.atObj(polyobj) && poly.parseJson(polyobj);
            if (r) _polys.push_back(poly);
        }
        return r;
    }

    friend std::ostream& operator<<(std::ostream& os, const Polys& ps)
    {
        GrowString gs;
        ps.buildJson(gs);
        gs.add(0);
        return os << gs;
    }
};
    
struct SVGPath: public PathTraits
{
    // stored like an SVG path
    string      _id;
    string      _svgd;  // the "d" string in an SVG path
    float       _strokeW = 1;
    Colour      _col = Colour(Colour::white);

    // values in the svgd are scaled by these factors that bring them to [0,1]
    Point2f     _scale = {1,1};

    SVGPath(const string& id, const string& d) : _id(id), _svgd(d) {}

    static void skipws(const char*& s) { while (u_isspace(*s)) ++s; }

    void scale(Point2f& p)
    {
        p.x *= _scale.x;
        p.y *= _scale.y;
    }

    Point2f parsePoint(const char*& s)
    {
        Point2f p;
        skipws(s);
        p.x = u_atof(&s);
        skipws(s);
        if (*s == ',')
        {
            skipws(++s);
            p.y = u_atof(&s);
        }
        scale(p);
        return p;
    }

    
    bool makePolys(Polys& ps, const char* name = 0)
    {
        // convert path to polygon set
        // supply name optionally for errors
        bool r = true;

        const char* s = _svgd.c_str();

        if (!name) name = "";

        Point2f pos = {0,0};
        Poly poly;
        poly._strokeW = _strokeW;
        poly._col = _col;
        
        while (*s && r)
        {
            switch (*s)
            {
            case 'M':
                pos = parsePoint(++s);
                ps.add(poly); poly.clear(); poly.add(pos);
                break;
            case 'm':
                pos += parsePoint(++s);
                ps.add(poly); poly.clear(); poly.add(pos);
                break;
            case 'L':
                pos = parsePoint(++s);
                poly.add(pos);
                break;
            case 'l':
                pos += parsePoint(++s);
                poly.add(pos);
                break;
            case 'Z':
            case 'z':
                ++s;
                ps.add(poly); poly.clear();
                break;
            case 'C':
            case 'c':
                LOG1("WARNING ############## makePolys, Curve element in ", _id << ": " << s << ' ' << name);
                break;
            default:
                LOG1("WARNING ############## makePolys, unrecognised path element in ", _id << ": " << s << ' ' << name);
                r = false;
                break;
            }
        }
        
        return r;
    }
};


