//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <string>
#include <stdlib.h>
#include "cutils.h"

struct WordWalker
{
    typedef std::string string;
    
    const char*     _s;
    const char*     _pos;

    // if true separates punctuation into "words"
    // otherwise words are strictly separated by spaces
    bool  _separatePunct = false;

    WordWalker(const char* s) { _init(s); }
    WordWalker(const string& s) { _init(s.c_str()); }

    string nextWord() 
    {
        while (u_isspace(*_pos)) ++_pos;
        const char* p = _pos;

        if (!*p) return string();  // done

        if (_separatePunct)
        {
            static const char* allowedWordChars = "-_";

            // words are [a-zA-Z0-9_-]+
            while (u_strchr(allowedWordChars, *p) || u_isalnum(*p)) ++p;

            // force at least one char
            if (p == _pos) ++p;
        }
        else
        {
            // end of word is the next space (or EOF)
            while (*p && !u_isspace(*p)) ++p;
        }
        
        const char* st = _pos;
        _pos = p;
        return string(st, _pos - st);
    }

    void reset()
    {
        _pos = _s;
    }

private:

    void _init(const char* s)
    {
        _s = s;
        reset();
    }
    
};















