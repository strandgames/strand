//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

struct GetPut
{
    virtual int _getc() = 0;
    virtual bool _putc(char c) = 0;
    
    int getc() { return _getc(); }
    bool putc(char c) { return _putc(c); }

    int getshort()
    {
        return _getc() | _getc() << 8;
    }

    int getint()
    {
        return getshort() | getshort() << 16;
    }

    std::string getstring()
    {
        std::string s;
        int c;
        while ((c = _getc()) > 0) s += (char)c;
        return s;
    }
    
    bool putshort(int v)
    {
        return _putc(v) && _putc(v >> 8);
    }

    bool putint(int v)
    {
        // put 32 bit int
        return putshort(v) && putshort(v >> 16);
    }

    bool putstring(const char* s)
    {
        bool r;
        do
        {
            // include zero terminator
            r = _putc(*s);
            if (!*s) break;
            ++s;
        } while (r);
        return r;
    }
};

struct GetPutBuf: public GetPut
{
    unsigned char* _bp;

    GetPutBuf(unsigned char* bp) : _bp(bp) {}

    int _getc() override
    {
        return *_bp++;
    }

    bool _putc(char c) override
    {
        *_bp++ = c;
        return true;
    }
};
