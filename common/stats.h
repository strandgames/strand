//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

#include <iostream>
#include <stdio.h>
#include <math.h>

struct Stats
{
    // * Collect statistics and calculate the mean, variance and peak.

    Stats() { reset(); }

    void                reset()
    {
        _n = 0;
        _mean = 0;
        _varSum = 0;

        // clear moving average
        memset(_av, 0, _avMax * sizeof(double));
        _avp = 0;
        
        clearPeak();
    }

    /// Add a new sample
    void                add(double x)
    {
        _updateMin(x);
        _updateMax(x);

        double dx = x - _mean;
        ++_n;
        if (dx)
        {
            _mean += dx/_n;
            if (_n > 1) _varSum += dx*(x - _mean);
        }
        _updatePeak(x);
        _updateAv(x);
    }
    
    /// Remove a sample.
    void                remove(double x)
    {
        if (_n > 1)
        {
            double t = x - _mean;
            _mean -= t/--_n;
            _varSum -= (x - _mean)*t;
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const Stats& si)
    {
        char buf[512];
        sprintf(buf, "n:%d mean:%.1f/%.1f std:%.1f peak:%.1f min:%.1f max:%.1f",
                si.count(),
                si.mean(), si.average(),
                si.stdDeviation(),
                si.peak(),
                si.minimum(),
                si.maximum());
        
        return os << buf;
    }

    /// Return the number of sampes collected.
    unsigned int        count() const { return _n; }

    /// Return the mean of the samples collected.
    double              mean() const { return _mean; }

    /// Return the variance of the samples collected.
    double              variance() const
    { return _n > 1 ? _varSum/(_n-1) : 0; }

    /// Return the standard deviation of the samples collected.
    double              stdDeviation() const { return sqrt(variance()); }

    /// Return the peak absolute value of the samples collected.
    double              peak() const { return _peak; }

    /// Return the maximum value observed.
    double              maximum() const { return _max; }

    /// Return the minimum value.
    double              minimum() const { return _min; }

    // moving average
    double              average() const
    {
        double t = 0;
        unsigned int k = _avLen;
        if (k > _n) k = _n;
        if (k > 0)
        {
            for (unsigned int i = 0; i < k; ++i) t += _av[i];
            t /= k;
        }
        return t;
    }

    /// Reset the peaks.
    void                clearPeak()
    {
        _peak = 0; 
        _min = 0;
        _max = 0;
    }

    bool                isAPeak(double v) const
    {
        return v == _peak || v == _max || v == _min;
    }

private:

    void                _updatePeak(double x)
    {
        if (x >= 0) { if (x > _peak) _peak = x; }
        else { if (-x > _peak) _peak = -x; }
    }

    void                _updateMin(double x)
    { if (x < _min || !_n)  _min = x; }

    void                _updateMax(double x)
    { if (x > _max || !_n) _max = x; }

    void                _updateAv(double x)
    {
        // update moving average
        _av[_avp++] = x;
        if (_avp == _avLen) _avp = 0;
    }

    unsigned int        _n;
    double              _mean;
    double              _varSum;
    double              _peak;
    double              _min;
    double              _max;

    // moving average
    static const int    _avMax = 16;
    unsigned int        _avLen = 4;
    unsigned int        _avp;
    double              _av[_avMax];
};



