//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2023.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#pragma once

// parse simple float without locale
// this would be std::from_chars if it worked in this compiler

#include "cutils.h"

static inline double u_atof(const char** sp)
{
    // format [-][0-9].[0-9][eE][-+][0-9]
    // consider, std::from_chars(s, e, v);    
    double v = 0;
    bool neg = false;
    const char* s = *sp;

    while (u_isspace(*s)) ++s;

    if (*s == '+') ++s;
    else if (*s == '-') { ++s; neg = true; }

    const char* s0 = s;

    while (u_isdigit(*s)) v = v*10 + (*s++ - '0');
    
    if (*s == '.')
    {
        ++s;
        double d = 0.1;
        while (u_isdigit(*s))
        {
            v = v + (*s++ - '0')*d;
            d /= 10;
        }
    }
    if (*s == 'e' || *s == 'E')
    {
        if (s == s0)
        {
            // no mantissa, eg -e+1
            v = 1;
        }
        
        ++s;
        bool eneg = false;
        if (*s == '+') ++s;
        else if (*s == '-') { eneg = true; ++s; }

        unsigned int ev = 0;
        while (u_isdigit(*s)) ev = ev*10 + (*s++ - '0');
        if (ev)
        {
            double e = pow(10, ev);
            if (eneg) v /= e;
            else v *= e;
        }
    }

    if (neg) v = -v;
    *sp = s;
    
    return v;
}

static inline double u_atof(const char* s)
{
    return u_atof(&s);
}

static inline double u_atof(const std::string& s)
{
    return u_atof(s.c_str());
}

static inline int u_atoi(const char** sp)
{
    bool neg = false;

    const char* s = *sp;
    int v = 0;
    while (u_isspace(*s)) ++s;
    if (*s == '-') { neg = true; ++s; }
    while (u_isdigit(*s)) v = v*10 + (*s++ - '0');
    if (neg) v = -v;
    *sp = s;
    return v;
}

static inline int u_atoi(const char* s)
{
    return u_atoi(&s);    
}

static inline int u_atoi(const std::string& s)
{
    return u_atoi(s.c_str());
}

static inline unsigned int u_atohex(const char** sp)
{
    const char* s = *sp;
    unsigned int v = 0;
    while (u_isspace(*s)) ++s;
    while (u_ishex(*s)) { v = (v<<4) + u_hexv(*s); ++s; }
    *sp = s;
    return v;
}

static inline unsigned int u_atohex(const char* s)
{
    return u_atohex(&s);
}

static inline unsigned int u_atohex(const std::string& s)
{
    return u_atohex(s.c_str());
}
