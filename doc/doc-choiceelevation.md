# Choice Elevation

This is the process of elevating a parser command to a choice.

## Example 1:

Let's say you have a cake with "eat me" written on it;

```
CAKE@ GETTABLE
* name
the cake
* x it
It has the words "eat me" written on it.
```

You obviously want to allow the player to eat it. So;

```
CAKE@ GETTABLE
* name
the cake
* x it
It has the words "eat me" written on it.
* eat it
You scoff the cake.
> put it in HELL
```

But, since this is the obvious thing to do, you wish to offer "eat the cake" as a choice as well. So;

```
CAKE@ GETTABLE
* name
the cake
* x it
It has the words "eat me" written on it.
*= eat it
You scoff the cake.
> put it in HELL
```

Just add "=" to the command, and it is marked as "elevate to choice". So here, we can either make the choice _or_ type the command.

## Example 2:

You want to offer the choice to get something, but only if not already carried.

```
CHALICE@ GETTABLE
* name
the golden chalice
* x it
It's what you've always wanted.
```

This will _already_ be gettable as indicated. So `get chalice` will already work. But it won't be prompted as a command. So;

```
CHALICE@ GETTABLE
* name
the golden chalice
* x it
It's what you've always wanted.
*=+ get yon it
> get it
```

How does this work?

* add a "get" override
* mark with "=" to indicate elevate to choice
* mark with "=+" to work more than once
* use the word "yon" to filter only on non-carried items.
* redirect action to the parent "get" with `> get it`

## Controlling Choice Elevation Globally

Just because a command is marked as "elevated" does not not _automatically_ mean it is offered as a choice. It's only a suggestion.

In the `core.str` library, there are two terms `CHOICE_OPT_ON` and `CHOICE_OPT_OFF`.

```
CHOICE_OPT_ON
{ (elevatechoices 1) }

CHOICE_OPT_OFF
{ (elevatechoices 0) }
```

These control whether elevated commands appear as choices. As you can see these are not themselves special and work by calling the KL function `elevatechoices`.

For convenience, there is also a choice term you can call in your game to affect this:

```
CHOICE_OPT?
Do you prefer choices over text input?
* yes
\nChoices On
* no
CHOICE_OPT_OFF
\nChoices Off
```

























