# The Design of Strand

_New_ Interactive Fiction tells a story through art, text and interaction. 

## Intro

The Strand IF system was invented to fill an emerging gap between classic IF and New IF. New IF is a modern and increasingly innovative form of flow based narrative, where the story takes the lead and is supported collectively by text input, art and choices and clicks - both on text and pictures.


Casting this into the nomenclature of classic IF, we can say that Strand supports parser-input, choice-input as well as point-and-click, but yet it is not limited to either.

## Design Goal

It was never a requirement that Strand was to be "better" or "superior" to existing IF systems. Indeed, Strand's original mission was to achieve the, so called, 90% rule:

> Do 90% of what games want **really well** and leave the remaining 10% to game design.

The motivation for this goal was the supposition that 9/10 features could be made simply and easy to author whilst the annoying and complicated 10% could be isolated from poisoning the main 90%.

In other words, the central goal of Strand was _ease of authoring_ and therefore its corollary, _rapid development_.

## Strategy

Ease of authoring was to be achieved by inventing a non-technical domain specific language (DSL). It was important that this language did not resemble programming with its inevitable collection of weird symbols and annoying syntax.

Because, whether you're a programmer or not, this is still a development impediment.

How nice it would be to "bosh-out" whole swathes of gameplay without programming; without variables, loops, if-then-else statements and all the other rubbish programming languages force on you.

**But how?**

Following the 9/10 idea, it was envisaged that the DSL would handle the 90% _without_ programming and the remaining 10% would be relegated to a companion, traditional programming language.

Because, for example, if someone wanted to have a chess game coded inside their game, it would clearly require a general programming system to be implemented. Such a thing would be deemed to fall into the 10% and thus be implemented outside of the DSL.

The strand IF system would therefore consist of a complementary blend of _two_ languages; a DSL, called _Strand_ and a system language called _KL_.

## Models

However, it transpires that the DSL is, in fact, quite capable of implementing well over the 90% originally envisaged. In practice, more like 99%.

The difference between a flow-based paradigm and a rules-based one is that combining flow blocks gives rise to _models_.

A _model_ is group of flow blocks working together to implement a specific behaviour. Rules can do the same thing, but a model is more cohesive and can also have context.

Models are implemented in the DSL and factored into a "core" package. It is then _very easy_ to build standard IF behaviour just as if these models were actually part of the runtime system.

Standard IF concepts such as doors, locations, gettables and so on are **not** part of the system runtime _at all_, but are in fact just DSL that can be further modified and extended by authors.

## Really?

Indeed. Here is the implementation of get-ability. No special core logic is needed.

```
GETTABLE@ THING
* be it
IT
* get it
DOGET
* drop it
DODROP

DOGET > put it in PLAYER
* ok
You get IT.
*
You can't get IT.

DODROP > put it in here
* ok
You drop IT.
*
You can't do that.
```

How about inventory. Nope this is just a bit of DSL. Also handling clothing.

```
INV
INV1 INV2

YOURSTUFF > what is in PLAYER
*
> be LAST

YOURCLOTHES > what is on me
*
> be LAST

INV1 YOURSTUFF
* them
You are carrying, LAST.
*
You are empty handed.

INV2 YOURCLOTHES
* them
You are wearing LAST.
```

The same is true of location, clothing, doors, containers and so on. There is no need for any of these traditional IF concepts to be implemented by the runtime system.







































