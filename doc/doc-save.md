# Strand Save/Load

As the philosophy of Strand is streamlined development, it was essential in the system design that maintaining the game state required _absolutely no work_ on the part of the game author.

Overview of game state.

## The Timeline "Tape"

The state of all variables, objects and properties is retained at all times on a "timeline". The timeline can be thought of as a length of "tape" upon which data can be written. At the start of the game, the timeline has the initial state of all properties and objects.

As the game is played, changes to the world (ie game state) are _appended_ to the timeline. That is to say they are _added_ and nothing prior is erased. This is done automatically with nothing required from the author.

At runtime, the game determines the value of any property by scanning from the _end_ of the "tape" backward until a value is found (or not if absent).

The benefits of this approach are multi-fold:

* undo is performed simply by _erasing_ the last move (or moves) from the tape.
* undo can be repeated until the entire game is unwound.
* save-game is simply the content of the tape.
* undo can be performed _after_ a game restore.
* redo is possible if the end tape is not erased but just rewound.
* checkpoints can be placed in the timeline.
* timeline branches are possible.


















