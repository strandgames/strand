# Trizx - Trizbort exporter

This utility will read `.trizbort` files emitted by the desktop version of [Trizbort](https://www.trizbort.com). It builds an internal model of locations and exits which can then be exported into other formats.

Although Trizbort itself supported several export formats, this is an alternative way to target a new format without having to change Trizbort itself.

## Usage

Example:

`trizx -strand map.trizbort`

Will export into the `Strand` format.

## Limitations

* Objects defined in the Trizbort map are currently ignored.
* up/down not fixed yet.







