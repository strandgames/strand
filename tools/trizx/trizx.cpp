//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include <stdio.h>
#include <iostream>
#include <string>
#include "cutils.h"
#include "strutils.h"
#include "utils.h"
#include <assert.h>

#include "fd.h"
#include "html.h"
#include "graph.h"

#define TRIZ_TRIZ       "trizbort"
#define TRIZ_MAP        "map"
#define TRIZ_ROOM       "room"
#define TRIZ_ID         "id"
#define TRIZ_NAME       "name"
#define TRIZ_LINE       "line"
#define TRIZ_DOCK       "dock"
#define TRIZ_PORT       "port"
#define TRIZ_DESC       "description"
#define TRIZ_STARTTEXT  "startText"
#define TRIZ_ENDTEXT    "endText"
#define TRIZ_ISDARK     "isDark"

struct Trizx
{
    typedef std::string string;
    typedef Graph<int>  Map;
    
    struct Room: public Map::Node
    {
        typedef Map::Node parentT;
        string     _name;
        string     _desc;
        string     _userID;  // for emitters
        bool       _inside = false;

        Room(int id, const string& name) : parentT(id), _name(name) {}

        friend std::ostream& operator<<(std::ostream& os, const Room& r)
        { return os << r._name << ":" << r._id; }
    };

    enum Direction
    {
        dir_void = 0,
        dir_n,
        dir_nne,
        dir_ne,
        dir_ene,
        dir_e,
        dir_ese,
        dir_se,
        dir_sse,
        dir_s,
        dir_ssw,
        dir_sw,
        dir_wsw,
        dir_w,
        dir_wnw,
        dir_nw,
        dir_nnw,
        dir_up,
        dir_down,
        dir_in,
        dir_out,
        dir_max,
    };

    static Direction parseDirection(const string& ds, bool error = true)
    {
        return parseDirection(ds.c_str(), error);
    }
    
    static Direction parseDirection(const char* ds, bool error = true)
    {
        if (equalsIgnoreCase(ds, "n")) return dir_n;
        else if (equalsIgnoreCase(ds, "e")) return dir_e;
        else if (equalsIgnoreCase(ds, "s")) return dir_s;
        else if (equalsIgnoreCase(ds, "w")) return dir_w;
        
        else if (equalsIgnoreCase(ds, "ne")) return dir_ne;
        else if (equalsIgnoreCase(ds, "se")) return dir_se;
        else if (equalsIgnoreCase(ds, "sw")) return dir_sw;
        else if (equalsIgnoreCase(ds, "nw")) return dir_nw;

        else if (equalsIgnoreCase(ds, "nne")) return dir_nne;
        else if (equalsIgnoreCase(ds, "ene")) return dir_ene;
        else if (equalsIgnoreCase(ds, "ese")) return dir_ese;
        else if (equalsIgnoreCase(ds, "sse")) return dir_sse;

        else if (equalsIgnoreCase(ds, "ssw")) return dir_ssw;
        else if (equalsIgnoreCase(ds, "wsw")) return dir_wsw;
        else if (equalsIgnoreCase(ds, "wnw")) return dir_wnw;
        else if (equalsIgnoreCase(ds, "nnw")) return dir_nnw;
        
        else if (equalsIgnoreCase(ds, "in")) return dir_in;
        else if (equalsIgnoreCase(ds, "out")) return dir_out;
        else if (equalsIgnoreCase(ds, "up")) return dir_up;
        else if (equalsIgnoreCase(ds, "down")) return dir_down;
        else
        {
            if (error)
            {
                LOG1("bad direction ", ds);
            }
        }
        return dir_void;
    }

    static const char* directionName(Direction d)
    {
        if (d < dir_max)
        {
            static const char* dirTab[] =
                {
                    "void",
                    "n",
                    "nne",
                    "ne",
                    "ene",
                    "e",
                    "ese",
                    "se",
                    "sse",
                    "s",
                    "ssw",
                    "sw",
                    "swe",
                    "w",
                    "wnw",
                    "nw",
                    "nnw",
                    "up",
                    "down",
                    "in",
                    "out",
                };

            return dirTab[d];
        }
        return "unknown";
    }

    struct Exit: public Map::Connection
    {
        typedef Map::Connection parentT;
        
        Direction      _d;

        Exit(Room* r1, Room* r2, Direction d) : parentT(r1, r2), _d(d) {}

        Room* from() const { return (Room*)_from; }
        Room* to() const { return (Room*)_to; }
        
        friend std::ostream& operator<<(std::ostream& os, const Exit& e)
        {
            return os << e.from()->_name << " " << directionName(e._d) << " to " << e.to()->_name; 
        }
    };

    typedef HNode::Nodes Nodes;
    
    NodeParser          _np;
    Map                 _map;

    Room* findRoom(int id) { return (Room*)_map.findNode(id); }

    string getStringAttr(NodeTag* n, const char* tag)
    {
        string s = n->getAttr(tag).toString();

        // take out any DOS newlines embedded
        return replaceAll(s, "\r", "");
    }

    void internMapRooms(NodeTag* root)
    {
        for (Nodes::iterator it = root->_content.begin();
             it != root->_content.end(); ++it)
        {
            HNode* n = it;
            if (n->type() == HNode::nodeTypeTag)
            {
                NodeTag* nt = (NodeTag*)n;
                if (equalsIgnoreCase(nt->_tag, TRIZ_ROOM))
                {
                    int id = nt->getAttr(TRIZ_ID).toInt();
                    string name = getStringAttr(nt, TRIZ_NAME);
                    string desc = getStringAttr(nt, TRIZ_DESC);
                    bool inside = getStringAttr(nt, TRIZ_ISDARK) == "yes";

                    bool ok = id > 0 && !name.empty();
                    
                    if (ok)
                    {
                        ok = !findRoom(id);

                        if (!ok)
                        {
                            LOG1("duplicate room id", id);
                        }

                        if (ok)
                        {
                            Room* r = new Room(id, name);
                            r->_desc = desc;
                            r->_inside = inside;
                            LOG3("adding room ", *r);
                            _map.add(r);
                        }
                    }

                    if (!ok)
                    {
                        LOG3("Bad room node ", nt->toString());
                    }
                }
                else if (equalsIgnoreCase(nt->_tag, TRIZ_LINE))
                {
                    // connections
                    // deal with these in second pass
                }
                else
                {
                    LOG3("skipping node ", nt->_tag);
                }
            }
        }
    }

    void internMapConnections(NodeTag* root)
    {
        for (Nodes::iterator it = root->_content.begin();
             it != root->_content.end(); ++it)
        {
            HNode* n = it;
            if (n->type() == HNode::nodeTypeTag)
            {
                NodeTag* nt = (NodeTag*)n;
                if (equalsIgnoreCase(nt->_tag, TRIZ_ROOM))
                {
                    // dealt with in first pass.
                }
                else if (equalsIgnoreCase(nt->_tag, TRIZ_LINE))
                {
                    // connections

                    var ids[2];
                    var ports[2];
                    int cc = 0;

                    for (Nodes::iterator it = nt->_content.begin();
                         it != nt->_content.end(); ++it)
                    {
                        HNode* n = it;
                        if (n->type() == HNode::nodeTypeTag)
                        {
                            NodeTag* cn = (NodeTag*)n;
                            if (equalsIgnoreCase(cn->_tag, TRIZ_DOCK))
                            {
                                if (cc < 2)
                                {
                                    ids[cc] = cn->getAttr(TRIZ_ID);
                                    ports[cc] = cn->getAttr(TRIZ_PORT);
                                }
                                ++cc;
                            }
                        }
                    }

                    string st = getStringAttr(nt, TRIZ_STARTTEXT);
                    string et = getStringAttr(nt, TRIZ_ENDTEXT);
                    
                    if (cc >= 2)
                    {
                        Room* r1 = findRoom(ids[0].toInt());
                        Room* r2 = findRoom(ids[1].toInt());

                        Direction d1 = parseDirection(ports[0].toString());
                        Direction d2 = parseDirection(ports[1].toString());

                        // direction can be overridden by special text
                        Direction d = parseDirection(st, false);
                        if (d) d1 = d;

                        d = parseDirection(et, false);
                        if (d) d2 = d;
                        
                        if (r1 && r2 && d1 && d2)
                        {
                            auto e = new Exit(r1, r2, d1);
                            LOG3("connecting ", *e);
                            _map.add(e);

                            e = new Exit(r2, r1, d2);
                            LOG3("connecting ", *e);
                            _map.add(e);
                        }
                        else
                        {
                            LOG1("bad connection ", nt->_tag);
                        }
                    }
                }
                else
                {
                    LOG3("skipping node ", nt->_tag);
                }
            }
        }
    }

    void internMap(NodeTag* root)
    {
        internMapRooms(root);
        internMapConnections(root);
    }

    bool intern(char* s)
    {
        _np.parse(s);

        NodeTag* root = _np.root()->findTag(TRIZ_TRIZ);
        if (!root)
        {
            LOG1("no root node ", TRIZ_TRIZ);
            return false;
        }
        
        NodeTag* map = root->findTag(TRIZ_MAP);
        if (map)
        {
            internMap(map);
        }
        else
        {
            LOG1("no map node ", root->_tag);
            return false;
        }
        return true;
    }

    void rationaliseExits()
    {
        // map silly partial directions to NESW
        for (auto ci : _map.connections())
        {
            Exit* ei = (Exit*)ci;

            switch (ei->_d)
            {
            case dir_ene:
            case dir_ese:
                ei->_d = dir_e;
                break;
            case dir_sse:
            case dir_ssw:
                ei->_d = dir_s;
                break;
            case dir_wsw:
            case dir_wnw:
                ei->_d = dir_w;
                break;
            case dir_nnw:
            case dir_nne:
                ei->_d = dir_n;
                break;
            }
        }
    }
};

struct StrandEmitter
{
    typedef std::string string;

    typedef Trizx::Room Room;
    typedef Trizx::Exit Exit;
    
    Trizx&       _tx;
    bool         _imgTerms = true;
    bool         _audioTerms = true;

    StrandEmitter(Trizx& tx) : _tx(tx)
    {
        _tx.rationaliseExits();
    }

    std::vector<string> ids;

    void bindID(Room* r)
    {
        if (r->_userID.empty())
        {
            string s = ::toUpper(trim(r->_name));

            // hacks to get rid of determiners
            if (startsWith(s, "THE ")) s = s.substr(4);
            if (startsWith(s, "A ")) s = s.substr(2);
            if (startsWith(s, "AN ")) s = s.substr(3);

            // get rid of spaces
            s = replaceAll(s, " ", "_");

            if (::contains(ids, s))
            {
                // clash!
                int cc = 1;
                            
                for (;;)
                {
                    string s1 = s + std::to_string(++cc);
                    if (!::contains(ids, s1))
                    {
                        s = s1;
                        break;
                    }
                }
            }

            ids.push_back(s);
            r->_userID = s;
        }
    }

    void emit(std::ostream& os)
    {
        LOG3("emitting strand ", "");

        // pre-pass, generate iDS for locations
        for (auto ri : _tx._map.nodes()) bindID((Room*)ri);

        for (auto ni : _tx._map.nodes())
        {
            Room* ri = (Room*)ni;

            string imgterm;
            string auterm;

            os << "//////////////////////// " << ri->_name << " ///////////\n\n";
            
            if (_imgTerms)
            {
                imgterm = ri->_userID + "IMG";
                os << imgterm << "\n";
                os << "\n";
            }

            if (_audioTerms)
            {
                auterm = ri->_userID + "AUDIO";
                os << auterm << "\n";
                os << "AUDIOSTOP\n";
                os << "\n";
            }

            os << "X" << ri->_userID << "\n";
            if (!ri->_desc.empty())
            {
                os << ri->_desc << "\n";
            }
            else
            {
                os << "You are in " << ri->_name << ".\n";
            }
            os << "\n";
            
            os << ri->_userID << "@ ";

            if (ri->_inside) os << "INSIDE";
            else os << "OUTSIDE";
            os << "\n";

            // NB: maintain case of name
            os << "* name\n" << ri->_name << "\n";

            if (_imgTerms)
            {
                os << "* img it\n" << imgterm << "\n";
            }

            os << "* x it\n";

            if (_imgTerms)
            {
                os << "> img it\n";
            }

            if (_audioTerms)
            {
                os << auterm << "\n";
            }
            
            os << "X" << ri->_userID << "\n";

            // exits
            for (auto ci : _tx._map.connections())
            {
                Exit* ei = (Exit*)ci;
                if (ei->_from == ri)
                {
                    string ds = Trizx::directionName(ei->_d);
                    os << "* " << ds << "\n";
                    os << "GO" << ei->to()->_userID << "\n";

                    os << "*+!= go to " << ei->to()->_userID << "\n";
                    os << "GO" << ei->to()->_userID << "\n";
                }
            }
            os << "\n";

            os << "GO" << ri->_userID << "\n";
            os << "> put PLAYER in " << ri->_userID << "\n";
            os << "XHERE\n";
            os << "\n";
        }
    }
};


using namespace std;

int main(int argc, char** argv)
{
    Logged initLog;

    bool emitStrand = false;
    
    const char* tfilename = 0;
    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (!stricmp(argv[i], "-d") && i < argc-1)
            {
                int d = atoi(argv[++i]);
                Logged::_logLevel = d;
            }
            else if (!stricmp(argv[i], "-strand"))
            {
                emitStrand = true;
            }
            else
            {
                cerr << "unrecognised option '" << argv[i] << "'\n";
            }
        }
        else
        {
            tfilename = argv[i];
        }
    }

    if (!tfilename)
    {
        fprintf(stderr, "Usage: %s [-d <level>] [-strand] <triz-file>\n", argv[0]);
        return -1;
    }

    FDLoad fd(tfilename, true);
    if (fd)
    {
        Trizx tx;
        char* s = (char*)fd._data;
        if (tx.intern(s))
        {
            if (emitStrand)
            {
                StrandEmitter e(tx);
                e.emit(cout);
            }
        }
        else
        {
            LOG1("problem with ", tfilename);
        }
    }
    else
    {
        LOG1("can't open '", tfilename << "'");
    }
    
    return 0;
}
