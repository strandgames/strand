#include <stdio.h>
#include <string>
#include "md5.h"
#include "fd.h"
#include "logged.h"

typedef std::string string;

static unsigned char KeyBefore[16+1] = "MISUNDERSTANDING";
static unsigned char KeyAfter[16+1] = "MISUNDERSTANDING";

int patchFile(const char* infile, const char* outfile, bool encode)
{
    int cc = 0;
    FDLoad fd(infile);
    if (fd)
    {
        unsigned char* d = fd._data;
        unsigned int sz = fd._size;

        if (encode && sz >= 16)
        {

            unsigned char* dp = d;
            unsigned int n = sz - 16;
            while (n)
            {
                if (!memcmp(dp, KeyBefore, 16))
                {
                    printf("(%d) patch '%s' at %08x\n", ++cc, outfile, (int)(dp - d));
                    memcpy(dp, KeyAfter, 16);
                    dp += 16;
                    n -= 16;
                }
                else
                {
                    --n;
                    ++dp;
                }
            }
        }

        if (!cc)
        {
            fprintf(stderr, "########## WARNING did not patch %s\n", infile);
        }

        FD fo;
        if (fo.open(outfile, FD::fd_new))
        {
            if (fo.write(d, sz))
            {
                printf("wrote '%s'\n", outfile);
            }
            else
            {
                fprintf(stderr, "write error on '%s'\n", outfile);
            }
        }
        else
        {
            fprintf(stderr, "can't write to '%s'\n", outfile);
        }
    }
    else
    {
        fprintf(stderr, "can't open '%s'\n", infile);
    }
    return cc;
}

int main(int argc, char** argv)
{
    const char* infile = 0;
    const char* outfile = 0;

    string key;

    bool ok = true;
    
    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (!strcmp(argv[i], "-key") && i < argc-1)
            {
                key = argv[++i];
            }
            else
            {
                fprintf(stderr, "unknown option '%s'\n", argv[i]);
                ok = false;
            }
        }
        else if (!infile) infile = argv[i];
        else if (!outfile) outfile = argv[i];
        else ok = false;
    }

    ok = ok && infile && outfile;

    if (ok)
    {
        bool encode = !key.empty();
        
        if (encode)
        {
            MD5 md5;
            unsigned char* d = md5.hash(key.c_str());
            memcpy(KeyAfter, d, 16);
        }

        patchFile(infile, outfile, encode);
    }
    else
    {
        fprintf(stderr, "Usage: %s <infile> <outfile> [-key foo]\n", argv[0]);
        return -1;
    }
    return 0;
}
