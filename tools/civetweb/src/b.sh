# link pthread static
# otherwise must distribute libwinpthread-1.dll
# adding -DDEBUG will cause output of what's happening
#DEFS="-DNO_CGI -DNO_SSL -DDEBUG -DUSE_ZLIB"
DEFS="-DNO_CGI -DNO_SSL -DDEBUG"
OPT="-O2"
CF="-I. -I../include -pthread $DEFS $OPT"
LDFLAGS="-Wl,-Bstatic -lpthread -lz -Wl,-Bdynamic -lws2_32 -lpthread -lcomdlg32"
gcc -c $CF civetweb.c
gcc -c $CF main.c
gcc -o civetweb $CF main.o civetweb.o $LDFLAGS


