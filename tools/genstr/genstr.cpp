//
//     _____  __                           __
//    / ___/ / /_ _____ ____ _ ____   ____/ /
//    \__ \ / __// ___// __ `// __ \ / __  / 
//   ___/ // /_ / /   / /_/ // / / // /_/ /  
//  /____/ \__//_/    \__,_//_/ /_/ \__,_/   
//                                           
//  Copyright (�) Strand Games 2024.
//
//  This program is free software: you can redistribute it and/or modify it
//  under the terms of the GNU Lesser General Public License (LGPL) as published
//  by the Free Software Foundation, either version 3 of the License, or (at
//  your option) any later version.
// 
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
//  for more details.
// 
//  You should have received a copy of the GNU Lesser General Public License
//  along with this program. If not, see <http://www.gnu.org/licenses/>.
// 
// 
//  info@strandgames.com
//

#include <stdio.h>
#include <iostream>
#include "cutils.h"
#include <string>
#include <strutils.h>
#include <utils.h>

using namespace std;

string prefix;
string charname;

bool genfaces = false;
bool eliminateNumbers = true;

typedef vector<string> strings;

int stemLength(const char* s)
{
    // length of word, not including any trailing digits

    int n = strlen(s);
    int res = n;
    
    if (n)
    {
        const char* np = s + n;
        while (np != s)
        {
            --np;
            if (!u_isdigit(*np))
            {
                ++np;  // to first ending digit (or end of string)
                break;
            }
        }
        res = np - s; 
    }
    return res;
}

string stem(const char* s)
{
    // return the start not including any training number (if any).
    return string(s, stemLength(s));
}

string stem(const string& s)
{
    return stem(s.c_str());
}

void genFaces(const strings& fnames)
{
    /* Generate strand code for character faces from picture files.
     *
     * The picture files use a naming convention like:
     * character<n>-something-something-face<n>.webp
     *
     * supply; `-prefix p` where p is the Strand code prefix for
     * the set of terms emitted.
     *
     * supply; `-char c` where c is the name of the game character.
     * used in the label of the asset resources. This might be the same
     * as `prefix` except often prefix is a shortened version.
     * without `char`, the first segment of the file name above it used
     * as the character name.
     */

    // morgan
    //  b:\tools\genstr\genstr -faces -prefix M -char m pirate3b*torso*b.webp

    // witch
    //  b:\tools\genstr\genstr -faces -prefix W -char witch witch5c*torso*a.webp

    // sultana
    //  b:\tools\genstr\genstr -faces -prefix SF -char sul sultana9*torso*b.webp

    // maddy
    //  b:\tools\genstr\genstr -faces -prefix MAD -char mad victoria81-maddy1*torso1*.webp

    // pris
    //  b:\tools\genstr\genstr -faces -prefix MAID -char maid lorna1*torso1*.webp
    // MU
    //  b:\tools\genstr\genstr -faces -prefix MU -char mu bot1e3*torso*s.webp

    // HECTOR
    //  b:\tools\genstr\genstr -faces -prefix HEC -char hec rog5evil*torso*s.webp
    // BEX
    //  b:\tools\genstr\genstr -num -faces -prefix BEX -char bex thea-*torso1as*.webp
    // Todd
    //  b:\tools\genstr\genstr -num -faces -prefix TOD -char todd nathan9*torso1as*.webp

    // Raveena
    //  b:\tools\genstr\genstr -num -faces -prefix RAV -char rav raveena*torso*.webp
    
    strings names;
    int n = fnames.size();
    names.resize(n);

    for (int i = 0; i < n; ++i)
    {
        // each name will be foo-bar-whatever1.webp
        // if one of the parts is the word "face*", then assume the
        // following specifies the name, otherwise choose the last.
        strings parts;

        string t = changeSuffix(filenameOf(fnames[i]), 0);
        split(parts, t, '-');

        int pz = parts.size();

        if (!pz)
        {
            cerr << "no file name parts in " << t << endl;
            return;
        }

        int faceidx = -1;
        for (int i = 0; i < pz; ++i)
        {
            if (equalsIgnoreCase(stem(parts[i]), "face"))
            {
                faceidx = i;
                break;
            }
        }

        if (faceidx < pz-1) names[i] = toUpper(parts[faceidx + 1]);
        else names[i] = toUpper(parts[pz-1]); // fall back to last
        
        if (pz > 1 && charname.empty())
        {
            // if `charname` not provided,
            // take first part as name of character. Drop any digit on end
            charname = toLower(alphaOf(parts[0]));
        }

        // if not present, then default to prefix
        if (charname.empty()) charname = toLower(prefix);
    }

    //for (int i = 0; i < n; ++i) cerr << fnames[i] << " -> " << names[i] << endl;

    // names might have a number on the end such as angry1 and angry2.
    // when there is just one, remove the number.
    if (eliminateNumbers)
    {
        for (int i = 0; i < n; ++i)
        {
            int cc = 1;
            string s1 = stem(names[i]);
            for (int j = 0; j<n; ++j)
            {
                if (i == j) continue;
                if (equalsIgnoreCase(s1, stem(names[j]))) ++cc;
            }

            if (cc == 1)
            {
                // when there is only one common stem, then drop any number
                //cout << "only one " << names[i] << " " << i << endl;
                names[i] = s1;
            }
        }
    }
 

    for (int i = 0; i < n; ++i)
    {
        cerr << fnames[i] << " -> " << names[i] << endl;
    }

    cout << "// call up\n";
    cout << prefix << "FACES\n";
    cout << prefix << "FACE ";
    for (int i = 0; i < n; ++i)
    {
        if (!names[i].empty())
            cout << prefix << "FACE" << names[i] << "A ";
    }
    cout << "\n\n";

    /*
      WFACE*
      * name witchface
      * parent alley
      * aspect 1
      * pos null,0,1,1
      * alpha 0
      * z 5
      * fidgetbox 0.05,0
      */

    string facename = charname + "face";
    strings anames;
    anames.resize(n);

    cout << "// Holding Group\n";
    cout << prefix << "FACE*\n";
    cout << "* name " << facename << "\n";
    cout << "* parent image props resource\n";
    cout << "* aspect 1\n";
    cout << "* pos null,0,null,1\n";
    cout << "* alpha 0\n";
    cout << "* z 5\n";
    cout << "* fidgetbox 0.04,0\n";
    cout << endl;

    cout << "// assets\n";
    for (int i = 0; i < n; ++i)
    {
        anames[i] = charname + "face" + toLower(names[i]);
        
        cout << prefix << "FACE" << names[i] << "A*\n";
        cout << "* name " << anames[i] << "\n";
        cout << "* parent " << facename << "\n";
        cout << "* resource images/" << filenameOf(fnames[i]) << "\n";
        cout << "* alpha 0\n";
        cout << "* z " << to_string(i+1) << "\n";
        cout << endl;
    }

    cout << endl;

    cout << "// activation\n";
    for (int i = 0; i < n; ++i)
    {
        cout << prefix << "FACE" << names[i] << "*\n";
        cout << "* name " << facename << endl;
        cout << "* xfade " << anames[i] << endl;
        cout << endl;
    }
    cout << endl;
    
}

int main(int argc, char** argv)
{
    strings args;
    for (int i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (!u_stricmp(argv[i], "-prefix") && i < argc-1)
            {
                prefix = toUpper(argv[++i]);
            }
            else if (!u_stricmp(argv[i], "-char") && i < argc-1)
            {
                charname = toLower(argv[++i]);
            }
            else if (!u_stricmp(argv[i], "-faces")) genfaces = true;
            else if (!u_stricmp(argv[i], "-num"))
            {
                // keep all numbers in file names
                eliminateNumbers = false;
            }
            else
            {
                cerr << "unrecognised option '" << argv[i] << "'\n";
            }

        }
        else
        {
            args.push_back(argv[i]);
        }
    }

    if (genfaces) genFaces(args);
    else
    {
        fprintf(stderr, "Usage: %s [-faces [-prefix <name>] [-char name]] <file1> <file2> ...\n", argv[0]);
        return -1;
    }
    
    return 0;
}
